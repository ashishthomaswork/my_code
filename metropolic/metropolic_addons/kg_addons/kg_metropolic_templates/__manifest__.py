# -*- coding: utf-8 -*-
{
    "name" : "KG Metropolic Templates",
    "version" : "0.1",
    "author": "KG",
    "category" : "Others",
    "description": """KG Metropolic """,
    "depends": ['kg_metropolic'],
    "data" : ["report/report_salequot.xml",
              "report/report_kgsaleorder.xml",
               "report/report_kgsaleproforma.xml",
                "report/report_purchaseorder.xml",
                  "report/report_deliveryslip.xml",
                      "report/report_invoice.xml",
                          "report/report_payslip.xml",
              "report/report_views.xml",
        
          ],
    'css': [],
    'installable': True,
    'auto_install': False,
    'application': True,
}