from odoo.tools import amount_to_text_en
# -*- coding: utf-8 -*-
from odoo import models,api,fields,_

# -*- coding: utf-8 -*-
from odoo import models,api,fields
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT, float_compare
from odoo.exceptions import UserError




class HrPayslip(models.Model):
    _inherit = "hr.payslip"    
    _description = "Payslip"

    @api.multi    
    def amount_to_text(self, amount, currency):
        convert_amount_in_words = amount_to_text_en.amount_to_text(amount, lang='en', currency='')        
        convert_amount_in_words = convert_amount_in_words.replace(' and Zero Cent', ' Only ')         
        return convert_amount_in_words