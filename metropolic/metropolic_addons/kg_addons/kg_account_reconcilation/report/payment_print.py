# -*- coding: utf-8 -*-

import time
from odoo import api, models, _
from odoo.tools import float_is_zero
from datetime import datetime
from dateutil.relativedelta import relativedelta


class ReportAccountPayment(models.AbstractModel):

    _name = 'report.kg_account_reconcilation.report_account_payment'
    
    def _get_move_lines(self, payment_ids):            
        data = {}
        res = []
        data['credit'] = []
        data['debit'] = []
        data['write_off'] = []
        reconcile_list = self.env['orchid.account.reconcilation'].search([('payment_id','=',payment_ids)])
        for reconcile in reconcile_list:
            print reconcile,'????'
            debit_lines = self.env['orchid.debit.lines'].search([('rec_id','=',reconcile.id)])
            credit_lines = self.env['orchid.credit.lines'].search([('rec_id','=',reconcile.id)])
            write_off_lines = self.env['orchid.writeoff.lines'].search([('rec_id','=',reconcile.id)])
            
            print debit_lines
            print credit_lines
            print write_off_lines
            for debit in debit_lines:
                data = {}
                data['amount'] = debit.amount
                data['account'] = debit.account_id.name
                data['ref'] = debit.move_line_id.move_id.name
                data['description'] = debit.move_line_id.name
                data['partner'] = reconcile.partner_id.name
                data['debit'] = 1
                data['credit'] = 0
                data['writeoff'] = 0
                res.append(data)
            for credit in credit_lines:
                data = {}
                data['amount'] = credit.amount
                data['account'] = credit.account_id.name
                data['ref'] = credit.move_line_id.move_id.name
                data['description'] = credit.move_line_id.name
                data['partner'] = reconcile.partner_id.name
                data['credit'] = 1
                data['debit'] = 0
                data['writeoff'] = 0
                res.append(data)
            for write_off in write_off_lines:
                data = {}
                data['amount'] = write_off.amount
                data['narration'] = write_off.writeoff_narration
                data['analytic'] = write_off.analytic_account_id.name
                data['account'] = write_off.writeoff_account_id.name
                data['writeoff_move'] = reconcile.writeoff_move_id.name
                data['partner'] = reconcile.partner_id.name
                # data['ref'] = credit.move_line_id.ref
                data['credit'] = 0
                data['debit'] = 0
                data['writeoff'] = 1
                res.append(data)
        print res
        return res

    @api.model
    def render_html(self, docids, data=None):
        obj_model = self.env['account.payment']
        docs = obj_model.browse(docids)
        report_obj = self.env['report']
        report = report_obj._get_report_from_name('kg_account_reconcilation.report_account_payment')
        rptlines = self._get_move_lines(docids[0])
        
        docargs = {
            'doc_model':  report.model,
            'doc_ids': docids,
            'matching_lines': rptlines,
            'docs': docs,
        }
        return self.env['report'].render('kg_account_reconcilation.report_account_payment', docargs)

  
