# -*- coding: utf-8 -*-
from odoo import fields,models,api,_
from odoo import tools
from copy import copy
# Modifications in account.payment
class AccountPayment(models.Model):
    _inherit = "account.payment"
    od_reco_lines = fields.One2many('orchid.account.reconcilation','payment_id',string="Reco Lines")
    @api.multi
    def od_action_open_reconcile_2(self):
        payment_id = self.id
        payment_type = self.payment_type
        partner_id = self.partner_id and self.partner_id.id
#        is_group = self.is_group
        partner_ids = []
        partner_ids.append(partner_id)
#        group_line = self.group_line
#        if group_line:
#        	partner_ids = []
#        	for line in group_line:
#        		print ";;;;;;;;;;;;;;;;;;;;;",line.child_partner_id
#        		partner_ids.append(line.child_partner_id and line.child_partner_id.id)
        partner_id = partner_ids[0]
        context = self.env.context
        print "lllllllllllllllllpartner_ids",partner_ids 
        ctx = context.copy()
        ctx['default_payment_id'] = payment_id
        ctx.update({'default_payment_type':payment_type,'default_partner_id':partner_id,'default_group_pay':False,'default_od_partner_ids':[(6,0,partner_ids)]})
        domain = [('payment_id','=',payment_id)]
        return {
            'domain': domain,
            'name': _('Reconcilation'),
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'orchid.account.reconcilation',
            'context': ctx,
            'type': 'ir.actions.act_window'
             }
