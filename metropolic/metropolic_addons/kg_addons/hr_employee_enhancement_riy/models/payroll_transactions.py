# -*- coding: utf-8 -*-
from odoo import fields,models,api,_
from odoo import tools
from datetime import date
from dateutil.relativedelta import relativedelta

from odoo.exceptions import UserError, AccessError
from odoo import api, fields, models, SUPERUSER_ID, _
from odoo.exceptions import except_orm
from datetime import datetime,date
class hr_payroll_transactions(models.Model):

    _name ="hr.payroll.transactions"


    @api.multi
    def unlink(self):
        payslip_id = self.payslip_id and self.payslip_id.id or False
        if payslip_id:
			raise UserError(_('you cannot delete this transaction,already linked with payslip'))

        leave_salary_request_id = self.leave_salary_request_id and self.leave_salary_request_id.id or False

        ticket_request_id = self.ticket_request_id and self.ticket_request_id.id or False
        if leave_salary_request_id:
			if self.leave_salary_request_id.kg_state != 'new':
				raise UserError(_('you cannot delete this transaction,already linked leave salary request'))

        if ticket_request_id:
			if self.ticket_request_id.kg_state != 'new':
				raise UserError(_('you cannot delete this transaction,already linked airfare request'))



        return super(hr_payroll_transactions, self).unlink()

    employee_id = fields.Many2one('hr.employee','Employee')
    transaction_note_id = fields.Char(string="Description")

    payslip_id = fields.Many2one('hr.payslip','Payslip')

    company_id = fields.Many2one('res.company','Company')

    amount = fields.Float('Allowance')
    date = fields.Date('Date')

    code = fields.Char('Code')


    rule_id = fields.Many2one('hr.salary.rule', 'Type')



    @api.onchange('rule_id')
    def onchange_rule_id(self):
        if self.rule_id and self.rule_id.id:
            self.code = self.rule_id and self.rule_id.category_id and self.rule_id.category_id.code
