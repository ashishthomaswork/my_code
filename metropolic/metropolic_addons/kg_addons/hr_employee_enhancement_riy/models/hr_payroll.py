#-*- coding:utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

import time
from datetime import datetime, timedelta
from dateutil import relativedelta

import babel

from odoo import api, fields, models, tools, _
from odoo.exceptions import UserError, ValidationError
from odoo.tools.safe_eval import safe_eval

from odoo.addons import decimal_precision as dp




class HrPayslip(models.Model):

    _inherit = 'hr.payslip'

    number_of_days_period = fields.Float(string="Number of Days in period")
    number_of_days_worked = fields.Float(string="Number of Days Worked")


