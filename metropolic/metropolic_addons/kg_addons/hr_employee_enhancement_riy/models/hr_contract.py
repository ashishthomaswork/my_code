#-*- coding:utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

import time
from datetime import datetime, timedelta
from dateutil import relativedelta

import babel

from odoo import api, fields, models, tools, _
from odoo.exceptions import UserError, ValidationError
from odoo.tools.safe_eval import safe_eval

from odoo.addons import decimal_precision as dp




class HrContract(models.Model):

    _inherit = 'hr.contract'
    
    
   
    @api.depends('wage', 'od_allowance_rule_line_ids.amt')
    def _get_total_wage(self):
        lines = self.od_allowance_rule_line_ids
     
        tot = 0
        for line in lines:
            tot = tot + line.amt

        self.od_total_wage = tot + self.wage
        
    @api.multi 
    @api.one 
    @api.constrains('employee_id','state') 
    def _check_constriant(self): 
        state = self.state
        employee_id = self.employee_id and self.employee_id.id
        contract = self.env['hr.contract'].search([('employee_id','=',employee_id),('state','in',('open','pending'))])
        if len(contract) > 1:
            raise UserError(_('two active contract found.'))
 

    kg_working_hours = fields.Float(string='Working Hour',default=9)
    od_total_wage = fields.Float(string="Total Wage",compute='_get_total_wage')
    od_allowance_rule_line_ids = fields.One2many('od.allowance.rule.line','contract_id','Rule Lines')
    
class odallowance_rule_line(models.Model):
    _name = 'od.allowance.rule.line'
    _description = 'Contract Allowance Rule Line'

    contract_id = fields.Many2one('hr.contract','Contract ID',ondelete='cascade')

    deduct_in_unpaid_leave = fields.Boolean('Deduct(Unpaid Leave)',default=True)
    rule_type = fields.Many2one('hr.salary.rule','Allowance Rule')
    code = fields.Char(string='Code')
    amt = fields.Float('Amount')




