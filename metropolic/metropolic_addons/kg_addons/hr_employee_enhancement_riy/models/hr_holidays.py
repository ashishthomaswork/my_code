# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

# Copyright (c) 2005-2006 Axelor SARL. (http://www.axelor.com)

import logging
import math
import datetime as dt
from datetime import  timedelta, tzinfo, time, date, datetime
from datetime import timedelta
from werkzeug import url_encode

from odoo import api, fields, models
from odoo.exceptions import UserError, AccessError, ValidationError
from openerp.tools import float_compare
from odoo.tools.translate import _
from datetime import date, timedelta

_logger = logging.getLogger(__name__)


HOURS_PER_DAY = 8

class LeaveDetails(models.Model):
    _name='leave.details'
    
    leave_type  = fields.Many2one('hr.holidays.status','Leave Type')
    
    employee_id = fields.Many2one('hr.employee','Employee')
    
    leave_date = fields.Date('Date')
    
    count = fields.Float('Count')
    
    holiday_id = fields.Many2one('hr.holidays','Holiday')

class Holidays(models.Model):
    _inherit = "hr.holidays"


    leave_details = fields.One2many('leave.details','holiday_id')
#     mail_content = "  Hello  " + i.employee_ref.name + ",<br>Your Document " + i.name + "is going to expire on " + \
#                                    str(i.expiry_date) + ". Please renew it before expiry date"
#                     main_content = {
#                         'subject': _('Document-%s Expired On %s') % (i.name, i.expiry_date),
#                         'author_id': self.env.user.partner_id.id,
#                         'body_html': mail_content,
#                         'email_to': i.employee_ref.work_email,
#                     }
#                     self.env['mail.mail'].create(main_content).send()

    @api.multi
    def dummy_approve(self):
        for record in self:
            if record.department_id and record.department_id.manager_id and record.department_id.manager_id.user_id and record.department_id.manager_id.user_id.partner_id and record.department_id.manager_id.user_id.partner_id.id:
                check= self.env['mail.followers'].search([('res_model','=','hr.holidays'),('partner_id','=',record.department_id.manager_id.user_id.partner_id.id),('res_id','=',record.id)])
                print check,'1111111111111111'
              
                if not check:
                    print 'inside loop'
                    fol={}
                    fol['res_model']='hr.holidays'
                    fol['res_id']=record.id
                    fol['partner_id']=record.department_id.manager_id.user_id.partner_id.id
                    fol_id=self.env['mail.followers'].create(fol)
                    print fol_id,'12222222222222'
                    subtypes= self.env['mail.message.subtype'].search([('res_model','=','hr.holidays')]).ids
                    if subtypes:
                        for i in subtypes:
                            self._cr.execute('INSERT INTO mail_followers_mail_message_subtype_rel (mail_followers_id, mail_message_subtype_id) values (%s, %s)', (fol_id.id, i))
            
            
            return record.action_approve()
    @api.multi
    def notify_mgr(self):
        for record in self:
            if record.employee_id and record.employee_id.parent_id and record.employee_id.parent_id.user_id and record.employee_id.parent_id.user_id.partner_id and record.employee_id.parent_id.user_id.partner_id.id:
                check= self.env['mail.followers'].search([('res_model','=','hr.holidays'),('partner_id','=',record.employee_id.parent_id.user_id.partner_id.id),('res_id','=',record.id)])
                print check,'1111111111111111'
              
                if not check:
                    print 'inside loop'
                    fol={}
                    fol['res_model']='hr.holidays'
                    fol['res_id']=record.id
                    fol['partner_id']=record.employee_id and record.employee_id.parent_id and record.employee_id.parent_id.user_id and record.employee_id.parent_id.user_id.partner_id and record.employee_id.parent_id.user_id.partner_id.id
                    fol_id=self.env['mail.followers'].create(fol)
                    print fol_id,'12222222222222'
                    subtypes= self.env['mail.message.subtype'].search([('res_model','=','hr.holidays')]).ids
                    if subtypes:
                        for i in subtypes:
                            self._cr.execute('INSERT INTO mail_followers_mail_message_subtype_rel (mail_followers_id, mail_message_subtype_id) values (%s, %s)', (fol_id.id, i))
            
            if record.employee_id and  record.employee_id.user_id and record.employee_id.user_id.partner_id and record.employee_id.user_id.partner_id.id:
                check= self.env['mail.followers'].search([('res_model','=','hr.holidays'),('partner_id','=',record.employee_id.user_id.partner_id.id),('res_id','=',record.id)])
                print check,'1111111111111111'
              
                if not check:
                    print 'inside loop'
                    fol={}
                    fol['res_model']='hr.holidays'
                    fol['res_id']=record.id
                    fol['partner_id']=record.employee_id and record.employee_id.user_id and record.employee_id.user_id.partner_id and record.employee_id.user_id.partner_id.id
                    fol_id=self.env['mail.followers'].create(fol)
                    print fol_id,'12222222222222'
                    subtypes= self.env['mail.message.subtype'].search([('res_model','=','hr.holidays')]).ids
                    if subtypes:
                        for i in subtypes:
                            self._cr.execute('INSERT INTO mail_followers_mail_message_subtype_rel (mail_followers_id, mail_message_subtype_id) values (%s, %s)', (fol_id.id, i))
        
            return record.action_confirm()
        

    @api.multi
    def action_validate_engineer(self):
        if not self.state == 'validate1':
            raise UserError(_('project engineer cannot approve in this stage'))
        self.state == 'validate'

        return True

    @api.multi
    def action_validate_final_md(self):
        if not self.state == 'validate':
            raise UserError(_('MD cannot approve in this stage'))
        self.state = 'kg_final_validate'

        return True

    @api.multi
    def action_validate_final_officer(self):
        if not self.state == 'validate':
            raise UserError(_('HR Officer cannot approve in this stage'))
        self.state = 'kg_final_validate'

        return True








    @api.depends('date_from')     
    def _compute_month(self):
        res = {}
        for leave_obj in self:
            if leave_obj.date_from:
                date_from = leave_obj.date_from
                print "cccccccccccccccccccccccccc",date_from
                array = date_from.split("-")
                leave_obj.od_month = str(array[1])
    @api.depends('date_from')     
    def _compute_year(self):
        res = {}
        for leave_obj in self:
            if leave_obj.date_from:

                date_from = leave_obj.date_from
                print "cccccccccccccccccccccccccc",date_from
                array = date_from.split("-")
                leave_obj.od_year = str(array[0])
        
            
  
    od_date_from = fields.Date('Start Date', readonly=True, index=True, copy=False,
        states={'draft': [('readonly', False)], 'confirm': [('readonly', False)]})
    od_date_to = fields.Date('End Date', readonly=True, copy=False,
        states={'draft': [('readonly', False)], 'confirm': [('readonly', False)]})
    od_month = fields.Char('Month',compute='_compute_month',store=True)
    od_year = fields.Char('Year',compute='_compute_year',store=True)


    state = fields.Selection([
        ('draft', 'To Submit'),
        ('cancel', 'Cancelled'),
        ('confirm', 'To Approve'),
        ('refuse', 'Refused'),
        ('validate1', 'First Approval'),
        ('validate', 'Second Approval'),
        ('kg_final_validate', 'Third Approval')
        ], string='Status', readonly=True, track_visibility='onchange', copy=False, default='draft',
            help="The status is set to 'To Submit', when a holiday request is created." +
            "\nThe status is 'To Approve', when holiday request is confirmed by user." +
            "\nThe status is 'Refused', when holiday request is refused by manager." +
            "\nThe status is 'Approved', when holiday request is approved by manager.")
    
    
    
    @api.onchange('od_date_from','od_date_to')
    def _onchange_order_line(self):
        if not self.od_date_from or self.od_date_to:
            self.leave_details=[]
        if self.od_date_from and self.od_date_to:
            DATETIME_FORMAT = "%Y-%m-%d"
            from_dt = datetime.strptime(self.od_date_from, DATETIME_FORMAT)
            to_dt = datetime.strptime(self.od_date_to, DATETIME_FORMAT)
            delta = to_dt - from_dt
        
            print delta
    
            if delta.days < 0: 
                self.leave_details=[]
        
            res = []
            
            for n in range(delta.days + 1): 
                res.append((0,0,{'leave_date':from_dt  + timedelta(days=n),'count':1,'employee_id':self.employee_id.id,'leave_type':self.holiday_status_id.id}))
               
               # res.append(from_dt  + timedelta(days=n)) 
            print res
            self.leave_details=res
           
#         for line in self.order_line:
#              for add_opt_product in line.add_opt_products:
#                  order_lines += line.new({'order_id': self.id, 
# 'product_id': add_opt_product.id, 'product_uom': add_opt_product.uom_id.id})
# 
#          self.order_line = order_lines



       


    @api.onchange('holiday_status_id')
    def _onchange_holiday_status_id(self):
        if self.holiday_status_id and self.holiday_status_id.id:
            self.od_date_from = False

            self.od_date_to = False


    @api.onchange('employee_id')
    def _onchange_employee(self):
        self.department_id = self.employee_id.department_id
        self.holiday_status_id = False
   








        
    @api.onchange('date_from')
    def _onchange_date_from(self):
        """ If there are no date set for date_to, automatically set one 8 hours later than
            the date_from. Also update the number_of_days.
        """
        date_from = self.date_from
        date_to = self.date_to

        # No date_to set so far: automatically compute one 8 hours later
        if date_from and not date_to:
            date_to_with_delta = fields.Datetime.from_string(date_from) + timedelta(hours=HOURS_PER_DAY)
            self.date_to = str(date_to_with_delta)

        # Compute and update the number of days
        if (date_to and date_from) and (date_from <= date_to):
            self.number_of_days_temp = self._get_number_of_days(date_from, date_to, self.employee_id.id)
        else:
            self.number_of_days_temp = 0
        if not self.od_date_from:
            self.od_date_from = self.date_from
        if self.date_from:
            print "3333333333333333333333333333333333333333333333333333"
            self.date_from = self.date_from + " 09:00:00"
            
    @api.onchange('od_date_from')
    def _onchange_od_date_from(self):
        print "::::::::::::::::::::::::::::",self.od_date_from

        
        if self.od_date_from:
            self.date_from = self.od_date_from + " 09:00:00"
            
    @api.onchange('od_date_to')
    def _onchange_od_date_to(self):

        
        if self.od_date_to:
            self.date_to = self.od_date_to + " 18:00:00"
            
        
            

    @api.onchange('date_to')
    def _onchange_date_to(self):
        """ Update the number_of_days. """
        date_from = self.date_from
        date_to = self.date_to

        # Compute and update the number of days
        if (date_to and date_from) and (date_from <= date_to):
            self.number_of_days_temp = self._get_number_of_days(date_from, date_to, self.employee_id.id)
        else:
            self.number_of_days_temp = 0
            
        if not self.od_date_to:
            self.od_date_to =  date_to
            
        if self.date_to:
            print "33333333333333333333333666663333333333333333333333333333333"
            self.date_to = str(self.date_to)[:10] + " 18:00:00"

