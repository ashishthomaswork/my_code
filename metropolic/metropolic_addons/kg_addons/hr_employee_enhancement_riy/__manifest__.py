# -*- coding: utf-8 -*-
###################################################################################
#    A part of OpenHRMS Project <https://www.openhrms.com>
#
#    Cybrosys Technologies Pvt. Ltd.
#    Copyright (C) 2018-TODAY Cybrosys Technologies (<https://www.cybrosys.com>).
#    Author: Jesni Banu (<https://www.cybrosys.com>)
#
#    This program is free software: you can modify
#    it under the terms of the GNU Affero General Public License (AGPL) as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
###################################################################################
{
    'name': 'HRMS Enhancement',
    'version': '10.0.1.1.0',
    'summary': """hrms Enhancement""",
    'description': 'This module helps you to add more information in employee records.',
    'category': 'Human Resources',
    'author': 'KG',
    'company': 'KG',
    'website': "klystronglobal.com",
    'depends': ['base', 'hr', 'mail','hr_contract','hr_payroll','hr_holidays'],
    'data': [
        'security/ir.model.access.csv',
        'views/hr_employee_view.xml',
        'views/hr_contract_view.xml',
		'views/hr_payroll_view.xml',
        'views/hr_holidays_view.xml',
        'views/payroll_transaction_view.xml',
        'views/working_company_view.xml',
        'views/visa_company_view.xml',
		'views/payroll_transaction_view.xml'

    ],
    'demo': [],
    'license': 'AGPL-3',
    'installable': True,
    'auto_install': False,
    'application': False,
}
