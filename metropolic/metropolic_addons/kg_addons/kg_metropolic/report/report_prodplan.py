# -*- coding: utf-8 -*-

import time
from odoo import api, models, _
from odoo.exceptions import UserError
from datetime import datetime, timedelta


class ReportProdplan(models.AbstractModel):
    _name = 'report.kg_metropolic.report_prodplan'
    
    def get_plan_lines(self, data):
        lines=[]
        date_from=data.get('date_from')
        date_to = data.get('date_to')

        machine_id =data.get('machine_id')[0]


        
        plans= self.env['kg.prod.shift.line'].search([('kg_date','<=',date_to),('kg_date','>=',date_from),('kg_product.kg_machine','=',machine_id)],order="kg_date asc")
        print plans,'222222222222222'
        for i in plans:
            vals={}
            vals['kg_pack']=i.kg_product.inner_outer_pack_conf or ''
            vals['uom_id']=i.kg_product.outer_pack_uom  or ''
            vals['kg_lbl']=i.kg_product.kg_lbl or ''
            vals['kg_width']=i.kg_product.kg_width or ''
            vals['kg_sht']=i.kg_product.kg_sht or ''
            vals['kg_emb']=i.kg_product.kg_emb  or ''

            vals['item_code']=i.kg_product.default_code

            vals['core']=i.kg_product.core_size_ply

            vals['lbl']=i.kg_product.kg_lbl

            vals['hndl'] = i.kg_product.kg_hndl

            vals['lam']=i.kg_product.kg_lam

            vals['perf']=i.kg_product.kg_perf

            vals['dia'] = i.kg_product.kg_dia

            vals['wght']= i.kg_product.weight_roll or i.kg_product.tn_weight or 0.0

            vals['kg_trrt']=i.kg_trrt  or ''
            vals['cph'] = i.kg_product.eff_output_carton or ''

            vals['gsm']=i.kg_product.kg_gsm

            vals['ply']=i.kg_product.kg_ply

            vals['fore']= i.kg_forecast
            vals['planned']= i.kg_planned
            vals['product']=i.kg_product.name
            vals['date']= datetime.strptime(i.kg_date, '%Y-%m-%d').strftime('%d/%m/%y')
            vals['day']=(i.kg_day)[:3]
            vals['shift']=i.kg_shift_id.name
            vals['qty']=i.kg_qty
            vals['production_id']=i.production_id and i.production_id.name or ''

            
            lines.append(vals)
        return lines
    
    @api.model
    def render_html(self, docids, data=None):
        if not data.get('form') or not self.env.context.get('active_model') or not self.env.context.get('active_id'):
            raise UserError(_("Form content is missing, this report cannot be printed."))

        self.model = self.env.context.get('active_model')
        docs = self.env[self.model].browse(self.env.context.get('active_id'))
        print docs
        report_lines = self.get_plan_lines(data.get('form'))
        docargs = {
            'doc_ids': self.ids,
            'doc_model': self.model,
            'data': data['form'],
            'docs': docs,
            'time': time,
            'get_plan_lines': report_lines,
        }
        return self.env['report'].render('kg_metropolic.report_prodplan', docargs)
