# -*- coding: utf-8 -*-
from odoo import models,api,fields,_

# -*- coding: utf-8 -*-
from odoo import models,api,fields
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT, float_compare
from odoo.exceptions import UserError




class HrEmployee(models.Model):
    _inherit = 'hr.employee'

    @api.model
    def create(self, vals):
        if vals.get('department_id') == 4:
            vals['identification_id'] = self.env['ir.sequence'].next_by_code('kg.prod.dept') or _('New')

        elif vals.get('department_id') == 2:
            vals['identification_id'] = self.env['ir.sequence'].next_by_code('kg.sale.dept') or _('New')

        else:
            vals['identification_id'] = self.env['ir.sequence'].next_by_code('kg.other.dept') or _('New')

        result = super(HrEmployee, self).create(vals)
        return result
