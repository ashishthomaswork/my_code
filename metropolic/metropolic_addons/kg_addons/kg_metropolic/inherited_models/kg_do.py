# -*- coding: utf-8 -*-
from odoo import models, fields, api, _
from odoo.exceptions import UserError


class StockPicking(models.Model):
	_inherit = 'stock.picking'

	kg_invoice_id = fields.Many2one('account.invoice', string='Invoice')


	@api.multi
	def create_invoice_from_delivery(self):
		if self.kg_invoice_id and self.kg_invoice_id.id:
			raise UserError(_('Invoice Already created')) 
		if self.picking_type_code != 'outgoing':
			raise UserError(_('this option only for delivery orders')) 
		sale_order_obj = self.sale_id
		print '////////////////////////////////////////////'
		print '////////////////////////////////////////////'
		print '////////////////////////////////////////////'
		print '////////////////////////////////////////////',sale_order_obj
		print '////////////////////////////////////////////'
		print '////////////////////////////////////////////'
		print '////////////////////////////////////////////'
		print '////////////////////////////////////////////'
		result = sale_order_obj.action_invoice_create(grouped = False)
		print result
		self.kg_invoice_id = result and result[0] or False
		if self.kg_invoice_id and self.kg_invoice_id.id:
			self.kg_invoice_id.action_invoice_open()
			self.kg_invoice_id.kg_do_id = self.id



		return True

