import logging

from ast import literal_eval

from odoo import api, fields, models, _
from odoo.exceptions import UserError
from odoo.tools.misc import ustr

from odoo.addons.base.ir.ir_mail_server import MailDeliveryException
from odoo.addons.auth_signup.models.res_partner import SignupError, now


class ResUsers(models.Model):
    _inherit = 'res.users'

    @api.model
    def create(self, values):
        # overridden to automatically invite user to sign up
        user = super(ResUsers, self).create(values)
        if user.email and not self.env.context.get('no_reset_password'):
            try:
                user.with_context(create_user=True).action_reset_password()
            except MailDeliveryException:
                user.partner_id.with_context(create_user=True).signup_cancel()

        if user.partner_id:
            partner = user.partner_id
            partner.write({'customer':False})
        return user