# -*- coding: utf-8 -*-
from odoo import models,api,fields,_

# -*- coding: utf-8 -*-
from odoo import models,api,fields
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT, float_compare
from odoo.exceptions import UserError

class MrpBom(models.Model):
    _inherit='mrp.bom'


    already_bom_created_product_ids = fields.Many2many('product.template','product_tmpl_id','bom_id',string="Already Bom Created Product") 
    inner_outer_pack_conf = fields.Char(string='Inner * Outer Packing Configuration') 
    outer_pack_uom = fields.Char(string='Outer Pack/UOM') 

    weight = fields.Float(string='Weight')  






    @api.multi
    def bom_created(self):

       self.product_tmpl_id.is_it_bom_created = True










  
    
    @api.onchange('product_tmpl_id')
    def onchange_product_tmpl_id(self):
        if self.product_tmpl_id:
            self.product_uom_id = self.product_tmpl_id.uom_id.id

            self.code = self.product_tmpl_id.default_code
            self.outer_pack_uom = self.product_tmpl_id.outer_pack_uom
            self.inner_outer_pack_conf = self.product_tmpl_id.inner_outer_pack_conf
            self.weight = self.product_tmpl_id.weight_roll or self.product_tmpl_id.defined_weight or self.product_tmpl_id.tn_weight


            if self.product_id.product_tmpl_id != self.product_tmpl_id:
                self.product_id = False
            b=[]    
            for i in self.product_tmpl_id.kg_product_raw_lines:
                a={}
                a['product_id']=i.product_id.id
                a['product_uom_id']=i.product_id.uom_id.id
                a['product_qty']=1
                c=(0,0,a)
                b.append(c)
            self.bom_line_ids=b



    @api.model
    def default_get(self, fields):
        res = super(MrpBom, self).default_get(fields)

        product_ids = []

        product_ids_with_bom = self.env['mrp.bom'].search([])

        for bom in product_ids_with_bom:
            product_ids.append(bom.product_tmpl_id and bom.product_tmpl_id.id)

        print "ffffffffffffffffffff",product_ids

        res['already_bom_created_product_ids'] = [(6, 0, product_ids)]

	
        return res



    @api.multi
    def update_product_master(self):

       bom_line_ids = self.bom_line_ids 
       b = []

       current_raw_lines_in_product = self.product_tmpl_id and self.product_tmpl_id.kg_product_raw_lines
       for l in current_raw_lines_in_product:
           l.unlink()

       
       for line in bom_line_ids:
            a={}
            a['product_id']=line.product_id and line.product_id.id or False
            c=(0,0,a)
            b.append(c)

       self.product_tmpl_id.kg_product_raw_lines=b

            







class mrp_production(models.Model):
    
    _inherit="mrp.production"

    kg_shift_id = fields.Many2one('kg.shift.master',string="Shift")

    kg_day = fields.Char('Day')





    @api.multi
    def create_mr(self):

        kg_mrp_id = self.id
        kg_req_date = self.date_planned_start
        line = self.move_raw_ids
        line_vals_array = []

		        
        for li in line:
			vals = (0,0,{'kg_product':li.product_id and li.product_id.id,
                   'kg_uom':li.product_uom and li.product_uom.id,
                   'kg_qty':li.product_uom_qty,
                   'kg_req_date':kg_req_date})

			line_vals_array.append(vals)

            

        self.env['kg.mat.req'].create({'kg_mrp_id':kg_mrp_id,'kg_mat_req_lines':line_vals_array})


