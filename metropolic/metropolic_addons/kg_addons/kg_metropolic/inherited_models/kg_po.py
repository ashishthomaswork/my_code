# -*- coding: utf-8 -*-
from odoo import models,api,fields,_

from datetime import datetime
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT, float_compare
from odoo.exceptions import UserError

class PurchaseOrder(models.Model):
    
    _inherit="purchase.order"
    
    local_curr_val = fields.Float('Local Currency Value',compute="convert_curr",store=True)
    company_currency_id = fields.Many2one('res.currency','Currency',compute="know_curr")

    is_it_finance_approved = fields.Boolean(string="Finance Approved")

    is_it_emergency_order = fields.Boolean(string="Emergency Order")


    @api.multi
    def invoice_print(self):
        self.is_it_finance_approved = True



    @api.multi
    def button_confirm(self):
		result = super(PurchaseOrder, self).button_confirm()

		is_it_emergency_order = self.is_it_emergency_order

		is_it_finance_approved = self.is_it_finance_approved

		if not is_it_finance_approved and not is_it_emergency_order:
			raise UserError(_('Finance not approved yet'))

		return result	
    
    
    @api.depends('company_id')
    def know_curr(self):
        for record in self:
            print record.env.context,'111111111',record.env.user.company_id.name
            currency=  record.env.user.company_id.currency_id
            
          
            record.company_currency_id =currency.id
    @api.depends('amount_total','currency_id')
    def convert_curr(self):
        for record in self:
            print record.env.context,'111111111',record.env.user.company_id.name
            curr_company=  record.env.user.company_id
            
            print curr_company.currency_id.name,'curreeeeeeeeee'
            new_val=0.0
            
            rec_rate= record.currency_id.rate
            if rec_rate ==0:
                rec_rate=1
            if rec_rate < 1.0:
                new_val=record.amount_total/rec_rate
            else:
                new_val=record.amount_total*rec_rate
            record.local_curr_val = new_val or 0.0




class PurchaseOrderLine(models.Model):
    
    _inherit="purchase.order.line"


    @api.onchange('product_id')
    def onchange_product_id(self):
        result = {}
        if not self.product_id:
            return result

        # Reset date, price and quantity since _onchange_quantity will provide default values
        self.date_planned = datetime.today().strftime(DEFAULT_SERVER_DATETIME_FORMAT)
        self.price_unit = self.product_qty = 0.0
        self.product_uom = self.product_id.uom_po_id or self.product_id.uom_id
        result['domain'] = {'product_uom': [('category_id', '=', self.product_id.uom_id.category_id.id)]}

        product_lang = self.product_id.with_context(
            lang=self.partner_id.lang,
            partner_id=self.partner_id.id,
        )
        self.name = product_lang.display_name
        if product_lang.description_purchase:
            self.name += '\n' + product_lang.description_purchase

        fpos = self.order_id.fiscal_position_id
        # if self.env.uid == SUPERUSER_ID:
        #     company_id = self.env.user.company_id.id
        #     self.taxes_id = fpos.map_tax(self.product_id.supplier_taxes_id.filtered(lambda r: r.company_id.id == company_id))
        # else:
        #     self.taxes_id = fpos.map_tax(self.product_id.supplier_taxes_id)

        self._suggest_quantity()
        self._onchange_quantity()

        return result


    

    @api.model
    def get_taxes(self):
        if self.order_id.tax_id_purchase:
            return self.order_id.tax_id_purchase and self.order_id.tax_id_purchase.id or False


    taxes_id = fields.Many2many('account.tax', string='Taxes', domain=['|', ('active', '=', False), ('active', '=', True)], default=get_taxes)
    