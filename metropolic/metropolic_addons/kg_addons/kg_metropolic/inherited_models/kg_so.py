# -*- coding: utf-8 -*-
from odoo import models,api,fields,_

from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT, float_compare
from odoo.exceptions import UserError
from datetime import datetime, timedelta, date

class SaleOrder(models.Model):
    
    _inherit="sale.order"
    
    local_curr_val = fields.Float('Local Currency Value',compute="convert_curr",store=True)
    company_currency_id = fields.Many2one('res.currency','Currency',compute="know_curr")
    kg_delivery = fields.Char(string='Delivery')
    kg_validity = fields.Date(string='Validity', readonly="1")


    @api.model
    def create(self, vals):
        vals['kg_validity'] = datetime.now() + timedelta(days=15)
        return super(SaleOrder, self).create(vals)


    @api.multi
    def action_cancel(self):
        for pid in self.picking_ids:
            if pid.state != 'cancel':
                raise UserError(_('Cannot cancel\n Active delivery orders found'))
        return super(SaleOrder, self).action_cancel()
    
    @api.depends('company_id')
    def know_curr(self):
        for record in self:
            print record.env.context,'111111111',record.env.user.company_id.name
            currency=  self.env.user.company_id.currency_id
            
          
            record.company_currency_id =currency.id
    @api.depends('amount_total','pricelist_id')
    def convert_curr(self):
        for record in self:
            print record.env.context,'111111111',record.env.user.company_id.name
            curr_company=  self.env.user.company_id
            
            print curr_company.currency_id.name,'curreeeeeeeeee'
            new_val=0.0
            rec_rate= record.pricelist_id.currency_id.rate
            if rec_rate ==0:
                rec_rate=1
            if rec_rate < 1.0:
                new_val=record.amount_total/rec_rate
            else:
                new_val=record.amount_total*rec_rate
            record.local_curr_val = new_val or 0.0
            
            
class SaleOrderLine(models.Model):
    _inherit='sale.order.line'



    @api.constrains('price_unit')
    def _check_price(self):
        for record in self:
            if record.price_unit < record.product_id.standard_price:
                raise UserError(_('Unit price cannot be less than standard price'))




    
    @api.multi
    @api.onchange('product_id')
    def product_id_change(self):
        if not self.product_id:
            return {'domain': {'product_uom': []}}

        vals = {}
        domain = {'product_uom': [('category_id', '=', self.product_id.uom_id.category_id.id)]}
        if not self.product_uom or (self.product_id.uom_id.id != self.product_uom.id):
            vals['product_uom'] = self.product_id.uom_id
            vals['product_uom_qty'] = 1.0

        product = self.product_id.with_context(
            lang=self.order_id.partner_id.lang,
            partner=self.order_id.partner_id.id,
            quantity=vals.get('product_uom_qty') or self.product_uom_qty,
            date=self.order_id.date_order,
            pricelist=self.order_id.pricelist_id.id,
            uom=self.product_uom.id
        )

        result = {'domain': domain}

        title = False
        message = False
        warning = {}
        if product.sale_line_warn != 'no-message':
            title = _("Warning for %s") % product.name
            message = product.sale_line_warn_msg
            warning['title'] = title
            warning['message'] = message
            result = {'warning': warning}
            if product.sale_line_warn == 'block':
                self.product_id = False
                return result

        name = product.name_get()[0][1]
        if product.description_sale:
            name += '\n' + product.description_sale
        vals['name'] = name
        
        vals['item_code']=self.product_id.default_code
        vals['kg_gsm']=self.product_id.kg_gsm
        vals['kg_ply']=self.product_id.kg_ply or False
        vals['kg_sheet']=self.product_id.kg_sht
        vals['kg_meter']=self.product_id.inner_outer_pack_conf
        vals['kg_weight']=self.product_id.weight_uom
        vals['kg_packing']=self.product_id.outer_pack_uom

        # self._compute_tax_id()

        if self.order_id.pricelist_id and self.order_id.partner_id:
            vals['price_unit'] = self.env['account.tax']._fix_tax_included_price_company(self._get_display_price(product), product.taxes_id, self.tax_id, self.company_id)
        self.update(vals)

        return result


    @api.model
    def get_taxes(self):
        if self.order_id.tax_id_sale:
            return self.order_id.tax_id_sale and self.order_id.tax_id_sale.id or False

    
    item_code = fields.Char('Item Code')
    
    kg_gsm = fields.Char('GSM')
    
    kg_ply = fields.Char('Ply')
    
    kg_sheet = fields.Char('Sheets')
    
    kg_meter = fields.Char('Meter')
    
    kg_weight  = fields.Char('Weight')
    
    kg_packing = fields.Char('Packing')


    tax_id =  fields.Many2many('account.tax', string='Taxes', domain=[('active', '=', True)], default=get_taxes)

    
            
            
            
    