# -*- coding: utf-8 -*-
from odoo import models,api,fields,_

# -*- coding: utf-8 -*-
from odoo import models,api,fields
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT, float_compare
from odoo.exceptions import UserError


from odoo.tools import amount_to_text_en



class HrPayslip(models.Model):
    _inherit = 'hr.payslip'
    telephone_mob_allowance = fields.Float(string='Telephone/Mobile Allowance')
    house_allowance = fields.Float(string='Housing Allowance')
    transportation_allowance = fields.Float(string='Transportation Allowance')
    ot_allowance = fields.Float(string='OT Allowance')
    basic = fields.Float(string='Basic')
    gross = fields.Float(string='Gross')
    net = fields.Float(string='Net')
    type_id = fields.Many2one('hr.contract.type', string="Contract Type",)

    total_no_of_unpaid_leaves = fields.Float(string="Total Unpaid Leaves")
    leave_deductions = fields.Float(string="Leave Deductions")

    other_allowances = fields.Float(string="Other Allowances")

    other_deductions = fields.Float(string="Other Deductions")

    remarks = fields.Text(string="Remarks")

    number_of_days_period = fields.Float(string="Number of Days in period")

    month_days = fields.Float(string="Days(month)")
    net_worked_days = fields.Float(string="Net Worked Days")
    food_allowance = fields.Float(string="Food Allowance")


	

	


    salary_advance = fields.Float(string='Salary Advance')
    fine = fields.Float(string='Fine')


    od_variance_line = fields.One2many('od.hr.variance.line','payslip_id',string="Variance Line")


class od_hr_variance_line(models.Model):
    _name = "od.hr.variance.line"

    payslip_id = fields.Many2one('hr.payslip','PaySlip',ondelete='cascade')
    date_value = fields.Date('Date')
    amount = fields.Float('Amount')
    rule_id = fields.Many2one('hr.salary.rule','Rule')
    transaction_id = fields.Many2one('od.payroll.transactions','Transaction')
   
    transaction_note = fields.Char(string="Transaction Note")


