# -*- coding: utf-8 -*-
# /######################################################################################
#
#    Klystron Technologies
#    Copyright (C) 2004-TODAY Klystron Technologies Pvt Ltd.(<http://klystrontech.com/>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# /######################################################################################

from odoo import models, fields, api, _


class FleetVehicle(models.Model):
    _inherit = 'fleet.vehicle'

    vehicle_status = fields.Selection([('own', 'Own'), ('hired', 'Hired')], string="Vehicle Status", default="own")
    supplier_from = fields.Many2one('res.partner', string="Supplier from")
    vehicle_location = fields.Char(string="Vehicle Location")
    stock_location_id = fields.Many2one('stock.location', string="Stock Location", store=True)

    @api.model
    def create(self, values):
        res = super(FleetVehicle, self).create(values)
        res_var = self.env['stock.location'].create({
            'name': res.license_plate,
            'usage': 'customer'
        })
        res.write({'stock_location_id': res_var.id})
        return res
