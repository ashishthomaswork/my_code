# -*- coding: utf-8 -*-
from odoo import models,api,fields,_

# -*- coding: utf-8 -*-
from odoo import models,api,fields
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT, float_compare
from odoo.exceptions import UserError

class kg_location(models.Model):
    
    _name='kg.location'
    
    name = fields.Char('Name',required=True)
    
    desc = fields.Text('Description')
class res_partner(models.Model):
    
    _inherit='res.partner'
    
    kg_location = fields.Many2one('kg.location','Location')
    
    kg_ind_class = fields.Char('Industry Classification')
    
    
    kg_acc_mgr   = fields.Many2one('hr.employee','Account/Relationship Manager')
    
    kg_vat = fields.Char('VAT')
    
    
    kg_contract_exp_date = fields.Date('Contract Expiry Date')
    
    kg_buis_area = fields.Char('Buisness Area')
    
    
    kg_supp_code = fields.Char('Supplier Code')
    
    
    kg_trno = fields.Char('TR No')
    
    kg_acc_no = fields.Char('Account No')
    
    kg_iban = fields.Char('IBAN')
    
    kg_saleteam = fields.Many2one('crm.team','Division')
    
    kg_cus_code = fields.Char('Customer Code')
    
    property_account_receivable_id = fields.Many2one(required=False)
    
    property_account_payable_id = fields.Many2one(required=False)
    
    @api.depends('is_company')
    def _compute_company_type(self):
        for partner in self:
            partner.company_type = 'company' if partner.customer else 'person'


    #kg_supp_code = fields.Char('Supplier Code')
    
    
    @api.model
    def create(self,vals):
        if vals.get('supplier') == True:
            supp_name = vals.get('name')
            sequence_code='supp.code'
            code = self.env['ir.sequence'].next_by_code(sequence_code)
    
            supp_code = supp_name[:2]+supp_name[-2:]+code
    
            vals['kg_supp_code']=supp_code.upper()
            
            
        if vals.get('customer') == True:
            cus_name = vals.get('name')
            sequence_code='cus.code'
            code = self.env['ir.sequence'].next_by_code(sequence_code)
    
            cus_code = cus_name[:2]+cus_name[-2:]+code
    
            vals['kg_cus_code']=cus_code.upper()
    
        return super(res_partner, self).create(vals)
    
    #
    #
    #