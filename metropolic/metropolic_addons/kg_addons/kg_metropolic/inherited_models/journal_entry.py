# -*- coding: utf-8 -*-
from odoo import models,api,fields,_

# -*- coding: utf-8 -*-

from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT, float_compare
from odoo.exceptions import UserError
import re
import StringIO
import cStringIO
import base64
import time
from dateutil.relativedelta import relativedelta
from datetime import datetime, timedelta, date
import xlwt
from xlwt import *
from odoo import fields, api, models
from odoo.tools.translate import _
from odoo.tools.misc import formatLang


class ReportFinancial(models.AbstractModel):
    _inherit = 'report.account.report_financial'

    def _compute_account_balance(self, accounts):
        """ compute the balance, debit and credit for the provided accounts
        """
        mapping = {
            'balance': "COALESCE(SUM(debit),0) - COALESCE(SUM(credit), 0) as balance",
            'debit': "COALESCE(SUM(debit), 0) as debit",
            'credit': "COALESCE(SUM(credit), 0) as credit",
        }

        res = {}
        for account in accounts:
            res[account.id] = dict((fn, 0.0) for fn in mapping.keys())
        if accounts:
            tables, where_clause, where_params = self.env['account.move.line']._query_get()
            tables = tables.replace('"', '') if tables else "account_move_line"
            wheres = [""]
            if where_clause.strip():
                wheres.append(where_clause.strip())
            filters = " AND ".join(wheres)
            request = "SELECT account_id as id, " + ', '.join(mapping.values()) + \
                       " FROM " + tables + \
                       " WHERE account_id IN %s " \
                            + filters + \
                       " GROUP BY account_id"
            params = (tuple(accounts._ids),) + tuple(where_params)
            print where_params,'paramsparamsparamsparamsparamsparamsparamsparams',wheres
            self.env.cr.execute(request, params)
            for row in self.env.cr.dictfetchall():
                res[row['id']] = row
        return res
    
    
    def get_account_lines(self, data):
        lines = []
        print data,'dataaaaaaaaaaaaaaaaaaaa',data.get('used_context'),data.get('analytic_account_ids')
        if data.get('analytic_account_ids'):
            data['used_context']['analytic_account_id']=data.get('analytic_account_ids')[0] or False
        if data.get('kg_location'):
            data['used_context']['kg_location']=data.get('kg_location')[0] or False
        if data.get('product_categ_id'):
            data['used_context']['product_categ_id']=data.get('product_categ_id')[0] or False
        print data.get('used_context'),'data12222222222222'
        account_report = self.env['account.financial.report'].search([('id', '=', data['account_report_id'][0])])
        child_reports = account_report._get_children_by_order()
        res = self.with_context(data.get('used_context'))._compute_report_balance(child_reports)
        if data['enable_filter']:
            comparison_res = self.with_context(data.get('comparison_context'))._compute_report_balance(child_reports)
            for report_id, value in comparison_res.items():
                res[report_id]['comp_bal'] = value['balance']
                report_acc = res[report_id].get('account')
                if report_acc:
                    for account_id, val in comparison_res[report_id].get('account').items():
                        report_acc[account_id]['comp_bal'] = val['balance']

        for report in child_reports:
            vals = {
                'name': report.name,
                'balance': res[report.id]['balance'] * report.sign,
                'type': 'report',
                'level': bool(report.style_overwrite) and report.style_overwrite or report.level,
                'account_type': report.type or False, #used to underline the financial report balances
            }
            if data['debit_credit']:
                vals['debit'] = res[report.id]['debit']
                vals['credit'] = res[report.id]['credit']

            if data['enable_filter']:
                vals['balance_cmp'] = res[report.id]['comp_bal'] * report.sign

            lines.append(vals)
            if report.display_detail == 'no_detail':
                #the rest of the loop is used to display the details of the financial report, so it's not needed here.
                continue

            if res[report.id].get('account'):
                sub_lines = []
                for account_id, value in res[report.id]['account'].items():
                    #if there are accounts to display, we add them to the lines with a level equals to their level in
                    #the COA + 1 (to avoid having them with a too low level that would conflicts with the level of data
                    #financial reports for Assets, liabilities...)
                    flag = False
                    account = self.env['account.account'].browse(account_id)
                    vals = {
                        'name': account.code + ' ' + account.name,
                        'balance': value['balance'] * report.sign or 0.0,
                        'type': 'account',
                        'level': report.display_detail == 'detail_with_hierarchy' and 4,
                        'account_type': account.internal_type,
                    }
                    if data['debit_credit']:
                        vals['debit'] = value['debit']
                        vals['credit'] = value['credit']
                        if not account.company_id.currency_id.is_zero(vals['debit']) or not account.company_id.currency_id.is_zero(vals['credit']):
                            flag = True
                    if not account.company_id.currency_id.is_zero(vals['balance']):
                        flag = True
                    if data['enable_filter']:
                        vals['balance_cmp'] = value['comp_bal'] * report.sign
                        if not account.company_id.currency_id.is_zero(vals['balance_cmp']):
                            flag = True
                    if flag:
                        sub_lines.append(vals)
                lines += sorted(sub_lines, key=lambda sub_line: sub_line['name'])
        return lines

class account_move_line(models.Model):
    
    _inherit='account.move.line'
    
    
  
    
    product_categ_id = fields.Many2one('product.category', related='product_id.categ_id')
    kg_location = fields.Many2one('kg.location','Location')
    
    @api.onchange('partner_id')
    def onchange_partner_id(self):
        self.kg_location=self.partner_id.kg_location
        
        
    @api.model
    def _query_get(self, domain=None):
        context = dict(self._context or {})
        domain = domain or []
        if not isinstance(domain, (list, tuple)):
            domain = safe_eval(domain)

        date_field = 'date'
        if context.get('aged_balance'):
            date_field = 'date_maturity'
        if context.get('date_to'):
            domain += [(date_field, '<=', context['date_to'])]
        if context.get('date_from'):
            if not context.get('strict_range'):
                domain += ['|', (date_field, '>=', context['date_from']), ('account_id.user_type_id.include_initial_balance', '=', True)]
            elif context.get('initial_bal'):
                domain += [(date_field, '<', context['date_from'])]
            else:
                domain += [(date_field, '>=', context['date_from'])]

        if context.get('journal_ids'):
            domain += [('journal_id', 'in', context['journal_ids'])]

        state = context.get('state')
        if state and state.lower() != 'all':
            domain += [('move_id.state', '=', state)]

        if context.get('company_id'):
            domain += [('company_id', '=', context['company_id'])]
            
        if context.get('product_categ_id'):
            domain += [('product_categ_id', '=', context['product_categ_id'])]
            
        if context.get('analytic_account_id'):
            domain += [('analytic_account_id', '=', context['analytic_account_id'])]
        if context.get('kg_location'):
            domain += [('kg_location', '=', context['kg_location'])]
            
       

        if 'company_ids' in context:
            domain += [('company_id', 'in', context['company_ids'])]

        if context.get('reconcile_date'):
            domain += ['|', ('reconciled', '=', False), '|', ('matched_debit_ids.create_date', '>', context['reconcile_date']), ('matched_credit_ids.create_date', '>', context['reconcile_date'])]

        if context.get('account_tag_ids'):
            domain += [('account_id.tag_ids', 'in', context['account_tag_ids'].ids)]

        if context.get('analytic_tag_ids'):
            domain += ['|', ('analytic_account_id.tag_ids', 'in', context['analytic_tag_ids'].ids), ('analytic_tag_ids', 'in', context['analytic_tag_ids'].ids)]

        if context.get('analytic_account_ids'):
            domain += [('analytic_account_id', 'in', context['analytic_account_ids'].ids)]

        where_clause = ""
        where_clause_params = []
        tables = ''
        if domain:
            query = self._where_calc(domain)
            tables, where_clause, where_clause_params = query.get_sql()
        return tables, where_clause, where_clause_params

        
        
    
    
    
    
#     @api.onchange('partner_id')
#     def onchange_partner_id(self):
#         self.kg_location=self.partner_id.kg_location
#         
        
class AccountingReport(models.TransientModel):
    _inherit='accounting.report'
    
    product_categ_id = fields.Many2one('product.category')
    kg_location = fields.Many2one('kg.location','Location')
    analytic_account_ids = fields.Many2one('account.analytic.account','Cost Centre')
    
    
    @api.multi 
    def check_report_xls(self):
            current_obj = self
            data = {} 
            data['form'] = self.read(['product_categ_id','kg_location','analytic_account_ids','date_from',  'date_to', 'account_report_id', 'target_move','enable_filter','debit_credit','filter_cmp','date_from_cmp','date_to_cmp'])[0]
            target_move = dict(self.env['accounting.report'].fields_get(allfields=['target_move'])['target_move']['selection'])[current_obj.target_move]
            used_context = self._build_contexts(data)
            comparision_context = self._build_comparison_context(data)
            data['form']['comparison_context'] = dict(comparision_context, lang=self.env.context.get('lang', 'en_US'))
            data['form']['used_context'] = dict(used_context, lang=self.env.context.get('lang', 'en_US'))
            account_res = self.env['report.account.report_financial'].get_account_lines(data.get('form'))
            #### From get lines ###
            fp = cStringIO.StringIO()
            wb = xlwt.Workbook(encoding='utf-8')

            header_style = xlwt.XFStyle()
            font = xlwt.Font()
            pattern = xlwt.Pattern()
            pattern.pattern = xlwt.Pattern.SOLID_PATTERN
            bg_color = current_obj.xls_theme_id.bg_color or 'black'
            pattern.pattern_fore_colour = xlwt.Style.colour_map[bg_color]
            font.height = int(current_obj.xls_theme_id.font_size)
            font.bold = current_obj.xls_theme_id.font_bold
            font.italic = current_obj.xls_theme_id.font_italic
            font_color = current_obj.xls_theme_id.font_color or 'white'
            font.colour_index = xlwt.Style.colour_map[font_color]
            header_style.pattern = pattern
            header_style.font = font
            al3 = Alignment()
            al3.horz = current_obj.xls_theme_id.header_alignment or 0x02
            header_style.alignment = al3


            column_header_style = xlwt.XFStyle()
            font = xlwt.Font()
            pattern = xlwt.Pattern()
            pattern.pattern = xlwt.Pattern.SOLID_PATTERN
            bg_color = current_obj.xls_theme_id.column_bg_color or 'red'
            pattern.pattern_fore_colour = xlwt.Style.colour_map[bg_color]
            font.height = int(current_obj.xls_theme_id.column_font_size)
            font.bold = current_obj.xls_theme_id.column_font_bold
            font.italic = current_obj.xls_theme_id.column_font_italic
            font_color = current_obj.xls_theme_id.column_font_color or 'white'
            font.colour_index = xlwt.Style.colour_map[font_color]
            column_header_style.pattern = pattern
            column_header_style.font = font
            al3 = Alignment()
            al3.horz = current_obj.xls_theme_id.column_header_alignment
            column_header_style.alignment = al3

            #Style for amount
            column_header_style_amount = xlwt.XFStyle()
            column_header_style_amount.pattern = pattern
            column_header_style_amount.font = font
            al3_amount = Alignment()
            al3_amount.horz = 3
            column_header_style_amount.alignment = al3_amount


            body_header_style = xlwt.XFStyle()
            font = xlwt.Font()
            pattern = xlwt.Pattern()
            pattern.pattern = xlwt.Pattern.SOLID_PATTERN
            bg_color = current_obj.xls_theme_id.body_bg_color or 'gold'
            pattern.pattern_fore_colour = xlwt.Style.colour_map[bg_color]
            font.height = int(current_obj.xls_theme_id.body_font_size)
            font.bold = current_obj.xls_theme_id.body_font_bold
            font.italic = current_obj.xls_theme_id.body_font_italic
            font_color = current_obj.xls_theme_id.body_font_color or 'white'
            font.colour_index = xlwt.Style.colour_map[font_color]
            body_header_style.pattern = pattern
            body_header_style.font = font
            al3 = Alignment()
            al3.horz = current_obj.xls_theme_id.body_header_alignment
            body_header_style.alignment = al3

            # Style for amount
            body_header_style_amount = xlwt.XFStyle()
            body_header_style_amount.pattern = pattern
            body_header_style_amount.font = font
            al3_amount = Alignment()
            al3_amount.horz = 3
            body_header_style_amount.alignment = al3_amount
            

            date_from = self.date_from
            date_to = self.date_to
 
            final_arr_data = {}

            filename = self.account_report_id.name+'.xls'
            worksheet = wb.add_sheet(current_obj.account_report_id.name + ".xls")
            for i in range(1,10):
              column = worksheet.col(i)
              column.width = 235 * 30
              column = worksheet.col(0)
              column.width = 350 * 30
            if not current_obj.debit_credit and not current_obj.enable_filter: 
                worksheet.write_merge(0,1,0,1, current_obj.account_report_id.name, header_style)
                
                if date_from:
                        worksheet.write(6, 0,"Date from:" , column_header_style)
                        worksheet.write(6, 1,date_from , column_header_style)
                if date_to:
                        worksheet.write(7, 0,"Date to:" , column_header_style)
                        worksheet.write(7, 1,date_to , column_header_style)
 
                worksheet.write(3, 0, "Target Moves:", column_header_style)
                worksheet.write(4, 0, target_move , body_header_style)

                worksheet.write(11, 0, "Name", column_header_style)
                worksheet.write(11, 1, "Balance", column_header_style)
                
                i=11
                for data in account_res:
    
                    if data['level'] != 0: 
                        if data['account_type'] == 'account_type' or data['account_type'] == 'sum':
                            worksheet.write(i+1, 0, data['name'], column_header_style)
                            worksheet.write(i+1, 1, data['balance'], column_header_style_amount)
                        else:
                            worksheet.write(i+1, 0, data['name'], body_header_style)
                            worksheet.write(i+1, 1, data['balance'], body_header_style_amount)
                  #  else:
                  #      worksheet.write(i+1, 0, data['name'], body_header_style)
                  #      worksheet.write(i+1, 1, data['balance'], body_header_style)
                    i+=1
    
            elif current_obj.enable_filter:  
                worksheet.write_merge(0,1,0,1, current_obj.account_report_id.name, header_style)
                
                if date_from:
                    worksheet.write(6, 0,"Date from:" , column_header_style)
                    worksheet.write(6, 1,date_from , column_header_style)
                if date_to:
                    worksheet.write(7, 0,"Date to:" , column_header_style)
                    worksheet.write(7, 1,date_to , column_header_style)
                if current_obj.filter_cmp == 'filter_date':
                    if current_obj.date_from_cmp:
                        worksheet.write(8, 0,"Compared Date from:" , column_header_style)
                        worksheet.write(8, 1,current_obj.date_from_cmp , column_header_style)
                    if current_obj.date_to_cmp:
                        worksheet.write(9, 0,"Compared Date to:" , column_header_style)
                        worksheet.write(9, 1,current_obj.date_to_cmp , column_header_style)
        
                worksheet.write(3, 0, "Target Moves:", column_header_style)
                worksheet.write(4, 0, target_move , body_header_style)
                worksheet.write(11, 0, "Name", column_header_style)
                worksheet.write(11, 1, "Balance", column_header_style)
                worksheet.write(11, 2, current_obj.label_filter, column_header_style)
                
                i=11
                for data in account_res:
                    print data
                    if data['level'] !=0: 
                        if data['account_type'] == 'account_type' or data['account_type'] == 'sum':
                            worksheet.write(i+1, 0, data['name'], column_header_style)
                            worksheet.write(i+1, 1, data['balance'], column_header_style_amount)
                            worksheet.write(i+1, 2, data['balance_cmp'], column_header_style_amount)
                        else:
                            worksheet.write(i+1, 0, data['name'], body_header_style)
                            worksheet.write(i+1, 1, data['balance'], body_header_style_amount)
                            worksheet.write(i+1, 2, data['balance_cmp'], body_header_style_amount)
            
                    i+=1
            else : 
                worksheet.write_merge(0,1,0,1, current_obj.account_report_id.name, header_style)
                
                if date_from:
                    worksheet.write(6, 0,"Date from:" , column_header_style)
                    worksheet.write(6, 1,date_from , column_header_style)
                if date_to:
                    worksheet.write(7, 0,"Date to:" , column_header_style)
                    worksheet.write(7, 1,date_to , column_header_style)
                worksheet.write(3, 0, "Target Moves:", column_header_style)
                worksheet.write(4, 0, target_move , body_header_style)
                worksheet.write(11, 0, "Name", column_header_style)
                worksheet.write(11, 1, "Debit", column_header_style)
                worksheet.write(11, 2, "Credit", column_header_style)
                worksheet.write(11, 3, "Balance", column_header_style)
                i=11
                for data in account_res:
                    if data['level'] !=0 : 
                        worksheet.write(i+1, 0, data['name'], body_header_style)
                        worksheet.write(i+1, 1, data['debit'] , body_header_style)
                        worksheet.write(i+1, 2, data['credit'] , body_header_style)
                        worksheet.write(i+1, 3, data['balance'] , body_header_style_amount)

                  
                    i+=1
            wb.save(fp)
            out = base64.encodestring(fp.getvalue())
            final_arr_data = {}
            final_arr_data['file_stream'] = out
            final_arr_data['name'] = filename
    
            create_id = self.env['account.report.view'].create(final_arr_data)
            
            return {
                'nodestroy': True,
                'res_id': create_id.id,
                'name': filename,
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'account.report.view',
                'view_id': False,
                'type': 'ir.actions.act_window',
            }
    
    @api.multi
    def check_report(self):
        res = super(AccountingReport, self).check_report()
        data = {}
        data['form'] = self.read(['product_categ_id','kg_location','analytic_account_ids','account_report_id', 'date_from_cmp', 'date_to_cmp', 'journal_ids', 'filter_cmp', 'target_move'])[0]
        for field in ['account_report_id']:
            if isinstance(data['form'][field], tuple):
                data['form'][field] = data['form'][field][0]
        comparison_context = self._build_comparison_context(data)
        res['data']['form']['comparison_context'] = comparison_context
        return res

    def _print_report(self, data):
        data['form'].update(self.read(['product_categ_id','kg_location','analytic_account_ids','date_from_cmp', 'debit_credit', 'date_to_cmp', 'filter_cmp', 'account_report_id', 'enable_filter', 'label_filter', 'target_move'])[0])
        print data,'datadatadatadatadatadatadatadatadatadatadata'
        return self.env['report'].get_action(self, 'account.report_financial', data=data)
    
    
    