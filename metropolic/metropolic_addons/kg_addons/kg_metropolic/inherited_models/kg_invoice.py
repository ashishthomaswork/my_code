# -*- coding: utf-8 -*-
from odoo import models,api,fields,_

# -*- coding: utf-8 -*-
from odoo import models,api,fields
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT, float_compare
from odoo.exceptions import UserError

class AccountInvoice(models.Model):
    
    _inherit="account.invoice"
    
    total_local = fields.Float('Total(Local Value)',compute="convert_curr",store=True)
    due_local = fields.Float('Amount Due(Local Value)',compute="convert_curr",store=True)
    kg_do_id  = fields.Many2one('stock.picking', string='Delivery Note')
  
    @api.depends('amount_total','currency_id','residual')
    def convert_curr(self):
        for record in self:
            print record.env.context,'111111111',record.env.user.company_id.name
            curr_company=  record.env.user.company_id
            
            print curr_company.currency_id.name,'curreeeeeeeeee'
            total=0.0
            due=0.0
            rec_rate= record.currency_id.rate
            if rec_rate ==0:
                rec_rate=1
            if rec_rate < 1.0:
                total=record.amount_total/rec_rate
                due = record.residual/rec_rate
            else:
                total=record.amount_total*rec_rate
                due = record.residual*rec_rate
            record.total_local = total or 0.0
            record.due_local = due or 0.0