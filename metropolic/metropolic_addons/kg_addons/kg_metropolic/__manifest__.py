# -*- coding: utf-8 -*-
{
    "name" : "KG Metropolic",
    "version" : "0.1",
    "author": "KG",
    "category" : "Others",
    "description": """KG Metropolic """,
    "depends": ['account_accountant', 'sale', 'purchase', 'project', 'mrp', 'stock', 'hr', 'crm', 'mail', 'document', 'partner_credit_limit', 'maintenance', 'hr_employee_enhancement_riy','order_line_serial_number','analytic','fleet'],
    "data" : [
         'data/sales_forecast_code.xml',
          'data/prod_plan_code.xml',
          'security/ir.model.access.csv',
          
          'data/product_category.xml',
			'data/hr_emp_id_seq.xml',
             'inherited_views/kg_invoice_view.xml',
			'wizard/salarysheet_wizard_view.xml',
			'wizard/raw_po_wizard_view.xml',
        'wizard/mat_req_wizard_view.xml',
        'wizard/delivery_wizard_view.xml',
                'wizard/prod_plan_view.xml',
              'inherited_views/kg_po_view.xml',
               'inherited_views/kg_so_view.xml',
                   'inherited_views/kg_partner_view.xml',
                   'inherited_views/product_view.xml',

                   'inherited_views/fleet.xml',
                   'inherited_views/mrp_view.xml',
                     'inherited_views/journal_entry_view.xml',
                 
                     'inherited_views/profit_loss_report_view.xml',
'inherited_views/hr_payroll_view.xml',
                   'views/maintenance_request_view.xml',
                   'inherited_views/payroll_transaction_view.xml',

                    'inherited_views/hr_contract_view.xml',
                           'inherited_views/kg_do_view.xml',

					'views/purchase_requisition_view.xml',

                   'views/shift_master.xml',
                   
                   'views/sales_forecast_view.xml',
                   'views/sales_target_view.xml',
					'views/kg_mat_req_view.xml',
                     'views/bank_view.xml',
                     'views/faculty_view.xml',
                        'views/offer_view.xml',
                        'views/spare_main_view.xml',
                        
                           'views/prod_plan_view.xml',
                        
                           'inherited_views/menu_view.xml',
                      'data/sales_team_data.xml',
                       'data/crm_stages_data.xml',
                       
                       'menu_views/kg_leave_views.xml',
                       'menu_views/kg_hr_enh_views.xml',
                       
                       'report/report_view.xml',
                       'report/report_prodplan.xml',
                       # 'report/report_po.xml',

        'inherited_views/kg_prod_conf_view.xml',
                      
            ],
    'css': [],
    'installable': True,
    'auto_install': False,
    'application': True,
}
