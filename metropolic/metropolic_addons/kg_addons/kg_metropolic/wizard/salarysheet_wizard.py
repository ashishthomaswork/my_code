from odoo import models, fields, api
from odoo.tools.translate import _
from odoo import SUPERUSER_ID
from odoo import models, fields, api
from odoo import exceptions, _
from odoo.exceptions import Warning
import datetime as dt
from datetime import  timedelta, tzinfo, time, date, datetime
from dateutil.relativedelta import relativedelta 
from odoo import models, fields, api
from odoo.tools.translate import _
from odoo.exceptions import Warning
import xlwt
from odoo.exceptions import UserError

from xlsxwriter.workbook import Workbook
#from cStringIO import StringIO
from io import StringIO
import io
import base64
from odoo.exceptions import UserError

class KGSalarysheetWizard(models.TransientModel):
    _name = 'kg.salarysheet.wizard'
    _description = 'kg.salarysheet.wizard'
    
    @api.one   
    def get_month_day_range(self,date):
		date = datetime.strptime(date, '%Y-%m-%d')
		last_day = date + relativedelta(day=1, months=+1, days=-1)
		first_day = date + relativedelta(day=1)
		return first_day, last_day    
    @api.multi
    @api.onchange('date_from')
    def date_from(self):
        if self.date_from:
            start_dt = datetime.strptime(self.date_from, "%Y-%m-%d")
    
        
            self.date_to= str(self.get_month_day_range(start_dt)[0][1])[:10]
        return {}

    @api.one
    def compute_date_diff(self,ds,dt):
        from datetime import datetime
        d1 = datetime.strptime(ds, "%Y-%m-%d")
        d2 = datetime.strptime(dt, "%Y-%m-%d")
        days = (d2 - d1).days+1
        if days < 0:
            days = 0
        return days    
    
#    @api.multi
#    def generate_data(self):
#        payslip_batch_id = self.payslip_batch_id and self.payslip_batch_id.id
#        payslip_obj = False

#        if not payslip_batch_id:
#            date_from = self.date_from
#            start = date_from[:7]       
#            end = self.date_to[:7]        
#            if start !=end:
#                raise UserError(_('select same month dates'))       
#            date_to = self.date_to
#            o_date_to = str(datetime.strptime(date_to, "%Y-%m-%d"))[:10]      
#            o_date_from = str(datetime.strptime(date_from, "%Y-%m-%d"))[:10]         
#            
#            
#            last_day_of_month = str(self.get_month_day_range(o_date_to)[0][1])[:10]
#            first_day_of_month = str(self.get_month_day_range(o_date_from)[0][0])[:10]      
#            if first_day_of_month != date_from or last_day_of_month != date_to:         
#                raise UserError(_('set proper dates'))          
#        
#            payslip_obj = self.env['hr.payslip'].search([('date_from','>=',date_from),('date_to','<=',date_to)])        
#        
#        else:        
#            payslip_obj = self.payslip_batch_id.slip_ids
#            
#            
#            
#        array = []
#        
#        for pay in payslip_obj:
#            employee_obj = pay.employee_id
#            bank_account_name = employee_obj.bank_account_id and employee_obj.bank_account_id.acc_number
#            kg_other_id = employee_obj.kg_other_id
#            kg_wps_routing_code = employee_obj.kg_wps_routing_code
#            date_from = pay.date_from
#            date_to = pay.date_to
#            line_ids = pay.line_ids
#            worked_days_line_ids = pay.worked_days_line_ids
#            variable_sal = 0
#            fixed_sal = 0
#            days_leave = 0
#            for w_line in worked_days_line_ids:
#                if w_line.code == 'Unpaid':
#                    days_leave = days_leave + w_line.number_of_days
#                
#            worked_days = pay.od_month_days
#            for line in line_ids:
#                if line and line.salary_rule_id and line.salary_rule_id.code == 'NET':
#                    fixed_sal = fixed_sal + line.total
#            vals = {'account_name':bank_account_name,'id':kg_other_id,
#            'wps_code':kg_wps_routing_code,'date_from':date_from,
#            'date_to':date_to,'worked_days':worked_days,'variable_sal':variable_sal,
#            'days_leave':days_leave,'fixed_sal':fixed_sal,'edr':'EDR'}
#            array.append(vals)
#            
#            
#                
#                
#            

#        return array    


    def get_employee_ids(self):

		employee_ids = []

		employee_ids_as_per_serach = []
		kg_visa_company_id = self.kg_visa_company_id and self.kg_visa_company_id.id
		kg_working_company_id = self.kg_working_company_id and self.kg_working_company_id.id
		if kg_working_company_id and not kg_visa_company_id:
			employee_ids = self.env['hr.employee'].search([('kg_working_company_id','=',kg_working_company_id)])
			
		if not kg_working_company_id and kg_visa_company_id:
			employee_ids = self.env['hr.employee'].search([('kg_visa_company_id','=',kg_visa_company_id)])

		if not kg_working_company_id and not kg_visa_company_id:
			employee_ids = self.env['hr.employee'].search([])

		if kg_working_company_id and kg_visa_company_id:
			employee_ids = self.env['hr.employee'].search([('kg_visa_company_id','=',kg_visa_company_id),('kg_working_company_id','=',kg_working_company_id)])
		for emp in employee_ids:
			employee_ids_as_per_serach.append(emp and emp.id)

		if len(employee_ids_as_per_serach) == 0:
			raise UserError(
                    _("no employees found for the particular search condition"),
                )
		print "vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv",len(employee_ids_as_per_serach)			
		return employee_ids_as_per_serach

    def get_payslip_ids(self,employee_ids):
		print "vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvemployee_ids",employee_ids
		date_from = self.date_from
		print "ffffffffffffffffffffffffffffffdate_from",date_from

		payslip_ids = []
		date_to = self.date_to

		dummy = self.env['hr.payslip'].search([('date_from','>=',date_from),('date_to','<=',date_to)])	
		print "vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv88888dummy",len(dummy)	
		in_payslip = []
		not_in_payslip = []

		for d in dummy:
			if d.employee_id and d.employee_id.id in employee_ids:
				in_payslip.append(d.employee_id and d.employee_id.id)

			else:
				not_in_payslip.append(d.employee_id and d.employee_id.id)
		print "vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv88888in_payslip",in_payslip	
		print "vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv88888not_payslip",not_in_payslip


		print "vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv88888in_payslip",len(in_payslip)
		print "vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv88888not_payslip",len(not_in_payslip)		
			
		payslip_obj = self.env['hr.payslip'].search([('date_from','>=',date_from),('date_to','<=',date_to),('employee_id','in',employee_ids)])	
		print "ffffffffffffffffffffffffffffffdate_to",date_to
		print "hhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh",len(payslip_obj)
		for pay in payslip_obj:
			payslip_ids.append(pay.id)
		if len(payslip_ids) == 0:
			raise UserError(
                    _("no payslip found for this search condition"),
                )			



	
			
		return payslip_obj



    def get_main_data(self,payslip_ids):








        array = []









        sl_no = 0
        for pay in payslip_ids:
			sl_no = sl_no + 1
			emp_id = pay.employee_id and pay.employee_id.identification_id
			name = pay.employee_id and pay.employee_id.name
			designation = pay.employee_id and pay.employee_id.job_id and pay.employee_id.job_id.name
			department = pay.employee_id and pay.employee_id.department_id and pay.employee_id.department_id.name 
			working_under = pay.employee_id and pay.employee_id.kg_working_company_id and pay.employee_id.kg_working_company_id.id 
			visa = pay.employee_id and pay.employee_id.kg_visa_company_id and pay.employee_id.kg_visa_company_id.id 



			basic = pay.contract_id and pay.contract_id.wage
			line_ids = pay.line_ids
			mobile_allowance = 0

			gross_salary = 0
########### grosalary fromulae sum of Basic + Other allowances




				
			for line in line_ids:

				if line.code == 'MA':
					mobile_allowance = mobile_allowance + line.od_monthly_salary
#				if line.code == 'GROSS':
#					gross_salary = gross_salary + line.total

#				if line.code == 'NET':
#					net_salary = net_salary + line.total					  

			daysinmonth = pay.number_of_days_period
			worked_days = pay.net_worked_days

			leave_days = pay.total_no_of_unpaid_leaves	
		

			print "dddddddddddddddddddddddddddddddddddddddddddworked_days",worked_days

#			leave_deductions = pay.kg_one_day_basic * leave_days	

			other_deductions = pay.other_deductions ##other dedcution means the deductions other than leave deductions

# 			overtime_hrs = pay.kg_total_ot_hours
# 
# 			kg_total_ot_amt = pay.kg_total_ot_amt
# 			overtime_rate = 0
# 
# 
# 			if not overtime_hrs:
# 
# 				overtime_rate = 0
# 
# 			else:
# 
# 				overtime_rate = float(kg_total_ot_amt) / float(overtime_hrs)
# 
# 			if not overtime_rate:
# 
# 				overtime_hrs = 0


#####It should be a sum of all other allowances apart from basic. like phone, leave salary, air ticket or incentives. 
				
			kg_other_allowance = pay.other_allowances   ##sum of allowances including incentives mobile etc.....ot not included




			if not daysinmonth:
				raise UserError(
                    _("Days between two dates is found to be zero"),
                )
			print "cccccccccccccccccccccccccccc",pay.contract_id and pay.contract_id.id							
#################################According to rajni'sformulae leave deductions
			contract_id = pay.contract_id and pay.contract_id.id

			contract_obj = self.env['hr.contract'].browse(contract_id)

# 			gross_fixed_salary = contract_obj.od_total_wage or 0.0
# 			oneday_gross_salary = float(gross_fixed_salary) / float(daysinmonth)
# 			leave_deductions = oneday_gross_salary * leave_days



###############################



			
			

# 			net_salary = basic + kg_other_allowance + pay.kg_total_ot_amt - pay.kg_total_deductions - leave_deductions



			kg_mode_of_payment = 'wps'
			kg_remarks = pay.remarks
			vals = {'employee_id':pay.employee_id and pay.employee_id.id,'sl_no':sl_no,'emp_id':emp_id,
            'name':name,'designation':designation,
            'department':department,'working_under':working_under,'visa':visa,
           'basic':basic,'mobile_allowance':pay.telephone_mob_allowance,
'daysinmonth':daysinmonth,'worked_days':worked_days,'leave_days':leave_days,
'leave_deductions':pay.leave_deductions,'other_deductions':other_deductions,'kg_total_ot_amt':pay.ot_allowance,
'kg_other_allowance':pay.other_allowances,'kg_mode_of_payment':kg_mode_of_payment,'food_allowance':pay.food_allowance,'salary_advance':pay.salary_advance,'fine':pay.fine,
'kg_remarks':kg_remarks,'net_salary':pay.net,'gross_salary':pay.gross,'payslip_obj':pay}
			array.append(vals)
		

        return array
		
			


    def excel_main(self):

        kg_type = self.kg_type

       # raise UserError(_('first set leave and eligibility and carryforward settings'))
		

        if kg_type == 'payroll':
            self.generate_payroll_report()

        else:
            self.generate_wps_report()

        return {
              'view_type': 'form',
              "view_mode": 'form',
              'res_model': 'kg.salarysheet.wizard',
              'res_id': self.id,
              'type': 'ir.actions.act_window',
              'target': 'new'
              }	

    def generate_wps_report(self):

		now = datetime.now()

		time = str(now).split(' ')

		time_split = str(time[1]).split(':')
		mergetime = str(time_split[0]) + str(time_split[1])      
        


		now = str(datetime.strptime(str(now)[:10], "%Y-%m-%d"))[:10]

		mm = now.split('-')[1]

		yy = now.split('-')[0]

		mmyy = mm + yy

		wizard_date_from = self.date_from
		wizard_date_to = self.date_to
		kg_visa_company_id = self.kg_visa_company_id and self.kg_visa_company_id.name
		kg_visa_routing_code = self.kg_visa_company_id and self.kg_visa_company_id.employer_routing_code

		company_labour_id = self.kg_visa_company_id and self.kg_visa_company_id.labour_id
		visa_company_account_no = self.kg_visa_company_id and self.kg_visa_company_id.account_no

		employee_ids = self.get_employee_ids()	
		payslip_ids = self.get_payslip_ids(employee_ids)
		main_data = self.get_main_data(payslip_ids)

		workbook= xlwt.Workbook(encoding="UTF-8")
		filename= 'WPS.xls'
		sheet= workbook.add_sheet('WPS',cell_overwrite_ok=True)
		style = xlwt.easyxf('font:height 400, bold True, name Arial; align: horiz center, vert center;borders: top medium,right medium,bottom medium,left medium')
		header = ['Sl No.','Mode','Labour ID','Routing code','IBAN','Pay Start date','Pay end date','Days in Month','Variable','Leave days','Net Pay']
		style2 = xlwt.easyxf('font: bold 1')
		for index,data in enumerate(header):
			sheet.write(0,index,data,style2)


		index = 1
		col = 0
		total_sal = 0

		no_employees = 0
		for vals in main_data:
			sl_no = vals.get('sl_no') or ''


			employee_id = vals.get('employee_id') 
			employee_obj = self.env['hr.employee'].browse(employee_id)
			kg_wps_routing_code = employee_obj.kg_wps_routing_code or ''

			account_no = employee_obj.bank_account_id and employee_obj.bank_account_id.acc_number or ''
			payslip_obj = vals.get('payslip_obj')

			xpayslip_id = payslip_obj and payslip_obj.id

			variable_lines = self.env['od.hr.variance.line'].search([('payslip_id','=',xpayslip_id)])
			
			xvariable_pay = 0             
			for line in variable_lines:
				if line.rule_id and line.rule_id.category_id and line.rule_id.category_id.id in [2,9]:
					xvariable_pay = xvariable_pay + line.amount
					

			date_from = payslip_obj.date_from or ''
			date_to = payslip_obj.date_to or ''
			emp_id = vals.get('emp_id') or ''
			name = vals.get('name') or ''
			designation = vals.get('designation') or ''
			department = vals.get('department') or ''
			working_under = vals.get('working_under') or ''
			if working_under:
				working_company_obj = self.env['kg.working.company'].browse(working_under)

				working_under = working_company_obj.name
			visa = vals.get('visa') or ''

			if visa:
				visa_company_obj = self.env['kg.visa.company'].browse(visa)

				visa = visa_company_obj.name
			pay_head = vals.get('pay_head') or ''
			basic =round (vals.get('basic') or 0.0,2)
			net =round (vals.get('net_salary') or 0.0,2)

			mobile_allowance = round (vals.get('mobile_allowance') or 0.0,2)
			gross_salary = round (vals.get('gross_salary') or 0.0,2)
			daysinmonth = round (vals.get('daysinmonth') or 0.0,2)
			worked_days = round (vals.get('worked_days') or 0.0,2)
			leave_days = round (vals.get('leave_days') or 0.0,2)
			leave_deductions = round (vals.get('leave_deductions') or 0.0,2)
			other_deductions = round (vals.get('other_deductions') or 0.0,2)
			overtime_hrs = round (vals.get('overtime_hrs') or 0.0,2)
			overtime_rate = round (vals.get('overtime_rate') or 0.0,2)
			kg_total_ot_amt = round (vals.get('kg_total_ot_amt') or 0.0,2)
			kg_other_allowance = round (vals.get('kg_other_allowance') or 0.0,2)
			net_salary = round (vals.get('net_salary') or 0.0,2)
			kg_mode_of_payment = vals.get('kg_mode_of_payment') or ''
			kg_remarks = vals.get('kg_remarks') or ''
            		food_allowance = vals.get('food_allowance') or 0.0
			salary_advance = vals.get('salary_advance') or 0.0
			fine = vals.get('fine') or 0.0



			fixed_pay = net

			variable = kg_total_ot_amt+kg_other_allowance+food_allowance-(leave_deductions + other_deductions+salary_advance+fine)
			if employee_obj.id == 204:
				print "vvvvvvvvvvvvvvvvvvvvvvvariable",variable
				print "ffffffffffffffffffffffvariable_lines",variable_lines



			if kg_mode_of_payment == 'wps':
				total_sal = total_sal + fixed_pay + variable

				no_employees = no_employees + 1
						

				edr = 'EDR'
				sheet.write(index,col+1,edr)




				sheet.write(index,col+2,emp_id)
				sheet.write(index,col+3,kg_wps_routing_code)
				sheet.write(index,col+4,account_no)
				sheet.write(index,col+5,date_from)
				sheet.write(index,col+6,date_to)
				sheet.write(index,col+7,daysinmonth)
				#sheet.write(index,col+8,basic)
				sheet.write(index,col+8,variable)
				sheet.write(index,col+9,leave_days)
                		sheet.write(index,col+10,fixed_pay)
                
				index = index+1	

        
			sheet.write(index,col,'')         
			sheet.write(index,col+1,'SCR')       
			sheet.write(index,col+2,company_labour_id)       
			sheet.write(index,col+3,kg_visa_routing_code)         
			sheet.write(index,col+4,now)
			sheet.write(index,col+5,mergetime)        
			sheet.write(index,col+6,mmyy) 
			days = self.compute_date_diff(wizard_date_from,wizard_date_to)[0] 

			sheet.write(index,col+7,no_employees) 
			
			
            
                        sheet.write(index,col+10,total_sal,style2)
                        sheet.write(index,col+11,'AED',style2)







		fp = io.BytesIO()
		workbook.save(fp)
		excel_file = base64.encodestring(fp.getvalue())
		self.write({'excel_file':excel_file,'file_name':filename})
		fp.close()
		
        

    def generate_payroll_report(self):
		workbook= xlwt.Workbook(encoding="UTF-8")
		filename= 'PayrollReport.xls'
		employee_ids = self.get_employee_ids()
		payslip_ids = self.get_payslip_ids(employee_ids)
		main_data = self.get_main_data(payslip_ids)
		sheet= workbook.add_sheet('Salarysheet',cell_overwrite_ok=True)
		style = xlwt.easyxf('font:height 400, bold True, name Arial; align: horiz center, vert center;borders: top medium,right medium,bottom medium,left medium')
		header = ['Sl No.','Emp ID','Name','Designation','Department','Working Under','Visa','Pay Head','Salary Basic','Mobile allowance','Gross Salary','Days In  Month','Days Worked','Leave Days ','Leave Deductions ','Other Deductions','Over time in Hrs.','Over Time Rate','Over time','Other Allowance','Net Salary','Mode of Payment','Remarks']
		style2 = xlwt.easyxf('font: bold 1')
		for index,data in enumerate(header):
			sheet.write(0,index,data,style2)
		index = 1
		col = 0
		for vals in main_data:
			sl_no = vals.get('sl_no') or ''
			emp_id = vals.get('emp_id') or ''
			name = vals.get('name') or ''
			designation = vals.get('designation') or ''
			department = vals.get('department') or ''




			working_under = vals.get('working_under') or ''
			visa = vals.get('visa') or ''

			if working_under:
				working_company_obj = self.env['kg.working.company'].browse(working_under)

				working_under = working_company_obj.name
			visa = vals.get('visa') or ''

			if visa:
				visa_company_obj = self.env['kg.visa.company'].browse(visa)

				visa = visa_company_obj.name
			pay_head = vals.get('pay_head') or ''
			basic = round (vals.get('basic') or 0.0,2)
			mobile_allowance = round (vals.get('mobile_allowance') or 0.0,2)
			gross_salary = round (vals.get('gross_salary') or 0.0,2)
			daysinmonth = round (vals.get('daysinmonth') or 0.0,2)
			worked_days = round (vals.get('worked_days') or 0.0,2)
			leave_days = round (vals.get('leave_days') or 0.0,2)
			leave_deductions = round (vals.get('leave_deductions') or 0.0,2)
			other_deductions = round (vals.get('other_deductions') or 0.0,2)
			overtime_hrs = round (vals.get('overtime_hrs') or 0.0,2)
			overtime_rate = round (vals.get('overtime_rate') or 0.0,2)
			kg_total_ot_amt = round (vals.get('kg_total_ot_amt') or 0.0,2)
			kg_other_allowance = round (vals.get('kg_other_allowance') or 0.0,2)
			net_salary = round (vals.get('net_salary') or 0.0,2)
			kg_mode_of_payment = vals.get('kg_mode_of_payment') or ''
			kg_remarks = vals.get('kg_remarks') or ''
			sheet.write(index,col,sl_no)
			sheet.write(index,col+1,emp_id)
			sheet.write(index,col+2,name)
			sheet.write(index,col+3,designation)
			sheet.write(index,col+4,department)
			sheet.write(index,col+5,working_under)
			sheet.write(index,col+6,visa)
			sheet.write(index,col+7,pay_head)
			sheet.write(index,col+8,basic)
			sheet.write(index,col+9,mobile_allowance)
			sheet.write(index,col+10,gross_salary)
			sheet.write(index,col+11,daysinmonth)
			sheet.write(index,col+12,worked_days)
			sheet.write(index,col+13,leave_days)
			sheet.write(index,col+14,leave_deductions)
			sheet.write(index,col+15,other_deductions)
			sheet.write(index,col+16,overtime_hrs)
			sheet.write(index,col+17,overtime_rate)
			sheet.write(index,col+18,kg_total_ot_amt)
			sheet.write(index,col+19,kg_other_allowance)
			sheet.write(index,col+20,net_salary)
			sheet.write(index,col+21,kg_mode_of_payment)
			sheet.write(index,col+22,kg_remarks)
			index = index+1	

		fp = io.BytesIO()
		workbook.save(fp)
		excel_file = base64.encodestring(fp.getvalue())
		self.write({'excel_file':excel_file,'file_name':filename})
		fp.close()
		
        


#    def print_excel_report(self):
#        workbook= xlwt.Workbook(encoding="UTF-8")

#        filename= 'Salarysheet.xls'

#        sheet= workbook.add_sheet('Salarysheet',cell_overwrite_ok=True)
#        result = self.generate_data()
#            

#            

#        style = xlwt.easyxf('font:height 400, bold True, name Arial; align: horiz center, vert center;borders: top medium,right medium,bottom medium,left medium')

#        header = ['Mode','Personal Number','WPS Routing Code','Account Number','Payment Start Date','Payment End Date','Total No. of days worked','Fixed Salary','Variable Salary','Leaves']
#        style2 = xlwt.easyxf('font: bold 1')
#        for index,data in enumerate(header):
#            sheet.write(0,index,data,style2)
#        index = 1    
#        col = 0
#        no_of_employees = 0
#        total_fixed_sal = 0
#        for vals in result:
#            no_of_employees = no_of_employees + 1
#            account_name = vals.get('account_name') or ''
#            id = vals.get('id') or ''
#            wps_code = vals.get('wps_code') or ''
#            date_from = vals.get('date_from') or False
#            date_to = vals.get('date_to') or ''
#            worked_days = vals.get('worked_days')
#            variable_sal = vals.get('variable_sal')
#            days_leave = vals.get('days_leave')
#            fixed_sal = vals.get('fixed_sal')
#            total_fixed_sal = total_fixed_sal + fixed_sal
#            edr = vals.get('edr')
#            sheet.write(index,col,edr)
#            sheet.write(index,col+1,id)
#            sheet.write(index,col+2,wps_code)
#            sheet.write(index,col+3,account_name)
#            sheet.write(index,col+4,date_from)
#            sheet.write(index,col+5,date_to)
#            sheet.write(index,col+6,worked_days)
#            sheet.write(index,col+7,fixed_sal)
#            sheet.write(index,col+8,variable_sal)
#            sheet.write(index,col+9,days_leave)
#            index = index+1		
#        new = index
#        
#        
#        
#        sheet.write(index,col,'SCR')
#        sheet.write(index,col+1,'230055')

#        sheet.write(index,col+5,'TOTAL',style2)
#        sheet.write(index,col+6,no_of_employees,style2)
#        sheet.write(index,col+7,total_fixed_sal,style2)
#        
#        fp = io.BytesIO()
#        workbook.save(fp)
#        excel_file = base64.encodestring(fp.getvalue())
#        self.write({'excel_file':excel_file,'file_name':filename})
#        fp.close()
#		
#        

#        return {
#              'view_type': 'form',
#              "view_mode": 'form',
#              'res_model': 'kg.salarysheet.wizard',
#              'res_id': self.id,
#              'type': 'ir.actions.act_window',
#              'target': 'new'
#              }
#    

    date_from = fields.Date(string="Date Start")
    date_to = fields.Date(string="Date End")
    excel_file = fields.Binary('Dowload Report Excel')
    file_name = fields.Char('Excel File',) 
    kg_visa_company_id = fields.Many2one('kg.visa.company',string="Visa company")
    kg_working_company_id = fields.Many2one('kg.working.company',string="Working company") 
    kg_type = fields.Selection([
        ('wps', 'WPS'),
        ('payroll', 'Payroll'),
        ], string=' Report Type',default='wps',) 

    
