from itertools import groupby
from datetime import datetime, timedelta,date

from odoo import api, fields, models, _
from odoo.exceptions import UserError
from odoo.tools import float_is_zero, float_compare, DEFAULT_SERVER_DATETIME_FORMAT
from odoo.tools.misc import formatLang

import odoo.addons.decimal_precision as dp


class kg_mat_req_wizard(models.TransientModel):
    _name = 'kg.mat.req.wizard'

    date_from = fields.Date('Date From')

    date_to = fields.Date('Date To')


    @api.onchange('date_from')
    def onchange_from(self):
        for record in self:
            if record.date_from:
                plan_id = record._context.get('active_id')

                plan_obj = self.env['kg.prod.plan'].browse(plan_id)

                if record.date_from < plan_obj.kg_start_date:
                    raise UserError(_('From Date Should be Within Range'))

    @api.onchange('date_to')
    def onchange_to(self):
        for record in self:
            if record.date_to:
                plan_id = record._context.get('active_id')

                plan_obj = self.env['kg.prod.plan'].browse(plan_id)

                if record.date_to > plan_obj.kg_end_date:
                    raise UserError(_('To Date Should be Within Range'))



#     @api.multi
#     def generate_mat(self):
#         for record in self:
#             plan_id = record._context.get('active_id')
# 
#             plan_obj = self.env['kg.prod.plan'].browse(plan_id)
#             mat_req_id = self.env['kg.mat.req'].create({'kg_availability_date':fields.Date.today()})
#             for line in plan_obj.kg_shift_lines:
#                 if line.kg_date >= record.date_from and line.kg_date <=record.date_to:
#                     print 'inside loop'
#                     qty= line.kg_qty
#                     print line.kg_product
#                     bom_obj =self.env['mrp.bom'].search([('product_tmpl_id','=',line.kg_product.product_tmpl_id.id)])
#                     if not bom_obj:
#                         raise UserError(_('Create BOM for product: %s') % line.kg_product.name)
# 
#                     print bom_obj,'111111111'
#                     for bom_line in bom_obj.bom_line_ids:
# 
#                         check=self.env['kg.mat.req.lines'].search([('kg_mat_req_id','=',mat_req_id.id),('kg_product','=',bom_line.product_id.id)])
#                         print check,'1111111111111'
#                         if not check:
#                             vals={}
#                             vals['kg_product']=bom_line.product_id.id
#                             vals['kg_qty']=(bom_line.product_qty)*qty
#                             vals['kg_req_date']=line.kg_date
#                             vals['kg_uom']=bom_line.product_uom_id.id
#                             vals['kg_mat_req_id']=mat_req_id.id
#                             self.env['kg.mat.req.lines'].create(vals)
#                         else:
#                             prev_qty = check.kg_qty
# 
#                             val= (bom_line.product_qty)*qty
#                             new_qty = prev_qty+val
#                             check.write({'kg_qty':new_qty})
# 
#             return True
#         
#         
    
    
    
    @api.multi
    def generate_mat(self):
        for record in self:
            plan_id = record._context.get('active_id')

            plan_obj = self.env['kg.prod.plan'].browse(plan_id)
            
            vals_picking={}
            vals_picking['min_date']=record.date_from
            vals_picking['picking_type_id']=self.env['stock.picking.type'].search([('name','=','Internal Transfers to production location')],limit=1).id
            vals_picking['location_id']=self.env['stock.location'].search([('name','=','Stock'),('usage','=','internal')],limit=1).id
            vals_picking['location_dest_id']=self.env['stock.location'].search([('name','=','Production'),('usage','=','internal')],limit=1).id
            pick_id =self.env['stock.picking'].create(vals_picking)
          #  mat_req_id = self.env['kg.mat.req'].create({'kg_availability_date':fields.Date.today()})
            for line in plan_obj.kg_shift_lines:
                if line.kg_date >= record.date_from and line.kg_date <=record.date_to:
                    print 'inside loop'
                    qty= line.kg_qty
                    print line.kg_product
                    bom_obj =self.env['mrp.bom'].search([('product_tmpl_id','=',line.kg_product.product_tmpl_id.id)])
                    if not bom_obj:
                        raise UserError(_('Create BOM for product: %s') % line.kg_product.name)

                    print bom_obj,'111111111'
                    for bom_line in bom_obj.bom_line_ids:

                        check=self.env['stock.move'].search([('picking_id','=',pick_id.id),('product_id','=',bom_line.product_id.id)])
                        print check,'1111111111111'
                        if not check:
                            vals={}
                            vals['product_id']=bom_line.product_id.id
                            vals['name']=bom_line.product_id.name
                            vals['product_uom_qty']=(bom_line.product_qty)*qty
                            vals['kg_req_date']=line.kg_date
                            vals['product_uom']=bom_line.product_uom_id.id
                            vals['picking_id']=pick_id.id
                            vals['location_id']=self.env['stock.location'].search([('name','=','Stock'),('usage','=','internal')],limit=1).id
                            vals['location_dest_id']=self.env['stock.location'].search([('name','=','Production'),('usage','=','internal')],limit=1).id
                            self.env['stock.move'].create(vals)
                        else:
                            prev_qty = check.product_uom_qty

                            val= (bom_line.product_qty)*qty
                            new_qty = prev_qty+val
                            check.write({'product_uom_qty':new_qty})

            return True
    
    
 