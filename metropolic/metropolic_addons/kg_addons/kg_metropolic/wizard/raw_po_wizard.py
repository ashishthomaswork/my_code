from itertools import groupby
from datetime import datetime, timedelta

from odoo import api, fields, models, _
from odoo.exceptions import UserError
from odoo.tools import float_is_zero, float_compare, DEFAULT_SERVER_DATETIME_FORMAT
from odoo.tools.misc import formatLang

import odoo.addons.decimal_precision as dp

class KGrawPoWizard(models.TransientModel):
    _name = 'kg.raw.po.wizard'
    _description = 'kg.raw.po.wizard'

    @api.multi
    def delete(self):
        wiz_line = self.wiz_line
    
        for line in wiz_line:
            if line.select:
            	line.unlink()

        return {
              'view_type': 'form',
              "view_mode": 'form',
              'res_model': 'kg.raw.po.wizard',
              'res_id': self.id,
              'type': 'ir.actions.act_window',
              'target': 'new'
              }
	    
    @api.multi
    def create_po(self):
        wiz_line = self.wiz_line
        if not wiz_line:
            raise UserError(_('no lines found for generating PO'))
        partner_id = self.supplier_id and self.supplier_id.id
        line_vals_array = []
        date_order = self.date

        for line in wiz_line:

            if line.requisition_id.rfq_id and line.requisition_id.rfq_id.id:
				raise UserError(_('already RFQ created'))
					
            product_id = line.product_id and line.product_id.id
            name = line.name
            product_uom = line.product_uom and line.product_uom.id
            
            product_qty = line.qty
            if line.product_id.type not in ('service','consu'):
            	line_vals = (0,0,{'product_id':product_id,'product_qty':product_qty,
            'product_uom':product_uom,'name':name,'date_planned':date_order,'price_unit':1})
            	line_vals_array.append(line_vals)

            
        vals = {'partner_id':partner_id,'order_line':line_vals_array}
        purchase_order_obj = self.env['purchase.order'].create(vals)
        for line in wiz_line:
			line.requisition_id.rfq_id = purchase_order_obj.id				
		
       	
			

        return True
    
    
    @api.model
    def default_get(self, fields_list):
        res = super(KGrawPoWizard, self).default_get(fields_list)

        ids = self._context.get('active_ids', [])
        purchase_req = self.env['kg.purhcase.req'].browse(ids)
        line_vals_array = []
        for line in purchase_req:
            product_id = line.kg_product and line.kg_product.id
            product_uom_qty = line.kg_qty
            name = line.kg_product.name
            product_uom = line.kg_product_uom and line.kg_product_uom.id

            vals = (0,0,{'product_id':product_id,'qty':product_uom_qty,'wiz_id':self.id,'product_uom':product_uom,'name':name,'requisition_id':line.id})
            line_vals_array.append(vals)
            
        res.update({'wiz_line':line_vals_array})
        return res
##            	
##            	
##            
##       
##        
        
    wiz_line = fields.One2many('kg.raw.po.wizard.line', 'wiz_id', string='Wiz Line',)
    supplier_id = fields.Many2one('res.partner',string='Vendor')
    date = fields.Date('Date',)
    
class KGrawPoWizardLine(models.TransientModel):
    _name = 'kg.raw.po.wizard.line'
    _description = 'kg.raw.po.wizard.line'
    wiz_id = fields.Many2one('kg.raw.po.wizard',string='Wiz')     
    product_id = fields.Many2one('product.product',string='Product')
    name = fields.Char(string='Description',)
    qty = fields.Float(string="Qty")
    unit_price = fields.Float(string="Cost")
    tax_id = fields.Many2one('account.tax',string='Vat')
    product_uom = fields.Many2one('product.uom', string='Product Unit of Measure') 
    select = fields.Boolean('Select') 
    requisition_id = fields.Many2one('kg.purhcase.req', string='Requisition')


