# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

import time
from datetime import datetime, timedelta
from dateutil import relativedelta
from dateutil.relativedelta import relativedelta
import babel

from odoo import api, fields, models, tools, _
from odoo.exceptions import UserError, ValidationError
from odoo.tools.safe_eval import safe_eval

from odoo.addons import decimal_precision as dp
import math



class HrPayslipEmployees(models.TransientModel):
    _inherit = 'hr.payslip.employees'


    @api.one
    def compute_date_diff(self,ds,dt):
        from datetime import datetime
        d1 = datetime.strptime(ds, "%Y-%m-%d")
        d2 = datetime.strptime(dt, "%Y-%m-%d")
        days = (d2 - d1).days+1
        if days < 0:
            days = 0
        return days

    @api.multi
    def compute_sheet(self):
        payslips = self.env['hr.payslip']
        [data] = self.read()
        active_id = self.env.context.get('active_id')
        if active_id:
            [run_data] = self.env['hr.payslip.run'].browse(active_id).read(['date_start', 'date_end', 'credit_note'])
        from_date = run_data.get('date_start')
        to_date = run_data.get('date_end')
        net_start_date = from_date
        net_to_date = to_date		
		

###getting two dates here






        if not data['employee_ids']:
            raise UserError(_("You must select employee(s) to generate payslip(s)."))
        for employee in self.env['hr.employee'].browse(data['employee_ids']):
			

			contract_id = self.env['hr.contract'].search([
                ('employee_id', '=', employee.id),
                ('state', '=', 'open')])
			if not contract_id:
				raise UserError(_('no running contract found for this employee %s') % (employee.name))
			if len(contract_id) >1:
				raise UserError(_('more that one running contract found for this employee %s') % (employee.name))
			if from_date < contract_id.date_start:
				net_start_date = contract_id.date_start
			if to_date < contract_id.date_start:
				raise UserError(_('remove this employee: %s from selection his/her joining date is %s') % (employee.name,contract_id.date_start))


			if to_date > contract_id.date_end and contract_id.date_end:
				net_to_date = contract_id.date_end
			basic = contract_id.wage
			housing_allowance = 0
			transport_allowance = 0
			phone_allowance = 0

			Food_allowance = 0
            
			for line in contract_id.od_allowance_rule_line_ids:
				if line.rule_type and line.rule_type.id == 33:
					housing_allowance = line.amt
				if line.rule_type and line.rule_type.id == 34:
					transport_allowance = line.amt
				if line.rule_type and line.rule_type.id == 41:
					Food_allowance= line.amt

				if line.rule_type and line.rule_type.id == 42:
					phone_allowance= line.amt





###taking all unpaid leave for the particular employee leaves start date belonging to payslip period
			unpaid_leave_ids = self.env['hr.holidays'].search([
                ('employee_id', '=', employee.id),
                ('state', 'not in', ['draft','cancel','refuse']),('od_date_from', '>=', net_start_date),('od_date_to', '<=', net_to_date),('holiday_status_id.name', '=', 'Unpaid')])
			total_number_of_unpaid_leave = 0
                        print total_number_of_unpaid_leave,'tttttttttttt',unpaid_leave_ids

			for unpaid in unpaid_leave_ids:

				total_number_of_unpaid_leave = total_number_of_unpaid_leave + unpaid.number_of_days_temp

########################3


####payroll transactions -----##

			other_allowance = 0
			other_deductions = 0
			fine = 0
			sal_adv = 0
			ot = 0            
            



			payroll_transaction_ids = self.env['hr.payroll.transactions'].search([
                ('employee_id', '=', employee.id),
                ('date', '>=', net_start_date),('date', '<=', net_to_date)])


			print "ddddddddddddddddddddpayroll_transaction_ids",payroll_transaction_ids


			for pti in payroll_transaction_ids:
				if pti.rule_id.id == 40:
					ot = ot + pti.amount

				if pti.rule_id.id == 37:
					fine = fine + pti.amount

				if pti.rule_id.id == 39:
					sal_adv = sal_adv + pti.amount


				if pti.rule_id.id == 43:
					other_deductions = other_deductions + pti.amount

				if pti.rule_id.id == 44:
					other_allowance = other_allowance + pti.amount


				print "eeeeeeeeeeeeeeeeeeeeeeeeeeother_allowance",other_allowance

				print "eeeeeeeeeeeeeeeeeeeeeeeeeeother_deductions",other_deductions

				print "eeeeeeeeeeeeeeeeeeeeeeeeeesal_adv",sal_adv 

				print "eeeeeeeeeeeeeeeeeeeeeeeeeefine",fine 
				print "eeeeeeeeeeeeeeeeeeeeeeeeeeot",ot					
			
#####
			print "ddddddddddddddddddddddddddddddddddddddddd",from_date,to_date
			month_days = self.compute_date_diff(from_date,to_date)[0]
			print "dddddddddddddddddddddddmonth_days",month_days
			if month_days == 0:
				raise UserError(_('the difference days between choosen date is zero'))


			print "ddddddddddddddddddddddddddddddddddddddddd",net_to_date,net_start_date
				
			number_of_days_period =  self.compute_date_diff(net_start_date,net_to_date)[0]	

			print "dddddddddddddddddddddddddddddddddddddddddnumber_of_days_period",number_of_days_period 
 			ttyme = datetime.fromtimestamp(time.mktime(time.strptime(net_start_date, "%Y-%m-%d")))	   		

        	
        	
 			locale = self.env.context.get('lang', 'en_US')
        	
			name = _('Salary Slip of %s for %s') % (employee.name, tools.ustr(babel.dates.format_date(date=ttyme, format='MMMM-y', locale=locale)))		
			
			gross = basic + housing_allowance + phone_allowance + Food_allowance + transport_allowance

			print "ccccccccccccccccccccccccccccccccgross",gross

			one_day_gross = float(gross / month_days)	
            
			net_worked_days = number_of_days_period - total_number_of_unpaid_leave

			net_gross = one_day_gross * number_of_days_period

			leave_ded = total_number_of_unpaid_leave * one_day_gross

			net_sal = net_gross + other_allowance - other_deductions - fine - sal_adv + ot - leave_ded
          

			slip_data = self.env['hr.payslip'].onchange_employee_id(from_date, to_date, employee.id, contract_id=False)
   
			res = {
                'employee_id': employee.id,
                'name': name,
                'contract_id': contract_id.id,
                'payslip_run_id': active_id,
                'date_from': net_start_date,
                'date_to': net_to_date,
                'company_id': employee.company_id.id,
				'salary_advance':sal_adv,
				'fine':fine,
				'number_of_days_period':number_of_days_period,
				'month_days':month_days,
				'net_worked_days':net_worked_days,
				'other_deductions':other_deductions,
				'other_allowances':other_allowance,
                'leave_deductions':leave_ded,
				'total_no_of_unpaid_leaves':total_number_of_unpaid_leave,
				'type_id':contract_id.type_id and contract_id.type_id.id,
				'telephone_mob_allowance':phone_allowance,
                'house_allowance':housing_allowance,
				'transportation_allowance':transport_allowance,
				'ot_allowance':ot,
				'basic':basic,
				'gross':net_gross,
				'net':net_sal,
				'food_allowance':Food_allowance
            }
			payslips += self.env['hr.payslip'].create(res)
            
            
            
        return {'type': 'ir.actions.act_window_close'}
