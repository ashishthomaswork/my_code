from itertools import groupby
from datetime import datetime, timedelta

from odoo import api, fields, models, _
from odoo.exceptions import UserError
from odoo.tools import float_is_zero, float_compare, DEFAULT_SERVER_DATETIME_FORMAT
from odoo.tools.misc import formatLang

import odoo.addons.decimal_precision as dp


class kg_prod_plan_print(models.TransientModel):
    
    _name='kg.prod.plan.print'
    
    date_from = fields.Date('Date From')
    
    date_to = fields.Date('Date To')

    machine_id = fields.Many2one('kg.machine','Machine')
    
    
    @api.multi
    def print_report(self):
        self.ensure_one()
        data = {}
        data['ids'] = self.env.context.get('active_ids', [])
        data['model'] = self.env.context.get('active_model', 'ir.ui.menu')
        data['form'] = self.read(['date_from', 'date_to','machine_id'])[0]
        print data,'11111111111'
    #    used_context = self._build_contexts(data)
     #   data['form']['used_context'] = dict(used_context, lang=self.env.context.get('lang') or 'en_US')
        return self.env['report'].get_action(self, 'kg_metropolic.report_prodplan', data=data)