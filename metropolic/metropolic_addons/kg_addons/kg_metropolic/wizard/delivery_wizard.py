# -*- coding: utf-8 -*-
# /######################################################################################
#
#    Klystron Technologies
#    Copyright (C) 2004-TODAY Klystron Technologies Pvt Ltd.(<http://klystrontech.com/>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# /######################################################################################

from odoo import models, fields, api, _
from odoo.exceptions import ValidationError


class DeliveryWizard(models.TransientModel):
    _name = 'delivery.wizard'
    _description = 'Delivery Assignment'

    location_dest_id = fields.Many2one('stock.location', string="Vehicle")

    name_ids = fields.Many2many('stock.picking', string="Stock Picking ID")

    @api.multi
    def assign_delivery(self):
        vals = {}
        for file in self:
            for name in file.name_ids:
                if name.state != ('draft' or 'confirmed'):
                    raise ValidationError(
                        "Selected reference state is not in Draft or Waiting Availability," + "\n\t Please select the references which is in Draft or Availability")
                elif name.state == ('draft' or 'confirmed'):
                    vals['location_dest_id'] = self.location_dest_id.id
                vals.update({
                    'state': 'assigned'
                })
        self.name_ids.write(vals)
