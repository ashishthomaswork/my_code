# -*- coding: utf-8 -*-
from odoo import models,api,fields,_

# -*- coding: utf-8 -*-
from odoo import models,api,fields
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT, float_compare
from odoo.exceptions import UserError

from datetime import date,datetime

from dateutil.relativedelta import relativedelta
from odoo.exceptions import Warning

class kg_prod_plan(models.Model):
    _name='kg.prod.plan'
    _inherit = ['mail.thread', 'ir.needaction_mixin']




    @api.multi
    def make_it_done(self):

        self.state = 'done'




    # @api.multi
    # @api.depends('kg_shift_lines.kg_qty')
    # def _compute_palnned(self):
    #     line = self.kg_shift_lines
    #     tot = 0
    #
    #     for li in line:
    #         tot = tot + li.kg_qty
    #
    #     self.kg_planned_qty = tot
    #
    #
    # @api.multi
    # @api.depends('kg_shift_lines.kg_qty','kg_planned_qty','kg_it_planned_qty')
    # def _compute_pending(self):
    #     kg_it_planned_qty = self.kg_it_planned_qty
    #     kg_planned_qty = self.kg_planned_qty
    #
    #
    #     self.kg_pending_qty = kg_it_planned_qty - kg_planned_qty




    #
    # @api.multi
    # @api.one
    # @api.constrains('kg_it_planned_qty','kg_planned_qty')
    # def _compute_checking_planned(self):
    #     kg_it_planned_qty = self.kg_it_planned_qty
    #     kg_planned_qty = self.kg_planned_qty
    #     if kg_planned_qty > kg_it_planned_qty:
	# 		raise UserError(_('planned qty greater than initial planned'))
	#
    #









    state = fields.Selection([
        ('new', 'New'),
        ('progress', 'In Progress'),
        ('done', 'Done')], string='State',
        copy=False, default='new', track_visibility='onchange')


  
    name = fields.Char('Name')


    kg_start_date = fields.Date('Start Date')

    kg_end_date = fields.Date('End Date')
    
    # kg_product = fields.Many2one('product.product','Product')
    #
    # uom_id = fields.Many2one('product.uom','UOM')
    #
    # kg_period  = fields.Many2one('kg.period','Period')
    #
    #
    # kg_pack = fields.Many2one('kg.pack','Pack')
    #
    # kg_core = fields.Many2one('kg.core','Core')
    #
    # kg_lbl = fields.Char('Label Application')
    #
    # kg_hndl = fields.Char('Handle Application')
    #
    # kg_emb = fields.Char('Embossing')
    #
    # kg_lam = fields.Char('Lamination')
    #
    # kg_width = fields.Char('Width(mm)')
    #
    # kg_perf = fields.Char('Perforation length(mm)')
    #
    # kg_dia = fields.Char('Diameter(mm)')
    #
    # kg_sht = fields.Char('Sheet Count')
    #
    # kg_ply = fields.Char('Plies')
    #
    # kg_gsm = fields.Char('GSM')
    #
    # kg_wght = fields.Char('Weight(grams)')
    #
    # kg_trrt = fields.Char('TRRT')
    #
    # kg_cph = fields.Char('CPH')


    kg_shift_lines = fields.One2many('kg.prod.shift.line','kg_prod_plan_id','Shift Lines')
    
    kg_raw_mat_lines = fields.One2many('kg.raw.mat.line','kg_prod_plan_id','Raw Material Lines')
    
    kg_sf_id = fields.Many2one('kg.sales.forecast','Sales Forecast')

    # kg_it_planned_qty = fields.Float('Initail Planned Qty')
    #
    # kg_planned_qty = fields.Float('Planned Qty',compute='_compute_palnned', store=True)
    # kg_pending_qty = fields.Float('Pending Qty',compute='_compute_pending', store=True)
    #
    project_id = fields.Many2one('account.analytic.account', 'Cost Center')

    @api.onchange('kg_start_date')
    def onchange_start_date(self):
        for record in self:
            if record.kg_start_date:

                start_dt = fields.Datetime.from_string(record.kg_start_date)

                dt =start_dt+relativedelta(days=4)

                record.kg_end_date = fields.Datetime.to_string(dt)
      #      record.kg_end_date =(datetime.strptime(record.kg_start_date,'%Y-%m-%d') + relativedelta())

    
    @api.onchange('kg_product')
    def onchange_product(self):
        for record in self:
            record.kg_pack = record.kg_product.kg_pack and record.kg_product.kg_pack.id or False
            record.kg_core = record.kg_product.kg_core and record.kg_product.kg_core.id or False
            record.kg_lbl = record.kg_product.kg_lbl
            record.kg_hndl = record.kg_product.kg_hndl
            record.kg_emb = record.kg_product.kg_emb
            record.kg_lam = record.kg_product.kg_lam
            record.kg_width = record.kg_product.kg_width
            record.kg_perf = record.kg_product.kg_perf
            record.kg_dia = record.kg_product.kg_dia
            record.kg_sht = record.kg_product.kg_sht
            record.kg_ply = record.kg_product.kg_ply
            record.kg_gsm = record.kg_product.kg_gsm
            record.kg_wght = record.kg_product.kg_wght
            record.kg_trrt = record.kg_product.kg_trrt
            record.kg_cph = record.kg_product.kg_cph 
            
            

            
            
    @api.model
    def create(self,vals):
        
        sequence_code='kg.prod.plan'
        vals['name']= self.env['ir.sequence'].next_by_code(sequence_code) 
     
        
        
        

            
        
        return super(kg_prod_plan, self).create(vals)
    
            
            
            
class kg_prod_shift_line(models.Model):
    
    _name='kg.prod.shift.line'
    
    state = fields.Selection([('new','New'),('prod_com','Production Completed'),('transfer','Transfered to Stock')],'State',default='new')
    
    
    
    @api.multi
    def send_to_stock(self):
        for record in self:
            vals={}
            vals['picking_type_id']=self.env['stock.picking.type'].search([('name','=','Internal Transfers to Stock from production')],limit=1).id
            vals['location_id']=self.env['stock.location'].search([('name','=','Production'),('usage','=','internal')],limit=1).id
            vals['location_dest_id']=self.env['stock.location'].search([('name','=','Stock'),('usage','=','internal')],limit=1).id
            pick_id =self.env['stock.picking'].create(vals)

            print "mmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmm",pick_id
         
            vals_line={}
                
            vals_line['product_id']=record.kg_product.id
            vals_line['name']=record.kg_product.name
            vals_line['product_uom']=record.kg_product.uom_id.id
            vals_line['product_uom_qty']=record.kg_act_prod
            vals_line['location_id']=self.env['stock.location'].search([('name','=','Production'),('usage','=','internal')],limit=1).id
            vals_line['location_dest_id']=self.env['stock.location'].search([('name','=','Stock'),('usage','=','internal')],limit=1).id
                
                    
            vals_line['picking_id']= pick_id.id
            self.env['stock.move'].create(vals_line)
            pick_id.action_confirm()
           # pick_id.force_assign()
           
                
            record.write({'picking_id':pick_id.id,'state':'transfer'})
            
    
    @api.multi
    def complete_prod(self):
        for record in self:
            vals={}
            vals['product_id']= record.kg_product.id
            vals['product_qty']= record.kg_act_prod

            if vals['product_qty'] <= 0:

                raise Warning('Actual produced qty shpuld be greater than zero.')


            vals['product_uom_id']=record.kg_product.uom_id.id
            bom_id = self.env['mrp.bom'].search([('product_tmpl_id','=',record.kg_product.product_tmpl_id.id)],limit=1)
            if not bom_id:
                raise Warning('Create BOM for Product.')
            vals['bom_id']=bom_id and bom_id.id 
            
            vals['date_planned_start']=record.kg_date
            vals['kg_shift_id']=record.kg_shift_id.id
            vals['kg_day']=record.kg_day
            vals['picking_type_id']=6
            vals['location_src_id']=19
            vals['location_dest_id']=19
            
            prod_id=self.env['mrp.production'].create(vals)

            move_raw_ids = prod_id.move_raw_ids
            prod_id.action_assign()

            for raw in move_raw_ids:
                if raw.state !='assigned':
                	raise UserError(_('rawmaterial not available in production location: "%s".') %
                (raw.product_id.name))

            wizard_obj = self.env['change.production.qty'].create({'mo_id':prod_id.id,'product_qty':vals['product_qty']})

            wizard_obj.change_prod_qty()

            res = self.env['mrp.product.produce'].create({'production_id':prod_id.id,'product_qty':vals['product_qty']}).do_produce()

            print "dddddddddddddddddddddddddddddddddd",res			

			



            prod_id.post_inventory()
            self.write({'production_id':prod_id.id,'state':'prod_com'})

            return True


            #prod_id.button_mark_done()

            #action = self.env.ref('mrp.act_mrp_product_produce').read()[0]

            #print "444444444444444444444444444444444action",action

            #return action
        

				
				
        




        








            
            
            
            
    
    name = fields.Char('Name')

    kg_product = fields.Many2one('product.product', 'Product')


    kg_machine = fields.Many2one(related='kg_product.kg_machine', relation='kg.machine', string='Machine',store=True)

    kg_day = fields.Char('Day')
    
    kg_date = fields.Date('Date')
    
    kg_shift_id = fields.Many2one('kg.shift.master',string="Shift")
    
    kg_qty = fields.Float('Target')

    kg_forecast = fields.Float('Forecast',compute='comp_real_mins1')

    kg_planned = fields.Float('Production',compute='comp_production')

    kg_act_prod = fields.Float('Actual Produced')

    kg_trrt = fields.Float('TRRT(hours)',compute='compute_trrt')

    kg_sch_min = fields.Float('Scheduled Mins')

    # @api.model
    # def read_group(self, domain, fields, groupby, offset=0, limit=None, orderby=False, lazy=True):
    #     res=super(kg_prod_shift_line, self).read_group(domain, fields, groupby, offset=offset, limit=limit, orderby=orderby,lazy=lazy)
    #     if 'kg_sch_min' in fields:
    #         for line in res:
    #             if '__domain' in line:
    #                 lines = self.search(cr, uid, line['__domain'], context=context)
    #                 pending_value = 0.0
    #                 for current_account in self.browse(lines, context=context):
    #                     pending_value += current_account.kg_sch_min
    #                 line['kg_sch_min'] = pending_value
    #     return res



    kg_start_time = fields.Float('Start Time')

    kg_end_time = fields.Float('End Time')

    kg_real_mins  = fields.Float('Scheduled Mins',compute='comp_real_mins1')

    kg_act_mins = fields.Float('Actual Mins',compute='comp_real_mins1')

    kg_dt = fields.Float('DT(mins)')

    kg_eff = fields.Float('Efficency(%)',compute='comp_real_mins1',group_operator='avg')

    kg_over_shift = fields.Boolean('Over Shift Hours',compute='comp_over_shift')


    kg_forecast_act_per = fields.Float('MTD Production Target(%)',compute='comp_real_mins1')

    @api.depends('kg_trrt')
    def comp_over_shift(self):
        for record in self:
            act_conf = self.env['kg.prod.conf'].search([('active', '=', True)])
            if not act_conf:
                raise UserError(_('Create Active Production Configuration'))

            shift_hrs = act_conf.kg_shift_hours


            dt=record.kg_date
            shift = record.kg_shift_id and record.kg_shift_id.id or False
            mach = record.kg_machine and record.kg_machine.id or False
            if dt and shift and mach:
                lines = self.search([('kg_prod_plan_id', '=', record.kg_prod_plan_id.id),('kg_machine', '=', mach),('kg_date','=',dt),('kg_shift_id','=',shift)])
                real_hr = 0
                if lines:
                    for line in lines:
                        real_hr += line.kg_trrt
                if real_hr > shift_hrs:
                    record.kg_over_shift=True
                else:
                    record.kg_over_shift=False
            else:
                record.kg_over_shift=False

    kg_remarks = fields.Char('Remarks')


    kg_prod_plan_id = fields.Many2one('kg.prod.plan','Production Plan')

    production_id = fields.Many2one('mrp.production','MRP Ref')
    
    
    picking_id = fields.Many2one('stock.picking','Picking')




    _sql_constraints = [('uniq_lines', 'unique(kg_product,kg_prod_plan_id,kg_date,kg_shift_id)', _("Already Planned For that Product in same shift and day!"))]

    # @api.depends('kg_trrt')
    # def compute_sch_mins(self):
    #     for record in self:
    #         trrt= record.kg_trrt
    #         record.kg_sch_min = trrt*60

    @api.model
    def read_group(self, domain, fields, groupby, offset=0, limit=None, orderby=False, lazy=True):
        """
            Inherit read_group to calculate the sum of the non-stored fields, as it is not automatically done anymore through the XML.
        """
        print fields,'fieldsfields11111111111111'

        if 'kg_start_time' in fields:
            fields.remove('kg_start_time')

        if 'kg_end_time' in fields:
            fields.remove('kg_end_time')

        res = super(kg_prod_shift_line, self).read_group(domain, fields, groupby, offset=offset, limit=limit,
                                                         orderby=orderby, lazy=lazy)
        fields_list = ['kg_real_mins', 'kg_act_mins','kg_eff','kg_forecast','kg_forecast_act_per']
        if any(x in fields for x in fields_list):
            # Calculate first for every product in which line it needs to be applied
            re_ind = 0
            prod_re = {}
            tot_products = self.browse([])
            for re in res:
                if re.get('__domain'):
                    products = self.search(re['__domain'])
                    tot_products |= products
                    for prod in products:
                        prod_re[prod.id] = re_ind
                re_ind += 1

            res_val = tot_products.comp_real_mins1(field_names=[x for x in fields if fields in fields_list])
            print res_val,'22222222222222'
            for key in res_val.keys():
                print key,'11111111111111'
                count=0
                for l in res_val[key].keys():
                    re = res[prod_re[key]]
                    count+=1

                    if re.get(l):
                        print re,'444444444444444444444'
                        print l,'55555555555555555',res_val[key][l]
                        #if l !='kg_forecast':
                        re[l] += res_val[key][l]


                    else:
                        re[l] = res_val[key][l]
                print re,'2222222222'


                # if 'kg_product_count' in re:
                #     re['kg_forecast']=re['kg_forecast']/re['kg_product_count']

                if 'kg_qty' in re and re['kg_qty']>0:
                    re['kg_eff']=(re['kg_act_prod']/re['kg_qty'])*100.0

                if 'kg_forecast' in re and re['kg_forecast']>0:
                    re['kg_forecast_act_per']=(re['kg_act_prod']/re['kg_forecast'])*100.0




        return res
    
    
    @api.onchange('kg_product')
    def onchange_product(self):
        for record in self:
            qty=0
            sf_lines = self.env['kg.sf.lines'].search([('kg_product_id','=',record.kg_product.id),('kg_sf_id','=',record.kg_prod_plan_id.kg_sf_id.id)])
            if sf_lines:
                for line in sf_lines:
                    qty+=line.kg_net
            record.kg_forecast = qty
            


    def comp_real_mins1(self, field_names=None):
        res = {}
        if field_names is None:
            field_names = []

        for val in self:
            res[val.id] = {}
            if val.kg_start_time < val.kg_end_time:
                value = val.kg_end_time - val.kg_start_time
                res[val.id]['kg_real_mins'] = value * 60
                res[val.id]['kg_act_mins'] = (value*60)-val.kg_dt

            else:
                res[val.id]['kg_real_mins'] = 0.0
                res[val.id]['kg_act_mins'] = 0.0

            if val.kg_qty > 0:
                act = val.kg_act_prod
                tar = val.kg_qty
                eff = (act/tar)*100
                res[val.id]['kg_eff'] =eff



            if val.kg_product:
                qty=0
                sf_lines = self.env['kg.sf.lines'].search([('kg_product_id','=',val.kg_product.id),('kg_sf_id','=',val.kg_prod_plan_id.kg_sf_id.id)])
                if sf_lines:
                    for line in sf_lines:
                        qty+=line.kg_net
                res[val.id]['kg_forecast']=qty

                if qty > 0:
                    res[val.id]['kg_forecast_act_per']=((val.kg_act_prod)/qty)*100.0

            for k, v in res[val.id].items():
                setattr(val, k, v)
        return res


 #   @api.depends('kg_trrt','kg_real_mins')
    # def comp_dt(self):
    #     for record in self:
    #         real_hrs = (record.kg_real_mins)/60.0
    #         sch_time = record.kg_trrt
    #         dt = sch_time-real_hrs
    #         if dt > 0:
    #             record.kg_dt = dt*60

    # @api.depends('kg_act_prod')
    # def comp_eff(self):
    #     for record in self:
    #         if record.kg_qty > 0:
    #             act = record.kg_act_prod
    #             tar = record.kg_qty
    #             eff = (act/tar)*100
    #             record.kg_eff =eff


    # @api.depends('kg_start_time','kg_end_time')
    # def comp_real_mins(self):
    #     for record in self:
    #         if record.kg_start_time and record.kg_end_time:
    #             if record.kg_start_time < record.kg_end_time:
    #                 val = record.kg_end_time - record.kg_start_time
    #                 record.kg_real_mins = val*60
    #             else:
    #                 raise UserError(_('End Time Should be greater than Start time'))

    @api.depends('kg_qty','kg_product')
    def compute_trrt(self):
        for record in self:
            if record.kg_product:
                cph= float(record.kg_product.eff_output_carton)
                if cph > 0:
                    dt = float(record.kg_product.sch_shift_hours)
                    target = record.kg_qty
                    trrt = (target/cph)+dt
                else:
                    trrt = 0
                record.kg_trrt = round(trrt,1)

    @api.depends('kg_qty','kg_product')
    def comp_production(self):
        for record in self:
            if record.kg_product:
                lines = self.search([('kg_prod_plan_id','=',record.kg_prod_plan_id.id),('kg_product','=',record.kg_product.id)])
                qty=0
                if lines:
                    for line in lines:
                        qty+=line.kg_qty
                record.kg_planned = qty
                
   

    # @api.depends('kg_product')
    # def comp_forecast(self):
    #     for record in self:
    #         if record.kg_product:
    #             qty=0
    #             sf_lines = self.env['kg.sf.lines'].search([('kg_product_id','=',record.kg_product.id),('kg_sf_id','=',record.kg_prod_plan_id.kg_sf_id.id)])
    #             if sf_lines:
    #                 for line in sf_lines:
    #                     qty+=line.kg_net
    #             record.kg_forecast=qty
    #
    #
    #


            
    @api.onchange('kg_date')
    def onchange_date(self):
        date = self.kg_date
        if not date:	
			return {}
        date = str(self.kg_date) + " "+"00:00:00"    

        date_time = datetime.strptime(date, '%Y-%m-%d %H:%M:%S')
        print "ssssssssssssssssssssssss",date_time
        weekday = int(date_time.weekday())	
		

        print "ddddddddddddddddddddddd",weekday
		

        dict_day = {0:"Monday",1:"Tuesday",2:"Wednesday",3:"Thursday",4:"Friday",5:"Saturday",6:"Sunday"}
        if weekday >=0:
			self.kg_day = dict_day[weekday]		



    @api.multi
    def create_mrp(self):
        product_tmpl_id = self.kg_prod_plan_id.kg_product.product_tmpl_id.id
        uom_id = self.kg_prod_plan_id.uom_id.id




        bom_id = self.env['mrp.bom'].search([('product_tmpl_id', '=',product_tmpl_id),('product_uom_id', '=',uom_id)])


        bom_main_qty = bom_id.product_qty


        if len(bom_id) == 0:
			raise UserError(_('no BOM found for this product and defined UOM,please define BOM First with product and uom'))

        if len(bom_id) > 1:
			raise UserError(_('multiple bom found for same product'))

        bom_line = bom_id.bom_line_ids

        bom_line_length = len(bom_line)

        if bom_line_length == 0:
			raise UserError(_('inside BOM no lines found'))

        shift_id = self.kg_shift_id and self.kg_shift_id.id

		


        for bl in bom_line:
			product_id = bl.product_id and bl.product_id.id
			product_uom_id = bl.product_uom_id and bl.product_uom_id.id
			product_qty = float(bl.product_qty / bom_main_qty) * float(self.kg_qty)
			self.env['kg.raw.mat.line'].create({'kg_product':product_id,'kg_product_uom':product_uom_id,
'kg_date':self.kg_date,'kg_shift_id':shift_id,'kg_qty':product_qty,'kg_prod_plan_id':self.kg_prod_plan_id.id})





			 

        vals_mrp={}
        vals_mrp['kg_shift_id']=shift_id
        vals_mrp['kg_day']=self.kg_day
        vals_mrp['product_id']=self.kg_prod_plan_id.kg_product.id
        vals_mrp['product_uom_id']=self.kg_prod_plan_id.kg_product.uom_id.id
        vals_mrp['product_qty']=self.kg_qty
        vals_mrp['bom_id']=bom_id.id
        vals_mrp['date_planned_start']=self.kg_date
        prod = self.env['mrp.production'].create(vals_mrp)

        self.production_id = prod.id
		
        lines = self.kg_prod_plan_id.kg_shift_lines
		
        lines_length = len(lines)

        production_ids = []
        for li in lines:		
		
		
			if li.production_id and li.production_id.id:
				production_ids.append(li.production_id.id)

        production_ids_len = len(production_ids)

		
        if production_ids_len == lines_length:

		
			self.state = 'done'
        else:
		
			self.state = 'progress'
        return True
		
		
				
			


            
            

                 
            
            
            
             
class kg_raw_mat_line(models.Model):
    
    _name='kg.raw.mat.line'


    @api.multi
    def action_open_quants(self):
        product = self.kg_product and self.kg_product.id
        action = self.env.ref('stock.product_open_quants').read()[0]
        action['domain'] = [('product_id', '=', product)]
        action['context'] = {'search_default_locationgroup': 1, 'search_default_internal_loc': 1}
        return action



    @api.multi
    def create_purchase_req(self):
        kg_purchase_requisition_id = self.kg_purchase_requisition_id and self.kg_purchase_requisition_id.id
        if kg_purchase_requisition_id:
			raise UserError(_('Already purchase requisition created'))

        kg_product = self.kg_product and self.kg_product.id
        kg_product_uom = self.kg_product_uom and self.kg_product_uom.id
        kg_date = self.kg_date  
        required_date = self.req_date
        notes = self.notes
        kg_qty = self.kg_qty 
        kg_prod_plan_id = self.kg_prod_plan_id and self.kg_prod_plan_id.id
        state = 'new'
        vals = {'kg_product':kg_product,
                 'kg_product_uom':kg_product_uom,
                 'kg_date':kg_date,
                 'required_date':required_date,
                 'notes':notes,
                 'kg_qty':kg_qty,
                 'kg_prod_plan_id':kg_prod_plan_id,
                 'state':state}
        purchase_req = self.env['kg.purhcase.req'].create(vals)
        self.kg_purchase_requisition_id = purchase_req and purchase_req.id


    



        return True









    @api.multi
    def kg_quick_po(self):

        print "vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv"
        
        return {
            'name':'Quick RFQ',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'kg.raw.po.wizard',
            'target': 'new',
            'type': 'ir.actions.act_window',
        }
    
    name = fields.Char('Name')
    
    kg_product = fields.Many2one('product.product','Product')
    
    kg_product_uom = fields.Many2one('product.uom','UOM')
    
    kg_date = fields.Date('Date')

    req_date = fields.Date('Req.Date')
    
    kg_shift_id = fields.Many2one('kg.shift.master',string="Shift")
    
    kg_qty = fields.Float('Quantity')

    req_qty = fields.Float('Req.Qty')

    notes = fields.Text('Notes')
    
    kg_prod_plan_id = fields.Many2one('kg.prod.plan','Production Plan')
    kg_purchase_requisition_id = fields.Many2one('kg.purhcase.req','Purchase Req.')



    
    
    @api.onchange('kg_product')
    def onchange_prod(self):
        
        for record in self:
            record.kg_product_uom =record.kg_product.uom_id.id
                       
            
            
            
            
            
            
            
            
            
            
            
            


            
            

                 
            
            
            
             
class kg_purhcase_req(models.Model):
    
    _name='kg.purhcase.req'
    _rec_name = 'kg_product'
    _inherit = ['mail.thread', 'ir.needaction_mixin']

    @api.multi
    def action_open_quants(self):
        product = self.kg_product and self.kg_product.id
        action = self.env.ref('stock.product_open_quants').read()[0]
        action['domain'] = [('product_id', '=', product)]
        action['context'] = {'search_default_locationgroup': 1, 'search_default_internal_loc': 1}
        return action

    kg_product = fields.Many2one('product.product','Product')

    rfq_id = fields.Many2one('purchase.order','RFQ')
    
    kg_product_uom = fields.Many2one('product.uom','UOM')
    
    kg_date = fields.Date('Date')
    required_date = fields.Date('Required Date')

    notes = fields.Text('Notes')

    
    
    kg_qty = fields.Float('Quantity')
    
    kg_prod_plan_id = fields.Many2one('kg.prod.plan','Production Plan')

    kg_sf_id = fields.Many2one('kg.sales.forecast','Sales Forecast')
    state = fields.Selection([
        ('new', 'New'),
        ('RFQ_created', 'RFQ Created')], string='State',
        copy=False, default='new', track_visibility='onchange')    
          
              
            
            
            
            
            
            
            
