# -*- coding: utf-8 -*-

from odoo import models, fields, api


class SaleOrder(models.Model):
	_inherit = 'sale.order'


	@api.multi
	def action_confirm(self):
		for order in self:
			order.state = 'sale'
			order.confirmation_date = fields.Datetime.now()
			if self.env.context.get('send_email'):
				self.force_quotation_send()
			order.order_line._action_procurement_create()
		return True
