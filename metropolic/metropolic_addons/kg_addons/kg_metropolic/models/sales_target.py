# -*- coding: utf-8 -*-
from odoo import models,api,fields,_

# -*- coding: utf-8 -*-
from odoo import models,api,fields
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT, float_compare
from odoo.exceptions import UserError



class kg_sales_target(models.Model):
    
    _name='kg.sales.target'
    
    name = fields.Char('Name')
    
    kg_start = fields.Date('Start Date')
    
    kg_end = fields.Date('End Date')
    
    kg_sales_id = fields.Many2one('res.users','SalesPerson')
    
    kg_st_lines = fields.One2many('kg.st.lines','kg_st_id','Sales Target Lines')
    
    
    
    @api.multi
    def fill_sku(self):
        for record in self:
            categies = self.env['product.category'].search([])
            for cat in categies:
                already = self.env['kg.st.lines'].search([('kg_category','=',cat.id),('kg_st_id','=',record.id)])
                if not already:
                    vals={}
                    
                    vals['kg_category']=cat.id
                #    vals['kg_uom_id']=product.uom_id.id
                    vals['kg_st_id']=record.id
                    self.env['kg.st.lines'].create(vals)
                    
    @api.multi
    def calculate(self):
        for record in self:
            for target in record.kg_st_lines:
                items = self.env['sale.order.line'].search([('order_id.date_order','>=',record.kg_start),('order_id.date_order','<=',record.kg_end),('product_id.categ_id','=',target.kg_category.id),('order_id.user_id','=',record.kg_sales_id.id)])
                qty=0.0
                
                for item in items:
                    qty+=item.product_uom_qty
                    
                print qty,'111111111111111111111'
                    
                target.write({'kg_ach':qty})
                
                
                
                
    
    
    
class kg_st_lines(models.Model):
    
    _name='kg.st.lines'
    
    name = fields.Char('Name')
    
    kg_product_id = fields.Many2one('product.product','Product')
    
    kg_category = fields.Many2one('product.category','Category')
    
    kg_uom_id = fields.Many2one('product.uom','UOM')
    
    kg_target = fields.Float('Target')
    
    kg_ach =fields.Float('Achieved')
    
    kg_st_id = fields.Many2one('kg.sales.target','Sales Target')
    
    @api.onchange('kg_product_id')
    def onchange_product(self):
        for record in self:
            record.kg_uom_id = record.kg_product_id.uom_id.id
    
    
        
    