# -*- coding: utf-8 -*-
from odoo import models,api,fields,_

# -*- coding: utf-8 -*-
from odoo import models,api,fields
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT, float_compare
from odoo.exceptions import UserError

class kg_fac_type(models.Model):
    _name = 'kg.fac.type'
    
    name = fields.Char('Name',required=True)


class kg_facility(models.Model):
    
    _name='kg.facility'
    
    
    name = fields.Char('Name')
    
    kg_type = fields.Many2one('kg.fac.type','Type')
    
    kg_iss_bank = fields.Many2one('kg.bank','Issuing Bank')
    
    kg_limit = fields.Float('Limit')
    
    kg_exp_date = fields.Date('Expiry Date')
    
    kg_util = fields.Float('Utilization')
    
    kg_aval_limit = fields.Float('Available Limit')
    
    kg_lines  = fields.One2many('account.payment','kg_fac_id','Lines')
    
class account_payment(models.Model):
    
    _inherit= 'account.payment'
    
    kg_fac_id = fields.Many2one('kg.facility','Facility')