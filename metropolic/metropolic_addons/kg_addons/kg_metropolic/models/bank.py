# -*- coding: utf-8 -*-
from odoo import models,api,fields,_

# -*- coding: utf-8 -*-
from odoo import models,api,fields
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT, float_compare
from odoo.exceptions import UserError


class kg_bank(models.Model):
    
    _name='kg.bank'    



    name = fields.Char('Name')
    
    kg_acc_no = fields.Char('Account Number')
    
    kg_city = fields.Char('City')
    
    kg_remarks = fields.Char('Remarks')
    
    