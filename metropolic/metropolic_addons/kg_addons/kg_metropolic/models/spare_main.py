# -*- coding: utf-8 -*-
from odoo import models,api,fields,_

# -*- coding: utf-8 -*-
from odoo import models,api,fields
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT, float_compare
from odoo.exceptions import UserError

from datetime import date,datetime

from dateutil.relativedelta import relativedelta


class kg_spare_main(models.Model):
    
    _name = 'kg.spare.main'
    
    name = fields.Char('Name')
    
    req_date = fields.Date('Required Date')
    
    state = fields.Selection([('new','New'),('approve','Approved'),('send_to_prod','Send to Production'),('done','Done'),('cancel','Cancel')],'State',default="new")
    
    spare_list = fields.One2many('kg.spare.line','spare_main_id','Spare List')
    
    picking_id  = fields.Many2one('stock.picking','Picking')
    
    
    @api.multi
    def approve(self):
        for record in self:
            record.write({'state':'approve'})
            return True


    @api.multi
    def send_to_prod(self):
        for record in self:
            vals={}
            vals['picking_type_id']=self.env['stock.picking.type'].search([('code','=','mrp_operation')],limit=1).id
            vals['location_id']=15
            vals['location_dest_id']=7
            pick_id =self.env['stock.picking'].create(vals)

            print "mmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmm",pick_id
            for line in record.spare_list:
                vals_line={}
                if line.spare_id:
                    vals_line['product_id']=line.spare_id.id
                    vals_line['name']=line.spare_id.name
                    vals_line['product_uom']=line.spare_id.uom_id.id
                vals_line['product_uom_qty']=line.qty
                vals_line['location_id']=15
                vals_line['location_dest_id']=7
                
                    
                vals_line['picking_id']= pick_id.id
                self.env['stock.move'].create(vals_line)
            pick_id.action_confirm()
           # pick_id.force_assign()
            
            for line in pick_id.pack_operation_product_ids:
                line.write({'qty_done':line.product_qty})
            
            pick_id.do_new_transfer()
                
            record.write({'picking_id':pick_id.id,'state':'send_to_prod'})
            
            
    @api.multi
    def done(self):
        for record in self:
            record.write({'state':'done'})
            return True
        
        
    @api.multi
    def cancel(self):
        for record in self:
            record.write({'state':'cancel'})
            
    
        
    @api.model
    def create(self,vals):
        
        sequence_code='kg.spare.main'
        vals['name']= self.env['ir.sequence'].next_by_code(sequence_code) 
     
        
        
        

            
        
        return super(kg_spare_main, self).create(vals)
    
    
    
    
    
    
class kg_spare_lines(models.Model):
    
    _name = 'kg.spare.line'
    
    spare_id = fields.Many2one('product.product','Spare Parts')
    
    qty = fields.Float('Qty')
    
    cost = fields.Float('Cost')
    
    req_date = fields.Date('Required Date',related='spare_main_id.req_date', store=True)
    
    spare_main_id = fields.Many2one('kg.spare.main','Spare Main')
    
    
    @api.onchange('qty','spare_id')
    def onchange_qty(self):
        for record in self:
            unit_cost = record.spare_id.standard_price
            qty = record.qty
            cost = unit_cost* qty
            record.cost= cost
            
    
    
    
    
    
    
    
    
    
