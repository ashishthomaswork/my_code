# -*- coding: utf-8 -*-
from odoo import models,api,fields,_

# -*- coding: utf-8 -*-
from odoo import models,api,fields
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT, float_compare
from odoo.exceptions import UserError

class kg_offer(models.Model):
    _name='kg.offer'
    
    name  =fields.Char('Offer Description')
    
    kg_customer = fields.Many2one('res.partner','Customer')
    
    kg_start_dt = fields.Date('Start Date')
    
    kg_end_dt = fields.Date('End Date')
    
    kg_location = fields.Char('Location')
    
    kg_remarks = fields.Text('Remarks')
    
    