# -*- coding: utf-8 -*-
from odoo import models,api,fields,_

# -*- coding: utf-8 -*-
from odoo import models,api,fields
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT, float_compare
from odoo.exceptions import UserError


class kg_period(models.Model):
    
    _name = 'kg.period'

    name = fields.Char('Name')
    
    kg_date_from = fields.Date('From')
    
    kg_date_to = fields.Date('To')



    
    
class kg_sales_forecast(models.Model):
    
    _name='kg.sales.forecast'
    _description = 'Sales Forecast'
    _inherit = ['mail.thread', 'ir.needaction_mixin']


    @api.multi
    def button_approve(self):

        self.kg_state = 'approved'


    @api.multi
    def button_rejected(self):

        self.kg_state = 'rejected'


    @api.multi
    def button_set_to_new(self):

        self.kg_state = 'new'


    @api.multi
    def button_cancel(self):
		production_plan = self.env['kg.prod.plan'].search([('kg_sf_id', '=', self.id)])
		for plan in production_plan:
			if plan.state != 'cancel':
				raise UserError(_('some production planning not in cancelled state'))
		self.kg_state = 'cancel'


    kg_state = fields.Selection([
        ('new', 'New'),
        ('approved', 'Approved'),
        ('rejected', 'Rejected'),
        ('cancel', 'Cancelled')], string='State',
        copy=False, default='new',track_visibility='onchange')



    
    name = fields.Char('Name')
    
    kg_period = fields.Many2one('kg.period','Period')
    
    kg_user_id = fields.Many2one('res.users','User',default=lambda self: self.env.user and self.env.user.id or False)
    
    kg_date  = fields.Date('Date',default=fields.Date.today())

    kg_dep = fields.Many2one('hr.department','Department')
    
    kg_lines = fields.One2many('kg.sf.lines','kg_sf_id','Lines')

    note = fields.Text('Type Here')
    
    kg_prod_plan_count = fields.Integer('Plan Count',compute='compute_plan_count')
    
    # project_id = fields.Many2one('account.analytic.account', 'Cost Center')

    purchase_req_created = fields.Boolean('Purchase Req Created')

    active = fields.Boolean('Active',default=True)

    @api.multi
    def button_group_sf(self):
        period = None
        for record in self:
            if period is not None:
                if record.kg_period.id !=period.id:
                    raise UserError(_('Only Same Period Are Allowed'))
                if record.kg_state != 'approved':
                    raise UserError(_('Only Approved Are Allowed'))
            period = record.kg_period
        vals={}
        vals['kg_period']=period.id
        vals['kg_dep']=2
        vals['purchase_req_created']=False
        vals['kg_state']='approved'
        sf_id = self.create(vals)
        for record in self:
            record.write({'active':False})

            for line in record.kg_lines:
                check=self.env['kg.sf.lines'].search([('kg_product_id','=',line.kg_product_id.id),('kg_sf_id','=',sf_id.id)])
                if not check:
                    vals_lines={}
                    vals_lines['kg_product_id']=line.kg_product_id.id
                    vals_lines['kg_qty']=line.kg_qty
                    vals_lines['kg_adj']=line.kg_adj
                    vals_lines['kg_sf_id']=sf_id.id
                    vals_lines['kg_uom_id']=line.kg_uom_id.id
                    self.env['kg.sf.lines'].create(vals_lines)
                else:
                    prev_qty = check.kg_qty
                    prev_adj = check.kg_adj
                    new_qty = prev_qty+(line.kg_qty)

                    new_adj = prev_adj+(line.kg_adj)
                    check.write({'kg_qty':new_qty,'kg_adj':new_adj})


        return True













    @api.multi
    def compute_plan_count(self):
        for record in self:
            plan_obj = self.env['kg.prod.plan']
            
            record.kg_prod_plan_count = plan_obj.search_count([('kg_sf_id', '=',record.id)]) 
            
            
            
    @api.multi
    def create_prod_plan(self):
		
		
			

        kg_state = self.kg_state
        if kg_state != 'approved':
			raise UserError(_('Approve first'))



        for record in self:
			if not record.kg_lines:
				raise UserError(_('No Products Found'))
			else:
				for line in record.kg_lines:
					vals={}
					vals['kg_product']=line.kg_product_id.id
        			# vals['project_id']=record.project_id.id

					vals['uom_id']=line.kg_uom_id.id
					vals['kg_it_planned_qty']=line.kg_qty
					vals['kg_period']=record.kg_period.id
					vals['kg_sf_id']=record.id
					vals['kg_pack'] = line.kg_product_id.kg_pack and line.kg_product_id.kg_pack.id or False
					vals['kg_core'] = line.kg_product_id.kg_core and line.kg_product_id.kg_core.id or False
					vals['kg_lbl'] = line.kg_product_id.kg_lbl
					vals['kg_hndl'] = line.kg_product_id.kg_hndl
					vals['kg_emb'] = line.kg_product_id.kg_emb
					vals['kg_lam'] = line.kg_product_id.kg_lam
					vals['kg_width'] = line.kg_product_id.kg_width
					vals['kg_perf'] = line.kg_product_id.kg_perf
					vals['kg_dia'] = line.kg_product_id.kg_dia
					vals['kg_sht'] = line.kg_product_id.kg_sht
					vals['kg_ply'] = line.kg_product_id.kg_ply
					vals['kg_gsm'] = line.kg_product_id.kg_gsm
					vals['kg_wght'] = line.kg_product_id.kg_wght
					vals['kg_trrt'] = line.kg_product_id.kg_trrt
					vals['kg_cph'] = line.kg_product_id.kg_cph
					self.env['kg.prod.plan'].create(vals)


        return True

    @api.multi
    def create_pur_req(self):
        for record in self:
           # kg_purchase_requisition_id = record.purchase_req_created and record.kg_purchase_requisition_id.id
            if record.purchase_req_created is True:
                raise UserError(_('Already purchase requisition created'))

            for prod in record.kg_lines:
                bom_obj = self.env['mrp.bom'].search([('product_tmpl_id', '=', prod.kg_product_id.product_tmpl_id.id)])

                if not bom_obj:
                    raise UserError(_('Create BOM for product: %s') % prod.kg_product_id.name)

                print bom_obj, '111111111'
                for bom_line in bom_obj.bom_line_ids:

                    check = self.env['kg.purhcase.req'].search([('kg_sf_id', '=', record.id), ('kg_product', '=', bom_line.product_id.id)])
                    print check, '1111111111111'
                    if not check:

                        kg_product = bom_line.product_id and bom_line.product_id.id
                        kg_product_uom = bom_line.product_uom_id and bom_line.product_uom_id.id
                        kg_date = record.kg_date
                        #required_date = self.req_date
                       # notes = self.notes
                        kg_qty = (prod.kg_net)*(bom_line.product_qty)
                       # kg_prod_plan_id = self.kg_prod_plan_id and self.kg_prod_plan_id.id
                        state = 'new'
                        vals = {'kg_product': kg_product,
                                'kg_product_uom': kg_product_uom,
                                'kg_date': kg_date,
                               # 'required_date': required_date,
                           #     'notes': notes,
                                'kg_qty': kg_qty,
                               # 'kg_prod_plan_id': kg_prod_plan_id,
                                'kg_sf_id':record.id,
                                'state': state}
                        self.env['kg.purhcase.req'].create(vals)
                    else:

                        old_qty = check.kg_qty

                        val = (bom_line.product_qty)*(prod.kg_net)

                        new = old_qty+val

                        check.write({'kg_qty':new})

                    record.write({'purchase_req_created':True})





            
                
            
    @api.model
    def create(self,vals):
        
        sequence_code='kg.sales.forecast'
        vals['name']= self.env['ir.sequence'].next_by_code(sequence_code) 
     
        
        
        

            
        
        return super(kg_sales_forecast, self).create(vals)
    
    



class kg_sf_lines(models.Model):
    
    _name= 'kg.sf.lines'




    @api.multi
    @api.depends('kg_adj', 'kg_qty')
    def _compute_net(self):
		for record in self:
			record.kg_net = record.kg_qty+record.kg_adj 
    
    name = fields.Char('Name')
    
    kg_product_id = fields.Many2one('product.product','Product')
    
    kg_uom_id = fields.Many2one("product.uom", related='kg_product_id.uom_id', string="UOM", readonly=True)
    
    kg_qty = fields.Float('Qty')
    
    kg_adj = fields.Float('Adjutments')
    
    kg_net = fields.Float('Net',compute='_compute_net', store=True)
    
    kg_comments = fields.Char('Comments')
    
    kg_sf_id = fields.Many2one('kg.sales.forecast','Sales Forecast ')
    
    
#    @api.onchange('kg_product_id')
#    def onchange_product(self):
#        for record in self:
#            record.kg_uom_id = record.kg_product_id.uom_id.id
    
    
#    @api.onchange('kg_qty','kg_adj')
#    def onchange_qty_adj(self):
#        for record in self:
#            record.kg_net=record.kg_qty+record.kg_adj
    
