# -*- coding: utf-8 -*-
# Date:25-04-18
#Author :Ashish Thomas
import json
from lxml import etree
from datetime import datetime
from dateutil.relativedelta import relativedelta

from odoo import api, fields, models, _
from odoo.tools import float_is_zero, float_compare
from odoo.tools.misc import formatLang

from odoo.exceptions import UserError, RedirectWarning, ValidationError

import odoo.addons.decimal_precision as dp
import logging


class SaleOrder(models.Model):
    
    _inherit='sale.order'
    
    tax_id_sale= fields.Many2one('account.tax','Transaction',default=lambda self: self.env['account.tax'].search([('name', '=', 'Domestic taxable supplies')]))


    # def _get_default_tax_sale(self):
    #     res = self.pool.get('account.tax').search([('name', '=', 'Domestic taxable supplies')])
    #     return res and res[0] or False
    #
    # defaults = {
    #     'tax_id_sale': _get_default_tax_sale,
    # }
    #

    @api.multi
    def _prepare_invoice(self):
        """
        Prepare the dict of values to create the new invoice for a sales order. This method may be
        overridden to implement custom invoice generation (making sure to call super() to establish
        a clean extension chain).
        """
        self.ensure_one()
        journal_id = self.env['account.invoice'].default_get(['journal_id'])['journal_id']
        if not journal_id:
            raise UserError(_('Please define an accounting sale journal for this company.'))
        invoice_vals = {
            'name': self.client_order_ref or '',
            'origin': self.name,
            'type': 'out_invoice',
            'tax_id_sale':self.tax_id_sale.id,
            'account_id': self.partner_invoice_id.property_account_receivable_id.id,
            'partner_id': self.partner_invoice_id.id,
            'partner_shipping_id': self.partner_shipping_id.id,
            'journal_id': journal_id,
            'currency_id': self.pricelist_id.currency_id.id,
            'comment': self.note,
            'payment_term_id': self.payment_term_id.id,
            'fiscal_position_id': self.fiscal_position_id.id or self.partner_invoice_id.property_account_position_id.id,
            'company_id': self.company_id.id,
            'user_id': self.user_id and self.user_id.id,
            'team_id': self.team_id.id
        }
        return invoice_vals
    
    
    @api.multi
    def write(self, vals):
        res= super(SaleOrder, self).write(vals)
        
        dom_taxes = self.env['account.tax'].search([('name','ilike','Domestic'),('type_tax_use','=','sale')]).ids
        print dom_taxes,'ccccccccccc'
        if self.tax_id_sale.id in dom_taxes:
        
            if not self.partner_id.country_id:
                raise UserError(_('Define Country column in Customer master'))
            
            if self.partner_id.country_id.phone_code != 971:
                raise UserError(_('Customer country and selected transaction are not matching'))
            
            
            if self.partner_id.state_id.country_id.phone_code != 971:
                raise UserError(_('Customer state and selected transaction are not matching'))
                
            
            if not self.partner_id.state_id:
                raise UserError(_('Define state column in Customer master'))
            
            
        
        
        intra_taxes = self.env['account.tax'].search([('name','ilike','Intra'),('type_tax_use','=','sale')]).ids
        print intra_taxes,'ccccccccccc'
        if self.tax_id_sale.id in intra_taxes:
        
            if not self.partner_id.country_id:
                raise UserError(_('Define Country column in Customer master'))
            
            if self.partner_id.country_id.kg_region != 'inside':
                raise UserError(_('Customer country and selected transaction are not matching'))
            
            
            if self.partner_id.state_id.country_id.phone_code == 971:
                raise UserError(_('Customer state and selected transaction are not matching'))
                
            
            if not self.partner_id.state_id:
                raise UserError(_('Define state column in Customer master'))
            
            
        export_taxes = self.env['account.tax'].search([('name','ilike','Exports'),('type_tax_use','=','sale')]).ids
        print export_taxes,'ccccccccccc'
        if self.tax_id_sale.id in export_taxes:
        
            if not self.partner_id.country_id:
                raise UserError(_('Define Country column in vendor master'))
            
            if self.partner_id.country_id.kg_region != 'outside':
                raise UserError(_('Customer country and selected transaction are not matching'))
            
            
            if self.partner_id.state_id.country_id.phone_code == 971:
                raise UserError(_('Customer state and selected transaction are not matching'))
                
            
            if not self.partner_id.state_id:
                raise UserError(_('Define state column in Customer master'))
        return res
    
    
    
    
    @api.model
    def create(self, vals):
        res= super(SaleOrder, self).create(vals)
        
        dom_taxes = self.env['account.tax'].search([('name','ilike','Domestic'),('type_tax_use','=','sale')]).ids
        print dom_taxes,'ccccccccccc'
        if res.tax_id_sale.id in dom_taxes:
        
            if not res.partner_id.country_id:
                raise UserError(_('Define Country column in Customer master'))
            
            if res.partner_id.country_id.phone_code != 971:
                raise UserError(_('Customer country and selected transaction are not matching'))
            
            
            if res.partner_id.state_id.country_id.phone_code != 971:
                raise UserError(_('Customer state and selected transaction are not matching'))
                
            
            if not res.partner_id.state_id:
                raise UserError(_('Define state column in Customer master'))
            
            
        
        
        intra_taxes = self.env['account.tax'].search([('name','ilike','Intra'),('type_tax_use','=','sale')]).ids
        print intra_taxes,'ccccccccccc'
        if res.tax_id_sale.id in intra_taxes:
        
            if not res.partner_id.country_id:
                raise UserError(_('Define Country column in Customer master'))
            
            if res.partner_id.country_id.kg_region != 'inside':
                raise UserError(_('Customer country and selected transaction are not matching'))
            
            
            if res.partner_id.state_id.country_id.phone_code == 971:
                raise UserError(_('Customer state and selected transaction are not matching'))
                
            
            if not res.partner_id.state_id:
                raise UserError(_('Define state column in Customer master'))
            
            
        export_taxes = self.env['account.tax'].search([('name','ilike','Exports'),('type_tax_use','=','sale')]).ids
        print export_taxes,'ccccccccccc'
        if res.tax_id_sale.id in export_taxes:
        
            if not res.partner_id.country_id:
                raise UserError(_('Define Country column in Customer master'))
            
            if res.partner_id.country_id.kg_region != 'outside':
                raise UserError(_('Customer country and selected transaction are not matching'))
            
            
            if res.partner_id.state_id.country_id.phone_code == 971:
                raise UserError(_('Customer state and selected transaction are not matching'))
                
            
            if not res.partner_id.state_id:
                raise UserError(_('Define state column in Customer master'))
        return res
    
    
    
    
    
    
class SaleAdvancePaymentInv(models.TransientModel):
    
    _inherit="sale.advance.payment.inv"
    
    
#     @api.multi
#     def _create_invoice(self, order, so_line, amount):
#         inv_obj = self.env['account.invoice']
#         ir_property_obj = self.env['ir.property']
# 
#         account_id = False
#         if self.product_id.id:
#             account_id = self.product_id.property_account_income_id.id or self.product_id.categ_id.property_account_income_categ_id.id
#         if not account_id:
#             inc_acc = ir_property_obj.get('property_account_income_categ_id', 'product.category')
#             account_id = order.fiscal_position_id.map_account(inc_acc).id if inc_acc else False
#         if not account_id:
#             raise UserError(
#                 _('There is no income account defined for this product: "%s". You may have to install a chart of account from Accounting app, settings menu.') %
#                 (self.product_id.name,))
# 
#         if self.amount <= 0.00:
#             raise UserError(_('The value of the down payment amount must be positive.'))
#         context = {'lang': order.partner_id.lang}
#         if self.advance_payment_method == 'percentage':
#             amount = order.amount_untaxed * self.amount / 100
#             name = _("Down payment of %s%%") % (self.amount,)
#         else:
#             amount = self.amount
#             name = _('Down Payment')
#         del context
#         taxes = self.product_id.taxes_id.filtered(lambda r: not order.company_id or r.company_id == order.company_id)
#         if order.fiscal_position_id and taxes:
#             tax_ids = order.fiscal_position_id.map_tax(taxes).ids
#         else:
#             tax_ids = taxes.ids
# 
#         invoice = inv_obj.create({
#             'name': order.client_order_ref or order.name,
#             'origin': order.name,
#             'type': 'out_invoice',
#             'reference': False,
#             'account_id': order.partner_id.property_account_receivable_id.id,
#             'partner_id': order.partner_invoice_id.id,
#             'partner_shipping_id': order.partner_shipping_id.id,
#             'tax_id_sale':order.tax_id_sale,
#             'invoice_line_ids': [(0, 0, {
#                 'name': name,
#                 'origin': order.name,
#                 'account_id': account_id,
#                 'price_unit': amount,
#                 'quantity': 1.0,
#                 'discount': 0.0,
#                 'uom_id': self.product_id.uom_id.id,
#                 'product_id': self.product_id.id,
#                 'sale_line_ids': [(6, 0, [so_line.id])],
#                 'invoice_line_tax_ids': [(6, 0, tax_ids)],
#                 'account_analytic_id': order.project_id.id or False,
#             })],
#             'currency_id': order.pricelist_id.currency_id.id,
#             'payment_term_id': order.payment_term_id.id,
#             'fiscal_position_id': order.fiscal_position_id.id or order.partner_id.property_account_position_id.id,
#             'team_id': order.team_id.id,
#             'user_id': order.user_id.id,
#             'comment': order.note,
#         })
#         invoice.compute_taxes()
#         invoice.message_post_with_view('mail.message_origin_link',
#                     values={'self': invoice, 'origin': order},
#                     subtype_id=self.env.ref('mail.mt_note').id)
#         return invoice

