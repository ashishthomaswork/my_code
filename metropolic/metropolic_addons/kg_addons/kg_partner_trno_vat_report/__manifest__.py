{
    'name': 'Partner TRNO',
    'summary': """TRNO IN CUSTOMER/SUPPLIER""",
    'version': '1',
    'description': """This module adds a field for adding TRNO in Customer/Supplier Record""",
    'author': 'KG',

    'category': 'Tools',
    'depends': ['base_setup', 'report','base', 'account_accountant', 'sale', 'purchase', 'stock', 'account'],
    'license': 'AGPL-3',
    'data': [
         'data/purchase_tax_data.xml', 
          'data/sale_tax_data.xml', 
               'data/vat_account_data.xml', 
   'views/kg_partner_trno_view.xml', 
   'views/kg_po_view.xml',
    'views/kg_so_view.xml', 
    'views/kg_invoice_view.xml',
    'views/kg_journal_view.xml',
    'views/kg_payment_view.xml',
    'views/kg_country_view.xml',
    
   "wizard/vat_wizard_view.xml",
     "views/report_taxpay.xml",
         "views/tax_reports.xml",
    "wizard/vat_wizard_po_view.xml",
    "wizard/tax_wizard_view.xml",
   
    ],
    'demo': [],
    'installable': True,
    'auto_install': False,
}
