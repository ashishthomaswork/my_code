# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from collections import defaultdict

from odoo import api, fields, models, _
from odoo.exceptions import UserError
from odoo.tools import float_compare, float_round

import logging
_logger = logging.getLogger(__name__)

class StockQuant(models.Model):
    _inherit = "stock.quant"






    def _quants_get_reservation_domain(self, move, pack_operation_id=False, lot_id=False, company_id=False, initial_domain=None):
        initial_domain = initial_domain if initial_domain is not None else [('qty', '>', 0.0)]
        domain = initial_domain + [('product_id', '=', move.product_id.id)]

        print "wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww111111deqw"

        if pack_operation_id:
            pack_operation = self.env['stock.pack.operation'].browse(pack_operation_id)
            domain += [('location_id', '=', pack_operation.location_id.id)]
            if pack_operation.owner_id:
                domain += [('owner_id', '=', pack_operation.owner_id.id)]
            if pack_operation.package_id and not pack_operation.product_id:
                domain += [('package_id', 'child_of', pack_operation.package_id.id)]
            elif pack_operation.package_id and pack_operation.product_id:
                domain += [('package_id', '=', pack_operation.package_id.id)]
            else:
                domain += [('package_id', '=', False)]
        else:

            print "CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCc11111111/****"			
            domain += ['|',('location_id', 'child_of', move.location_id.id),('location_id', '=', move.location_id.id)]
            if move.restrict_partner_id:
                domain += [('owner_id', '=', move.restrict_partner_id.id)]

        if company_id:
            domain += [('company_id', '=', company_id)]
        else:
            domain += [('company_id', '=', move.company_id.id)]
        print "qqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqq999999999999999999999999999999999999999999----DOMAIN------qqqq",domain
		

        return domain












    def _create_account_move_line(self, move, credit_account_id, debit_account_id, journal_id):
        # group quants by cost
        quant_cost_qty = defaultdict(lambda: 0.0)
        for quant in self:
            quant_cost_qty[quant.cost] += quant.qty

        AccountMove = self.env['account.move']

        print "vvvvvvvvvvvvvvvvvvv",move

        production_id = move.production_id and move.production_id.id
        production_name = move.production_id and move.production_id.name
        raw_material_production_name = move.raw_material_production_id and move.raw_material_production_id.name
        raw_material_production_id = move.raw_material_production_id and move.raw_material_production_id.id
        for cost, qty in quant_cost_qty.iteritems():
            move_lines = move._prepare_account_move_line(qty, cost, credit_account_id, debit_account_id)
            print "gggggggggggggggggggggggggggg",move_lines
            if move_lines:
                date = self._context.get('force_period_date', fields.Date.context_today(self))


                new_account_move = AccountMove.create({
                    'journal_id': journal_id,
                    'line_ids': move_lines,
                    'date': date,
                    'ref': move.picking_id.name or production_name or raw_material_production_name})
                print "8888888888888888888888888888888",new_account_move
                new_account_move.post()

            	if production_id or raw_material_production_id:
                	kg_mrp_id = production_id or raw_material_production_id
                	self.env['kg.mrp.account.lines'].create({'kg_mrp_id':kg_mrp_id,'account_move_id':new_account_move.id,
'internal_sequence_number':new_account_move.name,'ref':new_account_move.ref,'journal_id':new_account_move.journal_id and new_account_move.journal_id.id,'status':new_account_move.state,'amount':new_account_move.amount,'product_id':move.product_id and move.product_id.id or False})



    def _account_entry_move(self, move):
        """ Accounting Valuation Entries """

        print "rocks--------------------------------------------------------------------------"
        if move.product_id.type != 'product' or move.product_id.valuation != 'real_time':
            # no stock valuation for consumable products
            return False
        if any(quant.owner_id or quant.qty <= 0 for quant in self):
            # if the quant isn't owned by the company, we don't make any valuation en
            # we don't make any stock valuation for negative quants because the valuation is already made for the counterpart.
            # At that time the valuation will be made at the product cost price and afterward there will be new accounting entries
            # to make the adjustments when we know the real cost price.
            return False
###riyas need to enable####
#        parameter_obj = self.env['ir.config_parameter']
##        parameter_ids = parameter_obj.search([('key', '=', 'od_production_acc')])
##        if not parameter_ids:
##            raise osv.except_osv(_('Settings Warning!'),_('No production account defined\nPlz config it in System Parameters with od_production_acc!'))
#        parameter_data = parameter_obj.browse(cr,uid,parameter_ids)
#        acc_id = parameter_data.od_model_id and parameter_data.od_model_id.id 



        parameter_obj = self.env['ir.config_parameter']
        key =[('key', '=', 'kg_production_acc')]
        param_obj = parameter_obj.search(key)
        if not param_obj:



            raise UserError(_('No production account defined\nPlz config it in System Parameters with kg_production_acc!'))
        acc_id = int(param_obj.value)
#        acc_id = 12
        location_from = move.location_id
        location_to = self[0].location_id  # TDE FIXME: as the accounting is based on this value, should probably check all location_to to be the same
        company_from = location_from.usage == 'internal' and location_from.company_id or False
        company_to = location_to and (location_to.usage == 'internal') and location_to.company_id or False


        print "ddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddd++++++"







        # Create Journal Entry for products arriving in the company; in case of routes making the link between several
        # warehouse of the same company, the transit location belongs to this company, so we don't need to create accounting entries
        if company_to and (move.location_id.usage not in ('internal', 'transit') and move.location_dest_id.usage == 'internal' or company_from != company_to):

            print "GGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGG"
            journal_id, acc_src, acc_dest, acc_valuation = move._get_accounting_data_for_valuation()
            if location_from and location_from.usage == 'customer':  # goods returned from customer
                self.with_context(force_company=company_to.id)._create_account_move_line(move, acc_dest, acc_valuation, journal_id)
                print "ttttttttttttttttttttttttttttttttttttttttttttttttttttttttt"
            #####Riyas logic rocks################
            elif location_from and location_from.usage == 'production':
                print "ffffffffffffffffffffffffffffffffffffffffffff"
                self.with_context(force_company=company_to.id)._create_account_move_line(move, acc_id, acc_valuation, journal_id)
#####riyas block ends here######
            else:
                print "ttttttttttttttttttttttttttttttttttttt5555tttttttttttttttttttt"
                self.with_context(force_company=company_to.id)._create_account_move_line(move, acc_src, acc_valuation, journal_id)

        print "fffffffffffffffffffffffffffffffffffffffffffffffffffffffff-************"


        # Create Journal Entry for products leaving the company
        if company_from and (move.location_id.usage == 'internal' and move.location_dest_id.usage not in ('internal', 'transit') or company_from != company_to):
            journal_id, acc_src, acc_dest, acc_valuation = move._get_accounting_data_for_valuation()
            if location_to and location_to.usage == 'supplier':  # goods returned to supplier
                self.with_context(force_company=company_from.id)._create_account_move_line(move, acc_valuation, acc_src, journal_id)
                print "ttttttttttttttttttttttttttttttttttttt5555ttttttttttttttttt444ttt"
###riyas custom logic ############
            elif location_to and location_to.usage == 'production':
                print "ttttttttttttttttttttttttttttttttttttt5555tttttt666666tttttttttttttt"
                self.with_context(force_company=company_from.id)._create_account_move_line(move, acc_valuation, acc_id, journal_id)
                print "ttttttttttttttttttttttttttttttttttttt5555ttttttttttttttttt333ttt"
###############riyas custom logic
            else:
                print "tttttttttttwwwwwwtttttttttttttttttttttttttt5555tttttttttttttttttttt"
                self.with_context(force_company=company_from.id)._create_account_move_line(move, acc_valuation, acc_dest, journal_id)

        if move.company_id.anglo_saxon_accounting and move.location_id.usage == 'supplier' and move.location_dest_id.usage == 'customer':
            print "dddddddddddddddddddddddddddddddddddddddddddddddddd99dddddd55555555"
            # Creates an account entry from stock_input to stock_output on a dropship move. https://github.com/odoo/odoo/issues/12687
            journal_id, acc_src, acc_dest, acc_valuation = move._get_accounting_data_for_valuation()
            print "dddddddddddddddddddddddddddddddddddddddddddddddddddddddd55555555"
            self.with_context(force_company=move.company_id.id)._create_account_move_line(move, acc_src, acc_dest, journal_id)
