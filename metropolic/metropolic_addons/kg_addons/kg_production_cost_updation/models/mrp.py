# -*- coding: utf-8 -*-
from odoo import models,api,fields,_

# -*- coding: utf-8 -*-
from odoo import models,api,fields
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT, float_compare
from odoo.exceptions import UserError
from copy import copy

class mrp_production(models.Model):
    
    _inherit="mrp.production"

    @api.one
    @api.depends('kg_overhead_line.amount','kg_overhead_line.kg_cost')
    def _compute_overhead_total(self):
		tot = 0
		kg_overhead_line = self.kg_overhead_line
		for line in kg_overhead_line:
			tot = tot + line.amount
            

		self.kg_overhead_total = tot
        
    @api.model
    def _default_prod_lines(self):
#        cat_ids =self.env['kg.category'].search([])
        b=[]
        
        misc_id =self.env['product.product'].search([('name','=','Misc Cost')])
        if misc_id:
            a={}
            a['kg_product']=misc_id.id
            a['kg_cost']=0.25
            c=(0,0,a)
            b.append(c)
        misc_id =self.env['product.product'].search([('name','=','Exegenecies')])
        if misc_id:
            a={}
            a['kg_product']=misc_id.id
            a['kg_cost']=0.2
            c=(0,0,a)
            b.append(c)
        return b

    kg_acc_list = fields.One2many('kg.mrp.account.lines','kg_mrp_id')
    kg_overhead_line  = fields.One2many('kg.prod.cost','kg_mrp_id','Product Cost List',default=lambda self: self._default_prod_lines())

    kg_overhead_total = fields.Float('Overhead Total',compute="_compute_overhead_total",store=True) 

#@RRM  overriding function to calculate finshed product unit cost
    @api.multi
    def post_inventory(self):

        for order in self:
            moves_to_do = order.move_raw_ids.filtered(lambda x: x.state not in ('done', 'cancel'))

            print "Hello---------------------------------------",moves_to_do
            rawmaterial_moveids = []
            total_rawmaterial_cost = 0
            for rawmaterial in order.move_raw_ids:
				print "vvvvvvvvvvvvveeeeeee",rawmaterial
				rawmaterial_moveids.append(rawmaterial.id)

            print "ffffffffffffffffffffffffffffffffff",rawmaterial_moveids
            rawmaterials = self.env['stock.move'].browse(rawmaterial_moveids)
            print "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee",rawmaterials
            moves_to_do.action_done()


            print "how u ---------------------------------------",rawmaterials
            for line in rawmaterials:
				default_uom = line.product_id and line.product_id.uom_id
				###since rawmaterial price unit/cost in move line is based on the default uom mentioned in the product master
                ####in odoo if you purchased in any unit the final cost of each single piece will be based on product master uom

                #####if you directly multiply with qty_done it will be wrong
                ####so converted in to default uom qty
				actual_qty_done_default_uom = line.product_uom._compute_quantity(line.quantity_done,default_uom)

                ##############i got unit price in default uom(by default u will get) based and qunatity also I converted to default uom
				####now cost will be fine
				line_cost = line.price_unit * actual_qty_done_default_uom
				total_rawmaterial_cost = total_rawmaterial_cost + (line_cost)

            order._cal_price(moves_to_do)
            moves_to_finish = order.move_finished_ids.filtered(lambda x: x.state not in ('done','cancel'))
            qty_done = moves_to_finish.quantity_done
#            uom_rate = moves_to_finish.product_uom.factor_inv
#            if not uom_rate:
#                raise UserError(_('uom ratio not defined') % moves_to_finish.product_uom.name)
            uom_type = moves_to_finish.product_uom.uom_type

            default_uom = moves_to_finish.product_id and moves_to_finish.product_id.uom_id

			### to restrict final product to be pro
            if default_uom.id != moves_to_finish.product_uom.id:
				raise UserError(_('the finshed product UOM should be same as its default uom mentioned i n product master') %moves_to_finish.product_uom.name)

            total_overhead_cost = self.kg_overhead_total or 0
#            if (not total_overhead_cost) and (not self.kg_allow_zer_overheadcost):
#				raise UserError(_('you didnt define any overhead for this mrp,if you want to put oberhead go to Overhead tab and define overhead value or if you want to continue with zero overhead go to Miscellaneous Tab and tick Allow Zero Cost Overhead'))

            print "vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvtotal_overhead_cost",total_overhead_cost
            print "vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvtotal_rawmaterial_cost",total_rawmaterial_cost			
			
            total_cost = total_rawmaterial_cost + total_overhead_cost

            if qty_done:
                unit_cost = float(total_cost) / float(qty_done)
			
            	
	
            	moves_to_finish.write({'price_unit':unit_cost})		


            moves_to_finish.action_done()
            for move in moves_to_finish:
                #Group quants by lots
                lot_quants = {}
                raw_lot_quants = {}
                quants = self.env['stock.quant']
                if move.has_tracking != 'none':
                    for quant in move.quant_ids:
                        lot_quants.setdefault(quant.lot_id.id, self.env['stock.quant'])
                        raw_lot_quants.setdefault(quant.lot_id.id, self.env['stock.quant'])
                        lot_quants[quant.lot_id.id] |= quant
                for move_raw in moves_to_do:
                    if (move.has_tracking != 'none') and (move_raw.has_tracking != 'none'):
                        for lot in lot_quants:
                            lots = move_raw.move_lot_ids.filtered(lambda x: x.lot_produced_id.id == lot).mapped('lot_id')
                            raw_lot_quants[lot] |= move_raw.quant_ids.filtered(lambda x: (x.lot_id in lots) and (x.qty > 0.0))
                    else:
                        quants |= move_raw.quant_ids.filtered(lambda x: x.qty > 0.0)
                if move.has_tracking != 'none':
                    for lot in lot_quants:
                        lot_quants[lot].sudo().write({'consumed_quant_ids': [(6, 0, [x.id for x in raw_lot_quants[lot] | quants])]})
                else:
                    move.quant_ids.sudo().write({'consumed_quant_ids': [(6, 0, [x.id for x in quants])]})
            order.action_assign()
            if self.kg_overhead_line:
				order.kg_create_move()
#            if not order.kg_allow_zer_overheadcost:
            order.button_mark_done()
			
        return True

    @api.multi
    def button_mark_done(self):
        self.ensure_one()
        for wo in self.workorder_ids:
            if wo.time_ids.filtered(lambda x: (not x.date_end) and (x.loss_type in ('productive', 'performance'))):
                raise UserError(_('Work order %s is still running') % wo.name)
#        self.post_inventory()
        moves_to_cancel = (self.move_raw_ids | self.move_finished_ids).filtered(lambda x: x.state not in ('done', 'cancel'))
        moves_to_cancel.action_cancel()
        self.write({'state': 'done', 'date_finished': fields.Datetime.now()})
        self.env["procurement.order"].search([('production_id', 'in', self.ids)]).check()
        self.write({'state': 'done'})

    def get_id_from_param(self,param):
        parameter_obj = self.env['ir.config_parameter']
        key =[('key', '=', param)]
        param_obj = parameter_obj.search(key)
        if not param_obj:



            raise UserError(_('NoParameter Not defined\nconfig it in System Parameters with %s') % param)
        result_id = param_obj.value 
        return result_id


    @api.multi
    def kg_update_bom_info(self):
        production = self
        bom_obj = self.bom_id
        product_qty = self.product_qty
        mo_id = self.id

        production.product_qty = product_qty
        wizard_obj = self.env['change.production.qty'].create({'mo_id':mo_id,'product_qty':product_qty})


        wizard_obj.change_prod_qty()
        return True


    def kg_get_move_vals(self):
    
        date = fields.Date.context_today(self)
      
        ref = self.name
        journal_id = self.get_id_from_param('kg_overhead_journal')
        move_vals = {'date': date,'journal_id': journal_id,'ref':ref}
        return move_vals

    @api.multi
    def kg_create_move(self):
        self_id = self.id
        move_obj = self.env['account.move']
        move_vals = self.kg_get_move_vals()
        planned_qty = self.product_qty
#        factor = producing_qty/planned_qty
        move_lines = self.kg_get_account_move_lines()

        print "cvfffffffffffffffffffffffffffffsdddwweee",move_lines
		
        vals = copy(move_vals)
        vals.update({'line_ids':move_lines})
        move_id = move_obj.create(vals).id
        if move_id:
            account_move_obj = self.env['account.move'].browse(move_id)
            account_move_obj.post()



            self.env['kg.mrp.account.lines'].create({'kg_mrp_id':self_id,'account_move_id':account_move_obj.id,
'internal_sequence_number':account_move_obj.name,'ref':account_move_obj.ref,'journal_id':account_move_obj.journal_id and account_move_obj.journal_id.id,'status':account_move_obj.state,'amount':account_move_obj.amount})
        return move_id



    def kg_get_account_move_lines(self):
        move_lines =[]
        debit_amount = 0.0
        
        cred_amount  = 0.0
        ref = self.name
        date = fields.Date.context_today(self)
        move_line_vals = {'name': ref,'ref': ref,'date': date}
        mrp_obj = self
        for line in self.kg_overhead_line:
            qty = line.kg_qty_hour
            product_id = line.kg_product and line.kg_product.id or False
            account_id = line.kg_product and line.kg_product.categ_id and line.kg_product.categ_id.property_account_expense_categ_id and line.kg_product.categ_id.property_account_expense_categ_id.id or False
            print "vvvvvvvvvvvvvvvvvvvvvvvvvveeeeeeeeeeeeeeeeeeeeeeeeeeeeee111111111",account_id
			
            if not account_id:
                raise UserError(_('Overhead Product %s Expense Account Not Set') %line.product_id.name)

            if line.kg_auto:
                eval_code = line.kg_product and  line.kg_product.kg_eval_code
                if not eval_code:
                    raise UserError(_('Product %s Overhead Eval Code Expression Not set') %line.product_id.name)
                
                cost = eval(eval_code)
                cred_amount = float(qty * cost,2)
            else:
                cred_amount = float(line.amount)
           
            vals = copy(move_line_vals)
            vals.update({'account_id':account_id,'credit':cred_amount,'debit':0.0,'product_id':product_id,'quantity': qty})
            move_lines.append((0,0,vals))
            debit_amount += cred_amount
                
                
                
                
 
                    
        debit_lines = copy(move_line_vals)
        acc_id = int(self.get_id_from_param('kg_production_acc'))
        move_line_length =len(move_lines) - 1
        print "ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc++++++++++++++++++",move_line_length		
        cr_amount = 0.0
        for cr_index in range(move_line_length):
            cr_amount += move_lines[cr_index][2]['credit']
        credit_amount = debit_amount - cr_amount
        move_lines[move_line_length][2]['credit'] = credit_amount
        debit_lines.update({'account_id':acc_id,'debit':debit_amount,'credit':0.0})
        move_lines.append((0,0,debit_lines))
        mrp_obj.write({'kg_at_least_produced_once':True})
        return move_lines



    
    
class kg_prod_cost(models.Model):
    
    _name="kg.prod.cost"


    @api.one
    @api.depends('kg_qty_hour','kg_cost')
    def _compute_amount(self):
        self.amount = self.kg_qty_hour * self.kg_cost

    
    kg_product= fields.Many2one('product.product','Product')
    
    kg_qty_hour =fields.Float('Qty/Hour')
    
    kg_auto = fields.Float('Auto')
    
    kg_cost = fields.Float('Cost')

    amount = fields.Float(string="Amount",compute="_compute_amount")
    
    kg_mrp_id = fields.Many2one('mrp.production','Production Order')



class kg_mrp_account_lines(models.Model):
    _name = 'kg.mrp.account.lines'
    _description = 'kg.mrp.account.lines'
    kg_mrp_id = fields.Many2one('mrp.production','Production Order')
    product_id = fields.Many2one('product.product','Product')
    account_move_id = fields.Many2one('account.move',string="Number")
    internal_sequence_number = fields.Char(string="Internal Number")
    ref = fields.Char(string="Reference")
    journal_id = fields.Many2one('account.journal',string="Journal")
    amount = fields.Float(string='Amount')
    status = fields.Char(string='Status')
