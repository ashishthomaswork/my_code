# -*- coding: utf-8 -*-
{
    "name" : "KG Production Cost Updation",
    "version" : "0.1",
    "author": "KG",
    "category" : "Others",
    "description": """KG Production Cost Updation """,
    "depends": ['mrp','analytic'],
    "data" : ['data/def_product.xml',
              'views/mrp_view.xml',
			'views/mrp_bom_view.xml'
          
                      
            ],
    'css': [],
    'installable': True,
    'auto_install': False,
    'application': True,
}
