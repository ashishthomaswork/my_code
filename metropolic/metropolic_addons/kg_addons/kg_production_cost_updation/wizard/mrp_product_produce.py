# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from datetime import datetime

from odoo import api, fields, models, _
from odoo.addons import decimal_precision as dp
from odoo.exceptions import UserError
from odoo.tools import float_compare, float_round

class MrpProductProduce(models.TransientModel):
    _inherit = "mrp.product.produce"

    @api.multi
    def do_produce(self):
        product_qty = self.product_qty
        finished_pdct_qty = 0

       #production = self.env['mrp.production'].browse(self._context['active_id'])
        production = self.production_id

        production.kg_update_bom_info()
		

        move_finished_ids = production.move_finished_ids


        move_raw_ids = production.move_raw_ids

		


        print "vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvmove_finished_ids",move_finished_ids
        for fp in move_finished_ids:
			finished_pdct_qty = finished_pdct_qty + fp.product_uom_qty

        if product_qty != finished_pdct_qty:
			raise UserError(_('The finished product line and wizard quantity must be equal,if you want to update quantity first change in finished product line produced field first'))
		
        res = super(MrpProductProduce, self).do_produce()

        for rawline in move_raw_ids:
			if rawline.state != 'assigned':
				raise UserError(_('one of the raw material is not available'))
			product_uom_qty = rawline.product_uom_qty
			rawline.quantity_done = product_uom_qty



				
        return res

   
