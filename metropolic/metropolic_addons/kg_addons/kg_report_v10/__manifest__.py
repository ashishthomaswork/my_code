# -*- coding: utf-8 -*-
{
    "name" : "KG Report v10",
    "version" : "0.1",
    "author": "KG",
    "category" : "Base",
    "description": """KG Reporting Template""",
    "website": ["http://www.klystronglobal.com"],
    "depends": ['report'],
    "external_dependencies": {

        'python' : ['num2words'],

    },
    "data" : [
              # 'od_ir_actions_report_xml_view.xml',
              'od_ir_ui_view_view.xml',
              'company_view.xml'
            ],
    'css': [],
}


