# -*- coding: utf-8 -*-
{
    'name': 'Quotation Revision',
    'summary': 'Quotation Revised History',
    'website': 'http://www.aktivsoftware.com',
    'description': 'We can manage Quotation Revised History in sale order.',
    'license': 'AGPL-3',
    'author': 'Aktiv Software',
    'category': 'Sales',
    'version': '10.0.1.0.0',
    'depends': ['sale'],
    'data': [
        'views/sale_order_views.xml',
    ],
    'images': ['static/description/banner.jpg'],
    'auto_install': False,
    'installable': True,
    'application': False
}