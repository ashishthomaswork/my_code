# -*- coding: utf-8 -*-
{
    "name" : "KG PDC Management",
    "version" : "0.1",
    "author": "KG",
    "category" : "Others",
    "description": """KG PDC Management """,
    "depends": ['account_accountant'],
    "data" : [
         
             'views/account_payment_view.xml'  

            ],
    'css': [],
    'installable': True,
    'auto_install': False,
    'application': True,
}
