# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from itertools import groupby
from datetime import datetime, timedelta

from odoo import api, fields, models, _
from odoo.exceptions import UserError
from odoo.tools import float_is_zero, float_compare, DEFAULT_SERVER_DATETIME_FORMAT
from odoo.tools.misc import formatLang

import odoo.addons.decimal_precision as dp

class AccountPayment(models.Model):
    _inherit = 'account.payment'


    @api.multi
    def release_pdc(self):
		
			


        if self.kg_cleared:
			raise UserError(_('it is already cleared'))
        self.kg_cleared = True


    kg_pdc = fields.Boolean(string='PDC')
    kg_cleared = fields.Boolean(string='Cleared')

