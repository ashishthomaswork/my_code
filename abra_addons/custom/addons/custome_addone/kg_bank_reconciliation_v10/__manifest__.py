# -*- coding: utf-8 -*-
{
    "name" : "KG Bank Reconciliation",
    "version" : "0.1",
    "author": "KGERP",
    "category" : "Accounting",
    "description": """KGERP Accounting""",
    "depends": ['account_accountant'],
    "data" : [
               'views/account_view.xml',
               'views/bank_reconciliation_view.xml',
            ],
    'css': [],
    'installable': True,
    'application': True,
    'auto_install': False,
}
