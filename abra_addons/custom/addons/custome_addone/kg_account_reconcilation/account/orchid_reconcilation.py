# -*- coding: utf-8 -*-
from odoo import fields,models,api,_
from odoo import tools
from pprint import pprint
from odoo.exceptions import Warning

class OrchidAccountReconcilation(models.Model):
    _name = "orchid.account.reconcilation"
    name = fields.Char(string="Name",required=True,default='/',readonly=True)
    
    payment_id = fields.Many2one('account.payment',string="Payment",readonly=True)
    payment_type = fields.Selection([('inbound','Receive Money'),('outbound','Send Money')],string="Payment Type",required=True)
    partner_id = fields.Many2one('res.partner',string="Partner",required=True)
#     move_line_id = fields.Many2one('account.move.line',string="Payment Move Line",compute="_get_payment_move_line",readonly=True)
    
    amount = fields.Monetary(string='Amount Available', required=True,compute="_get_balance_amount")
    currency_id = fields.Many2one('res.currency', string='Currency', required=True, default=lambda self: self.env.user.company_id.currency_id)
#     type = fields.Selection([('customer','Customer'),('supplier','Supplier')],string="Partner Type",required=True)
    debit_lines = fields.One2many('orchid.debit.lines','rec_id',string="Debit Lines")
    credit_lines = fields.One2many('orchid.credit.lines','rec_id',string="Credit Lines")
    writeoff_lines = fields.One2many('orchid.writeoff.lines','rec_id',string="WriteOff Lines")   
    state = fields.Selection([('draft','Draft'),('confirm','Confirm'),('reconciled','Reconciled')],string="Status",default='draft')
    writeoff = fields.Boolean(string="Write-Off")
    writeoff_account_id = fields.Many2one('account.account',string='Post Difference In')
    writeoff_amount = fields.Float(string="Difference Amount",compute="_compute_diff_amount")
    writeoff_journal_id = fields.Many2one('account.journal',string='Journal')
    writeoff_narration = fields.Char(string="Narration")
    writeoff_move_id = fields.Many2one('account.move',string="Write-off Move")
    analytic_account_id = fields.Many2one('account.analytic.account',string="Analytic Account")
    group_pay = fields.Boolean(string="Group Pay")
    od_partner_ids = fields.Many2many('res.partner','od_reconcile_partner_rel','rec_id','partner_id',string='Partners')
    


    


     
    @api.model
    def default_get(self, fields):
        res = super(OrchidAccountReconcilation, self).default_get(fields)
        res_obj = self.env['res.partner'].search([])
        part = []
        for partner in res_obj:
        	part.append(partner.id)
        if 'od_partner_ids' not in res:
        	res['od_partner_ids'] = [(6,0,part)]
        else:
        	payment_id = res['payment_id']
        	already_reco_obj = self.env['orchid.account.reconcilation'].search([('payment_id','=',payment_id)])
        	used_partners = []
        	if already_reco_obj:
        		for u_p in already_reco_obj:
        			used_partners.append(u_p.partner_id and u_p.partner_id.id)
        	default_partners = res['od_partner_ids'][0][2]
        	new_aprtner_list = list(set(default_partners) - set(used_partners))
        	if len(new_aprtner_list) == 0:
        		raise Warning("there is no partner's data pending for reconcilation,already records existing,if you want to create delete that records")
        		
        	res['od_partner_ids'] = [(6,0,new_aprtner_list)]
        	res['partner_id'] = new_aprtner_list[0]
        return res

    
    @api.model
    def create(self,vals):
        SequenceObj = self.env['ir.sequence']
        st_number = SequenceObj.next_by_code('orchid.account.reconcilation')
        vals['name'] = st_number
        return super( OrchidAccountReconcilation, self).create( vals)
    @api.one 
    def unlink(self):
        if self.state == 'reconciled':
            raise Warning("You Can't Delete The Reconciled Record,Please make it Unreconcile for Delete")
        return super(OrchidAccountReconcilation,self).unlink()
    
    def get_diff_amount(self):
        diff_amount = self.calc_diff_amount()
        self.writeoff_amount = diff_amount
        return diff_amount
    def calc_diff_amount(self):
        amount_available = abs(self.amount)
        amount_to_allocate = 0.0 
        for line in self.debit_lines:
            amount_to_allocate += abs(line.amount)
        return amount_to_allocate - amount_available
    @api.one 
    @api.depends('debit_lines')
    def _compute_diff_amount(self):
        self.writeoff_amount = self.calc_diff_amount()
#     @api.one 
#     @api.depends('payment_id')
#     def _get_payment_move_line(self):
#         payment_id = self.payment_id and self.payment_id.id
#         payment_type  = self.payment_type
#         domain = []
#         if payment_type == 'inbound':
#             domain = [('payment_id','=',payment_id),('credit','>',0.0)]
#         elif payment_type == 'outbound':
#             domain = [('payment_id','=',payment_id),('debit','>',0.0)]
#             
#         if payment_id and domain:
#             mv_lines =self.env['account.move.line'].search(domain,limit=1)
#             if mv_lines:
#                 self.move_line_id = mv_lines.id
    
    @api.one 
    @api.depends('credit_lines')
    def _get_balance_amount(self):
        amount = 0.0
        for line in self.credit_lines:
            amount += line.amount
        self.amount = amount
    
    
    @api.multi
    def button_reset(self):
        self.write({'state':'draft'})
    
    @api.multi
    def button_confirm(self):
        payment_type  = self.payment_type
        account_id = False
        partner_id = self.partner_id and self.partner_id.id
        if payment_type == 'inbound':
            account_id = self.partner_id and self.partner_id.property_account_receivable_id and self.partner_id.property_account_receivable_id.id
            print "///////////////////////1111",account_id 
        elif payment_type == 'outbound':
            account_id = self.partner_id and self.partner_id.property_account_payable_id and self.partner_id.property_account_payable_id.id
            print "///////////////////////999999999",account_id        
        move_lines =[]
#        if self.group_pay and self.payment_id:
#            for line in self.payment_id.group_line:
#                partner_id = line.child_partner_id and line.child_partner_id.id
#                move_lines += self.env['account.move.line'].get_move_lines_for_manual_reconciliation(account_id, partner_id=partner_id, excluded_ids=None, str=False, offset=0, limit=None, target_currency_id=False)
#        else:
#            move_lines =self.env['account.move.line'].get_move_lines_for_manual_reconciliation(account_id, partner_id=partner_id, excluded_ids=None, str=False, offset=0, limit=None, target_currency_id=False)
        
        move_lines =self.env['account.move.line'].get_move_lines_for_manual_reconciliation(account_id, partner_id=partner_id, excluded_ids=None, str=False, offset=0, limit=None, target_currency_id=False)            
        debit_lines = []
        credit_lines = []
        print "555555555555555555555555555555555555555555move_lines",move_lines
        
        for line in move_lines:
            line['move_line_id'] = line['id']
            
            if line.get('debit',0.0) != 0.0:
                line['balance'] = line['debit']
                debit_lines.append(line)
            if line.get('credit',0.0) != 0.0 :
                line['balance'] = line['credit']
                credit_lines.append(line)
        self.debit_lines.unlink()
        self.credit_lines.unlink()
        if payment_type == 'inbound':
            self.debit_lines  = debit_lines
            self.credit_lines = credit_lines
        elif payment_type == 'outbound':
            self.debit_lines  = credit_lines
            self.credit_lines = debit_lines
        
        
        self.write({'state':'confirm'})
        
        
    @api.multi 
    def button_unreconcile(self):
        for line in self.debit_lines:
            line.move_line_id.remove_move_reconcile()

            	
        self.writeoff_move_id.button_cancel()
        self.writeoff_move_id.line_ids.remove_move_reconcile()
        self.writeoff_move_id.unlink()
        
        
        self.write({'state':'confirm'})
    @api.multi
    def button_process(self):
        amount = abs(self.amount)
        for line in self.debit_lines:
            if amount > 0.0:
                amount_residual = abs(line.amount_residual)
                amount_to_allocate = min(amount_residual,amount)
                line['amount'] = amount_to_allocate
                amount -= amount_to_allocate
    
    def check_amount(self):
        check_amount = 0.0
        amount = self.amount 
        for line in self.debit_lines:
            check_amount += line.amount
#             if line.amount > abs(line.amount_residual):
#                 raise Warning("Allocating More than  Amount in some lines ,Please Check it")
#         for line in self.credit_lines:
#             if line.amount > abs(line.amount_residual):
#                 raise Warning("Allocating More than Amount in some lines ,Please Check it")
        print "check amount"
#        if check_amount > amount and not self.writeoff :
#            raise Warning("Amount Allocation Exceeded")
#        if self.writeoff and check_amount > amount + self.writeoff_amount:
#            raise Warning("Amount Allocation Exceeded")
    
    
    def create_writeoff_move(self,writeoff_lines):
        journal_id = self.writeoff_journal_id and self.writeoff_journal_id.id
        analytic_account_id = self.analytic_account_id and self.analytic_account_id.id or False
        date = fields.Date.today()
        move_vals = {'journal_id':journal_id,'date':date,'ref':self.payment_id.name}
        move_line = []
        debit_line_vals = False
        credit_line_vals = False
        payment_type  = self.payment_type
        name = self.writeoff_narration
        partner_id = self.partner_id and self.partner_id.id or False
        
        
        
        if payment_type == 'inbound':
            amount = self.writeoff_amount
            debit_account_id = False
            credit_account_id = False
            if amount < 0.0:
            	
                debit_account_id = self.partner_id and self.partner_id.property_account_receivable_id and self.partner_id.property_account_receivable_id.id
                debit_line_vals = (0,0,{'account_id':debit_account_id,'name':name,'credit':0.0,'debit':abs(amount),'partner_id':partner_id,'analytic_account_id':analytic_account_id})
                move_line.append(debit_line_vals)
                for line in writeoff_lines:
                	credit_account_id = line.writeoff_account_id and line.writeoff_account_id.id
                	credit_line_vals =  (0,0,{'account_id':credit_account_id,'name':line.writeoff_narration,'credit':abs(line.amount),'debit':0.0,'partner_id':partner_id,'analytic_account_id':line.analytic_account_id and line.analytic_account_id.id})
                	move_line.append(credit_line_vals) 
                	
                
                
            else:
                
                credit_account_id = self.partner_id and self.partner_id.property_account_receivable_id and self.partner_id.property_account_receivable_id.id
                credit_line_vals =  (0,0,{'account_id':credit_account_id,'name':name,'credit':abs(amount),'debit':0,'partner_id':partner_id,'analytic_account_id':analytic_account_id})
                move_line.append(credit_line_vals)
                for line in writeoff_lines:
                	debit_account_id = line.writeoff_account_id and line.writeoff_account_id.id
                	debit_line_vals = (0,0,{'account_id':debit_account_id,'name':line.writeoff_narration,'debit':abs(line.amount),'credit':0.0,'partner_id':partner_id,'analytic_account_id':line.analytic_account_id and line.analytic_account_id.id})
                	move_line.append(debit_line_vals)
            
            amount  = abs(self.writeoff_amount)
            
        if payment_type == 'outbound':
            debit_account_id = False
            credit_account_id = False
            amount = self.writeoff_amount 
            if amount < 0.0:
                credit_account_id= self.partner_id and self.partner_id.property_account_payable_id and self.partner_id.property_account_payable_id.id
                credit_line_vals =  (0,0,{'account_id':credit_account_id,'name':name,'credit':abs(amount),'debit':0.0,'partner_id':partner_id,'analytic_account_id':analytic_account_id})
                move_line.append(credit_line_vals)
                for line in writeoff_lines:
                	debit_account_id = line.writeoff_account_id and line.writeoff_account_id.id
                	debit_line_vals = (0,0,{'account_id':debit_account_id,'name':line.writeoff_narration,'credit':0.0,'debit':abs(line.amount),'partner_id':partner_id,'analytic_account_id':line.analytic_account_id and line.analytic_account_id.id})
                	move_line.append(debit_line_vals)
                	
                
            else:
                debit_account_id= self.partner_id and self.partner_id.property_account_payable_id and self.partner_id.property_account_payable_id.id
                debit_line_vals = (0,0,{'account_id':debit_account_id,'name':name,'credit':0.0,'debit':abs(amount),'partner_id':partner_id,'analytic_account_id':analytic_account_id})
                move_line.append(debit_line_vals)
                for line in writeoff_lines:
                	credit_account_id = line.writeoff_account_id and line.writeoff_account_id.id
                	credit_line_vals = (0,0,{'account_id':credit_account_id,'name':line.writeoff_narration,'credit':abs(line.amount),'debit':0,'partner_id':partner_id,'analytic_account_id':line.analytic_account_id and line.analytic_account_id.id})
                	move_line.append(credit_line_vals)
                	
            name = self.writeoff_narration
            amount  =abs(self.writeoff_amount)
            partner_id = self.partner_id and self.partner_id.id or False
        move_pool = self.env['account.move']
        
        move_vals['line_ids'] = move_line
        move = move_pool.create(move_vals)
        move_id = move.id  
        move.post()
        
        return move_id
            
    
    
    def get_cred_move_id(self,move_id):
        
        domain = [('move_id','=',move_id),('credit','>',0.0)]
        mv_lines =self.env['account.move.line'].search(domain,limit=1)
        return mv_lines.id
    
    def get_debit_move_id(self,move_id):
        
        domain = [('move_id','=',move_id),('debit','>',0.0)]
        mv_lines =self.env['account.move.line'].search(domain,limit=1)
        return mv_lines.id
        
    
    
    def get_matching_dict(self,debit_move_dict,credit_move_dict):
        matching_list = []
        debit_dict ={}
        credit_dict = {}
        payment_type  = self.payment_type
        if payment_type == 'inbound':
            debit_dict = debit_move_dict
            credit_dict = credit_move_dict
        elif payment_type == 'outbound':
            debit_dict = credit_move_dict
            credit_dict = debit_move_dict
        for cred_move_id,credamt in credit_dict.iteritems():
            if credamt > 0.0:
                for deb_move_id,debamt in debit_dict.iteritems():
                    balance = credamt - debamt
                    if credamt <=0.0:
                        continue
                    if debamt ==0.0:
                        continue
                    if balance > 0.0:
                        
                        matching_list.append({'credit_move_id':cred_move_id,'debit_move_id':deb_move_id,'amount':debamt})
                        debit_dict[deb_move_id] = 0.0
                    if balance <0.0:
                        
                        matching_list.append({'credit_move_id':cred_move_id,'debit_move_id':deb_move_id,'amount':credamt})
                        debit_dict[deb_move_id] = abs(balance)
                        credit_dict[cred_move_id] = 0.0
                    if balance == 0.0:
                       
                        matching_list.append({'credit_move_id':cred_move_id,'debit_move_id':deb_move_id,'amount':debamt})
                        debit_dict[deb_move_id] = 0.0
                        credit_dict[cred_move_id] = 0.0
                        credamt = 0.0
                    credamt = balance
        return matching_list
    
    @api.multi 
    def button_reconcile(self):
        self.check_amount()
        reconcile_obj = self.env['account.partial.reconcile']
        payment_type  = self.payment_type
        writeoff_amount = self.writeoff_amount
        move_id = False
        writeoff_lines = self.writeoff_lines
        writeoff_amount_line = 0
        if self.writeoff:
        	if not writeoff_lines:
        		raise Warning("set write-off details")
        	for w_l in writeoff_lines:
        		writeoff_amount_line = writeoff_amount_line + w_l.amount
        	writeoff_amount_line = round(writeoff_amount_line,2)
#        	if abs(writeoff_amount_line) != abs(writeoff_amount):
#        		raise Warning("the sum of line amounts is not equal to the difference amount")
        		
        	move_id = self.create_writeoff_move(writeoff_lines)
        	self.writeoff_move_id = move_id
            
        debit_move_dict = {}
        credit_move_dict = {}
        for line in self.debit_lines:
            debit_move_dict[line.move_line_id.id] = line.amount
            if line.amount ==0:
            	line.unlink()
        for line in self.credit_lines:
            credit_move_dict[line.move_line_id.id] = line.amount
            if line.amount ==0:
            	line.unlink()
        if self.writeoff:
            if payment_type == 'inbound':
                if writeoff_amount <0.0:
                    new_deb_move_id = self.get_debit_move_id(move_id)
                    debit_move_dict[new_deb_move_id] = abs(writeoff_amount)
                else:
                    new_cred_move_id = self.get_cred_move_id(move_id)
                    credit_move_dict[new_cred_move_id] = writeoff_amount
            if payment_type == 'outbound':
                if writeoff_amount <0.0:
                    new_cred_move_id = self.get_cred_move_id(move_id)
                    credit_move_dict[new_cred_move_id] = abs(writeoff_amount)
                else:
                    new_deb_move_id = self.get_debit_move_id(move_id)
                    debit_move_dict[new_deb_move_id] = writeoff_amount
      
        
        
        matching_list = self.get_matching_dict(debit_move_dict, credit_move_dict)
        
        for rec_val in matching_list:
            reconcile_obj.create(rec_val)
#         raise Warning("exception")      
        self.write({'state':'reconciled'})    
       
   
class OrchidDebitLines(models.Model):
    _name = "orchid.debit.lines"
    move_line_id = fields.Many2one('account.move.line',string="Move Line")
    rec_id = fields.Many2one('orchid.account.reconcilation',string="Reconcilation",ondelete="cascade")
    account_id = fields.Many2one('account.account',string="Account",related="move_line_id.account_id")
    partner_id = fields.Many2one('res.partner',string="Partner",related="move_line_id.partner_id")
    amount_residual = fields.Monetary(string="Residual Amount",related="move_line_id.amount_residual")
    total_amount_str = fields.Char(string="Original Amount")
    name = fields.Char(string="Label")
    date = fields.Date(string="Date")
    balance = fields.Float(string="Balance")
    company_currency_id = fields.Many2one('res.currency', related='move_line_id.company_currency_id', string="Company Currency", readonly=True,
        help='Utility field to express amount currency', store=True)
    amount = fields.Float(string="Allocate")
    full_reconcile = fields.Boolean(string="Full Reconcile")
    
    @api.onchange('full_reconcile')
    def onchange_full_reconcile(self):
        if self.full_reconcile:
            self.amount = abs(self.amount_residual)

class OrchidCreditLines(models.Model):
    _name = "orchid.credit.lines"
    move_line_id = fields.Many2one('account.move.line',string="Move Line")
    name = fields.Char(string="Label")
    date = fields.Date(string="Date")
    rec_id = fields.Many2one('orchid.account.reconcilation',string="Reconcilation",ondelete="cascade")
    account_id = fields.Many2one('account.account',string="Account",related="move_line_id.account_id")
    partner_id = fields.Many2one('res.partner',string="Partner",related="move_line_id.partner_id")
    amount_residual = fields.Monetary(string="Residual Amount",related="move_line_id.amount_residual")
    total_amount_str = fields.Char(string="Original Amount")
    balance = fields.Float(string="Balance")
    
    company_currency_id = fields.Many2one('res.currency', related='move_line_id.company_currency_id', string="Company Currency", readonly=True,
        help='Utility field to express amount currency', store=True)
    amount = fields.Float(string="Allocate")
    full_reconcile = fields.Boolean(string="Full Reconcile")
    
    @api.onchange('full_reconcile')
    def onchange_full_reconcile(self):
        if self.full_reconcile:
            self.amount = abs(self.amount_residual)
            
class OrchidWriteOffLines(models.Model):
    _name = "orchid.writeoff.lines"
    rec_id = fields.Many2one('orchid.account.reconcilation',string="Reconcilation",ondelete="cascade")
    writeoff_account_id = fields.Many2one('account.account',string='Post Difference In')
    writeoff_narration = fields.Char(string="Narration")
    analytic_account_id = fields.Many2one('account.analytic.account',string="Analytic Account")
    amount = fields.Float(string="Amount")
    

    



    
