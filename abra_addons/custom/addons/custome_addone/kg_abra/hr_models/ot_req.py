# -*- coding: utf-8 -*-
from odoo import fields,models,api,_
from odoo import tools
from datetime import date
from dateutil.relativedelta import relativedelta

from odoo.exceptions import UserError, AccessError
from odoo import api, fields, models, SUPERUSER_ID, _

class kg_ot_req(models.Model):
    
    _name='kg.ot.req'
    
    def _default_employee(self):
        return self.env.context.get('default_employee_id') or self.env['hr.employee'].search([('user_id', '=', self.env.uid)], limit=1)

    
    name = fields.Char('Name')
    
    kg_project = fields.Many2one('project.project','Project')
    
    
    
    kg_client = fields.Many2one('res.partner','Partner')
    
    kg_req_by = fields.Many2one('hr.employee','Requested By',default=_default_employee)
    
    kg_dep_id = fields.Many2one('hr.department','Department')
    
    kg_req_date = fields.Date('Requested Date')
    
    kg_ot_lines = fields.One2many('kg.ot.line','kg_ot_id')
    
    @api.onchange('kg_req_by')
    def onchange_req(self):
        for record in self:
            record.kg_dep_id=record.kg_req_by.department_id and record.kg_req_by.department_id.id or False
            
            
            
    @api.multi
    def dep_approve(self):
        for record in self.kg_ot_lines:
            record.write({'state':'dept_app'})
            
            
    @api.multi
    def hr_approve(self):
        for record in self.kg_ot_lines:
            record.write({'state':'hr_app'})
            
    
class kg_ot_line(models.Model):
    
    _name = 'kg.ot.line'


    @api.multi
    def show_attendnce(self):
        raise UserError(_('Attendance integration under progress.'))
    
    
        employee_id = self.kg_emp_id and self.kg_emp_id.id
        
        

        needed_records = []

        attendance_ids = self.env['kg.daily.attend'].search([('kg_emp_id','=',employee_id)])
        for att in attendance_ids:
			needed_records.append(att.id)

        domain = [
            ('id', 'in', needed_records)]

        return {
            'name': _('Attendance'),
            'domain': domain,
            'res_model': 'kg.daily.attend',
            'type': 'ir.actions.act_window',
            'view_id': False,
            'view_mode': 'tree,form',
            'view_type': 'form',
            'help': _('''<p class="oe_view_nocontent_create">
                           Click to Create for New Documents
                        </p>'''),
            'limit': 80,
        }


			
    kg_job_id = fields.Many2one('hr.job','Job')
    
    kg_emp_id = fields.Many2one('hr.employee','Employee')
    
    kg_hrs = fields.Float('Hours')
    
    kg_ot_id =  fields.Many2one('kg.ot.req','OT')
    
    state = fields.Selection([('new','New'),('dept_app','Dept Approved'),('hr_app','HR Approved'),('reject','Reject')],'Status',default='new')
    
    
    @api.multi
    def dep_approve(self):
        for record in self:
            record.write({'state':'dept_app'})
            
            
    @api.multi
    def hr_approve(self):
        for record in self:
            record.write({'state':'hr_app'})
            
    @api.multi
    def reject(self):
        for record in self:
            record.write({'state':'reject'})
            
            
            
