# -*- coding: utf-8 -*-
from odoo import fields,models,api,_
from odoo import tools
from datetime import date
from dateutil.relativedelta import relativedelta

from odoo.exceptions import UserError, AccessError
from odoo import api, fields, models, SUPERUSER_ID, _

class kg_end_settle(models.Model):
    
    _name = 'kg.end.settle'
    
    name = fields.Char('Name')
    
    kg_emp_id = fields.Many2one('hr.employee','Employee')
    
    kg_contract = fields.Many2one('hr.contract','Contract')
    
    kg_job = fields.Many2one('hr.job','Job')
    
    kg_dep = fields.Many2one('hr.department','Department')
    
    kg_join_date = fields.Date('Joining Date')
    
    kg_visa_type =fields.Selection([('limited','Limited'),('unlimited','Unlimited')],'Visa Type')
    
    
    kg_last_day = fields.Date('Last Working Day',default=fields.Date.today())
    
    kg_grat_amt = fields.Float('Gratuity Amount')
    
    kg_gross_tot = fields.Float('Gross Total')
    
    kg_lines =fields.One2many('kg.settle.line','kg_end_settle_id','Lines')
    
    
        
    
    @api.onchange('kg_emp_id')
    def onchange_emp(self):
        self.kg_job=self.kg_emp_id.job_id.id
        self.kg_join_date=self.kg_emp_id.joining_date
        self.kg_visa_type=self.kg_emp_id.kg_visa_type
        self.kg_dep=self.kg_emp_id.department_id.id
        

    @api.multi
    def show(self):
        for record in self:
            join_day = fields.Datetime.from_string(record.kg_join_date)
            last_day = fields.Datetime.from_string(record.kg_last_day)
            diff_days = relativedelta(last_day, join_day)

            employee_id = record.kg_emp_id and record.kg_emp_id.id or False
            company_id = record.kg_emp_id.company_id and record.kg_emp_id.company_id.id or False
               
            yrs = diff_days.years
            basic=record.kg_contract.wage or 0.0
            daily_wage=basic/30
            print '00000000000008888888888888888888888888888',daily_wage
            grat=0.0
            print yrs,'0000000000000'
            if yrs <1 and record.kg_visa_type=='limited':
                grat=0.0
            if yrs >= 1 and yrs <=5 and record.kg_visa_type=='limited':


                
                grat=daily_wage*21*yrs
                print "vvvvvvvvvvvvvvvvvvvvvvvvvvvvvv",grat
                
            if yrs > 5 and record.kg_visa_type=='limited':
                grat=daily_wage*30*yrs
                
                
            if yrs > 1 and yrs <=3 and record.kg_visa_type=='unlimited':
                grat=(daily_wage*21*yrs)/3.0
                
            if yrs > 3 and yrs <=5 and record.kg_visa_type=='unlimited':
                grat=(daily_wage*21*yrs*2)/3.0
                
            if yrs > 5 and record.kg_visa_type=='unlimited':
                grat=daily_wage*21*yrs

            payroll_transaction_records = self.env['hr.payroll.transactions'].search([('employee_id','=',employee_id),('payslip_id','=',False)])

            tot = 0
                
            for trans in payroll_transaction_records:
				amount = 0
				if trans.code == 'DED':
					amount = (trans.amount * (-1))
                	
				          
				else:
					amount = trans.amount 

				tot = tot + (amount)   
				self.env['kg.settle.line'].create({'kg_end_settle_id':self.id,'kg_amt':amount,'kg_desc':trans.rule_id and trans.rule_id.id})       
            
#            line_tot=0.0
#            
#            for line in record.kg_lines:
#                if line.kg_desc.category_id.code == 'DED':
#                    line_tot-=line.kg_amt
#                else:
#                    line_tot+=line.kg_amt
#                    
            gross_tot = grat+tot
            
            
            record.write({'kg_grat_amt':grat,'kg_gross_tot':gross_tot})
            
            
                
                
                
                
                
                
                
        
    
    


class kg_settle_line(models.Model):
    
    _name = 'kg.settle.line'
    
    name = fields.Char('Name')
    
    kg_desc = fields.Many2one('hr.salary.rule','Description')
    
    kg_amt = fields.Float('Amount')
    
    kg_end_settle_id = fields.Many2one('kg.end.settle','Parent Id')
