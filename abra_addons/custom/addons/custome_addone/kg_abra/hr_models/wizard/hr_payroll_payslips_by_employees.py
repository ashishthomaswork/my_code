# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, _
from odoo.exceptions import UserError


class HrPayslipEmployees(models.TransientModel):
    _inherit = 'hr.payslip.employees'

    @api.multi
    def compute_sheet(self):
        payslips = self.env['hr.payslip']
        [data] = self.read()
        active_id = self.env.context.get('active_id')
        if active_id:
            [run_data] = self.env['hr.payslip.run'].browse(active_id).read(['date_start', 'date_end', 'credit_note'])
        from_date = run_data.get('date_start')
        to_date = run_data.get('date_end')
        if not data['employee_ids']:
            raise UserError(_("You must select employee(s) to generate payslip(s)."))
        for employee in self.env['hr.employee'].browse(data['employee_ids']):
            payroll_transaction = self.env['hr.payroll.transactions'].search([('employee_id', '=', employee.id),('payslip_id', '=', False)])
            transaction_lines = []
            for line in payroll_transaction:
                if line.date >= from_date and line.date <= to_date:
#                    product_id = line.product_id and line.product_id.id
#                    salary_rule = self.env['hr.salary.rule'].search([('product_id', '=', product_id)])
#                    if len(salary_rule) > 1:
#                        raise UserError(_('same product linked in two salary rule'))
#                        
#                    if len(salary_rule) == 0:
##                        raise UserError(_('no salary rule defined for this product "%s".') %
#                (line.product_id.name))
                    od_val = (0,0,{'date_value':line.date,'amount':line.amount,'rule_id':line.rule_id and line.rule_id.id,'transaction_id':line.id,'transaction_note':line.transaction_note_id})
                    transaction_lines.append(od_val)
                    
                    
#            contract_id = slip_data['value'].get('contract_id')
            print employee,'employeeemployeeemployeeemployeeemployeeemployeeemployeeemployee'
            contract_obj = self.env['hr.contract'].search([('employee_id', '=', employee.id)])
            contract_date_start = contract_obj.date_start
            contract_date_stop = contract_obj.date_end
#            if contract_date_start > from_date:
#                from_date = contract_date_start
#            if contract_date_stop and contract_date_stop < to_date:
#                to_date = contract_date_stop                        
                    
                    
            slip_data = self.env['hr.payslip'].onchange_employee_id(from_date, to_date, employee.id, contract_id=False)

            print contract_obj.state,contract_obj.employee_id,'contractcontractcontractcontractcontractcontractcontractcontractcontract',contract_obj
            if contract_obj.state not in ('open','pending'):
                raise UserError(_('no active contract found'))
            res = {
                'employee_id': employee.id,
                'bank_account_id':employee.bank_account_id and employee.bank_account_id.id,
                'name': slip_data['value'].get('name'),
                'struct_id': slip_data['value'].get('struct_id'),
                'contract_id': slip_data['value'].get('contract_id'),
                'payslip_run_id': active_id,
                'input_line_ids': [(0, 0, x) for x in slip_data['value'].get('input_line_ids')],
                'worked_days_line_ids': [(0, 0, x) for x in slip_data['value'].get('worked_days_line_ids')],
                'date_from': from_date,
                'date_to': to_date,
                'od_variance_line':transaction_lines,
                'credit_note': run_data.get('credit_note'),
            }
            payslips += self.env['hr.payslip'].create(res)
        payslips.compute_sheet()
        return {'type': 'ir.actions.act_window_close'}
