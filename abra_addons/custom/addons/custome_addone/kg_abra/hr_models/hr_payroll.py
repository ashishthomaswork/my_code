#-*- coding:utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

import time
from datetime import datetime, timedelta
from dateutil import relativedelta
from dateutil.relativedelta import relativedelta
import babel

from odoo import api, fields, models, tools, _
from odoo.exceptions import UserError, ValidationError
from odoo.tools.safe_eval import safe_eval

from odoo.addons import decimal_precision as dp
import math



class hr_payslip_line(models.Model):
    _inherit = 'hr.payslip.line'
    
    @api.depends('code','salary_rule_id')     
    def _compute_monthly_salary(self):
        res = {}
        for payslip_line_obj in self:

            contract_id = payslip_line_obj.contract_id and payslip_line_obj.contract_id.id or payslip_line_obj.contract_id.employee_id and payslip_line_obj.contract_id.employee_id.id
            contract_obj = payslip_line_obj.contract_id or payslip_line_obj.employee_id.contract_id
            name = payslip_line_obj.name

            amount = 0
            if payslip_line_obj.code == 'GROSS':
                amount = contract_obj.od_total_wage
                payslip_line_obj.od_monthly_salary = amount

            elif payslip_line_obj.code == 'BASIC':
                amount = contract_obj.wage
                payslip_line_obj.od_monthly_salary = amount


            if contract_obj:

                for xo_line in contract_obj.od_allowance_rule_line_ids:
                    if xo_line.rule_type.name == name and payslip_line_obj.code != 'Gross':
                        amount = xo_line.amt
            
                    payslip_line_obj.od_monthly_salary = amount
    od_monthly_salary = fields.Float('Monthly Salary',compute='_compute_monthly_salary',store=True)


class HrPayslip(models.Model):
    _inherit = 'hr.payslip'



    @api.model
    def get_worked_day_lines(self, contract_ids, date_from, date_to):

        if len(contract_ids) != 1:
		
            raise UserError(_("more than one contract found or no contract found"))
            			


        print "21211111111111111111111111111111111111111111111111contract_ids",contract_ids

        print "21211111111111111111111111111111111111111111111111date_from",date_from	

        print "21211111111111111111111111111111111111111111111111date_to",date_to			
        """
        @param contract_ids: list of contract id
        @return: returns a list of dict containing the input that should be applied for the given contract between date_from and date_to
        """

        od_day_from = datetime.strptime(str(date_from),"%Y-%m-%d")
        od_day_to = datetime.strptime(str(date_to),"%Y-%m-%d")


        def was_on_leave_interval(employee_id, date_from, date_to):
            date_from = fields.Datetime.to_string(date_from)
            date_to = fields.Datetime.to_string(date_to)
            return self.env['hr.holidays'].search([
                ('state', '=', 'validate'),
                ('employee_id', '=', employee_id),
                ('type', '=', 'remove'),
                ('date_from', '<=', date_from),
                ('date_to', '>=', date_to)
            ], limit=1)

        res = []
        #fill only if the contract as a working schedule linked
        uom_day = self.env.ref('product.product_uom_day', raise_if_not_found=False)
        print "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%uom_day",uom_day
        working_hours = self.env['hr.contract'].browse(contract_ids).working_hours
		

        if not working_hours:
		
            raise UserError(_("define working hours in Contract"))



        for contract in self.env['hr.contract'].browse(contract_ids).filtered(lambda contract: contract.working_hours):

            print "vcxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxcontract",contract
            uom_hour = contract.employee_id.resource_id.calendar_id.uom_id or self.env.ref('product.product_uom_hour', raise_if_not_found=False)
            print "vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvuom_hour",uom_hour
            interval_data = []
            holidays = self.env['hr.holidays']
            attendances = {
                 'name': _("Normal Working Days paid at 100%"),
                 'sequence': 1,
                 'code': 'WORK100',
                 'number_of_days': 0.0,
                 'number_of_hours': 0.0,
                 'contract_id': contract.id,
            }
            leaves = {}
            day_from = fields.Datetime.from_string(date_from)
            day_to = fields.Datetime.from_string(date_to)
            nb_of_days = (day_to - day_from).days + 1

            print ";;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;nb_of_days",nb_of_days

            # Gather all intervals and holidays
            for day in range(0, nb_of_days):
                working_intervals_on_day = contract.working_hours.get_working_intervals_of_day(start_dt=day_from + timedelta(days=day))

                print "bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb-----------working_intervals_on_day",working_intervals_on_day
                for interval in working_intervals_on_day:
                    interval_data.append((interval, was_on_leave_interval(contract.employee_id.id, interval[0], interval[1])))
            print "vvvvvvvvvvvvxxxxxxxxxxxxxxxxccccccccccccccccccccinterval_data",interval_data
            # Extract information from previous data. A working interval is considered:
            # - as a leave if a hr.holiday completely covers the period
            # - as a working period instead
            for interval, holiday in interval_data:
                holidays |= holiday
                hours = (interval[1] - interval[0]).total_seconds() / 3600.0

                kg_number_of_days = 1 ## leave saved in leave form
				
                if holiday:
					

#                    kg_number_of_days = holiday.number_of_days_temp
                    #if he was on leave, fill the leaves dict
                    if holiday.holiday_status_id.name in leaves:
                        leaves[holiday.holiday_status_id.name]['number_of_hours'] += hours
                        

                        leaves[holiday.holiday_status_id.name]['number_of_days'] += kg_number_of_days
                    else:
                        leaves[holiday.holiday_status_id.name] = {
                            'name': holiday.holiday_status_id.name,
                            'sequence': 5,
                            'code': holiday.holiday_status_id.name,
                            'number_of_days': kg_number_of_days,
                            'number_of_hours': hours,
                            'contract_id': contract.id,
                        }
                else:
                    #add the input vals to tmp (increment if existing)
                    attendances['number_of_hours'] += hours
                    attendances['number_of_days'] += kg_number_of_days

            # Clean-up the results
            leaves = [value for key, value in leaves.items()]
            for data in [attendances] + leaves:
#                data['number_of_days'] = uom_hour._compute_quantity(data['number_of_hours'], uom_day)\
#                    if uom_day and uom_hour\
#                    else data['number_of_hours'] / 8.0
                res.append(data)

        qry = "SELECT id from hr_holidays where state in ('validate','validate1') and date_to >='"+str(od_day_from)+"' and date_to <='"+str(od_day_to)+"' and number_of_days_temp - floor(number_of_days_temp) >0 and employee_id ='"+str(contract.employee_id.id)+"';"
        idss = self.env.cr.execute(qry)
        holiday_ids = self.env.cr.fetchall()
        holiday_ids_repeat_ids = []
        if holiday_ids:
            holiday_ids_repeat_ids.append(list(holiday_ids[0])[0])
        
			

        print "cdqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqholiday_ids",holiday_ids
        total_holidays = 0
        for res_values in res:
            for holi in self.env['hr.holidays'].browse(holiday_ids_repeat_ids):

                print "vcxxxxxxxxxxxxxxxxxxxxxx",holi
                print "ccccccccccccccccccccccccccccccccccccccccccccccccqqqqqq",holiday_ids_repeat_ids
                holiday_obj = holi

                print "vvbbbbbbbbbbbbbbbbbbbbbbbbnnnn",holiday_obj
				
                leave_type = holiday_obj.holiday_status_id and holiday_obj.holiday_status_id.name
                number_of_days_temp = holiday_obj.number_of_days_temp
                total_holidays = total_holidays + number_of_days_temp


                number_of_days_temp_int = math.floor(number_of_days_temp)
                if res_values['code'] == 'WORK100':
                    res_values['number_of_days'] = res_values['number_of_days'] + (number_of_days_temp - number_of_days_temp_int)

                if res_values['code'] == leave_type:
                    if number_of_days_temp < 1:

                        res_values['number_of_days'] = res_values['number_of_days'] -1 + (number_of_days_temp - number_of_days_temp_int)

                    else:
                        res_values['number_of_days'] = res_values['number_of_days']  - (number_of_days_temp - number_of_days_temp_int)
#            res_values['number_of_hours'] =  res_values['number_of_days'] * working_hours_on_day
#        print "vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv--------------------------------****",holiday_ids
#        print "vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv--------------------------------****",hai






        return res
    
    
    
    
    
    
    @api.model
    def get_payslip_lines(self, contract_ids, payslip_id):
        def _sum_salary_rule_category(localdict, category, amount):
            if category.parent_id:
                localdict = _sum_salary_rule_category(localdict, category.parent_id, amount)
            if category.code in localdict['categories'].dict:
                amount += localdict['categories'].dict[category.code]
            localdict['categories'].dict[category.code] = amount
            return localdict

        class BrowsableObject(object):
            def __init__(self, employee_id, dict, env):
                self.employee_id = employee_id
                self.dict = dict
                self.env = env

            def __getattr__(self, attr):
                return attr in self.dict and self.dict.__getitem__(attr) or 0.0

        class InputLine(BrowsableObject):
            """a class that will be used into the python code, mainly for usability purposes"""
            def sum(self, code, from_date, to_date=None):
                if to_date is None:
                    to_date = fields.Date.today()
                self.env.cr.execute("""
                    SELECT sum(amount) as sum
                    FROM hr_payslip as hp, hr_payslip_input as pi
                    WHERE hp.employee_id = %s AND hp.state = 'done'
                    AND hp.date_from >= %s AND hp.date_to <= %s AND hp.id = pi.payslip_id AND pi.code = %s""",
                    (self.employee_id, from_date, to_date, code))
                return self.env.cr.fetchone()[0] or 0.0

        class WorkedDays(BrowsableObject):
            """a class that will be used into the python code, mainly for usability purposes"""
            def _sum(self, code, from_date, to_date=None):
                if to_date is None:
                    to_date = fields.Date.today()
                self.env.cr.execute("""
                    SELECT sum(number_of_days) as number_of_days, sum(number_of_hours) as number_of_hours
                    FROM hr_payslip as hp, hr_payslip_worked_days as pi
                    WHERE hp.employee_id = %s AND hp.state = 'done'
                    AND hp.date_from >= %s AND hp.date_to <= %s AND hp.id = pi.payslip_id AND pi.code = %s""",
                    (self.employee_id, from_date, to_date, code))
                return self.env.cr.fetchone()

            def sum(self, code, from_date, to_date=None):
                res = self._sum(code, from_date, to_date)
                return res and res[0] or 0.0

            def sum_hours(self, code, from_date, to_date=None):
                res = self._sum(code, from_date, to_date)
                return res and res[1] or 0.0

        class Payslips(BrowsableObject):
            """a class that will be used into the python code, mainly for usability purposes"""
            def total_sick_leave_taken(self,date,employee_id,holiday_status_id):
                year = date.split("-")[0]
                start_date = year +"-"+"01"+"-"+"01"
                end_date = year +"-"+"12"+"-"+"31"

                self.cr.execute("SELECT sum(number_of_days_temp)\
                            FROM hr_holidays hp\
                            WHERE hp.employee_id = %s AND hp.holiday_status_id = %s and hp.state not in %s AND hp.od_date_from >= %s AND hp.od_date_to <= %s",
                            (employee_id,holiday_status_id,('refuse','cancel'),start_date,end_date))
                res = self.cr.fetchone()
                return res and res[0] or 0.0  
                
                
            def sick_leave_future_days(self,date,employee_id,holiday_status_id):
                year = date.split("-")[0]
                end_date = year +"-"+"12"+"-"+"31"
                date = date
                start_date = str(datetime.strptime(str(date)[:10],"%Y-%m-%d") + timedelta(days=1))[:10] 
                self.cr.execute("SELECT sum(number_of_days_temp)\
                            FROM hr_holidays hp\
                            WHERE hp.employee_id = %s AND hp.holiday_status_id = %s and hp.state not in %s AND hp.od_date_from >= %s AND hp.od_date_to <= %s",
                            (employee_id,holiday_status_id,('refuse','cancel'),start_date,end_date))
                leave1 = self.cr.fetchone()

                return leave1 or 0.0                  
                
                
                
                            
            
            def days_between(self,date_from,date_to=None):
                if date_to is None:
                    date_to = datetime.now().strftime('%Y-%m-%d')
                start_date = datetime.strptime(date_from,"%Y-%m-%d")
                end_date = datetime.strptime(date_to,"%Y-%m-%d")
                od_nb_of_days = (end_date - start_date).days + 1
                return od_nb_of_days

            def sum(self, code, from_date, to_date=None):
                if to_date is None:
                    to_date = fields.Date.today()
                self.env.cr.execute("""SELECT sum(case when hp.credit_note = False then (pl.total) else (-pl.total) end)
                            FROM hr_payslip as hp, hr_payslip_line as pl
                            WHERE hp.employee_id = %s AND hp.state = 'done'
                            AND hp.date_from >= %s AND hp.date_to <= %s AND hp.id = pl.slip_id AND pl.code = %s""",
                            (self.employee_id, from_date, to_date, code))
                res = self.env.cr.fetchone()
                return res and res[0] or 0.0

        #we keep a dict with the result because a value can be overwritten by another rule with the same code
        result_dict = {}
        rules_dict = {}
        worked_days_dict = {}
        inputs_dict = {}
        blacklist = []
        payslip = self.env['hr.payslip'].browse(payslip_id)
        for worked_days_line in payslip.worked_days_line_ids:
            worked_days_dict[worked_days_line.code] = worked_days_line
        for input_line in payslip.input_line_ids:
            inputs_dict[input_line.code] = input_line

        categories = BrowsableObject(payslip.employee_id.id, {}, self.env)
        inputs = InputLine(payslip.employee_id.id, inputs_dict, self.env)
        worked_days = WorkedDays(payslip.employee_id.id, worked_days_dict, self.env)
        payslips = Payslips(payslip.employee_id.id, payslip, self.env)
        rules = BrowsableObject(payslip.employee_id.id, rules_dict, self.env)

        baselocaldict = {'categories': categories, 'rules': rules, 'payslip': payslips, 'worked_days': worked_days, 'inputs': inputs}
        #get the ids of the structures on the contracts and their parent id as well
        contracts = self.env['hr.contract'].browse(contract_ids)
        structure_ids = contracts.get_all_structures()
        #get the rules of the structure and thier children
        rule_ids = self.env['hr.payroll.structure'].browse(structure_ids).get_all_rules()
        #run the rules by sequence
        sorted_rule_ids = [id for id, sequence in sorted(rule_ids, key=lambda x:x[1])]
        sorted_rules = self.env['hr.salary.rule'].browse(sorted_rule_ids)

        for contract in contracts:
            employee = contract.employee_id
            localdict = dict(baselocaldict, employee=employee, contract=contract)
            for rule in sorted_rules:
                key = rule.code + '-' + str(contract.id)
                localdict['result'] = None
                localdict['result_qty'] = 1.0
                localdict['result_rate'] = 100
                #check if the rule can be applied
                if rule.satisfy_condition(localdict) and rule.id not in blacklist:
                    #compute the amount of the rule
                    amount, qty, rate = rule.compute_rule(localdict)
                    #check if there is already a rule computed with that code
                    previous_amount = rule.code in localdict and localdict[rule.code] or 0.0
                    #set/overwrite the amount computed for this rule in the localdict
                    tot_rule = amount * qty * rate / 100.0
                    localdict[rule.code] = tot_rule
                    rules_dict[rule.code] = rule
                    #sum the amount for its salary category
                    localdict = _sum_salary_rule_category(localdict, rule.category_id, tot_rule - previous_amount)
                    #create/overwrite the rule in the temporary results
                    result_dict[key] = {
                        'salary_rule_id': rule.id,
                        'contract_id': contract.id,
                        'name': rule.name,
                        'code': rule.code,
                        'category_id': rule.category_id.id,
                        'sequence': rule.sequence,
                        'appears_on_payslip': rule.appears_on_payslip,
                        'condition_select': rule.condition_select,
                        'condition_python': rule.condition_python,
                        'condition_range': rule.condition_range,
                        'condition_range_min': rule.condition_range_min,
                        'condition_range_max': rule.condition_range_max,
                        'amount_select': rule.amount_select,
                        'amount_fix': rule.amount_fix,
                        'amount_python_compute': rule.amount_python_compute,
                        'amount_percentage': rule.amount_percentage,
                        'amount_percentage_base': rule.amount_percentage_base,
                        'register_id': rule.register_id.id,
                        'amount': amount,
                        'employee_id': contract.employee_id.id,
                        'quantity': qty,
                        'rate': rate,
                    }
                else:
                    #blacklist this rule and its children
                    blacklist += [id for id, seq in rule._recursive_search_of_rules()]

        return [value for code, value in result_dict.items()]

    # YTI TODO To rename. This method is not really an onchange, as it is not in any view
    # employee_id and contract_id could be browse records    
    
    
    @api.one
    def compute_date_diff(self,ds,dt):
        from datetime import datetime
        d1 = datetime.strptime(ds, "%Y-%m-%d")
        d2 = datetime.strptime(dt, "%Y-%m-%d")
        days = (d2 - d1).days+1
        if days < 0:
            days = 0
        return days
    @api.depends('contract_id','date_from','date_to')    
    def _compute_netdays(self):
#        res = {}
        contract_id = self.contract_id and self.contract_id.id or self.employee_id and self.employee_id.contract_id.id
        for payslip_line_obj in self:

            no_of_days = 0
            if payslip_line_obj.date_from and payslip_line_obj.date_to:
                no_of_days = self.compute_date_diff(payslip_line_obj.date_from,payslip_line_obj.date_to)[0]
            if contract_id:
                print "bbbbbbbbbbbbbbbbbbbbbbbbb"
                date_start = self.contract_id.date_start
                if payslip_line_obj.date_from < date_start:
                    date_start = date_start
                else:
                    date_start = payslip_line_obj.date_from
                date_stop = self.contract_id.date_end
                if date_stop:
                    if payslip_line_obj.date_to > date_stop:
                        date_stop = self.contract_id.date_end
                    
                    else:
                        date_stop = payslip_line_obj.date_to  
                else:
                    date_stop = payslip_line_obj.date_to 
                no_of_days = self.compute_date_diff(date_start,date_stop)[0] 
            print "?????????????????????????????",no_of_days
            payslip_line_obj.od_payslip_days = no_of_days   
            
    def get_month_day_range(self,date):

        last_day = date + relativedelta(day=1, months=+1, days=-1)
        first_day = date + relativedelta(day=1)
        return first_day, last_day                       
                                  
    @api.depends('contract_id','date_from','date_to')    
    def _compute_month_days(self):
#        res = {}
        contract_id = self.contract_id and self.contract_id.id
        for payslip_line_obj in self:
            od_month_days = 0
            if payslip_line_obj.date_from:
	            ds = datetime.strptime(str(payslip_line_obj.date_from), "%Y-%m-%d")
	            first_day,last_day = self.get_month_day_range(ds)
	            od_month_days = self.compute_date_diff(str(first_day)[:10],str(last_day)[:10])[0]
	                           
            payslip_line_obj.od_month_days = od_month_days 




    @api.depends('line_ids')    
    def _compute_total_deductions(self):
#        res = {}
        line_ids = self.line_ids
        total_deductions = 0
        for line in line_ids:


            if line.category_id and line.category_id.code == 'DED':
	            total_deductions = total_deductions + line.total
            
	            
        self.kg_total_deductions = total_deductions	                           
        
    @api.depends('line_ids')    
    def _compute_other_allowance(self):
#        res = {}
        line_ids = self.line_ids
        other_allowance = 0
        for line in line_ids:
            print "ffffffffffffffffffffffffffffffffffffffffffff",line

            if line.category_id and line.category_id.code == 'ALW':

	            print "ccccccccccccccccccccccccccccccccccccccccc",line.code
	            print "ccccccccccccccccccccccccccccccccccccccccc",line.total


	            amt = 0
	            if line.od_monthly_salary:
					amt = line.od_monthly_salary
	            else:
					amt = line.total
	            other_allowance = other_allowance + amt
            
	            
        self.kg_other_allowance = other_allowance	



    @api.depends('contract_id','employee_id','od_month_days')    
    def _compute_one_day_wage(self):
        
        for record in self:
#        res = {}
         #   contract_id = record.contract_id and record.contract_id.id 
    
    
            od_month_days = record.od_month_days
    		
            contract =  record.contract_id or record.employee_id.contract_id
            print contract
            monthly_salary = contract.wage or 0.0
            print monthly_salary,'',record.contract_id.id,record.employee_id,record.employee_id.contract_id
    
            record.kg_one_day_basic = float(monthly_salary) / float(od_month_days)	
              
                
               



    @api.depends('od_variance_line')    
    def _compute_total_ot(self):
#        res = {}
        line_ids = self.od_variance_line
        hours = 0
        for line in line_ids:


            if line.rule_id and line.rule_id.code == 'OT':
	            hours = hours + line.hours
            
        if hours == 0:
			hours = 1	            
        self.kg_total_ot_hours = hours

    @api.depends('od_variance_line')    
    def _compute_total_ot_amt(self):
#        res = {}

        kg_overtime_rate = 0

        line_ids = self.od_variance_line
        for line in line_ids:


            if line.rule_id and line.rule_id.code == 'OT':
	            kg_overtime_rate = kg_overtime_rate + line.amount

            
	            
        self.kg_total_ot_amt = kg_overtime_rate


    @api.depends('line_ids')    
    def _compute_kg_total_gross(self):
#        res = {}
        line_ids = self.line_ids
        gross = 0
        for line in line_ids:


            if line.category_id and line.category_id.code == 'GROSS':
	            gross = gross + line.total
            
	            
        self.kg_total_gross = gross	



    @api.depends('worked_days_line_ids','kg_one_day_basic')    
    def _compute_kg_leave_deductions(self):
#        res = {}

        kg_one_day_basic = self.kg_one_day_basic 
        line_ids = self.worked_days_line_ids
        leave_days = 0
        for line in line_ids:


            if line.code == 'Unpaid':
	            leave_days = leave_days + line.number_of_days
            
	            
        self.kg_leave_deductions = leave_days * kg_one_day_basic
           
    od_payslip_days = fields.Float('Payslip Days',compute='_compute_netdays',store=True)
    kg_hold = fields.Boolean('Hold')
    od_month_days = fields.Float('Monthly Days',compute='_compute_month_days',store=True)
    od_variance_line = fields.One2many('od.hr.variance.line','payslip_id',string="Variance Line")
    bank_account_id = fields.Many2one('res.partner.bank',string="Account Number")
    kg_remarks = fields.Char(string="Remarks")
    kg_one_day_basic = fields.Float('One Day Wage',compute='_compute_one_day_wage',store=True)
    kg_total_deductions = fields.Float('Total Deductions',compute='_compute_total_deductions',store=True)	
    kg_other_allowance = fields.Float('Other Allowance',compute='_compute_other_allowance',store=True)


    kg_total_ot_hours = fields.Float('Total OT(H)',compute='_compute_total_ot',store=True)

    kg_total_ot_amt = fields.Float('Total OT Amt',compute='_compute_total_ot_amt',store=True)	
    kg_total_gross = fields.Float('Total Gross',compute='_compute_kg_total_gross',store=True) 
    kg_leave_deductions = fields.Float('Leave Deductions',compute='_compute_kg_leave_deductions',store=True)
     
class od_hr_variance_line(models.Model):
    _name = "od.hr.variance.line"

    payslip_id = fields.Many2one('hr.payslip','PaySlip',ondelete='cascade')
    date_value = fields.Date('Date')
    amount = fields.Float('Amount')
    hours = fields.Float('Hours')
    rule_id = fields.Many2one('hr.salary.rule','Rule')
    transaction_id = fields.Many2one('hr.payroll.transactions','Transaction')
   
    transaction_note = fields.Char(string="Transaction Note")
class hr_salary_rule(models.Model):
    _inherit = "hr.salary.rule" 
    product_id = fields.Many2one('product.product', 'Payroll Item',) 
   

