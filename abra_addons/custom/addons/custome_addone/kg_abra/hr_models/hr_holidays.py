# -*- coding: utf-8 -*-
from odoo import fields,models,api,_
from odoo import tools
from datetime import date
from dateutil.relativedelta import relativedelta

from odoo.exceptions import UserError, AccessError
from odoo import api, fields, models, SUPERUSER_ID, _
from odoo.exceptions import except_orm
from datetime import datetime,date
class hr_holidays(models.Model):

    _inherit ="hr.holidays"

    leave_eligibility = fields.Float(string="Leave Eligible")


    @api.multi
    def calculate_leaveeligibility(self):
		employee_id = self.employee_id and self.employee_id.id

		company_id = self.company_id and self.company_id.id
		eligibile_leave_year = self.employee_id and self.employee_id.eligible_leave_year
		if not eligibile_leave_year:
			raise except_orm('define leave eligible in employee master')

		joining_date = self.employee_id and self.employee_id.joining_date

		if not joining_date:
			raise except_orm('define joining date in employee master')

		total_annual_leaves = 0

		joining_date = str(joining_date) + " " + "00:00:00"
		total_annual_leave_ids = self.env['hr.holidays'].search([('employee_id','=',employee_id),('company_id','=',company_id),('holiday_status_id','=',1)])
		for annual_leaves in total_annual_leave_ids:
			total_annual_leaves = total_annual_leaves + annual_leaves.number_of_days_temp
			

		date_from = self.date_from
		diff = (datetime.strptime(date_from, '%Y-%m-%d %H:%M:%S') -  datetime.strptime(joining_date, '%Y-%m-%d %H:%M:%S')).days +1
            
		leave_eligible_one_day = float(eligibile_leave_year) / float(365) 

		leave_eligibility = leave_eligible_one_day *  diff  

		self.write({'leave_eligibility':leave_eligibility - total_annual_leaves}) 
		return True 
			

