# -*- coding: utf-8 -*-
from odoo import fields,models,api,_
from odoo import tools
from datetime import date
from dateutil.relativedelta import relativedelta

from odoo.exceptions import UserError, AccessError
from odoo import api, fields, models, SUPERUSER_ID, _

class kg_join_note(models.Model):

    _name= 'kg.join.note'

    name = fields.Char('Name')

    kg_ref_no = fields.Char('Reference No')

    kg_staff_id = fields.Char('Staff ID No')

    kg_date = fields.Date('Date')

    kg_emp_name = fields.Char('Name')

    kg_des = fields.Many2one('hr.job','Designation')

    kg_doj = fields.Date('Date of Joining')

    kg_dep = fields.Many2one('hr.department','Department')

    kg_nationality  =  fields.Many2one('res.country','Nationality')

    kg_pass_no = fields.Char('Passport No')

    kg_doi = fields.Date('Date of Issue')

    kg_doe = fields.Date('Date of Expiry')

    kg_init_adv  = fields.Float('Inital Advance')

    kg_basic = fields.Float('Basic')

    kg_allow = fields.Float('Allowance')

    kg_comp_acc = fields.Boolean('Company Accomodation')

    kg_comp_trans = fields.Boolean('Company Transportation')

    kg_att_reg  = fields.Boolean('Attendance Registaration')

    kg_uniform = fields.Boolean('Uniforms / PPE')

    