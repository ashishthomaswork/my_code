# -*- coding: utf-8 -*-
from odoo import fields,models,api,_
from odoo import tools
from datetime import date
from dateutil.relativedelta import relativedelta

from odoo.exceptions import UserError, AccessError
from odoo import api, fields, models, SUPERUSER_ID, _

class kg_interview_assess(models.Model):

    _name = 'kg.interview.assess'

    name = fields.Char('Name')

    kg_cand = fields.Char('Canditate Name')

    kg_interview_by = fields.Many2one('hr.employee','Interviewed By')
    kg_manpower_id = fields.Many2one('kg.manpower.req','ManPower')

    kg_post = fields.Many2one('hr.job','Position Title')

    kg_dep = fields.Many2one('hr.department','Department')

    kg_source = fields.Char('Source of Application')

    kg_date = fields.Date('Date')

    kg_tot_exp  = fields.Integer('Total Experience')

    kg_visa_status = fields.Char('Visa Status')

    kg_ed_cour = fields.Selection([('Inappropriate','Inappropriate'),('Marginal','Marginal'),('Adequate','Adequate'),('Good','Good'),('Excellent','Excellent')],'Education/Training/Cetification Courses')

    kg_comm_skills =fields.Selection([('Inappropriate','Inappropriate'),('Marginal','Marginal'),('Adequate','Adequate'),('Good','Good'),('Excellent','Excellent')],'Communication Skills')

    kg_over_per_att =fields.Selection([('Inappropriate','Inappropriate'),('Marginal','Marginal'),('Adequate','Adequate'),('Good','Good'),('Excellent','Excellent')],'Overall Personality & Attitude')

    kg_rel_ind_exp  = fields.Selection([('Inappropriate','Inappropriate'),('Marginal','Marginal'),('Adequate','Adequate'),('Good','Good'),('Excellent','Excellent')],'Relevant Industry Experience')

    kg_job_stab = fields.Selection([('Inappropriate','Inappropriate'),('Marginal','Marginal'),('Adequate','Adequate'),('Good','Good'),('Excellent','Excellent')],'Job Stability')

    kg_log_think = fields.Selection([('Inappropriate','Inappropriate'),('Marginal','Marginal'),('Adequate','Adequate'),('Good','Good'),('Excellent','Excellent')],'Logical Thinking')

    kg_flex_adapt =fields.Selection([('Inappropriate','Inappropriate'),('Marginal','Marginal'),('Adequate','Adequate'),('Good','Good'),('Excellent','Excellent')],'Flexibility & Adaptability')


    kg_overall_comments = fields.Text('Overall Commments')

    kg_recom = fields.Selection([('Selected','Selected'),('Hold','Waitlist/Hold'),('Not','Not Suitable For Post')],'Recommendation')

    kg_job_spec_lines = fields.One2many('kg.job.spec.comp','kg_int_id','Job Specific Competance Lines')
    
    
    kg_state = fields.Selection([('new','New'),('wait_fin_app','Waiting for Finance Approval'),('fin_app','Finance Approved'),('under_short','Under Shortlisting'),('tele_inter','Telephonic Interview'),('dep_level_inter','Department Level Interview'),('direct_inter','Direct Interview'),('refer_check','Reference Check'),('rel_offer_letter','Release Offer Letter'),('cand_join','Canditate Joined'),('under_prob','Under Probation'),('confirmed','Confirmed')],'State',default='new')


    kg_app_id = fields.Many2one('hr.applicant','Applicant')
class kg_job_spec_comp(models.Model):

    _name ='kg.job.spec.comp'

    name = fields.Char('Job Specific Competance')

    kg_choice = fields.Selection([('Inappropriate','Inappropriate'),('Marginal','Marginal'),('Adequate','Adequate'),('Good','Good'),('Excellent','Excellent')],'Choice')

    kg_int_id  = fields.Many2one('kg.interview.assess','Interview ID')