# -*- coding: utf-8 -*-
from odoo import fields,models,api,_
from odoo import tools
from datetime import date
from dateutil.relativedelta import relativedelta

from odoo.exceptions import UserError, AccessError
from odoo import api, fields, models, SUPERUSER_ID, _

class kg_manpower_req(models.Model):
    
    
    _name="kg.manpower.req"
    
    name = fields.Char('Name')
    
    kg_dep_id = fields.Many2one('hr.department','Department')
    
    kg_req_by = fields.Many2one('res.users','Requested By')
    
    kg_pos_req = fields.Many2one('hr.job','Position')
    
    kg_no = fields.Integer('No of Vacancies')
    
    kg_priority = fields.Selection([('immed','Immediate'),('not_imm','Not Immediate')],'Priority')
    
    kg_reason_vacancy = fields.Text('Reason For Vacancy')
    
    kg_reason_type = fields.Selection([('Resignation','Resignation'),('Termination','Termination'),('new_pos','New Position')],'Type')
    
    kg_inter_arr  = fields.Boolean('Internal Arrangement Acceptable')
    
    kg_prop_emp = fields.Many2one('hr.employee','Proposed Employee')
    
    kg_rea_prop = fields.Text('Reason for Proposal')
    
    kg_min_edu = fields.Text('Minimum Education Qualification & Any Certification Requirements')

    kg_no_years = fields.Integer('No of Years Experience Required')

    kg_job_desc = fields.Text('Job Description')

    kg_key_skills = fields.Char('Key Skills')

    kg_budget = fields.Float('Budget')

    kg_proposed = fields.Float('Proposed')

    kg_remarks = fields.Text('Remarks If Any')

    kg_req_rec_date = fields.Date('Requisition received Date')

    kg_source = fields.Char('Source of Recruitment')

    kg_mode_int  = fields.Char('Mode of Interview')

    kg_selected_cand = fields.Char('Selected Canditate')
    
    
    kg_int_count = fields.Integer('Interview Count',compute='compute_int_count')
    
    
    state = fields.Selection([
        ('new', 'New'),
          ('inprog', 'In Progress'),
        ('completed', 'Completed'),
        ], string='Status',default='new')
    
    
    @api.multi
    def action_approve(self):
        self.state = 'completed'
        return True
    
    
    @api.multi
    def action_inprog(self):
        self.state = 'inprog'
        return True

    
    
    @api.multi
    def compute_int_count(self):
        for record in self:
            est_obj = self.env['kg.interview.assess']
            
            record.kg_int_count = est_obj.search_count([('kg_manpower_id', '=',record.id)]) 


    
    
    