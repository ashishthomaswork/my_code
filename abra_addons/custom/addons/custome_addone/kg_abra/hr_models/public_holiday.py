# -*- coding: utf-8 -*-
from odoo import fields,models,api,_
from odoo import tools
from datetime import date
from dateutil.relativedelta import relativedelta

from odoo.exceptions import UserError, AccessError
from odoo import api, fields, models, SUPERUSER_ID, _

class kg_public_holiday(models.Model):

    _name ="kg.public.holiday"

    name = fields.Char('Name',required="1")

    date = fields.Date('Date',required="1")

    kg_note = fields.Text('Remarks',)

