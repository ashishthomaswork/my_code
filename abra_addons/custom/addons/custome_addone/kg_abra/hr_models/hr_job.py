# -*- coding: utf-8 -*-
from odoo import fields,models,api,_
from odoo import tools
from datetime import date
from dateutil.relativedelta import relativedelta

from odoo.exceptions import UserError, AccessError
from odoo import api, fields, models, SUPERUSER_ID, _



class kg_stages_duration_line(models.Model):
    _name ="kg.stages.duration.line"

    job_id = fields.Many2one('hr.job','Position')

    stage_id = fields.Many2one('hr.recruitment.stage','Stage')
    start = fields.Date('Start Date')
    end_date = fields.Date('End Date')
    remarks = fields.Text('Remarks')


class hr_job(models.Model):

    _inherit ="hr.job"

    kg_job_line = fields.One2many('kg.stages.duration.line','job_id','Stages Duration Line')


class hr_applicant(models.Model):

    _inherit ="hr.applicant"

    kg_applicant_doc_line = fields.One2many('hr.employee.document','applicant_id','Document Line')
    
    
    kg_int_count = fields.Integer('Interview Count',compute='compute_int_count')
    

    
    
    @api.multi
    def compute_int_count(self):
        for record in self:
            est_obj = self.env['kg.interview.assess']
            
            record.kg_int_count = est_obj.search_count([('kg_app_id', '=',record.id)]) 


    @api.multi
    def action_get_attachment_tree_view(self):
        self.ensure_one()
        domain = [
            ('applicant_id', '=', self.id)]
        return {
            'name': _('Documents'),
            'domain': domain,
            'res_model': 'hr.employee.document',
            'type': 'ir.actions.act_window',
            'view_id': False,
            'view_mode': 'tree,form',
            'view_type': 'form',
            'help': _('''<p class="oe_view_nocontent_create">
                           Click to Create for New Documents
                        </p>'''),
            'limit': 80,
            'context': "{'default_applicant_id': '%s'}" % self.id
        }

    @api.multi
    def create_employee_from_applicant(self):
        result = super(hr_applicant, self).create_employee_from_applicant()
        kg_applicant_doc_line = self.kg_applicant_doc_line
        for line in kg_applicant_doc_line:

			employee_id = result['res_id']


			print "444444444444444",employee_id
			line.write({'employee_ref':employee_id})
            

        return result



