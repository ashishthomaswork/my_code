# -*- coding: utf-8 -*-
from odoo import fields,models,api,_
from odoo import tools
from datetime import date
from dateutil.relativedelta import relativedelta

from odoo.exceptions import UserError, AccessError
from odoo import api, fields, models, SUPERUSER_ID, _

class kg_req_cert(models.Model):
    _name = 'kg.req.cert'

    @api.multi
    def action_approve(self):
        self.state = 'completed'
        return True
    
    
    @api.multi
    def action_inprog(self):
        self.state = 'inprog'
        return True




    name = fields.Char('Name')

    state = fields.Selection([
        ('new', 'New'),
          ('inprog', 'In Progress'),
        ('completed', 'Completed'),
        ], string='Status',default='new')


    kg_emp_id = fields.Many2one('hr.employee','Employee')

    kg_emp_code = fields.Char('EMP Code')

    kg_des = fields.Many2one('hr.job','Designation')

    kg_dep = fields.Many2one('hr.department','Department')

    kg_doj = fields.Date('Date of Joining')

    kg_date = fields.Date('Date')

    kg_pass_no = fields.Char('Passport No')

    kg_toc = fields.Char('Type of Certificate')

    kg_por = fields.Char('Purpose of Request')

    kg_add_to  = fields.Char('Addressed To')

    

