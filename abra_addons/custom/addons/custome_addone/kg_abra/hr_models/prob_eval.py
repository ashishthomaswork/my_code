# -*- coding: utf-8 -*-
from odoo import fields,models,api,_
from odoo import tools
from datetime import date
from dateutil.relativedelta import relativedelta

from odoo.exceptions import UserError, AccessError
from odoo import api, fields, models, SUPERUSER_ID, _

class kg_prob_eval(models.Model):

    _name = "kg.prob.eval"

    name = fields.Char('Name')

    kg_emp_id = fields.Many2one('hr.employee','Employee')

    kg_emp_code = fields.Char('Emp Code')

    kg_des = fields.Many2one('hr.job','Designation')

    kg_dep = fields.Many2one('hr.department','Department')

    kg_doj = fields.Date('Date of Joining')

    kg_dor = fields.Date('Date of Review')

    kg_know_job = fields.Selection([('exceed','Exceed Requirments of Job'),('meets','Meets Requirments of Job'),('unable','Unable to fulfill Present Assignment')],'Knowledge of JOb')

    kg_qual_qty =fields.Selection([('exceed','Exceed Requirments of Job'),('meets','Meets Requirments of Job'),('unable','Unable to fulfill Present Assignment')],'Quality/Quantity of Work')

    kg_att_job =fields.Selection([('exceed','Exceed Requirments of Job'),('meets','Meets Requirments of Job'),('unable','Unable to fulfill Present Assignment')],'Attitude TOwards Job')

    kg_learning = fields.Selection([('exceed','Exceed Requirments of Job'),('meets','Meets Requirments of Job'),('unable','Unable to fulfill Present Assignment')],'Learning')

    kg_inter = fields.Selection([('exceed','Exceed Requirments of Job'),('meets','Meets Requirments of Job'),('unable','Unable to fulfill Present Assignment')],'INterpersonal /Communication Skill')

    kg_any = fields.Selection([('exceed','Exceed Requirments of Job'),('meets','Meets Requirments of Job'),('unable','Unable to fulfill Present Assignment')],'Any Training Required')

    kg_chall = fields.Selection([('exceed','Exceed Requirments of Job'),('meets','Meets Requirments of Job'),('unable','Unable to fulfill Present Assignment')],'Challenges')

    kg_over_eval = fields.Text('Overall Evalution')