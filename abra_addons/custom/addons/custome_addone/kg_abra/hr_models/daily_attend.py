# -*- coding: utf-8 -*-
from odoo import fields,models,api,_
from odoo import tools
from datetime import date
from dateutil.relativedelta import relativedelta

from odoo.exceptions import UserError, AccessError
from odoo import api, fields, models, SUPERUSER_ID, _


    
class kg_daily_attend(models.Model):
    _name = 'kg.daily.attend'
    
    name = fields.Char('Name')
    
    kg_emp_id = fields.Many2one('hr.employee','Employee')
    
    kg_dep = fields.Many2one('hr.department','Department')
    
    kg_company_id = fields.Many2one('res.company','Company')
    
    kg_date = fields.Date('Date')
    
    kg_in  =fields.Char('IN')
    
    kg_out  = fields.Char('OUT')
    
    kg_pull_morn = fields.Char('Pull Morning')
    
    kg_pull_even = fields.Char('Pull Evening')
    
    kg_normal_hrs  = fields.Float('Normal Hours')
    
    kg_ot_hrs = fields.Float('Overtime Hours')
    
    kg_modified_by = fields.Many2one('hr.employee','Modified By')
    
    kg_ot_max = fields.Float('Overtime Maximum')
    
    kg_modified_on = fields.Datetime('Modified on')
    
    kg_prev_in = fields.Char('Previous In')
    
    kg_prev_out = fields.Char('Previous Out')
    
    