# -*- coding: utf-8 -*-
from odoo import fields,models,api,_
from odoo import tools
from datetime import date
from dateutil.relativedelta import relativedelta

from odoo.exceptions import UserError, AccessError
from odoo import api, fields, models, SUPERUSER_ID, _
from datetime import datetime,date
from odoo.exceptions import except_orm

class kg_leave_salary_request(models.Model):

    _name ="kg.leave.salary.request"



    kg_emp_id = fields.Many2one('hr.employee','Employee')


    company_id = fields.Many2one(related='kg_emp_id.company_id', store=True, string='Company', readonly=True)

    name = fields.Char('Description')

    amount = fields.Float('Amount')

    deduction = fields.Float('Deduction')

    net = fields.Float('Net')	


    requested_date = fields.Date('Requested Date',default=fields.Datetime.now)

    include_in_payslip = fields.Boolean('Include in Payslip')

    kg_leave_id = fields.Many2one('hr.holidays','Leave Application')

    kg_emp_code = fields.Char('Emp Code')

    kg_des = fields.Many2one('hr.job','Designation')

    kg_dep = fields.Many2one('hr.department','Department')

    kg_leave_start = fields.Date('Start Date')
    kg_leave_end = fields.Date('End Date')

    kg_note = fields.Text('Remarks')
    
    kg_state = fields.Selection([('new','New'),('approved','Approved'),('released','Released')],'State',default='new')
    @api.onchange('amount','deduction')
    def onchange_emp(self):
        self.net = self.amount - self.deduction
    
    @api.multi
    def approve(self):
        for record in self:
            kg_emp_id = record.kg_emp_id and record.kg_emp_id.id
            company_id = record.kg_emp_id.company_id and record.kg_emp_id.company_id.id

            leave_ids = []
            annual_leave_ids = self.env['hr.holidays'].search([('employee_id','=',kg_emp_id),('company_id','=',company_id),('holiday_status_id','=',1),('state','not in',('cancel','refuse'))])

            for ann in annual_leave_ids:
				leave_ids.append(ann.id)

            max_leave_id = False
            if leave_ids:
				max_leave_id = max(leave_ids)
            max_leave_obj = self.env['hr.holidays'].browse(max_leave_id)

	
            date_to = max_leave_obj.date_to
            kg_leave_start = str(record.kg_leave_start) + " " + "00:00:00"

            if not max_leave_id:
				date_to = record.kg_emp_id and record.kg_emp_id.joining_date
            date_to = str(date_to)[:10] + " " + "00:00:00"

            print "ffffffffffffffffffffffffffffkg_leave_start",kg_leave_start
            print "ffffffffffffffffffffffffffffdate_to",date_to
				
            diff = (datetime.strptime(kg_leave_start, '%Y-%m-%d %H:%M:%S') -  datetime.strptime(date_to, '%Y-%m-%d %H:%M:%S')).days +1

            print "bbbbbbbbbbbbbbbbbbbbbb",diff
            if diff < 365:
            	raise except_orm('The number of days worked is less than 365')


##########################Adding to payroll transaction object

            ######Search for leave salary rule
            leave_salary_rule_id = self.env['hr.salary.rule'].search([('code','=','LSR'),('company_id','=',company_id)]) and self.env['hr.salary.rule'].search([('code','=','LSR'),('company_id','=',company_id)]).id or False

            if not leave_salary_rule_id:
            	raise except_orm('Configure Leave Salary rule with code:LSR,check the company also')
########################
					
            self.env['hr.payroll.transactions'].create({'employee_id':kg_emp_id,'rule_id':leave_salary_rule_id,
'date':record.kg_leave_start,'amount':record.net,'leave_salary_request_id':self.id,'transaction_note_id':record.name,'company_id':company_id})			
            record.write({'kg_state':'approved'})


    @api.multi
    def set_to_draft(self):
        self.kg_state = 'new'
        transaction_record = self.env['hr.payroll.transactions'].search([('leave_salary_request_id','=',self.id)])
        transaction_record.unlink()



    @api.multi
    def unlink(self):
        if self.kg_state != 'new':
			raise UserError(_('you cannot delete this record because it is not in new state'))




            
    @api.multi
    def release(self):
        for record in self:
            record.write({'kg_state':'released'})
    





