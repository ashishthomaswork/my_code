# -*- coding: utf-8 -*-
from odoo import fields,models,api,_
from odoo import tools
from datetime import date
from dateutil.relativedelta import relativedelta

from odoo.exceptions import UserError, AccessError
from odoo import api, fields, models, SUPERUSER_ID, _

class kg_emp_transfer(models.Model):

    _name ="kg.emp.transfer"

    name = fields.Char('Name')

    kg_emp_id = fields.Many2one('hr.employee','Employee')

    kg_emp_code = fields.Char('Emp Code')

    kg_des = fields.Many2one('hr.job','Designation')

    kg_dep = fields.Many2one('hr.department','Department')

    kg_doj = fields.Date('Date of Joining')

    kg_div = fields.Char('Division')

    kg_transfer_to = fields.Many2one('hr.department','Transfer To')

    kg_eff_from  = fields.Date('Effective From')

    kg_reporting_to = fields.Many2one('hr.employee','Reporting To')

    kg_reason = fields.Text('Reason For Transfer')
    
    
    kg_app_changes = fields.Many2many('kg.app.changes')



class kg_app_changes(models.Model):
    
    _name="kg.app.changes"
    
    
    name = fields.Char('Name')
    
