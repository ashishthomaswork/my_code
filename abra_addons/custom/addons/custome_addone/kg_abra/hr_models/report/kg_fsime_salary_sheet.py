# -*- coding: utf-8 -*-

from odoo import tools
from odoo import models, fields, api


class KGFsimeSalarySheet(models.Model):
    _name = "kg.fsime.salary.sheet"


    _description = "Salary Sheet"
    _auto = False
    _order = 'kg_other_id desc'
    
    kg_other_id = fields.Char('Emp ID')
    employee_id = fields.Many2one('hr.employee','Name')
    job_id = fields.Many2one('hr.job',string='Designation') 
    department_id = fields.Many2one('hr.department','Department')

    working_company_id = fields.Many2one('kg.working.company',string="Working Under")
    visa_company_id = fields.Many2one('kg.visa.company',string="Visa")
    kg_pay_head = fields.Selection([
        ('indirect', 'Indirect'),
        ('direct', 'Direct'),
        ], string='Pay Head',default='direct',)


    basic = fields.Float(string="Salary Basic")
    mobile_allowance = fields.Float(string="Mobile allowance")
	
    gross = fields.Float(string="Gross Salary")
    payslip_days = fields.Float(string="Days In Month")
    worked_days = fields.Float(string="Worked Days")
    leave_days = fields.Float(string="Leave Days")
    leave_deductions = fields.Float(string="Deductions")
    other_deductions = fields.Float(string="Other Deductions")
    overtime = fields.Float(string="Overtime in Hrs")	
    overtime_rate = fields.Float(string="Overtime Rate")	
    overtime_amount = fields.Float(string="Overtime")	
    other_allowance = fields.Float(string="Other Allowance")	
	
    net_salary = fields.Float(string="Net salary")
    kg_payment_method = fields.Selection([
        ('wps', 'WPS'),
        ('bank', 'Bank'),
        ('cash', 'Cash'),
        ], string='Payment Method',required="1",default='wps')
	
    kg_remarks = fields.Char('Remarks')    

    





    def _select(self):
        select_str = """
            SELECT min(hr_payslip.id) as id,
            hr_employee.kg_other_id AS kg_other_id,
hr_payslip.employee_id as employee_id,
    hr_employee.job_id as job_id,
    hr_employee.department_id as department_id,

    hr_employee.working_company_id as working_company_id,
    hr_employee.visa_company_id as visa_company_id,


    hr_employee.kg_payment_method as kg_payment_method,

    ( SELECT sum(hr_payslip_line.od_monthly_salary) AS sum

           FROM hr_payslip_line

          WHERE hr_payslip_line.slip_id = hr_payslip.id AND (hr_payslip_line.code='BASIC')) AS basic,


    ( SELECT sum(hr_payslip_line.od_monthly_salary) AS sum

           FROM hr_payslip_line

          WHERE hr_payslip_line.slip_id = hr_payslip.id AND hr_payslip_line.code::text = 'MA'::text) AS mobile_allowance,


    ( SELECT sum(hr_payslip_line.total) AS sum

           FROM hr_payslip_line

          WHERE hr_payslip_line.slip_id = hr_payslip.id AND hr_payslip_line.code::text = 'GROSS'::text) AS gross,

    hr_payslip.od_payslip_days as payslip_days,




  ( SELECT sum(hr_payslip_worked_days.number_of_days) AS sum
           FROM hr_payslip_worked_days
          WHERE ((hr_payslip_worked_days.payslip_id = hr_payslip.id) AND (hr_payslip_worked_days.code in ('Legal Leaves 2018','WORK100','Sick Leaves','Compensatory Days')))) AS worked_days,




   



  ( SELECT sum(hr_payslip_worked_days.number_of_days) AS sum
           FROM hr_payslip_worked_days
          WHERE ((hr_payslip_worked_days.payslip_id = hr_payslip.id) AND ((hr_payslip_worked_days.code)::text = 'Unpaid'::text))) AS leave_days,


    ( SELECT sum(hr_payslip_worked_days.number_of_days) AS sum
           FROM hr_payslip_worked_days
          WHERE ((hr_payslip_worked_days.payslip_id = hr_payslip.id) AND ((hr_payslip_worked_days.code)::text = 'Unpaid'::text))) * hr_payslip.kg_one_day_basic as leave_deductions,


hr_payslip.kg_total_deductions as other_deductions,

hr_payslip.kg_total_ot_hours as overtime,

   
hr_payslip.kg_total_ot_amt as overtime_amount,
(hr_payslip.kg_total_ot_amt / hr_payslip.kg_total_ot_hours) as overtime_rate,  
hr_payslip.kg_other_allowance as other_allowance,
       hr_payslip.kg_total_gross + hr_payslip.kg_total_ot_amt - hr_payslip.kg_total_deductions - hr_payslip.kg_leave_deductions as net_salary,


    hr_payslip.kg_remarks as kg_remarks

        """
        return select_str


    def _from(self):
        from_str = """
                hr_payslip
     JOIN hr_contract ON hr_contract.id = hr_payslip.contract_id

     JOIN hr_employee ON hr_employee.id = hr_contract.employee_id
 
 
        """
        return from_str

    def _group_by(self):
        group_by_str = """
                GROUP BY hr_payslip.id,
            hr_employee.kg_other_id,
hr_payslip.employee_id,
    hr_employee.job_id,
    hr_employee.department_id,
    hr_employee.working_company_id,
    hr_employee.visa_company_id,





    
    hr_payslip.od_payslip_days,


hr_payslip.kg_total_deductions,



 hr_employee.kg_payment_method,

    hr_payslip.kg_remarks
        """
        return group_by_str

    @api.model_cr
    def init(self):
        # self._table = sale_report
        tools.drop_view_if_exists(self.env.cr, self._table)
        self.env.cr.execute("""CREATE or REPLACE VIEW %s as (
            %s
            FROM ( %s )
            %s
            )""" % (self._table, self._select(), self._from(), self._group_by()))
