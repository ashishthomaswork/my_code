# -*- coding: utf-8 -*-
from odoo import fields,models,api,_
from odoo import tools
from datetime import date
from dateutil.relativedelta import relativedelta

from odoo.exceptions import UserError, AccessError
from odoo import api, fields, models, SUPERUSER_ID, _

class kg_attend_summ(models.Model):
    
    _name='kg.attend.summ'
    
        
    name = fields.Char('Name')
    
 
    
    kg_dep = fields.Many2one('hr.department','Department')
    
    kg_company_id = fields.Many2one('res.company','Company')
    
    kg_date = fields.Date('Date')
    
    kg_total_staff = fields.Integer('Total No od Staff')
    
    kg_present = fields.Integer('Present')
    
    kg_leave = fields.Integer('On Leave')

    kg_absent = fields.Integer('Absent')