# -*- coding: utf-8 -*-
from odoo import fields,models,api,_
from odoo import tools
from datetime import date
from dateutil.relativedelta import relativedelta

from odoo.exceptions import UserError, AccessError
from odoo import api, fields, models, SUPERUSER_ID, _

HOURS_PER_DAY = 8

class hr_holidays_status(models.Model):
    
    _inherit='hr.holidays.status'
    
    double_validation = fields.Boolean('Apply Double Validation',default=True)
    
    kg_paid_no_paid = fields.Selection([('paid','Paid'),('Unpaid','Unpaid')],'Type')

class Holidays(models.Model):
    
    _name = 'hr.holidays'
    
    _inherit = ['hr.holidays','mail.thread']
    
  #  _inherit = ['mail.thread', 'ir.needaction_mixin']
    
    state = fields.Selection([
        ('draft', 'To Submit'),
        ('cancel', 'Cancelled'),
        ('confirm', 'To Approve'),
     
        ('refuse', 'Refused'),
        ('validate1', 'Immediate Manager Approved'),
        ('validate', 'Approved')
        ], string='Status', readonly=True, track_visibility='onchange', copy=False, default='confirm',
            help="The status is set to 'To Submit', when a holiday request is created." +
            "\nThe status is 'To Approve', when holiday request is confirmed by user." +
            "\nThe status is 'Refused', when holiday request is refused by manager." +
            "\nThe status is 'Approved', when holiday request is approved by manager.")
    
    
    double_validation = fields.Boolean('Apply Double Validation', default=True)
    
    
    @api.multi
    def _track_subtype(self, init_values):
        if 'state' in init_values and self.state == 'validate':
            return 'hr_holidays.mt_holidays_approved'
        elif 'state' in init_values and self.state == 'validate1':
            return 'kg_abra.kg_holidays_first_validated'
        elif 'state' in init_values and self.state == 'confirm':
#             if self.employee_id.parent_id:
#                 vals_mail={}
#                 message="<b>Dear %(hr_man)s,</b><br/><br/> Leave Request from : %(hd_name)s  has been created" % {
#                              'hr_man': self.employee_id.parent_id and self.employee_id.parent_id.name or False,
#                                'hd_name': self.employee_id.name,
#                                   
#                         }
#                  
#                 vals_mail['message']=message
#                 vals_mail['partner']=self.employee_id.parent_id.user_id.login
#                 
#                 template_obj = self.env['mail.mail']
#                 template_data = {
#                 'subject': 'leave Approval',
#                 'body_html': message,
#              #   'email_from': sender,
#                 'email_to':self.employee_id.parent_id.user_id.login
#                 }
#                 template_id = template_obj.create(template_data)
#                 template_obj.send(template_id)
#              #   self.send_mail(vals_mail)  
                
            return 'hr_holidays.mt_holidays_confirmed'
        elif 'state' in init_values and self.state == 'refuse':
            return 'kg_abra.kg_holidays_refused'
        return super(Holidays, self)._track_subtype(init_values)
    
    
    
    
    
    
    @api.model
    def create(self,vals):
        
        res=super(Holidays, self).create(vals)
        emp_partner_id = res.employee_id and res.employee_id.user_id and res.employee_id.user_id.partner_id.id or False
        manager_id =res.employee_id and res.employee_id.parent_id and  res.employee_id.parent_id.user_id and res.employee_id.parent_id.user_id.partner_id.id or False
        hr_manager_id =res.employee_id and res.employee_id.kg_hr_manager and  res.employee_id.kg_hr_manager.user_id and res.employee_id.kg_hr_manager.user_id.partner_id.id or False
        partner_ids=[]
        if emp_partner_id is not False:
            partner_ids.append(emp_partner_id)
        if manager_id is not False:
            partner_ids.append(manager_id)
        if hr_manager_id is not False:
            partner_ids.append(hr_manager_id)
            
        print partner_ids,'1111111111111111'
        for partner in partner_ids:
            check= self.env['mail.followers'].search([('res_model','=','hr.holidays'),('partner_id','=',partner),('res_id','=',res.id)])
            print check,'1111111111111111'
          
            if not check:
                print 'inside loop',res
                fol={}
                fol['res_model']='hr.holidays'
                fol['res_id']=res.id
                fol['partner_id']=partner
                fol_id=self.env['mail.followers'].create(fol)
                print fol_id,'12222222222222'
                subtypes= self.env['mail.message.subtype'].search([('res_model','=','hr.holidays')]).ids
                if subtypes:
                    for i in subtypes:
                        self._cr.execute('INSERT INTO mail_followers_mail_message_subtype_rel (mail_followers_id, mail_message_subtype_id) values (%s, %s)', (fol_id.id, i))
        
        
        
                    
        res.write({'state':''})
        
        res.write({'state':'confirm'})
        return res





