# -*- coding: utf-8 -*-
from odoo import fields,models,api,_
from odoo import tools
from datetime import date
from dateutil.relativedelta import relativedelta

from odoo.exceptions import UserError, AccessError
from odoo import api, fields, models, SUPERUSER_ID, _

class kg_ot_master(models.Model):

    _name ="kg.ot.master"

    rate = fields.Float('Rate')

    public_holiday_id = fields.Many2one('kg.public.holiday',required="0")

    kg_note = fields.Text('Remarks',)

