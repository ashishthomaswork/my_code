# -*- coding: utf-8 -*-
from odoo import fields,models,api,_
from odoo import tools
from datetime import date
from dateutil.relativedelta import relativedelta

from odoo.exceptions import UserError, AccessError
from odoo import api, fields, models, SUPERUSER_ID, _
from datetime import datetime,date
from odoo.exceptions import except_orm



    
    

class kg_airfare_request(models.Model):

    _name ="kg.airfare.request"


    @api.multi
    def set_to_draft(self):
        self.kg_state = 'new'
        transaction_record = self.env['hr.payroll.transactions'].search([('ticket_request_id','=',self.id)])
        transaction_record.unlink()

    @api.multi
    def unlink(self):
        if self.kg_state != 'new':
			raise UserError(_('you cannot delete this record because it is not in new state'))


    name = fields.Char('Description')
    amount = fields.Float('Amount')
    kg_emp_id = fields.Many2one('hr.employee','Employee')

    requested_date = fields.Date('Requested Date',default=fields.Datetime.now)
    company_id = fields.Many2one(related='kg_emp_id.company_id', store=True, string='Company', readonly=True)

    include_in_payslip = fields.Boolean('Include in Payslip')

    kg_leave_id = fields.Many2one('hr.holidays','Leave Application')

    kg_emp_code = fields.Char('Emp Code')

    kg_des = fields.Many2one('hr.job','Designation')

    kg_dep = fields.Many2one('hr.department','Department')

    kg_leave_start = fields.Date('Start Date')
    kg_leave_end = fields.Date('End Date')

    kg_note = fields.Text('Remarks')
    
    kg_airfare = fields.Many2one('kg.airfare','Airfare')
    
    kg_state = fields.Selection([('new','New'),('approved','Approved'),('released','Released')],'State',default='new')

    @api.onchange('kg_airfare')
    def onchange_kg_airfare(self):
        self.amount=self.kg_airfare.kg_amount


    
    
    @api.onchange('kg_emp_id')
    def onchange_emp(self):
        self.kg_emp_code=self.kg_emp_id.barcode
        self.kg_des=self.kg_emp_id.job_id.id
        self.kg_dep=self.kg_emp_id.department_id.id
    
    
    @api.onchange('kg_leave_id')
    def onchange_leave(self):
        self.kg_leave_start=self.kg_leave_id.date_from
        self.kg_leave_end =self.kg_leave_id.date_to
        
        
    @api.multi
    def approve(self):
        for record in self:
            old_record = self.search([('kg_emp_id', '=', self.kg_emp_id.id),('id', '!=', self.id)],order='kg_leave_start desc',limit=1)
            print "mmmmmmmmmmmmm",old_record
            if not old_record:
                record.write({'kg_state':'approved'})

            old_date = record.kg_emp_id and record.kg_emp_id.joining_date
            old_date = str(fields.Datetime.from_string(old_date))
            company_id = record.company_id and record.company_id.id or False

            if not company_id:
                raise UserError(_('put company in employee master'))


            new_date = str(fields.Datetime.from_string(record.kg_leave_start))

            if old_record:
                old_date = str(fields.Datetime.from_string(old_record.kg_leave_start))
            diff = (datetime.strptime(new_date, '%Y-%m-%d %H:%M:%S') -  datetime.strptime(old_date, '%Y-%m-%d %H:%M:%S')).days +1
            print "DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD",diff
            if diff > 365:
                record.write({'kg_state':'approved'}) 

            else:
                raise UserError(_('Not Eligble For AirFare Request'))
##########################Adding to payroll transaction object

            ######Search for airfare rule
            leave_airfare_rule_id = self.env['hr.salary.rule'].search([('code','=','AFR'),('company_id','=',company_id)]) and self.env['hr.salary.rule'].search([('code','=','AFR'),('company_id','=',company_id)]).id or False

            if not leave_airfare_rule_id:
            	raise except_orm('Configure Airfare rule with code:AFR,check the company also')
########################
					
            self.env['hr.payroll.transactions'].create({'employee_id':self.kg_emp_id.id,'rule_id':leave_airfare_rule_id,
'date':record.kg_leave_start,'amount':record.amount,'ticket_request_id':self.id,'transaction_note_id':record.name})
                
    @api.multi
    def release(self):
        for record in self:
            record.write({'kg_state':'released'})
            
                 
        
        
    





