# -*- coding: utf-8 -*-
import time
from datetime import datetime
from odoo import fields, models, api, _
from odoo.exceptions import except_orm
from odoo import exceptions


class SalaryAdvancePayment(models.Model):
    _inherit = "salary.advance"
    
    
    kg_return_date = fields.Date('Return Date')
    
    