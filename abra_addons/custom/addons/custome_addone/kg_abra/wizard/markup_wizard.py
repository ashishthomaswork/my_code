# -*- coding: utf-8 -*-
from datetime import date
from dateutil.relativedelta import relativedelta

from odoo.exceptions import UserError, AccessError, ValidationError
from odoo import api, fields, models, SUPERUSER_ID, _

class kg_markup_wizard(models.TransientModel):
    _name = 'kg.markup.wizard'
    
    name = fields.Char('Name')
    
    mat_mark = fields.Float('Materials Markup')
    
    spe_mark  = fields.Float('Special Materials Markup')
    
    sub_mark = fields.Float('Subcontract Materials Markup')
    
    man_mark = fields.Float('Manpower Markup')
    
    parent_est_id = fields.Many2one('kg.estimation','Estimation')
    
    @api.model
    def default_get(self, fields_list):
        res = super(kg_markup_wizard, self).default_get(fields_list)
        est_obj = self.env['kg.estimation'].browse(self._context.get('active_id'))
        res.update({'parent_est_id':est_obj.id})
        return res
    
    @api.multi
    def save_markup(self):
        for record in self:
            for line in record.parent_est_id.kg_fin_lines:
                if line.kg_cat_id.name == 'Manpower':
                    line.write({'kg_mp':record.man_mark})
                elif line.kg_cat_id.name == 'Subcontractor Materials':
                    line.write({'kg_mp':record.sub_mark})
                elif line.kg_cat_id.name == 'Special Materials':
                    line.write({'kg_mp':record.spe_mark})
                else:
                    line.write({'kg_mp':record.mat_mark})
            
            
    
    
    
    