# -*- coding: utf-8 -*-
from datetime import date
from dateutil.relativedelta import relativedelta

from odoo.exceptions import UserError, AccessError, ValidationError
from odoo import api, fields, models, SUPERUSER_ID, _


class kg_pur_req(models.TransientModel):
    _name = 'kg.rfq.wiz'

    supplier_id = fields.Many2one('res.partner', string='Supplier')

    @api.multi
    def create_rfq(self):
        pr_ids = self._context.get('active_ids', False)
        pr_objs = self.env['kg.pur.req'].browse(pr_ids)
        rfq_exist = False
        for pr_obj in pr_objs:
            if pr_obj.kg_po_id:
                rfq_exist = True
        if rfq_exist:
            raise UserError(_('RFQ Exist for some requests!'))
        rfq = {
            'partner_id': self.supplier_id.id,
            'date_order': date.today(),
            'company_id': self.env.user.company_id.id,
            'currency_id': self.env.user.company_id.currency_id.id
        }
        rfq_id = self.env['purchase.order'].create(rfq)
        for pr_obj in pr_objs:
            lines = {
                'product_id': pr_obj.kg_item.id,
                'name': pr_obj.kg_item.name,
                'date_planned': pr_obj.kg_req_date,
                'product_qty': pr_obj.kg_qty,
                'product_uom': pr_obj.kg_uom.id,
                'price_unit': pr_obj.kg_item.standard_price,
                'order_id': rfq_id.id,
                'account_analytic_id': pr_obj.kg_project and pr_obj.kg_project.analytic_account_id.id,

            }

            self.env['purchase.order.line'].create(lines)
            pr_obj.kg_po_id = rfq_id.id
