from itertools import groupby
from datetime import datetime, timedelta

from odoo import api, fields, models, _
from odoo.exceptions import UserError
from odoo.tools import float_is_zero, float_compare, DEFAULT_SERVER_DATETIME_FORMAT
from odoo.tools.misc import formatLang

import odoo.addons.decimal_precision as dp

class KG_subjoborder_wizard(models.TransientModel):
    _name = 'kg.subjoborder.wizard'
    _description = 'kg.subjoborder.wizard'

    kg_description = fields.Char('Description')
    uom_id = fields.Many2one('product.uom',string="Uom")
    qty = fields.Float(string='Qty')

    kg_cat = fields.Many2one('kg.category','Category')

    start_date = fields.Date('Start Date')

    kg_parent_mrp_id = fields.Many2one('mrp.production','Parent Job')
    wiz_line = fields.One2many('kg.subjoborder.wizard.line', 'wiz_id', string='Wiz Line',)

	
	


  

    @api.multi
    def create_job(self):
        wiz_line = self.wiz_line
        mrp_obj = self.kg_parent_mrp_id
        dec = self.kg_description
        kg_parent_mrp_id = self.kg_parent_mrp_id and self.kg_parent_mrp_id.id


        kg_cat = self.kg_cat and self.kg_cat.id

        qty = self.qty
        uom_id = self.uom_id and self.uom_id.id 
        product_obj = self.env['product.template'].create({'name':dec,'uom_id':uom_id,'uom_po_id':uom_id,'kg_intermediate_pdt':True})

        product_id = product_obj and product_obj.id or False 
        mrp_product_id = self.env['product.product'].search([('product_tmpl_id','=',product_id)]) and self.env['product.product'].search([('product_tmpl_id','=',product_id)]).id or False 

        bom_obj = self.env['mrp.bom'].create({'product_tmpl_id':product_id,'uom_id':uom_id,'product_qy':qty})
        bom_id = bom_obj and bom_obj.id or False

        picking_type_id = mrp_obj.picking_type_id and mrp_obj.picking_type_id.id or False
        location_dest_id = mrp_obj.location_dest_id and mrp_obj.location_dest_id.id or False
        location_src_id = mrp_obj.location_src_id and mrp_obj.location_src_id.id or False
        kg_mr_picking_type_id = mrp_obj.kg_mr_picking_type_id and mrp_obj.kg_mr_picking_type_id.id or False

        start_date = str(self.start_date) + " 07:00:00"

        for line in wiz_line:
			product_id = line.product_id and line.product_id.id or False
			uom_id = line.product_id and line.product_id.uom_id and line.product_id.uom_id.id
			qty = line.qty	
			self.env['mrp.bom.line'].create({'product_id':product_id,'product_uom_id':uom_id,'product_qty':qty,'bom_id':bom_id})
		
        mrp_vals = {'product_id':mrp_product_id,'product_qty':qty,'product_uom_id':uom_id,
'bom_id':bom_id,'kg_client':mrp_obj.kg_client and mrp_obj.kg_client.id or False,'kg_tech_est_id':mrp_obj.kg_tech_est_id and mrp_obj.kg_tech_est_id.id or False,'kg_cat':kg_cat,
'kg_so_id':mrp_obj.kg_so_id and mrp_obj.kg_so_id.id,'kg_est_id':mrp_obj.kg_est_id and mrp_obj.kg_est_id.id or False,'kg_project_id':mrp_obj.kg_project_id and mrp_obj.kg_project_id.id,
'company_id':mrp_obj.company_id and mrp_obj.company_id.id,'kg_suborder':True,'kg_parent_id':kg_parent_mrp_id,'date_planned_start':start_date,
'picking_type_id': picking_type_id, 'location_src_id':location_src_id, 'location_dest_id':location_dest_id,'kg_mr_picking_type_id':kg_mr_picking_type_id }

        mrp_obj = self.env['mrp.production'].create(mrp_vals)
        mrp_obj.kg_update_bom_info()
       

       

    


        return True
	    

    
    
    @api.model
    def default_get(self, fields_list):
        res = super(KG_subjoborder_wizard, self).default_get(fields_list)
        mrp_obj = self.env['mrp.production'].browse(self._context.get('active_id'))
        rawmaterial_line = mrp_obj.kg_rawmaterials
        line_vals_array = []
        product_ids = []
        for line in rawmaterial_line:
            if line.kg_select:
                product_id = line.kg_product_id and line.kg_product_id.id
                product_uom_qty = line.kg_qty
                vals = (0,0,{'product_id':product_id,'qty':product_uom_qty,'wiz_id':self.id})
                line_vals_array.append(vals)
                product_ids.append(product_id)
        set_product_ids = list(set(product_ids))
        if len(set_product_ids) != len(product_ids):
            	raise UserError(_('some rawmaterilas defined more than one line,make it one line and update qty'))

	      	
#            purchase_price = line.product_id and line.product_id.standard_price or 0.0
#            tax_id = 1

#            if line.product_id.type not in ('service','consu'):
#            	vals = (0,0,{'unit_price':purchase_price,'product_id':product_id,'qty':product_uom_qty,'wiz_id':self.id,'product_uom':product_uom,'name':name,
#'tax_id':1})
#            	line_vals_array.append(vals)
#            
       
        res.update({'kg_parent_mrp_id':mrp_obj.id,'wiz_line':line_vals_array})
        return res

    
class KG_subjoborder_wizard_line(models.TransientModel):
    _name = 'kg.subjoborder.wizard.line'
    _description = 'kg.subjoborder.wizard.line'
    wiz_id = fields.Many2one('kg.subjoborder.wizard',string='Wiz')     
    product_id = fields.Many2one('product.product',string='Product')
    qty = fields.Float(string="Qty")
 
