# -*- coding: utf-8 -*-
{
    "name" : "KG Abra",
    "version" : "0.1",
    "author": "KG",
    "category" : "Others",
    "description": """KG Abra """,
    "depends": ['account_accountant', 'sale', 'purchase', 'project', 'mrp', 'stock', 'hr', 'crm', 'fleet','mail','stock_account','oh_employee_documents_expiry','ohrms_loan','ohrms_salary_advance','base', 'mail'],
    "data" : ['security/abra_groups.xml',
             'security/ir.model.access.csv',
# #            'report/od_customer_schedular_view.xml',
# #            'report/od_shift_details_view.xml'
      'data/kg_est_code.xml',
      'data/kg_boq_code.xml',
      'data/kg_out_log_code.xml',
       'data/kg_log_req_data.xml',
          'data/kg_sample_material_code.xml',
        'data/kg_req_pass_code.xml',
         'data/kg_pack_list_code.xml',
          'data/kg_des_req_code.xml',
          'data/kg_rfi_code.xml',
          'data/kg_helpdesk_code.xml',
          'data/kg_picking_seq.xml',
           'data/kg_job_data.xml',
		  'wizard/subjob_wizard_view.xml',
		  'wizard/create_rfq_wizard_view.xml',
           'wizard/markup_wizard_view.xml',

      'views/kg_estimation_view.xml',
      'views/kg_boq_request_view.xml',
      'views/kg_log_req_view.xml',
       'views/kg_sample_material_view.xml',
        'views/kg_request_passport.xml',
        'views/kg_pack_list_view.xml',
        'views/kg_out_log_view.xml',
        'views/kg_partner_view.xml',
#        'views/kg_loan_view.xml',
        'views/kg_timesheet_view.xml',
        

      'data/kg_painting_data.xml',
      'data/kg_packing_spec_data.xml',
        'data/kg_category_data.xml',
        'data/kg_att_incl_data.xml',
        
        'data/kg_glass_spec_data.xml',
        
         'data/kg_app_changes_data.xml',
        
       
   
     
         'hr_views/prob_eval_view.xml',
         'hr_views/hr_payroll_view.xml',

         'hr_views/hr_contract_view.xml',
         'hr_views/loans_view.xml',
         'hr_views/payroll_transaction_view.xml',
         'hr_models/report/kg_fsime_salary_sheet_view.xml',





         'hr_views/public_holiday_view.xml',
         'hr_views/ot_master_view.xml',


         'hr_views/hr_holidays_view.xml',


         'hr_views/shift_master.xml',
         'hr_views/hr_job_view.xml',

         'hr_views/end_settlement_request_view.xml',




         
         'hr_views/req_cert_view.xml',
         
         'hr_views/joining_note_view.xml',
         
         'hr_views/interview_assess_view.xml',
         
          'hr_views/manpower_req_view.xml',
          'hr_views/leave_salary_request_view.xml',
          'hr_views/airfare_request_view.xml',
          
            'hr_views/daily_attend_view.xml',
            
            'hr_views/attend_summ_view.xml',
            
            
            
                        'hr_views/end_settle_view.xml',
                        
                        'hr_views/ot_req_view.xml',


         
         'hr_views/emp_transfer_view.xml',
         'hr_views/kg_hr_masters.xml',
         
          'inherited_models/attachment_in_all_views.xml',
          'inherited_models/user_view.xml',
          
           'inherited_reports/po_report.xml',
           
            'inherited_reports/purchase_order_templates.xml',
            
            
            
             'inherited_reports/so_report.xml',
           
            'inherited_reports/sale_report_templates.xml',
            
            
              'inherited_reports/inv_report.xml',
           
            'inherited_reports/report_invoice.xml',
            
             'inherited_reports/purchase_quotation_templates.xml',
             
             'inherited_models/crm_lead_view.xml',
             
               'views/kg_design_request_view.xml',
               
                 'data/kg_doc_req_data.xml',
                 
                 'views/kg_sale_revision_view.xml',
                       
                 'views/kg_inv_req_view.xml',
               
                       'views/kg_pur_req_view.xml',
                       
                        'views/kg_mat_req_view.xml',
                        
                         'views/kg_qc_view.xml',
                         
                             'views/kg_rfi_view.xml',
                             
                                        'views/kg_helpdesk_view.xml',
                                        
                                           'views/kg_addmat_req_view.xml',
                                           
                                            'views/kg_install_req_view.xml',
                                            
                                                      'views/kg_fitout_view.xml',
                                                      
                                                                        'inherited_models/po_view.xml',
                                                                        
                                                                         'inherited_models/kg_fleet_view.xml',
                                                                         
                                                                         
                                                                         
                        'views/kg_tripsumm_view.xml',
                        
                        'reports/kg_report_insrep.xml',
                        
                          'reports/kg_report_views.xml',
                          
                          
                           'views/kg_overinstall_view.xml',

    'views/kg_overinstall_expense_views.xml',         
    
      'views/kg_delivery_request_view.xml',          
      'views/kg_invoice_request_view.xml',     
      'views/kg_packing_requisition_view.xml',      
      
       'views/kg_export_view.xml',    
       
       'views/menu_view.xml',    
       
          'views/kg_other_menu_views.xml',   
          
          'hr_views/salary_advance_view.xml',        
           'hr_views/hr_holiday_status.xml',             
           'views/kg_cust_targ_view.xml',                              
                       

            ],
    'css': [],
    'installable': True,
    'auto_install': False,
    'application': True,
}
