# -*- coding: utf-8 -*-
from odoo import fields,models,api,_
from odoo import tools
from datetime import date
from dateutil.relativedelta import relativedelta

from odoo.exceptions import UserError, AccessError,ValidationError
from odoo import api, fields, models, SUPERUSER_ID, _

class PurchaseOrder(models.Model):
    
    _inherit = "purchase.order"
    
    kg_att_url1 = fields.Char('')
    
    kg_att_url2 = fields.Char('')
    
    
    kg_att_url3 = fields.Char('')
    
    
    kg_att_url4 = fields.Char('')
    
    kg_att_url5 = fields.Char('')
    
    kg_fin_app = fields.Boolean('Finance Approved')
    
    kg_app_by = fields.Many2one('res.users','Apporved By')
    
    kg_store_mgr = fields.Many2one('res.users','Store Manager')
    
    kg_po_sent  = fields.Boolean('Sent PO to Store manager')
    
    
    @api.multi
    def fin_app(self):
        for record in self:
            record.write({'kg_app_by':self.env.user.id,'kg_fin_app':True})
            
    @api.multi
    def send_po_str_mgr(self):
        for record in self:
            if not record.kg_store_mgr:
                raise ValidationError(_('Add Store manager.'))
            subject = "<b>  %(comp_name)s   Order (Ref  %(order)s ) </b>"% {
                     'comp_name': record.company_id.name,
                       'order': record.name,
                }
            
            
            
            body_html="<b>Dear %(store_mgr)s,</b><br/><br/> Purchase Order with Ref %(order)s has been created.Please Don Needfu" % {
                     'store_mgr': record.kg_store_mgr.name,
                       'order': record.name,
                }
            
        
         
            
            
           
        
    
            main_content = {
                'subject': subject,
          #      'author_id': record.env.user.partner_id.id,
                'body_html': body_html,
                'email_to': record.kg_store_mgr.login,
            }
            print main_content,'Sore Mnaager'
            self.env['mail.mail'].create(main_content).send()
    
    
    
    