#-*- coding:utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

import time
from datetime import datetime, timedelta
from dateutil import relativedelta

import babel

from odoo import api, fields, models, tools, _
from odoo.exceptions import UserError, ValidationError
from odoo.tools.safe_eval import safe_eval

from odoo.addons import decimal_precision as dp




class HrContract(models.Model):

    _inherit = 'hr.contract'
    
    
   
    @api.depends('wage', 'od_allowance_rule_line_ids.amt')
    def _get_total_wage(self):
        lines = self.od_allowance_rule_line_ids
     
        tot = 0
        for line in lines:
            tot = tot + line.amt

        self.od_total_wage = tot + self.wage
        
    @api.multi 
    @api.one 
    @api.constrains('employee_id','state') 
    def _check_constriant(self): 
        state = self.state
        employee_id = self.employee_id and self.employee_id.id
        contract = self.env['hr.contract'].search([('employee_id','=',employee_id),('state','in',('open','pending'))])
        if len(contract) > 1:
            raise UserError(_('two active contract found.'))
 


    od_total_wage = fields.Float(string="Total Wage",compute='_get_total_wage')
    od_allowance_rule_line_ids = fields.One2many('od.allowance.rule.line','contract_id','Rule Lines')
    
    kg_leave_salary_amt= fields.Float('Leave Salary Amount',compute='get_leave_salary_amt')
    
    @api.depends('wage','od_allowance_rule_line_ids')
    def get_leave_salary_amt(self):
        for record in self:
            lines = record.od_allowance_rule_line_ids
         
            tot = 0
            for line in lines:
                if line.kg_incl_leave_sal== True:
                    tot = tot + line.amt
    
            record.kg_leave_salary_amt = tot + record.wage
            
            
    kg_oneday_absent_ded= fields.Float('One Day Absent Deduction',compute='get_kg_oneday_absent_ded')
    
    @api.depends('wage','od_allowance_rule_line_ids')
    def get_kg_oneday_absent_ded(self):
        for record in self:
            lines = record.od_allowance_rule_line_ids
         
            tot = 0
            for line in lines:
                if line.kg_def_leave== True:
                    tot = tot + line.amt
    
            record.kg_oneday_absent_ded =( tot + record.wage)/30.0
    
class odallowance_rule_line(models.Model):
    _name = 'od.allowance.rule.line'
    _description = 'Contract Allowance Rule Line'

    contract_id = fields.Many2one('hr.contract','Contract ID',ondelete='cascade')
    rule_type = fields.Many2one('hr.salary.rule','Allowance Rule')
    code = fields.Char(string='Code')
    amt = fields.Float('Amount')
    
    kg_incl_leave_sal = fields.Boolean('Include in Leave Salary')
    
    kg_def_leave = fields.Boolean('Deduct on Leave')
