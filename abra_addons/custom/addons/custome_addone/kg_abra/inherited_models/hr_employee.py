# -*- coding: utf-8 -*-
from odoo import fields,models,api,_
from odoo import tools
from datetime import date
from dateutil.relativedelta import relativedelta

from odoo.exceptions import UserError, AccessError
from odoo import api, fields, models, SUPERUSER_ID, _

class kg_airfare(models.Model):
    
    _name="kg.airfare"
    
    name  = fields.Char('City',required="True")
    
    kg_country = fields.Many2one('res.country','Country')
    
    kg_amount = fields.Float('Amount')
    

class KgSponsorCompany(models.Model):
    _name = 'kg.sponsor.company'

    name = fields.Char(string = 'Name')

class KgVisaCompany(models.Model):
    _name = 'kg.visa.company'

    name = fields.Char(string = 'Name')


class KgBranch(models.Model):
    _name = 'kg.branch'

    name = fields.Char(string = 'Name')

class KgVisaProfession(models.Model):
    _name = 'kg.visa.profession'

    name = fields.Char(string='Name')



class hr_employee(models.Model):
    
    _inherit="hr.employee"
    
    kg_it_admin = fields.Many2one('hr.employee','IT Admin')
    shift_id = fields.Many2one('kg.shift.master','Shift')
    
    
    kg_hr_manager = fields.Many2one('hr.employee','HR Manager')


    kg_other_id = fields.Char(string="Other ID")
    kg_wps_routing_code = fields.Char(string="WPS Routing Code")

    working_company_id = fields.Many2one('kg.sponsor.company','Working Company')

    visa_company_id = fields.Many2one('kg.visa.company','Visa Company')



    kg_joining_date = fields.Date(string="Joining Date")
    kg_visa_no = fields.Char(string="VISA Number") 
    kg_uid_number = fields.Char(string="UID/Visa Number") 
    kg_eid_number = fields.Char(string="Emirates ID Number")
    kg_blood_group = fields.Char(string="Blood Group")

    kg_religion = fields.Char(string="Religion")

    kg_bank_name = fields.Char(string="Bank Name")
    kg_high_quali = fields.Char(string="Highest Qualification")
    kg_insurancecard_number = fields.Char(string="Insurance Card Number")

    kg_local_tel = fields.Char(string='Tel')
    kg_local_mobile = fields.Char(string='Mobile')
    kg_local_address = fields.Text(string='Address')
    kg_local_email = fields.Char(string='Email')
    kg_local_second_email = fields.Char(string='Other Email')
    kg_local_contact = fields.Char(string='Contact Person')
    kg_local_contact_mobile = fields.Char(string='Mobile')
    kg_local_contact_rel = fields.Char(string='Relationship')

    kg_home_tel = fields.Char(string='Tel')
    kg_home_mobile = fields.Char(string='Mobile')
    kg_home_address = fields.Text(string='Address')
    kg_home_contact = fields.Char(string='Contact Person')
    kg_home_contact_mobile = fields.Char(string='Mobile')
    kg_home_contact_rel = fields.Char(string='Relationship')
    eligible_leave_year = fields.Float('Leave Eligible(Year)')

    leave_percentage_carryforward = fields.Float('Leave Carryforward(%)')
    shift_id = fields.Many2one('kg.shift.master','Shift')

    kg_category = fields.Selection([
        ('mgt', 'Management'),
        ('nmgt', 'Non-Management'),
        ], string='Staff Category',required="1",default='mgt')

    kg_payment_method = fields.Selection([
        ('wps', 'WPS'),
        ('bank', 'Bank'),
        ('cash', 'Cash'),
        ], string='Payment Method',required="1",default='wps')
    
    kg_att_url1 = fields.Char('')
    
    kg_att_url2 = fields.Char('')
    
    
    kg_att_url3 = fields.Char('')
    
    
    kg_att_url4 = fields.Char('')
    
    kg_att_url5 = fields.Char('')
    
    kg_trainer = fields.Many2one('hr.employee','Trainer')
    
    kg_start_date  = fields.Date('Start Date')
    
    kg_end_date = fields.Date('End Date')  
    
    
    kg_airfare = fields.Selection([
        ('once', 'Yearly One'),
        ('twice', 'Yearly twice')
        ], string='Airfare Eligibility',required="1",default='once')
    
    kg_visa_type =fields.Selection([('limited','Limited'),('unlimited','Unlimited')],'Visa Type')


    kg_arabic_name = fields.Char(string='Arabic Name')
    kg_labour_card_no = fields.Char(string='Labour Card No')
    kg_branch = fields.Many2one('kg.branch',string='Branch')
    kg_visa_profession = fields.Many2one('kg.visa.profession', string='Visa Profession')
    kg_probation_period = fields.Integer(string='Probation Period(Days)')
    kg_notice_period = fields.Integer(string='Notice Period(Days)')
    kg_type = fields.Selection([
        ('mgt', 'Manager'),
        ('admin', 'Admin'),
        ('tech', 'Technicians'),
        ], string='Staff Category',required="1",default='mgt')
    kg_visa_expiry = fields.Date(string='Visa Expiry Date')
    kg_overtime_cost = fields.Selection([
        ('neligibile', 'Not eligible'),
        ('eligibile', 'Eligible')
        ], string='Overtime Cost',required="1",default='neligibile')

    

    @api.onchange('kg_type')
    def _onchange_type(self):
        if self.kg_type == 'mgt':
            self.manager = True
        else:
            self.manager = False
