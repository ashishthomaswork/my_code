# -*- coding: utf-8 -*-
from odoo import fields,models,api,_
from odoo import tools
from datetime import date
from dateutil.relativedelta import relativedelta

from odoo.exceptions import UserError, AccessError
from odoo import api, fields, models, SUPERUSER_ID, _


class StockPicking(models.Model):
	_inherit='stock.picking'

	kg_pickup = fields.Boolean('Pickup Required')

	kg_log_req_count = fields.Integer('Logistics Count',compute='compute_log_req_count')

	kg_qc_req = fields.Boolean('QA Required')

	kg_qc_count = fields.Integer('QC Count',compute='compute_qc_count')

	kg_purchase_req_line = fields.One2many('kg.pur.req','kg_picking_id',string='Purchase Requisition')

	kg_project = fields.Many2one('project.project',string='Project')

	kg_job_no = fields.Many2one('mrp.production',string='Production')

	kg_pr_job_exist = fields.Boolean('PR Job Created', default=False)






	@api.multi
	def compute_log_req_count(self):
		for record in self:
			est_obj = self.env['kg.log.req']

			record.kg_log_req_count = est_obj.search_count([('kg_del_note', '=',record.id)])



	@api.multi
	def compute_qc_count(self):
		for record in self:
			est_obj = self.env['kg.qc']

			record.kg_qc_count = est_obj.search_count([('kg_delivery_note', '=',record.id)])


	@api.multi
	def kg_create_purchase_requisition(self):
		for record in self:
			for line in record.move_lines:
				kg_mat_cat = line.product_id and line.product_id.categ_id and line.product_id.categ_id.id
				kg_item = line.product_id and line.product_id.id
				pr_ids = self.env['kg.pur.req'].create({
					'kg_job_no':record.kg_job_no and record.kg_job_no.id,
					'kg_project':record.kg_project and record.kg_project.id,
					'kg_picking_id':record.id,
					'kg_item':kg_item,
					'kg_mat_cat':kg_mat_cat,
					'kg_qty':line.product_uom_qty,
					'kg_uom':line.product_uom and line.product_uom.id,
				})
			record.kg_pr_job_exist = True





