# -*- coding: utf-8 -*-
from odoo import fields,models,api,_
from odoo import tools
from datetime import date,datetime
from dateutil.relativedelta import relativedelta

from odoo.exceptions import UserError, AccessError
from odoo import api, fields, models, SUPERUSER_ID, _
from odoo.api import multi


class crm_lead(models.Model):
    
    _inherit="crm.lead"
    
    
    
    kg_att_url1 = fields.Char('')
    
    kg_att_url2 = fields.Char('')
    
    
    kg_att_url3 = fields.Char('')
    
    
    kg_att_url4 = fields.Char('')
    
    kg_att_url5 = fields.Char('')
    
    
    kg_boq_count = fields.Integer('BOQ Count',compute='compute_boq_count')
    
    kg_design_req_count = fields.Integer('Design Request Count',compute='compute_design_req_count')
    
    
    kg_ind_mast_id = fields.Many2one('kg.ind.mast','Industry Master')
    
    
    kg_no_update = fields.Boolean('No Updates',compute='comp_update')
    
    kg_lost_reason  = fields.Text('Lost Reason')
    
    
    @api.multi
    def comp_update(self):
        for record in self:
            if record.create_date and record.write_date:
                create_date = fields.Datetime.from_string(record.create_date)
                write_date = fields.Datetime.from_string(record.write_date)
                difference_write = relativedelta(write_date, datetime.now())
                diff_create = relativedelta(create_date, datetime.now())
                days_write = difference_write.days
                days_create = diff_create.days
                if days_create >= 7:
                    if days_write >= 7:
                        record.kg_no_update  =True

    @api.multi
    def compute_boq_count(self):
        for record in self:
            BOQ = self.env['kg.boq.request']
            
            record.kg_boq_count = BOQ.search_count([('kg_opp_id', '=',record.id)]) 
            
    @api.multi
    def compute_design_req_count(self):
        for record in self:
            des_req = self.env['kg.design.request']
            
            record.kg_design_req_count = des_req.search_count([('kg_opp_id', '=',record.id)]) 

    
    
    @api.multi
    def create_boq(self):
        
        for record in self:
            vals={}
            vals['kg_data_request']=date.today()
            
            vals['kg_opp_id']=record.id
            
            vals['kg_client']=record.partner_id.id

            vals['kg_dep_id']=2         
            vals['kg_att_url1']=record.kg_att_url1
            vals['kg_att_url2']=record.kg_att_url2
            vals['kg_att_url3']=record.kg_att_url3
            vals['kg_att_url4']=record.kg_att_url4
            vals['kg_att_url5']=record.kg_att_url5
            self.env['kg.boq.request'].create(vals)
            
            
    @api.multi
    def create_design_req(self):
        for record in self:
            vals={}
            vals['kg_opp_id']=record.id
            vals['kg_dep_id']=2            
            vals['kg_client']=record.partner_id.id
            self.env['kg.design.request'].create(vals)
    
