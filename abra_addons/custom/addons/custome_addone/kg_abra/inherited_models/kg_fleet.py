# -*- coding: utf-8 -*-
from odoo import fields,models,api,_
from odoo import tools
from datetime import date
from dateutil.relativedelta import relativedelta

from odoo.exceptions import UserError, AccessError
from odoo import api, fields, models, SUPERUSER_ID, _


class FleetVehicle(models.Model):
    _inherit='fleet.vehicle'
    
    kg_veh_type = fields.Many2one('kg.veh.type','Vehicle Type')
    
    


class kg_veh_type(models.Model):
    
    _name ='kg.veh.type'
    
    name = fields.Char('Name',required=True)