# -*- coding: utf-8 -*-
from odoo import fields,models,api,_
from odoo import tools
from datetime import date
from dateutil.relativedelta import relativedelta

from odoo.exceptions import UserError, AccessError
from odoo import api, fields, models, SUPERUSER_ID, _
from odoo.api import multi
from copy import copy

class kg_ind_mast(models.Model):

    _name ='kg.ind.mast'

    name = fields.Char('Name',required=True)

    remarks = fields.Char('Remarks')

class res_partner(models.Model):

    _inherit='res.partner'


    kg_att_url1 = fields.Char('')

    kg_att_url2 = fields.Char('')


    kg_att_url3 = fields.Char('')


    kg_att_url4 = fields.Char('')

    kg_att_url5 = fields.Char('')

    kg_ind_mast_id = fields.Many2one('kg.ind.mast','Industry Master')

    kg_trno  = fields.Char('TRNo')





class product_template(models.Model):

    _inherit="product.template"


    kg_att_url1 = fields.Char('')

    kg_att_url2 = fields.Char('')


    kg_att_url3 = fields.Char('')


    kg_att_url4 = fields.Char('')

    kg_att_url5 = fields.Char('')

    kg_intermediate_pdt = fields.Boolean(string='Intermediate Product')




class mrp_production(models.Model):

    _inherit="mrp.production"




    @api.multi
    def open_qc(self):


        action = self.env.ref('kg_abra.action_kg_qc').read()[0]



        return action


    @api.multi
    def ot_req(self):
        raise UserError(_('Sorry, Under Construction'))

        return True



####main produce function

#    @api.multi
#    def complete_production(self):

#        mr_req =self.env['kg.mat.req'].search([('kg_mrp_id','=',self.id)])
#        sub_jobs = self.kg_child_ids

    @api.model
    def create(self, values):
        if not values.get('name', False) or values['name'] == _('New'):
            if values.get('picking_type_id'):
                values['name'] = self.env['stock.picking.type'].browse(values['picking_type_id']).sequence_id.next_by_id()
            else:
                if values.get('kg_parent_id'):
                    parent_obj = self.env['mrp.production'].browse(values.get('kg_parent_id'))
                    total_suborders = len(self.env['mrp.production'].search([('kg_parent_id','=',values.get('kg_parent_id'))])) or 0
                    parent_name = parent_obj and parent_obj.name
                    count = total_suborders + 1
                    values['name'] = parent_name + "-"+str(count)
                else:
                    values['name'] = self.env['ir.sequence'].next_by_code('mrp.production') or _('New')
        if not values.get('procurement_group_id'):
            values['procurement_group_id'] = self.env["procurement.group"].create({'name': values['name']}).id
        production = super(mrp_production, self).create(values)
#        production._generate_moves() ##commented because two times it creating rawmaterial line
        return production



    @api.multi
    def check(self):
        mr_req =self.env['kg.mat.req'].search([('kg_mrp_id','=',self.id)])
        sub_jobs = self.kg_child_ids
        kg_suborder = self.kg_suborder
        if kg_suborder or self.one_job:



            req_id = mr_req and mr_req.id or False

            if len(mr_req) == 0:

                raise UserError(_('no material issued for this production'))


            if req_id and mr_req.kg_state == 'draft':
                raise UserError(_('store not yet released materials'))


            if req_id and mr_req.kg_state == 'release':
                raise UserError(_('Please acknowledge the MR'))


        else:

            main_bom_id = self.bom_id and self.bom_id.id
            main_bom_obj = self.bom_id
            no_of_job = len(sub_jobs)
            cancelled_jobs = []


            for job in sub_jobs:

                if job.state == 'cancel':
                    cancelled_jobs.append(job.id)

                if job.state not in ('cancel','done'):
                    raise UserError(_('some jobs are under progress,you cannot complete this main job until all the jobs finish'))

                bom_lines = self.env['mrp.bom.line'].create({'bom_id':main_bom_id,'product_id':job.product_id and job.product_id.id,'product_qty':job.product_qty,'product_uom_id':job.product_uom_id and job.product_uom_id.id})

            if no_of_job == len(cancelled_jobs) and no_of_job:
                    raise UserError(_('all the sub jobs are cancelled,better to start new Production order'))


            self.kg_update_bom_info()
            self.action_assign()

        return True


    @api.multi
    def kg_create_additional_material_requisition(self):
#        move_raw_ids = self.move_raw_ids
#        additional_items = []
#        for line in move_raw_ids:

#			if line.kg_additem:
#				additional_items.append(line.id)
#        if len(additional_items) == 0:
#			raise UserError(_('there is no additional material requisition needed'))

        add_mr_req =self.env['kg.addmat.req'].search([('kg_mrp_id','=',self.id)])

        if len(add_mr_req) > 0:

            raise UserError(_('ADD.MR already created'))
        sub = self.kg_suborder

        state = self.state
        if state in ('cancel','done'):

            raise UserError(_('check the state of this job order,you cannot raise ADD.Material Requistion in cancel and done state'))


        start_date = self.date_planned_start

        kg_client = self.kg_client and self.kg_client.id or False
        kg_project = self.kg_project_id and self.kg_project_id.id or False
        uid = self.env.user and self.env.user.id
        name = "ADD.Material Requistion" + "-"+self.name

        consumed_lines = self.move_raw_ids

        material_requisition_obj = self.env['kg.addmat.req'].create({'name':name,'kg_raised_by':uid,'kg_client':kg_client,'kg_project':kg_project,'kg_mrp_id':self.id})

#        for line in consumed_lines:
#			product_id = line.product_id and line.product_id.id
#			qty = line.product_uom_qty
#			umo_id = line.product_uom and line.product_uom.id
#			vals = {'kg_product':product_id,'kg_uom':umo_id,'kg_qty':qty,'kg_req_date':start_date,'kg_mat_req_id':material_requisition_obj and material_requisition_obj.id}

#			material_requisition_line_obj = self.env['kg.mat.req.lines'].create(vals)

        if not kg_client or not kg_project:
            raise UserError(_('please check client details and project details are filled properly'))

        if not start_date:
            raise UserError(_('please define the Deadline Start'))




    @api.multi
    def kg_create_material_requisition(self):


        mr_req =self.env['kg.mat.req'].search([('kg_mrp_id','=',self.id)])
        smove_lines = []
        picking_obj = self.env['stock.picking']

        if len(mr_req) > 0:

            raise UserError(_('MR already created'))



        sub = self.kg_suborder

        state = self.state
        if state in ('cancel','done'):

            raise UserError(_('check the state of this job order,you cannot raise Material Requistion in cancel and done state'))


        start_date = self.date_planned_start

        kg_client = self.kg_client and self.kg_client.id or False
        kg_project = self.kg_project_id and self.kg_project_id.id or False
        uid = self.env.user and self.env.user.id
        name = "Material Requistion" + "-"+self.name



        #Create BOM for master MO when no sub orders
        if self.one_job and len(self.kg_child_ids) == 0:
            if self.bom_id and len(self.bom_id.bom_line_ids) == 0:
                for rml in self.kg_rawmaterials:
                    bom_lines = self.env['mrp.bom.line'].create({'bom_id':self.bom_id.id,'product_id':rml.kg_product_id and rml.kg_product_id.id,'product_qty':rml.kg_qty,'product_uom_id':rml.kg_uom and rml.kg_uom.id})
                    self.kg_update_bom_info()

        material_requisition_obj = self.env['kg.mat.req'].create({'name':name,'kg_raised_by':uid,'kg_client':kg_client,'kg_project':kg_project,'kg_mrp_id':self.id})
        consumed_lines = self.move_raw_ids


        for line in consumed_lines:
            product_id = line.product_id and line.product_id.id
            name = line.product_id and line.product_id.name
            qty = line.product_uom_qty
            umo_id = line.product_uom and line.product_uom.id
            vals = {'kg_product':product_id,'kg_uom':umo_id,'kg_qty':qty,'kg_req_date':start_date,'kg_mat_req_id':material_requisition_obj and material_requisition_obj.id}
            smove = {'name':name,'product_id': product_id, 'product_uom_qty': qty, 'product_uom': umo_id}
            smove_lines.append((0,0,smove))

            material_requisition_line_obj = self.env['kg.mat.req.lines'].create(vals)

        picking_type_id = self.kg_mr_picking_type_id and self.kg_mr_picking_type_id.id or False
        location_id = self.kg_mr_picking_type_id and self.kg_mr_picking_type_id.default_location_src_id and self.kg_mr_picking_type_id.default_location_src_id.id or False
        location_dest_id = self.kg_mr_picking_type_id and self.kg_mr_picking_type_id.default_location_dest_id and self.kg_mr_picking_type_id.default_location_dest_id.id or False
        kg_project_id = self.kg_project_id and self.kg_project_id.id or False
        if not picking_type_id:
            raise UserError(_('Material requisition picking type not set!'))
        picking = picking_obj.create({
            'kg_project':kg_project_id,
            'kg_job_no':self.id,
            'move_lines':smove_lines,
            'partner_id':kg_client,
            'picking_type_id': picking_type_id,
            'location_id':location_id,
            'location_dest_id':location_dest_id,
        })
        material_requisition_obj.picking_id = picking.id
        if not kg_client or not kg_project:
            raise UserError(_('please check client details and project details are filled properly'))

        if not start_date:
            raise UserError(_('please define the Deadline Start'))







    @api.multi
    def kg_update_bom_info(self):
        production = self
        bom_obj = self.bom_id
        product_qty = self.product_qty
        mo_id = self.id

        production.product_qty = product_qty
        wizard_obj = self.env['change.production.qty'].create({'mo_id':mo_id,'product_qty':product_qty})


        wizard_obj.change_prod_qty()
        return True



    def create_suborders(self):
        raw_material_lines = self.kg_rawmaterials
        if len(raw_material_lines) == 0:
            raise UserError(_('no Rawmaterials specified'))
        raw_material_line_ids = []
        for line in raw_material_lines:
            if line.kg_select:
                raw_material_line_ids.append(line.id)

        if len(raw_material_line_ids) ==0:
            raise UserError(_('no Rawmaterials selected'))

        return {
              'view_type': 'form',
              "view_mode": 'form',
              'res_model': 'kg.subjoborder.wizard',
              'type': 'ir.actions.act_window',
              'target': 'new'
              }



    def kg_get_move_vals(self):

        date = fields.Date.context_today(self)

        ref = self.name
        journal_id = self.get_id_from_param('kg_overhead_journal')
        move_vals = {'date': date,'journal_id': journal_id,'ref':ref}
        return move_vals


    def get_id_from_param(self,param):
        parameter_obj = self.env['ir.config_parameter']
        key =[('key', '=', param)]
        param_obj = parameter_obj.search(key)
        if not param_obj:



            raise UserError(_('NoParameter Not defined\nconfig it in System Parameters with %s') % param)
        result_id = param_obj.value
        return result_id






    def kg_get_account_move_lines(self):
        move_lines =[]
        debit_amount = 0.0

        cred_amount  = 0.0
        ref = self.name
        date = fields.Date.context_today(self)
        move_line_vals = {'name': ref,'ref': ref,'date': date}
        mrp_obj = self
        for line in self.kg_overhead_line:
            qty = line.kg_qty_hour
            product_id = line.kg_product and line.kg_product.id or False
            account_id = line.kg_product and line.kg_product.categ_id and line.kg_product.categ_id.property_account_expense_categ_id and line.kg_product.categ_id.property_account_expense_categ_id.id or False
            print "vvvvvvvvvvvvvvvvvvvvvvvvvveeeeeeeeeeeeeeeeeeeeeeeeeeeeee111111111",account_id

            if not account_id:
                raise UserError(_('Overhead Product Expense Account Not Set'))

            if line.kg_auto:
                eval_code = line.kg_product and  line.kg_product.kg_eval_code
                if not eval_code:
                    raise UserError(_('Product %s Overhead Eval Code Expression Not set') %line.product_id.name)

                cost = eval(eval_code)
                cred_amount = round(qty * cost,2)
            else:
                cred_amount = round(line.amount)

            vals = copy(move_line_vals)
            vals.update({'account_id':account_id,'credit':cred_amount,'debit':0.0,'product_id':product_id,'quantity': qty})
            move_lines.append((0,0,vals))
            debit_amount += cred_amount





        debit_lines = copy(move_line_vals)
        acc_id = int(self.get_id_from_param('kg_production_acc'))
        move_line_length =len(move_lines) - 1
        print "ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc++++++++++++++++++",move_line_length
        cr_amount = 0.0
        for cr_index in range(move_line_length):
            cr_amount += move_lines[cr_index][2]['credit']
        credit_amount = debit_amount - cr_amount
        move_lines[move_line_length][2]['credit'] = credit_amount
        debit_lines.update({'account_id':acc_id,'debit':debit_amount,'credit':0.0})
        move_lines.append((0,0,debit_lines))
        return move_lines


    @api.multi
    def kg_create_move(self):
        self_id = self.id
        move_obj = self.env['account.move']
        move_vals = self.kg_get_move_vals()
        planned_qty = self.product_qty
#        factor = producing_qty/planned_qty
        move_lines = self.kg_get_account_move_lines()

        print "cvfffffffffffffffffffffffffffffsdddwweee",move_lines

        vals = copy(move_vals)
        vals.update({'line_ids':move_lines})
        move_id = move_obj.create(vals).id
        if move_id:
            account_move_obj = self.env['account.move'].browse(move_id)
            account_move_obj.post()



            self.env['kg.mrp.account.lines'].create({'kg_mrp_id':self_id,'account_move_id':account_move_obj.id,
'internal_sequence_number':account_move_obj.name,'ref':account_move_obj.ref,'journal_id':account_move_obj.journal_id and account_move_obj.journal_id.id,'status':account_move_obj.state,'amount':account_move_obj.amount})
        return move_id



    @api.one
    @api.depends('kg_overhead_line.amount','kg_overhead_line.kg_cost')
    def _compute_overhead_total(self):
        tot = 0
        kg_overhead_line = self.kg_overhead_line
        for line in kg_overhead_line:
            tot = tot + line.amount


        self.kg_overhead_total = tot



#@RRM  overriding function to calculate finshed product unit cost
    @api.multi
    def post_inventory(self):

        for order in self:
            moves_to_do = order.move_raw_ids.filtered(lambda x: x.state not in ('done', 'cancel'))

            print "Hello---------------------------------------",moves_to_do
            rawmaterial_moveids = []
            total_rawmaterial_cost = 0
            for rawmaterial in order.move_raw_ids:
                print "vvvvvvvvvvvvveeeeeee",rawmaterial
                rawmaterial_moveids.append(rawmaterial.id)

            print "ffffffffffffffffffffffffffffffffff",rawmaterial_moveids
            rawmaterials = self.env['stock.move'].browse(rawmaterial_moveids)
            print "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee",rawmaterials
#            print "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee",hai
            moves_to_do.action_done()


            print "how u ---------------------------------------",rawmaterials
            for line in rawmaterials:
                default_uom = line.product_id and line.product_id.uom_id
                ###since rawmaterial price unit/cost in move line is based on the default uom mentioned in the product master
                ####in odoo if you purchased in any unit the final cost of each single piece will be based on product master uom

                #####if you directly multiply with qty_done it will be wrong
                ####so converted in to default uom qty
                actual_qty_done_default_uom = line.product_uom._compute_quantity(line.quantity_done,default_uom)

                ##############i got unit price in default uom(by default u will get) based and qunatity also I converted to default uom
                ####now cost will be fine
                line_cost = line.price_unit * actual_qty_done_default_uom
                total_rawmaterial_cost = total_rawmaterial_cost + (line_cost)

            order._cal_price(moves_to_do)
            moves_to_finish = order.move_finished_ids.filtered(lambda x: x.state not in ('done','cancel'))
            qty_done = moves_to_finish.quantity_done
#            uom_rate = moves_to_finish.product_uom.factor_inv
#            if not uom_rate:
#                raise UserError(_('uom ratio not defined') % moves_to_finish.product_uom.name)
            uom_type = moves_to_finish.product_uom.uom_type

            default_uom = moves_to_finish.product_id and moves_to_finish.product_id.uom_id

            ### to restrict final product to be pro
            if default_uom.id != moves_to_finish.product_uom.id:
                raise UserError(_('the finshed product UOM should be same as its default uom mentioned i n product master') %moves_to_finish.product_uom.name)

            total_overhead_cost = self.kg_overhead_total or 0
            if (not total_overhead_cost) and (not self.kg_allow_zer_overheadcost):
                raise UserError(_('you didnt define any overhead for this mrp,if you want to put oberhead go to Overhead tab and define overhead value or if you want to continue with zero overhead go to Miscellaneous Tab and tick Allow Zero Cost Overhead'))

            print "vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvtotal_overhead_cost",total_overhead_cost
            print "vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvtotal_rawmaterial_cost",total_rawmaterial_cost

            total_cost = total_rawmaterial_cost + total_overhead_cost

            if qty_done:
                unit_cost = float(total_cost) / float(qty_done)



                moves_to_finish.write({'price_unit':unit_cost})


            moves_to_finish.action_done()
            for move in moves_to_finish:
                #Group quants by lots
                lot_quants = {}
                raw_lot_quants = {}
                quants = self.env['stock.quant']
                if move.has_tracking != 'none':
                    for quant in move.quant_ids:
                        lot_quants.setdefault(quant.lot_id.id, self.env['stock.quant'])
                        raw_lot_quants.setdefault(quant.lot_id.id, self.env['stock.quant'])
                        lot_quants[quant.lot_id.id] |= quant
                for move_raw in moves_to_do:
                    if (move.has_tracking != 'none') and (move_raw.has_tracking != 'none'):
                        for lot in lot_quants:
                            lots = move_raw.move_lot_ids.filtered(lambda x: x.lot_produced_id.id == lot).mapped('lot_id')
                            raw_lot_quants[lot] |= move_raw.quant_ids.filtered(lambda x: (x.lot_id in lots) and (x.qty > 0.0))
                    else:
                        quants |= move_raw.quant_ids.filtered(lambda x: x.qty > 0.0)
                if move.has_tracking != 'none':
                    for lot in lot_quants:
                        lot_quants[lot].sudo().write({'consumed_quant_ids': [(6, 0, [x.id for x in raw_lot_quants[lot] | quants])]})
                else:
                    move.quant_ids.sudo().write({'consumed_quant_ids': [(6, 0, [x.id for x in quants])]})
            order.action_assign()
            if not order.kg_allow_zer_overheadcost:
                order.kg_create_move()
            order.button_mark_done()

        return True

#####overriding this function because this function again calling post_inventory
##in our case there is no second production
###at a streach complete or make another mrp for remaining
###so am commanding self.post_inventory()
######button make as done hided,no purpose user need to click that so am calling this function from post_inventory
###because this mark as done doing other functions also to unreserve unnecessary qty from move line
    @api.multi
    def button_mark_done(self):
        self.ensure_one()
        for wo in self.workorder_ids:
            if wo.time_ids.filtered(lambda x: (not x.date_end) and (x.loss_type in ('productive', 'performance'))):
                raise UserError(_('Work order %s is still running') % wo.name)
#        self.post_inventory()
        moves_to_cancel = (self.move_raw_ids | self.move_finished_ids).filtered(lambda x: x.state not in ('done', 'cancel'))
        moves_to_cancel.action_cancel()
        self.write({'state': 'done', 'date_finished': fields.Datetime.now()})
        self.env["procurement.order"].search([('production_id', 'in', self.ids)]).check()
        self.write({'state': 'done'})



    @api.multi
    def kg_transfer_produced_goods(self):
        picking_obj = self.env['stock.picking']
        for record in self:
            smove_lines = []
            kg_client = record.kg_client and record.kg_client.id or False
            for line in record.move_finished_ids:
                product_id = line.product_id and line.product_id.id
                name = line.product_id and line.product_id.name
                qty = line.product_uom_qty
                umo_id = line.product_uom and line.product_uom.id
                smove = {'name': name, 'product_id': product_id, 'product_uom_qty': qty, 'product_uom': umo_id}
                smove_lines.append((0, 0, smove))


            location_id = self.kg_mr_picking_type_id and self.kg_mr_picking_type_id.default_location_src_id and self.kg_mr_picking_type_id.default_location_src_id.id or False
            location_dest_id = self.kg_mr_picking_type_id and self.kg_mr_picking_type_id.default_location_dest_id and self.kg_mr_picking_type_id.default_location_dest_id.id or False
            kg_project_id = self.kg_project_id and self.kg_project_id.id or False
            picking = picking_obj.create({
                'kg_project': kg_project_id,
                'kg_job_no': self.id,
                'move_lines': smove_lines,
                'partner_id': kg_client,
                'location_id': location_dest_id,
                'location_dest_id': location_id,
                'picking_type_id':self.kg_mr_picking_type_id and self.kg_mr_picking_type_id.id,
            })










    kg_packing_info = fields.Char(string='Packing Info')
    kg_att_url1 = fields.Char('')

    kg_att_url2 = fields.Char('')


    kg_att_url3 = fields.Char('')


    kg_att_url4 = fields.Char('')

    kg_att_url5 = fields.Char('')


    kg_project_id = fields.Many2one('project.project','Project')

    kg_suborder = fields.Boolean('Sub Order')

    one_job = fields.Boolean('Make it One Job')

    kg_cat = fields.Many2one('kg.category','Category')


    kg_est_id = fields.Many2one('kg.estimation','Estimation')

    kg_tech_est_id = fields.Many2one('kg.estimation','Technical Estimation')

    kg_client = fields.Many2one('res.partner','Client')

    kg_prj_item_id = fields.Many2one('kg.project.items','Project Item ')

    kg_so_id = fields.Many2one('sale.order','Sale Order')

    kg_parent_id = fields.Many2one('mrp.production','Parent Id')

    kg_child_ids = fields.One2many('mrp.production','kg_parent_id','Sub Jobs')

    kg_rawmaterials = fields.One2many('kg.rawmaterial.lines','kg_mrp_id','Raw Materials')
    kg_acc_list = fields.One2many('kg.mrp.account.lines','kg_mrp_id')

    kg_allow_zer_overheadcost =fields.Boolean('Allow Zero Overhead', default=True)


    kg_overhead_line  = fields.One2many('kg.prod.cost','kg_mrp_id','Product Cost List')

    kg_material_req_line  = fields.One2many('kg.mat.req','kg_mrp_id','MR Lines')

#    kg_additional_line  = fields.One2many('kg.mat.req','kg_mrp_id','MR Lines')

    kg_overhead_total = fields.Float('Overhead Total',compute="_compute_overhead_total",store=True)
    kg_timesheet_line = fields.One2many('kg.timesheet','kg_job_id','Time Sheet')
    kg_mr_picking_type_id = fields.Many2one('stock.picking.type', 'MR Picking Type')



class kg_rawmaterial_lines(models.Model):

    _name='kg.rawmaterial.lines'
    name= fields.Char('Name')

    kg_product_id = fields.Many2one('product.product','Product')

    kg_uom = fields.Many2one('product.uom','UOM')

    kg_qty = fields.Float('Qty')

    kg_select= fields.Boolean('Select')

    kg_mrp_id = fields.Many2one('mrp.production','MRP')


    @api.multi
    def select(self):
        for obj in self:
            obj.kg_select = True


    @api.multi
    def unselect(self):
        for obj in self:
            obj.kg_select = False






class kg_mrp_account_lines(models.Model):
    _name = 'kg.mrp.account.lines'
    _description = 'kg.mrp.account.lines'
    kg_mrp_id = fields.Many2one('mrp.production','Production Order')
    product_id = fields.Many2one('product.product','Product')
    account_move_id = fields.Many2one('account.move',string="Number")
    internal_sequence_number = fields.Char(string="Internal Number")
    ref = fields.Char(string="Reference")
    journal_id = fields.Many2one('account.journal',string="Journal")
    amount = fields.Float(string='Amount')
    status = fields.Char(string='Status')


class kg_prod_cost(models.Model):

    _name="kg.prod.cost"


    @api.one
    @api.depends('kg_qty_hour','kg_cost')
    def _compute_amount(self):
        self.amount = self.kg_qty_hour * self.kg_cost
    @api.onchange('kg_product')
    def onchange_product_id(self):

        self.kg_qty_hour = self.kg_product.lst_price


    kg_product= fields.Many2one('product.product','Product')

    kg_qty_hour =fields.Float('Qty/Hour')

    kg_auto = fields.Float('Auto')

    kg_cost = fields.Float('Cost')

    amount = fields.Float(string="Amount",compute="_compute_amount")

    kg_mrp_id = fields.Many2one('mrp.production','Production Order')









class mrp_bom(models.Model):

    _inherit="mrp.bom"

    kg_att_url1 = fields.Char('')

    kg_att_url2 = fields.Char('')


    kg_att_url3 = fields.Char('')


    kg_att_url4 = fields.Char('')

    kg_att_url5 = fields.Char('')
class kg_project_feedback(models.Model):

    _name='kg.project.feedback'


    name = fields.Char('Remarks')

    kg_date = fields.Date('Date')

    kg_department  = fields.Many2one('hr.department','Department')

    kg_project_id = fields.Many2one('project.project','Project')


class kg_project_snag(models.Model):

    _name='kg.project.snag'


    name = fields.Char('Remarks')

    kg_amount = fields.Float('Amount')

    kg_product  = fields.Many2one('product.product','Product')

    kg_project_id = fields.Many2one('project.project','Project')


class project_project(models.Model):

    _inherit="project.project"

    kg_feedback_lines = fields.One2many('kg.project.feedback','kg_project_id','FeedBack')

    kg_snag_lines = fields.One2many('kg.project.snag','kg_project_id','Snag')

    kg_att_url1 = fields.Char('')

    kg_att_url2 = fields.Char('')


    kg_att_url3 = fields.Char('')


    kg_att_url4 = fields.Char('')

    kg_att_url5 = fields.Char('')



    kg_class = fields.Selection([('design','Design'),('production','Production'),('fitout','Fitout'),('others','Others')],'Classification')


    kg_categ = fields.Selection([('maint','Maintenance'),('storage','Storage')],'Sub-Category')

    kg_items = fields.One2many('kg.project.items','kg_project_id','Items',default=lambda self: self._default_items())


    kg_timesheet_line = fields.One2many('kg.timesheet','kg_project_id','Timesheet')

    kg_exist_proj  = fields.Many2one('project.project','Existing Project')

    kg_start_date = fields.Date('Start Date')

    kg_end_date = fields.Date('End Date')

    kg_sq_mtr = fields.Float('Square Metre')

    kg_rate_per_mtr = fields.Float('Rate Per Square Mtr')

    kg_internal_location = fields.Many2one('stock.location','Internal Location')

    kg_desc = fields.Text('Description')

    kg_spe_inst = fields.Text('Special Instructions')

    kg_pack_list_count = fields.Integer('Packing List Count',compute='compute_pack_list_count')

    kg_log_req_count = fields.Integer('Logistics Requistion Count',compute='compute_log_req_count')

    kg_deilvery_request_count = fields.Integer('Delivery Request Count',compute='compute_del_req_count')

    kg_invoice_request_count = fields.Integer('invoce Request Count',compute='compute_inv_req_count')

    kg_pack_req_count = fields.Integer('Packing Req Count',compute='comp_pack_req_form')


    kg_est_id = fields.Many2one('kg.estimation','Estimation')

    kg_tech_est_id = fields.Many2one('kg.estimation','Technical Estimation')

    kg_sale_order_id = fields.Many2one('sale.order','Sale Order')

    kg_install_count  = fields.Integer('Installations',compute='compute_install_count')

    kg_progress = fields.Float('Progress',compute='compute_progress')


    kg_mr_picking_type_id = fields.Many2one('stock.picking.type','MR Picking Type')

    kg_picking_type_id = fields.Many2one('stock.picking.type','Project Picking Type')

    kg_project_location_src_id = fields.Many2one('stock.location','Project Source Location')




    @api.depends('kg_items')
    def compute_progress(self):
        for record in self:
            prg=0.0
            for line in record.kg_items:
                prg+=line.kg_progress

            record.kg_progress = prg


    @api.model
    def _default_items(self):
        order_id = self.env.context.get('default_kg_sale_order_id', False)
        b=[]
        if order_id is not False:
            order_lines = self.env['sale.order.line'].search([('order_id','=',order_id)])
            b=[]
            for line in order_lines:
                a={}
                a['kg_product_id']=line.product_id.id
                a['kg_uom']=line.product_uom.id
                a['kg_qty']=line.product_uom_qty
                c=(0,0,a)
                b.append(c)
        return b









    @api.multi
    def comp_pack_req_form(self):
        for record in self:
            est_obj = self.env['kg.packing.requisition']

            record.kg_pack_req_count = est_obj.search_count([('kg_project_id', '=',record.id)])


    @api.multi
    def compute_inv_req_count(self):
        for record in self:
            est_obj = self.env['kg.invoice.request']

            record.kg_invoice_request_count = est_obj.search_count([('kg_project_id', '=',record.id)])



    @api.multi
    def compute_del_req_count(self):
        for record in self:
            est_obj = self.env['kg.delivery.request']

            record.kg_deilvery_request_count = est_obj.search_count([('kg_project_id', '=',record.id)])


    @api.multi
    def compute_log_req_count(self):
        for record in self:
            est_obj = self.env['kg.log.req']

            record.kg_log_req_count = est_obj.search_count([('kg_project_id', '=',record.id)])


    @api.multi
    def compute_pack_list_count(self):
        for record in self:
            est_obj = self.env['kg.pack.list']

            record.kg_pack_list_count = est_obj.search_count([('kg_project_id', '=',record.id)])

    @api.multi
    def compute_install_count(self):
        for record in self:
            inst_obj = self.env['kg.install.req']

            record.kg_install_count = inst_obj.search_count([('kg_project_id', '=',record.id)])



class kg_project_items(models.Model):
    _name="kg.project.items"

    name = fields.Char('Description')

    kg_product_id = fields.Many2one('product.product','Product')

    kg_uom = fields.Many2one('product.uom','UOM')

    kg_qty = fields.Float('Qty')

    kg_mrp_id = fields.Many2one('mrp.production','Production')

    kg_progress = fields.Float('Progress')

    kg_project_id  = fields.Many2one('project.project','Project')


    @api.multi
    def create_mrp(self):
        for record in self:
            check_bom = False
            active_product_id = record.kg_product_id and record.kg_product_id.id or False
            already_created_mrp =self.env['mrp.production'].search([('kg_prj_item_id','=',record.id),('state','!=','cancel')])
            if len(already_created_mrp) >0:
                raise UserError(_('Already job order created'))

            if not check_bom:
                vals_check={}
                vals_check['product_tmpl_id']=record.kg_product_id.product_tmpl_id.id
                vals_check['product_uom_id']=record.kg_uom.id
                check_bom=self.env['mrp.bom'].create(vals_check)
            if record.kg_project_id and record.kg_project_id.kg_sale_order_id and record.kg_project_id.kg_tech_est_id and record.kg_project_id.kg_est_id:
                vals={}
                vals['product_id']=record.kg_product_id.id
                vals['kg_qty']=record.kg_qty
                vals['product_uom_id']=record.kg_uom.id
                vals['bom_id']=check_bom.id
                vals['kg_client']=record.kg_project_id.kg_tech_est_id.kg_client and  record.kg_project_id.kg_tech_est_id.kg_client.id
                vals['kg_project_id']=record.kg_project_id.id
                vals['kg_est_id']=record.kg_project_id.kg_est_id.id
                vals['kg_tech_est_id']=record.kg_project_id.kg_tech_est_id.id
                vals['kg_so_id']=record.kg_project_id.kg_sale_order_id.id
                vals['kg_prj_item_id']=record.id
                vals['picking_type_id']=record.kg_project_id.kg_picking_type_id and record.kg_project_id.kg_picking_type_id.id
                vals['location_src_id']=record.kg_project_id.kg_project_location_src_id and record.kg_project_id.kg_project_location_src_id.id
                vals['location_dest_id']=record.kg_project_id.kg_project_location_src_id and record.kg_project_id.kg_project_location_src_id.id
                vals['kg_mr_picking_type_id']=record.kg_project_id.kg_mr_picking_type_id and record.kg_project_id.kg_mr_picking_type_id.id

                mrp_id = self.env['mrp.production'].create(vals)

                unit_lines =self.env['kg.unit.lines'].search([('kg_unit_id.kg_est_id','=',record.kg_project_id.kg_tech_est_id.id),('kg_cat','not in',[14,15])])
                for line in unit_lines:
                    unit_product = line.kg_unit_id.kg_unit_id and line.kg_unit_id.kg_unit_id.id
                    if unit_product == active_product_id:
                        vals_line={}
                        vals_line['kg_product_id']=line.kg_mat.id
                        vals_line['kg_uom']=line.kg_mat.uom_id.id
                        vals_line['kg_qty']=line.kg_qty
                        vals_line['kg_mrp_id']=mrp_id.id
                        self.env['kg.rawmaterial.lines'].create(vals_line)

                record.write({'kg_mrp_id':mrp_id.id})






class project_task(models.Model):

    _inherit="project.task"

    kg_att_url1 = fields.Char('')

    kg_att_url2 = fields.Char('')


    kg_att_url3 = fields.Char('')


    kg_att_url4 = fields.Char('')

    kg_att_url5 = fields.Char('')


class project_task(models.Model):

    _inherit="project.task"

    kg_att_url1 = fields.Char('')

    kg_att_url2 = fields.Char('')


    kg_att_url3 = fields.Char('')


    kg_att_url4 = fields.Char('')

    kg_att_url5 = fields.Char('')



class fleet_vehicle(models.Model):

    _inherit="fleet.vehicle"

    kg_att_url1 = fields.Char('')

    kg_att_url2 = fields.Char('')


    kg_att_url3 = fields.Char('')


    kg_att_url4 = fields.Char('')

    kg_att_url5 = fields.Char('')









