# -*- coding: utf-8 -*-
from odoo import fields,models,api,_
from odoo import tools
from datetime import date
from dateutil.relativedelta import relativedelta

from odoo.exceptions import UserError, AccessError,ValidationError
from odoo import api, fields, models, SUPERUSER_ID, _

class SaleOrder(models.Model):
    
    _inherit="sale.order"
    
    
    kg_sample_req = fields.Boolean('Sample Required')
    
    kg_is_des_req  = fields.Boolean('Is Design Required')
    
    
    kg_est_id = fields.Many2one('kg.estimation','Estimation')
    
    kg_opp_id =fields.Many2one('crm.lead','Opportunity')
    
    kg_att_url1 = fields.Char('')
    
    kg_att_url2 = fields.Char('')
    
    
    kg_att_url3 = fields.Char('')
    
    
    kg_att_url4 = fields.Char('')
    
    kg_att_url5 = fields.Char('')
    
    kg_sample_count = fields.Integer('Sample Count',compute='compute_sample_count')
    
    kg_est_proc = fields.Boolean('Skip Estimation Procedures')
    
    kg_tech_est = fields.Many2one('kg.estimation','Technical Estimation')
    
    kg_hide_lines  =fields.Boolean('Hide and Add New Lines')
    
    kg_dummy_lines = fields.One2many('dummy.so.lines','order_id','Lines')
    
    kg_project_id = fields.Many2one('project.project','Project')

    kg_mr_picking_type_id = fields.Many2one('stock.picking.type','MR Picking Type')

    kg_picking_type_id = fields.Many2one('stock.picking.type','Project Picking Type')

    kg_project_location_src_id = fields.Many2one('stock.location','Project Source Location')

    @api.constrains('kg_dummy_lines')
    def _check_sum(self):
        for record in self:
            subtotal=0
            for line in record.kg_dummy_lines:
                subtotal+=line.kg_subtotal
            if subtotal != record.amount_untaxed:
                
                raise ValidationError("Line Total Does not Match Estimation Value")
    
    state = fields.Selection([
        ('draft', 'Quotation'),
        ('sent', 'Quotation Sent'),
        ('sale', 'Sales Order'),
        ('done', 'Locked'),
        ('cancel', 'Cancelled'),
         ('revision', 'Revision'),
        ], string='Status', readonly=True, copy=False, index=True, track_visibility='onchange', default='draft')
    
    
    @api.multi
    def compute_sample_count(self):
        for record in self:
            sample_obj = self.env['kg.sample.material']
            
            record.kg_sample_count = sample_obj.search_count([('kg_so_id', '=',record.id)]) 
    
    @api.multi
    def create_sample(self):
        for record in self:
            vals={}
            vals['kg_client']=record.partner_id.id
            vals['kg_so_id']=record.id
            self.env['kg.sample.material'].create(vals)
            
            
    @api.multi
    def action_confirm(self):
        super(SaleOrder, self).action_confirm()
        project_obj = self.env['project.project']
        loc_obj = self.env['stock.location']
        pick_type_obj = self.env['stock.picking.type']
        stock_loc = loc_obj.search([('name','=','Stock'),('usage','=','internal')])
        # Project Internal Transfer SEQ
        res_model, pickng_seq_id1 = self.env['ir.model.data'].get_object_reference('kg_abra', 'seq_picking_project_internal_transfer')
        # Project Material Requistion SEQ
        res_model, pickng_seq_id2 = self.env['ir.model.data'].get_object_reference('kg_abra', 'seq_picking_project_manufacture')
        if not stock_loc:
            raise UserError("Stock Location not found")
        for record in self:
            name = record.name

            # Project internal location
            kg_project_location_src_id = loc_obj.create({
                'name': 'Production - ' + str(name),
                'usage':'internal',
            })

            #Create picking type(MR and Internal) for each project
            kg_mr_picking_type_id = pick_type_obj.create({
                'name': 'MR - ' + str(name),
                'code': 'internal',
                'default_location_src_id':stock_loc[0].id,
                'default_location_dest_id':kg_project_location_src_id.id,
                'sequence_id':pickng_seq_id2,
            })
            kg_picking_type_id = pick_type_obj.create({
                'name': 'Project - ' + str(name),
                'code': 'mrp_operation',
                'default_location_src_id': kg_project_location_src_id.id,
                'default_location_dest_id': kg_project_location_src_id.id,
                'sequence_id':pickng_seq_id1,
            })
            record.kg_project_location_src_id = kg_project_location_src_id.id
            record.kg_mr_picking_type_id = kg_mr_picking_type_id.id
            record.kg_picking_type_id = kg_picking_type_id.id
            if not record.kg_est_proc:
                b = []
                for line in record.order_line:
                    a = {}
                    a['kg_product_id'] = line.product_id.id
                    a['kg_uom'] = line.product_uom.id
                    a['kg_qty'] = line.product_uom_qty
                    c = (0, 0, a)
                    b.append(c)
                project = project_obj.create({
                    'name': name,
                    'kg_est_id': record.kg_est_id and record.kg_est_id.id,
                    'kg_tech_est_id': record.kg_tech_est and record.kg_tech_est.id,
                    'kg_sale_order_id': record.id,
                    'kg_mr_picking_type_id':kg_mr_picking_type_id.id,
                    'kg_picking_type_id':kg_picking_type_id.id,
                    'kg_project_location_src_id':kg_project_location_src_id.id,
                    'kg_items':b,
                })
                record.related_project_id = project and project.analytic_account_id
                record.kg_project_id = project and project.id
            if record.kg_est_proc != True:
                if not record.project_id:
                    raise UserError("Add Project in Sale Order")
                
        return True
                    
            
            
class dummy_so_lines(models.Model):
    
    _name = 'dummy.so.lines'
    
    
    
    name = fields.Char('Description',required=True)
    
    kg_subtotal = fields.Float('Subtotal',required=True)
    
    order_id = fields.Many2one('sale.order','Sale Order')
    
    
    
    