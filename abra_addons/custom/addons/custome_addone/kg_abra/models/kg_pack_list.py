# -*- coding: utf-8 -*-
from odoo import fields,models,api,_
from odoo import tools
from datetime import date
from dateutil.relativedelta import relativedelta

from odoo.exceptions import UserError, AccessError
from odoo import api, fields, models, SUPERUSER_ID, _


class kg_pack_list(models.Model):

    _name="kg.pack.list"

    name = fields.Char('PL#')

    kg_consign = fields.Many2one('res.partner','Consignee')

    kg_ship_addr = fields.Many2one('res.partner','Shipping Address')

    kg_date = fields.Date('Date')

    kg_boxes_list = fields.One2many('kg.pack.boxes','kg_pack_id','List of Boxes')
    
    
    kg_att_url1 = fields.Char('')
    
    kg_att_url2 = fields.Char('')
    
    
    kg_att_url3 = fields.Char('')
    
    
    kg_att_url4 = fields.Char('')
    
    kg_att_url5 = fields.Char('')
    
    kg_project_id = fields.Many2one('project.project','Project')

    @api.model
    def create(self, vals):
        sequence_code = 'pack.list'
        vals['name'] = self.env['ir.sequence'].next_by_code(sequence_code)
        return super(kg_pack_list, self).create(vals)


class kg_pack_boxes(models.Model):

    _name= "kg.pack.boxes"

    _description = "List of Boxes"

    name = fields.Char('Name')

    kg_no  = fields.Integer('No of Boxes')

    kg_len= fields.Float('Length',help="Length")

    kg_wid = fields.Float('Width',help="Width")

    kg_heig = fields.Float('Height',help="Height")

    kg_net_weigh = fields.Float('Net Weight Per Box')

    kg_gross_weigh = fields.Float('Gross Weight Per Box')

    kg_total_net_weigh = fields.Float('Total Net Weight',compute="compute_totalt")

    kg_total_gross_weigh = fields.Float('Total Gross Weight',compute="compute_total")

    kg_pack_id = fields.Many2one('kg.pack.list','Packing ID')

    kg_prod_list = fields.One2many('kg.prod.boxes','kg_box_id','Prod List')


    @api.depends('kg_no','kg_net_weigh','kg_gross_weigh')
    def compute_total(self):

        for record in self:
            record.kg_total_net_weigh=record.kg_no*record.kg_net_weigh
            record.kg_total_gross_weigh = record.kg_no * record.kg_gross_weigh



class kg_prod_boxes(models.Model):

    _name="kg.prod.boxes"

    name = fields.Char('Name')

    kg_product = fields.Many2one('product.product','Product')

    kg_hs_code = fields.Char('HS Code')

    kg_origin_country = fields.Many2one('res.country','Country of Origin')

    kg_tot_qty = fields.Float('Total Qty')

    kg_box_id = fields.Many2one('kg.pack.boxes','Box ID')










