# -*- coding: utf-8 -*-
from odoo import fields,models,api,_
from odoo import tools
from datetime import date
from dateutil.relativedelta import relativedelta

from odoo.exceptions import UserError, AccessError
from odoo import api, fields, models, SUPERUSER_ID, _

class kg_design_request(models.Model):
    
    _name="kg.design.request"
    
    name = fields.Char('Name')
    
    kg_project_type= fields.Selection([('perm','Permanent'),('act','Activations'),('event','Event / Exhibtion'),('merch','Merchandising'),('fnb','FNB / Retail')],'Project Type')
    
    kg_group = fields.Char('Group/Division/Brand')
    
    kg_date = fields.Date('Date')
    
    kg_location = fields.Char('Location')
    
    kg_project_ref = fields.Char('Project Ref')
    
    kg_project_id = fields.Many2one('project.project','Project')
    
    kg_prj_mgr = fields.Many2one('hr.employee','Project Manager')
    
    kg_client = fields.Many2one('res.partner','Client')
    
    
    kg_req_date = fields.Date('Required Date')
    
    
    kg_raised_by = fields.Many2one('res.users','Raised By',default=lambda self: self.env.user and self.env.user.id or False)
    
    kg_assign_to = fields.Many2one('res.users','Assigned To')
    
    
    kg_studio_lead = fields.Char('Studio Lead')
    
    kg_history = fields.Char('History')
    
    kg_market_rank = fields.Char('Market Rank / Share')
    
    kg_geo = fields.Char('Geography')
    
    kg_strat = fields.Char('Strategy')
    
    kg_challenge = fields.Char('Challenges/Issues')
    
    kg_rep = fields.Char('Reputation')
    
    kg_exist = fields.Char('Existing communication')
    
    kg_comp = fields.Char('Competitors')
    
    kg_category = fields.Many2one('product.category','Category')
    
    kg_product_range =fields.Char('Product Range')
    
    kg_price_point  = fields.Char('Price Point')
    
    kg_asso_serv = fields.Char('Associated Services')
    
    kg_usp =  fields.Char('Unique Selling Point')
    
    kg_rtb = fields.Char('Reasons to Believe')
    
    kg_pwhd = fields.Char('Product W * H * D')
    
    kg_pawhd = fields.Char('Pack W * H * D')
    
    kg_comp1 = fields.Char('Competitors')
    
    kg_categ_champ = fields.Char('Category Champion')
    
    kg_cross_sell = fields.Char('Cross Selling / Up selling')
    
    kg_target_market = fields.Char('Target market')
    
    kg_exist_consumer = fields.Char('Existing Consumers')
    
    kg_target_consumer = fields.Char('Target Consumer')
    
    kg_channel_market = fields.Char('Channel To Market')
    
    kg_purc_trig  = fields.Char('Purchase Triggers')
    
    kg_shopping_trial = fields.Char('Shopping Trial')
    
    kg_doc_req  = fields.Many2many('kg.doc.req')
    
    
    
    kg_reson_prj  = fields.Char('Reason For Project')
    
    kg_obj_bui = fields.Char('Object/Business Result')
    
    kg_iss_cha = fields.Char('Issues/Challenges/Barriers')
    
    kg_design_style = fields.Char('Design, Style, Ambiance')
    
    kg_material_col = fields.Char('Material / color / finishing')
    
    kg_must_have = fields.Char('Must Have/Must Avoid')
    
    kg_opp_id = fields.Many2one('crm.lead','Oppurtunity ')
    
    kg_boq_id = fields.Many2one('kg.boq.request','BOQ Request')
    
    kg_app_by = fields.Many2one('res.users','Approved By')
    
   # kg_state = fields.Selection([('draft','Draft'),('approved','Approved')],'State',default='draft')
    
    kg_est_count = fields.Integer('Estimation Count',compute='compute_est_count')
    
    
    kg_timesheet_lines = fields.One2many('kg.timesheet','kg_design_req_id','Timesheet')

    
    
#     @api.model
#     def create(self,vals):
#         sequence_code='kg.design.request'
#         vals['name']= self.env['ir.sequence'].next_by_code(sequence_code) 
#     
#         return super(kg_design_request, self).create(vals)
    
    @api.multi
    def compute_est_count(self):
        for record in self:
            est_obj = self.env['kg.estimation']
            
            record.kg_est_count = est_obj.search_count([('kg_des_id', '=',record.id)]) 
    
    
    @api.multi
    def kg_approve(self):
        
        for record in self:
            record.write({'kg_state':'assign','kg_app_by':self.env.user.id})
            
            
    @api.multi
    def create_est(self):
        for record in self:
            vals={}
            
            vals['kg_opny']=record.kg_opp_id.id or False
            vals['kg_boq_id']=record.kg_boq_id.id or False
            vals['kg_des_id']=record.id or False
            vals['kg_client']=record.kg_client.id or False
            vals['kg_project_id']=record.kg_project_id.id or False 
            self.env['kg.estimation'].create(vals)
            
            
            
    _inherit = ['mail.thread', 'ir.needaction_mixin']
    
    
    
    
    @api.multi
    def _track_subtype(self, init_values):
        self.ensure_one()
        if 'kg_state' in init_values and self.kg_state == 'In Progress':
            return 'kg_abra.kg_design_inprogress'
        
        if 'kg_state' in init_values and self.kg_state == 'new':
            return 'kg_abra.kg_design_new'
        
        if 'kg_state' in init_values and self.kg_state == 'assign':
            return 'kg_abra.kg_design_assign'
        
        if 'kg_state' in init_values and self.kg_state == 'Quote Created':
            return 'kg_abra.kg_design_quote_created'
        
        
        if 'kg_state' in init_values and self.kg_state == 'Completed':
            return 'kg_abra.kg_design_complete'
        return super(kg_design_request, self)._track_subtype(init_values)
    
    kg_state = fields.Selection([('new','New'),('assign','Assigned'),('In Progress','In Progress'),('Completed','Completed')],'State',track_visibility='onchange',default='new')

    kg_dep_id = fields.Many2one('hr.department','Assigned Department')
    @api.multi
    def write(self, vals):
        """Override default Odoo write function and extend."""
        # Do your custom logic here
        
        if vals.get('kg_state') and vals.get('kg_state') !='new' and vals.get('kg_state') !='assign':
            
                vals_mail={}
                message="<b>Dear %(hr_man)s,</b><br/><br/>Your  Design Request : %(hd_name)s  has been changed to %(state)s State" % {
                         'hr_man': self.kg_raised_by.partner_id.name,
                           'hd_name': self.name,
                           'state': vals.get('kg_state')
                           
                             
                    }
            
                vals_mail['message']=message
                vals_mail['partner']=self.kg_raised_by.partner_id.email
                self.send_mail(vals_mail)   
        print vals,'hshddhhhd'
        if vals.get('kg_assign_to'):
            vals['kg_state']='assign'
            user_obj=self.env['res.users'].browse([vals.get('kg_assign_to')])
            check=self.env['mail.followers'].search([('partner_id','=',user_obj.partner_id.id),('res_model','=','kg.design.request'),('res_id','=',self.id)])
            if not check:
                fol={}
                fol['res_model']='kg.design.request'
                fol['res_id']=self.id
                fol['partner_id']=user_obj.partner_id.id
                fol_id=self.env['mail.followers'].create(fol)
                subtypes= self.env['mail.message.subtype'].search([('res_model','=','kg.design.request')]).ids
                if subtypes:
                    for i in subtypes:
                        self._cr.execute('INSERT INTO mail_followers_mail_message_subtype_rel (mail_followers_id, mail_message_subtype_id) values (%s, %s)', (fol_id.id, i))
    
            
        
            
            
            vals_mail={}
            message="<b>Dear %(hr_man)s,</b><br/><br/> Design Request : %(hd_name)s  has been assigned to You" % {
                         'hr_man': user_obj.partner_id.name,
                           'hd_name': self.name,
                             
                    }
            
            vals_mail['message']=message
            vals_mail['partner']=user_obj.partner_id.email
            self.send_mail(vals_mail)  
            
            
            
        return super(kg_design_request, self).write(vals)
    
    
    @api.model
    def create(self,vals):
        
        sequence_code='kg.design.request'
        vals['name']= self.env['ir.sequence'].next_by_code(sequence_code) 
     
        
        dep_obj=self.env['hr.department'].browse([vals.get('kg_dep_id')])    
        partner_id=dep_obj.manager_id.user_id.partner_id
        
        

            
        
        res= super(kg_design_request, self).create(vals)
        
        dep_mgr_id =res.kg_dep_id.manager_id.user_id.partner_id.id
         
        fol={}
        fol['res_model']='kg.design.request'
        fol['res_id']=res.id
        fol['partner_id']=dep_mgr_id
        fol_id=self.env['mail.followers'].create(fol)
        subtypes= self.env['mail.message.subtype'].search([('res_model','=','kg.design.request')]).ids
        if subtypes:
            for i in subtypes:
                self._cr.execute('INSERT INTO mail_followers_mail_message_subtype_rel (mail_followers_id, mail_message_subtype_id) values (%s, %s)', (fol_id.id, i))

        
        
        vals_mail={}
        message="<b>Dear %(hr_man)s,</b><br/><br/> New Design Request : %(hd_name)s from  %(emp_name)s has been created" % {
                     'hr_man': partner_id.name,
                       'hd_name': res.name,
                         'emp_name': res.kg_raised_by.name,
                }
        
        vals_mail['message']=message
        vals_mail['partner']=partner_id.email
        res.send_mail(vals_mail)
        
        
        
        if vals.get('kg_assign_to'):
            
            user_obj=self.env['res.users'].browse([vals.get('kg_assign_to')])
            check=self.env['mail.followers'].search([('partner_id','=',user_obj.partner_id.id),('res_model','=','kg.design.request'),('res_id','=',res.id)])
            if not check:
                fol={}
                fol['res_model']='kg.design.request'
                fol['res_id']=res.id
                fol['partner_id']=user_obj.partner_id.id
                fol_id=self.env['mail.followers'].create(fol)
                subtypes= self.env['mail.message.subtype'].search([('res_model','=','kg.design.request')]).ids
                if subtypes:
                    for i in subtypes:
                        self._cr.execute('INSERT INTO mail_followers_mail_message_subtype_rel (mail_followers_id, mail_message_subtype_id) values (%s, %s)', (fol_id.id, i))
    
            
        
            
            
            vals_mail={}
            message="<b>Dear %(hr_man)s,</b><br/><br/> Design Request : %(hd_name)s  has been assigned to You" % {
                         'hr_man': user_obj.partner_id.name,
                           'hd_name': res.name,
                             
                    }
            
            vals_mail['message']=message
            vals_mail['partner']=user_obj.partner_id.email
            self.send_mail(vals_mail)
        res.write({'kg_state':''})
        res.write({'kg_state':'new'})
        return res
    
    
    
    def send_mail(self,vals):
        main_content = {
                'subject': "Design Request!",
              
                'body_html': '<pre>%s</pre>' % vals['message'],
                 'email_to': vals['partner'],
            }
        return self.env['mail.mail'].create(main_content).send()

    
    
    
    
    
class kg_doc_req(models.Model):
    
    _name="kg.doc.req"
    
    name = fields.Char('Name')
    
    
    
    