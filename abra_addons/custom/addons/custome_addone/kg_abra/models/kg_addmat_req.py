# -*- coding: utf-8 -*-
from odoo import fields,models,api,_
from odoo import tools
from datetime import date
from dateutil.relativedelta import relativedelta

from odoo.exceptions import UserError, AccessError
from odoo import api, fields, models, SUPERUSER_ID, _

class kg_addmat_req(models.Model):
    
    _name='kg.addmat.req'
    
    name = fields.Char('Name')
    
    kg_client = fields.Many2one('res.partner','Client')
    
    kg_project = fields.Many2one('project.project','Project')
    
    #kg_date = fields.Date('Date')
    
    
    
    
    kg_raised_by = fields.Many2one('res.users','Raised By',default=lambda self: self.env.user and self.env.user.id or False)
    
    kg_assign_to = fields.Many2one('res.users','Assigned To')
    
    
    
    kg_desc = fields.Text('Description')
    
    kg_addmat_req_lines = fields.One2many('kg.addmat.req.lines','kg_addmat_req_id','Lines')
    
    _inherit = ['mail.thread', 'ir.needaction_mixin']
    
    
    
    
    @api.multi
    def _track_subtype(self, init_values):
        self.ensure_one()
        if 'kg_state' in init_values and self.kg_state == 'In Progress':
#             records = self._get_followers(cr, uid, ids, None, None, context=context)
# followers = records[ids[0]]['message_follower_ids']


            
            return 'kg_abra.kg_addmat_inprogress'
        
        if 'kg_state' in init_values and self.kg_state == 'new':
            return 'kg_abra.kg_addmat_new'
        
        if 'kg_state' in init_values and self.kg_state == 'assign':
            return 'kg_abra.kg_addmat_assign'
        
        if 'kg_state' in init_values and self.kg_state == 'Quote Created':
            return 'kg_abra.kg_addmat_quote_created'
        
        
        if 'kg_state' in init_values and self.kg_state == 'Completed':
            return 'kg_abra.kg_addmat_complete'
        return super(kg_addmat_req, self)._track_subtype(init_values)
    
    kg_state = fields.Selection([('new','New'),('assign','Assigned'),('In Progress','In Progress'),('Completed','Completed')],'State',track_visibility='onchange',default='new')

    kg_dep_id = fields.Many2one('hr.department','Assigned Department')
    @api.multi
    def write(self, vals):
        """Override default Odoo write function and extend."""
        # Do your custom logic here
        
        if vals.get('kg_state') and vals.get('kg_state') !='new' and vals.get('kg_state') !='assign' :
            
                vals_mail={}
                message="<b>Dear %(hr_man)s,</b><br/><br/>Your  Additional Material Request : %(hd_name)s  has been changed to %(state)s State" % {
                         'hr_man': self.kg_raised_by.partner_id.name,
                           'hd_name': self.name,
                           'state': vals.get('kg_state')
                           
                             
                    }
            
                vals_mail['message']=message
                vals_mail['partner']=self.kg_raised_by.partner_id.email
                self.send_mail(vals_mail)   
        print vals,'hshddhhhd'
        if vals.get('kg_assign_to'):
            vals['kg_state']='assign'
            user_obj=self.env['res.users'].browse([vals.get('kg_assign_to')])
            check= self.env['mail.followers'].search([('res_model','=','kg.addmat.req'),('partner_id','=',user_obj.partner_id.id),('res_id','=',self.id)])
            print check,'1111111111111111'
          
            if not check:
                fol={}
                fol['res_model']='kg.addmat.req'
                fol['res_id']=self.id
                fol['partner_id']=user_obj.partner_id.id
                fol_id=self.env['mail.followers'].create(fol)
                subtypes= self.env['mail.message.subtype'].search([('res_model','=','kg.addmat.req')]).ids
                if subtypes:
                    for i in subtypes:
                        self._cr.execute('INSERT INTO mail_followers_mail_message_subtype_rel (mail_followers_id, mail_message_subtype_id) values (%s, %s)', (fol_id.id, i))

                    
            
            vals_mail={}
            message="<b>Dear %(hr_man)s,</b><br/><br/> Additional Material Request : %(hd_name)s  has been assigned to You" % {
                         'hr_man': user_obj.partner_id.name,
                           'hd_name': self.name,
                             
                    }
            
            vals_mail['message']=message
            vals_mail['partner']=user_obj.partner_id.email
            self.send_mail(vals_mail)  
            
            
        return super(kg_addmat_req, self).write(vals)
    
    
    @api.model
    def create(self,vals):
     
        
        dep_obj=self.env['hr.department'].browse([vals.get('kg_dep_id')])    
        partner_id=dep_obj.manager_id.user_id.partner_id
        
        

            
        
        res= super(kg_addmat_req, self).create(vals)
        
        dep_mgr_id =res.kg_dep_id.manager_id.user_id.partner_id.id
        check= self.env['mail.followers'].search([('res_model','=','kg.addmat.req'),('partner_id','=',dep_mgr_id),('res_id','=',res.id)])
        print check,'1111111111111111'
          
        if not check: 
            fol={}
            fol['res_model']='kg.addmat.req'
            fol['res_id']=res.id
            fol['partner_id']=dep_mgr_id
            fol_id=self.env['mail.followers'].create(fol)
            subtypes= self.env['mail.message.subtype'].search([('res_model','=','kg.addmat.req')]).ids
            if subtypes:
                for i in subtypes:
                    self._cr.execute('INSERT INTO mail_followers_mail_message_subtype_rel (mail_followers_id, mail_message_subtype_id) values (%s, %s)', (fol_id.id, i))

        
        
        vals_mail={}
        message="<b>Dear %(hr_man)s,</b><br/><br/> New Additional Material Requestion : %(hd_name)s from  %(emp_name)s has been created" % {
                     'hr_man': partner_id.name,
                       'hd_name': res.name,
                         'emp_name': res.kg_raised_by.name,
                }
        
        vals_mail['message']=message
        vals_mail['partner']=partner_id.email
        res.send_mail(vals_mail)
        res.write({'kg_state':''})
        res.write({'kg_state':'new'})
        return res
    
    
    
    def send_mail(self,vals):
        main_content = {
                'subject': "Additional Material Requestion!",
              
                'body_html': '<pre>%s</pre>' % vals['message'],
                 'email_to': vals['partner'],
            }
        return self.env['mail.mail'].create(main_content).send()


    
    
class kg_addmat_req_lines(models.Model):
    _name="kg.addmat.req.lines"
    
    name = fields.Char('Name')
    
    kg_product = fields.Many2one('product.product','Item')
    
    kg_req_date = fields.Date('Required Date')
    
    kg_uom = fields.Many2one('product.uom','UOM')
    
    kg_qty = fields.Float('Product Qty')
    
#     kg_unit = fields.Float('Unit Rate')
#     
#     
#     kg_total = fields.Float('Total')
    
    kg_addmat_req_id = fields.Many2one('kg.addmat.req','Mat Req id')
    
#     @api.onchange('kg_qty','kg_unit')
#     def calc_total(self):
#         for record in self:
#             record.kg_total =record.kg_qty*record.kg_unit
#     
    @api.onchange('kg_product')
    def onchange_product(self):
        for record in self:
            record.kg_uom  = record.kg_product.uom_id.id or False