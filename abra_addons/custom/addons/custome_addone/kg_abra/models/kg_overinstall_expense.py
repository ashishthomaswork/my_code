# -*- coding: utf-8 -*-
from odoo import fields,models,api,_
from odoo import tools
from datetime import date
from dateutil.relativedelta import relativedelta

from odoo.exceptions import UserError, AccessError
from odoo import api, fields, models, SUPERUSER_ID, _

class kg_overinstall_expense(models.Model):
    
    _name = 'kg.overinstall.expense'
    
    name  = fields.Char('Name')
    
    kg_job_no = fields.Many2one('mrp.production','Job No')
    
    kg_project = fields.Many2one('project.project','Project')
    
    kg_location = fields.Char('Location')
    
    kg_country = fields.Many2one('res.country','Country')
    
    kg_advance = fields.Float('Advance')
    
    kg_sec_dep = fields.Float('Security Deposit')
    
    kg_install_date = fields.Date('Installation Start Date')
    
    kg_comp_date = fields.Date('Installation Completion Date')
    
    kg_expense_lines = fields.One2many('kg.overinstall.exp.lines','kg_parent_id')
    
    kg_install_req_id = fields.Many2one('kg.install.req','Install Request')
    
    
    
class kg_overinstall_exp_lines(models.Model):
    
    _name= 'kg.overinstall.exp.lines'
    
    
    name = fields.Char('Particulars')
    
    kg_date =fields.Date('Date')
    
    kg_ovt_alw = fields.Float('Overtime Allowances')
    
    kg_tel_exp = fields.Float('Telephone Expenses')
    
    kg_food_exp = fields.Float('Food Expenses')
    
    kg_taxi_hire = fields.Float('Taxi Hire Charges')
    
    kg_hotel_acc= fields.Float('Hotel Accomadation')
    
    kg_out_lab = fields.Float('Outsourced Labour Charges')
    
    kg_transport = fields.Float('Transport Charges')
    
    kg_misc = fields.Float('Misc')
    
    kg_card_airfare = fields.Float('Card Payment  / Air Fare')   
    
    kg_card_hotel_acc = fields.Float('Card Payment   /  Hotel Accom ')
    
    
    kg_parent_id = fields.Many2one('kg.overinstall.expense','Parent')
    