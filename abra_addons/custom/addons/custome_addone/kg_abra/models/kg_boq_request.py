# -*- coding: utf-8 -*-
from odoo import fields,models,api,_
from odoo import tools
from datetime import date
from dateutil.relativedelta import relativedelta

from odoo.exceptions import UserError, AccessError
from odoo import api, fields, models, SUPERUSER_ID, _

class kg_boq_request(models.Model):
    
    _name = "kg.boq.request"
    _inherit = ['mail.thread', 'ir.needaction_mixin']
    
    
    
    
    @api.multi
    def _track_subtype(self, init_values):
        self.ensure_one()
        if 'kg_state' in init_values and self.kg_state == 'In Progress':
            return 'kg_abra.kg_boq_inprogress'
        
        if 'kg_state' in init_values and self.kg_state == 'new':
            return 'kg_abra.kg_boq_new'
        
        if 'kg_state' in init_values and self.kg_state == 'assign':
            return 'kg_abra.kg_boq_assign'
        
        if 'kg_state' in init_values and self.kg_state == 'Quote Created':
            return 'kg_abra.kg_boq_quote_created'
        
        
        if 'kg_state' in init_values and self.kg_state == 'Completed':
            return 'kg_abra.kg_boq_complete'
        return super(kg_boq_request, self)._track_subtype(init_values)
    
    name=  fields.Char('BRF/ BOQ/ Quote #')
    
    kg_category = fields.Selection([('Production','Production'),('Design','Design'),('Fitout','Fitout'),('Others','Others')],'Category')

    kg_dimension = fields.Char('Dimension / Over All Size')

    kg_no = fields.Integer('BRF/ BOQ/ Quote #')
    
    kg_dep_id = fields.Many2one('hr.department','Assigned Department')
    
    kg_data_request = fields.Datetime('Date Requested')
    
    kg_date_require = fields.Datetime('Date Required')
    
    kg_client = fields.Many2one('res.partner','Client')
    
    kg_project = fields.Many2one('project.project','Project')
    
    kg_project_type = fields.Selection([('fitout','Fitout'),('production','Production')],'Project Type')
    
<<<<<<< HEAD
    kg_add_proj_type = fields.Selection([('anim','Animation'),('permanent','Permanent')],'Project Type')
    
=======
>>>>>>> 53dbf3ae642dcf11b75f3101395219e62b22b5a7
    kg_to_be_confirm = fields.Boolean('TBC')

    kg_project_del_date = fields.Date('Project Delivery Date')
    
    kg_life_span  = fields.Selection([('Permanent','Permanent'),('Temporary','Temporary')],'Structure Life Span')
    
    kg_life_span_met =   fields.Selection([('Month','Month'),('Week','Week'),('Day','Day')])
    
    kg_life_span_val = fields.Integer()
    
    kg_product_quality =   fields.Selection([('high','High-end'),('med','Medium-end'),('low','Low-end')],'Product Quality')
    
    
    kg_ac_spec =   fields.Selection([('Plexi','Plexi'),('Ordinary','Ordinary')],'Acrylic Specifications')
    
    kg_ee_spec =   fields.Selection([('branded','Branded (Euro Specs)'),('Ordinary','Ordinary')],'Electrical Spec')

    kg_war =   fields.Selection([('one','1 year'),('two','2 Year'),('wo','W/O Warranty')],'Warranty Period')
    
    kg_glass_spec =   fields.Many2many('kg.glass.spec',string='Glass Specifications')

    kg_glass_type = fields.Char(string='Glass Type')
    
    kg_mdf = fields.Many2many('kg.mdf',string='MDF')

    kg_mdf_brand = fields.Char('Brand')
    
    kg_hardware = fields.Selection([('Branded','Branded'),('Regular','Regular')],'Hardware')
    
    kg_spe_mats = fields.Many2many('kg.spe.mat',string='Special Material')
    
    kg_branding = fields.Many2many('kg.branding',string='Branding')
    
    kg_type = fields.Many2many('kg.type',string='Type')
    
    kg_painting = fields.Many2many('kg.painting')
    
    kg_packaging = fields.Selection([('Export','Export'),('Local','Local'),('Both','Both')],'Packaging')
    
    kg_packing_spec = fields.Many2many('kg.packing.spec')
    
    kg_delivery_location =fields.Many2one('kg.delivery.location','Delivery Location')
    
    kg_delivery_mode = fields.Many2one('kg.delivery.mode','Delivery Mode')
    
    kg_delivery_terms = fields.Many2one('kg.delivery.terms','Delivery Terms')

    
    kg_requires_site_ip = fields.Boolean('Require Site Inspection')
    
    kg_requires_site_install = fields.Boolean('Require Site Installation')

    kg_no_days_onsite = fields.Integer(' ')

    kg_no_project_manager = fields.Integer(' ')

    kg_no_technicians = fields.Integer(' ')

    kg_no_persons_onsite = fields.Integer(' ', compute='_compute_no_persons')


    kg_att_incl = fields.Many2many('kg.att.incl')

    kg_req_item_break = fields.Boolean('Requires Itemized Breakdown')

    kg_pls_req_items = fields.Text('Pls Required items')

    kg_other_cmnt = fields.Text('Other Comments')

    kg_sub_by = fields.Many2one('res.users','Submitted By',default=lambda self: self.env.user and self.env.user.id or False)

    kg_sub_date =fields.Date('Submitted Date')

    kg_rec_by = fields.Many2one('res.users','Received By')

    kg_rec_date = fields.Date('Received Date')



    kg_compl_by  = fields.Many2one('res.users',"Completed By")

    kg_compl_date = fields.Date('Completed Date')
    
    kg_opp_id = fields.Many2one('crm.lead','Oppurtunity ')
    
    kg_design_req = fields.Boolean('Design Required')
    
    
    
    kg_att_url1 = fields.Char('')
    
    kg_att_url2 = fields.Char('')
    
    
    kg_att_url3 = fields.Char('')
    
    
    kg_att_url4 = fields.Char('')
    
    kg_att_url5 = fields.Char('')
    
    kg_design_count = fields.Integer('Design Request Count',compute='compute_design_req_count')
    
    kg_est_count = fields.Integer('Estimation Count',compute='compute_est_count')
    
    kg_fit_count = fields.Integer('Fitout Count',compute='compute_fit_count')
    
    
   
    
    kg_raised_by = fields.Many2one('res.users','Raised By')
    
    kg_assign_to = fields.Many2one('res.users','Assigned To',track_visibility='onchange')
    
    
    
    kg_state = fields.Selection([('new','New'),('assign','Assigned'),('In Progress','In Progress'),('Quote Created','Quote Created'),('Completed','Completed')],'State',track_visibility='onchange',default='new')

    kg_est_requested = fields.Boolean(string='Estimation Requested', default=False)

    kg_rev =fields.Boolean('Revision')
    
    kg_org_boq_id = fields.Many2one('kg.estimation','Original BOQ')
<<<<<<< HEAD
    
    
    kg_rev_count = fields.Integer('Revision Count',compute='compute_rev_count')


=======
>>>>>>> 53dbf3ae642dcf11b75f3101395219e62b22b5a7
    
    
    kg_rev_count = fields.Integer('Revision Count',compute='compute_rev_count')


    #Edit access to Estimation manager
    can_edit_field = fields.Boolean(compute='_compute_can_edit_name')

    def _compute_can_edit_name(self):
      self.can_edit_field = self.env.user.has_group('kg_abra.group_est_manager')


    
    @api.multi
    @api.depends('kg_no_project_manager','kg_no_technicians')
    def _compute_no_persons(self):
        self.kg_no_persons_onsite = self.kg_no_project_manager + self.kg_no_technicians

    @api.multi
    @api.depends('kg_no_project_manager','kg_no_technicians')
    def _compute_no_persons(self):
        self.kg_no_persons_onsite = self.kg_no_project_manager + self.kg_no_technicians

    @api.multi
    def compute_est_count(self):
        for record in self:
            est_obj = self.env['kg.estimation']
            
            record.kg_est_count = est_obj.search_count([('kg_boq_id', '=',record.id),('kg_rev', '!=', True)]) 
            
            
            
    @api.multi
    def compute_fit_count(self):
        for record in self:
            fit_obj = self.env['kg.fitout']
            
            record.kg_fit_count = fit_obj.search_count([('kg_boq_id', '=',record.id)]) 
    
    
    
    @api.multi
    def compute_design_req_count(self):
        for record in self:
            design_req = self.env['kg.design.request']
            
            record.kg_design_count = design_req.search_count([('kg_opp_id', '=',record.kg_opp_id.id),('kg_boq_id', '=',record.id)]) 
    
    
    @api.multi
    def create_design_req(self):
        for record in self:
            vals={}
            vals['kg_opp_id']=record.kg_opp_id.id or False
            
            vals['kg_boq_id']=record.id or False
            
            
            vals['kg_project_id']=record.kg_project.id or False
            
            vals['kg_client']=record.kg_client.id or False
            
       #     vals['name']=record.id or False
            
            self.env['kg.design.request'].create(vals)
    
    
    @api.multi
    def create_est(self):
        for record in self:
            vals={}
            vals['kg_opny']=record.kg_opp_id.id or False
            vals['kg_boq_id']=record.id or False
            vals['kg_client']=record.kg_client and record.kg_client.id or False
            vals['kg_prj_mgr']=record.kg_raised_by and record.kg_raised_by.id or False
            vals['kg_delivery_location']=record.kg_delivery_location and record.kg_delivery_location.id or False

            self.env['kg.estimation'].create(vals)
            self.kg_est_requested = True
            
    @api.multi
    def create_fitout(self):
        for record in self:
            vals={}
            vals['name']='Fitout of '+record.name
            vals['kg_boq_id']=record.id or False
            
            self.env['kg.fitout'].create(vals)

    
    
    
    
    @api.onchange('kg_life_span')
    def onchange_life_span(self):
        self.kg_life_span_met=''
        self.kg_life_span_val=0


    @api.onchange('kg_to_be_confirm')
    def _onchange_tbc(self):
        if self.kg_to_be_confirm:
            self.kg_project_del_date = False



    @api.multi
    def compute_rev_count(self):
        for record in self:
            record.kg_rev_count = self.search_count([('kg_org_boq_id', '=',record.id)]) 
        
        
  
    @api.one
    def create_revision(self,default=None):
        default = dict(default or {})
        rev_count=self.search_count([('kg_org_boq_id', '=',self.id),('kg_rev','=',True)]) 
        default.update({
            'kg_est_requested':True,
            'kg_org_boq_id':self.id,
            'name':self.name+"_"+str(rev_count+1),
            'kg_rev':True})
        super(kg_boq_request, self).copy(default)
        
        
    @api.multi
    def write(self, vals):
        """Override default Odoo write function and extend."""
        # Do your custom logic here
        
        if  vals.get('kg_state') and vals.get('kg_state') !='new' and vals.get('kg_state') !='assign':
            
                vals_mail={}
                message="<b>Dear %(hr_man)s,</b><br/><br/>Your  BOQ Request : %(hd_name)s  has been changed to %(state)s State" % {
                         'hr_man': self.kg_raised_by.partner_id.name,
                           'hd_name': self.name,
                           'state': vals.get('kg_state')
                           
                             
                    }
            
                vals_mail['message']=message
                vals_mail['partner']=self.kg_raised_by.partner_id.email
                self.send_mail(vals_mail)   
        print vals,'hshddhhhd'
        if vals.get('kg_to_be_confirm'):
            vals['kg_project_del_date'] = False
        if vals.get('kg_assign_to'):
            vals['kg_state']='assign'
            user_obj=self.env['res.users'].browse([vals.get('kg_assign_to')])
            fol={}
            fol['res_model']='kg.boq.request'
            fol['res_id']=self.id
            fol['partner_id']=user_obj.partner_id.id
            print user_obj.partner_id.id
            fol_id=self.env['mail.followers'].create(fol)
            print 'After-------------------------------------------------<<'
            subtypes= self.env['mail.message.subtype'].search([('res_model','=','kg.boq.request')]).ids
            if subtypes:
                for i in subtypes:
                    self._cr.execute('INSERT INTO mail_followers_mail_message_subtype_rel (mail_followers_id, mail_message_subtype_id) values (%s, %s)', (fol_id.id, i))

                    
            
            
            vals_mail={}
            message="<b>Dear %(hr_man)s,</b><br/><br/> BOQ Request : %(hd_name)s  has been assigned to You" % {
                         'hr_man': user_obj.partner_id.name,
                           'hd_name': self.name,
                             
                    }
            
            vals_mail['message']=message
            vals_mail['partner']=user_obj.partner_id.email
            self.send_mail(vals_mail)  
            
            
            
        return super(kg_boq_request, self).write(vals)
    
    
    @api.model
    def create(self,vals):
        if vals.get('kg_rev') != True:
            sequence_code='kg.boq.request'
            vals['kg_no']= self.env['ir.sequence'].next_by_code(sequence_code) 
            vals['name']='BOQ/'+vals['kg_no']
        
        dep_obj=self.env['hr.department'].browse([vals.get('kg_dep_id')])    
        partner_id=dep_obj.manager_id.user_id.partner_id
        
       

            
        
        res= super(kg_boq_request, self).create(vals)
        
        print res.kg_dep_id
        print res.kg_dep_id.manager_id
        print res.kg_dep_id.manager_id.user_id
        print res.kg_dep_id.manager_id.user_id.partner_id
        dep_mgr_id =res.kg_dep_id.manager_id.user_id.partner_id.id or False

        if not dep_mgr_id:
            raise UserError(_('Please assign HR department manager(With related user).'))

         
        fol={}
        fol['res_model']='kg.boq.request'
        fol['res_id']=res.id
        fol['partner_id']=dep_mgr_id
        fol_id=self.env['mail.followers'].create(fol)
        subtypes= self.env['mail.message.subtype'].search([('res_model','=','kg.boq.request')]).ids
        if subtypes:
            for i in subtypes:
                self._cr.execute('INSERT INTO mail_followers_mail_message_subtype_rel (mail_followers_id, mail_message_subtype_id) values (%s, %s)', (fol_id.id, i))

                    
        vals_mail={}
        message="<b>Dear %(hr_man)s,</b><br/><br/> New BOQ Request : %(hd_name)s from  %(emp_name)s has been created" % {
                     'hr_man': partner_id.name,
                       'hd_name': res.name,
                         'emp_name': res.kg_raised_by.name,
                }
        
        vals_mail['message']=message
        vals_mail['partner']=partner_id.email
        res.send_mail(vals_mail)
        res.write({'kg_state':''})
        res.write({'kg_state':'new'})
        return res
    
    
    
    def send_mail(self,vals):
        main_content = {
                'subject': "BOQ Request!",
              
                'body_html': '<pre>%s</pre>' % vals['message'],
                 'email_to': vals['partner'],
            }
        return self.env['mail.mail'].create(main_content).send()

    
class kg_glass_spec(models.Model):
    _name="kg.glass.spec"
    name  = fields.Char('Name')
    
class kg_mdf(models.Model):
    _name="kg.mdf"
    name  = fields.Char('Name')
    
    
class kg_type(models.Model):
    _name="kg.type"
    name  = fields.Char('Name')
    
class kg_branding(models.Model):
    _name="kg.branding"
    name  = fields.Char('Name')

class kg_att_incl(models.Model):
    _name="kg.att.incl"

    name = fields.Char('Name',required=True)
    
    
class kg_painting(models.Model):
    
    _name="kg.painting"
    
    name = fields.Char('Name',required=True)
    
    
class kg_packing_spec(models.Model):
    
    _name="kg.packing.spec"
    
    name = fields.Char('Name',required=True)
    
class kg_delivery_mode(models.Model):
    
    _name="kg.delivery.mode"
    
    name = fields.Char('Name',required=True)
    
class kg_delivery_terms(models.Model):
    
    _name="kg.delivery.terms"
    
    name = fields.Char('Name',required=True)
    
class kg_delivery_location(models.Model):
    
    _name="kg.delivery.location"
    
    name = fields.Char('Name',required=True)
    


class kg_special_material(models.Model):

    _name = "kg.spe.mat"
    
    
    name = fields.Char('Name', required=1)    
    
    
    
    