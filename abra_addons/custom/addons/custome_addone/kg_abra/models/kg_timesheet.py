# -*- coding: utf-8 -*-
from odoo import fields,models,api,_
from odoo import tools
from datetime import date
from dateutil.relativedelta import relativedelta

from odoo.exceptions import UserError, AccessError
from odoo import api, fields, models, SUPERUSER_ID, _

class kg_timesheet(models.Model):

    _name="kg.timesheet"

    name = fields.Char('Name')

    kg_job_id = fields.Many2one('mrp.production','JOb')

    kg_project_id = fields.Many2one('project.project','Project')

    kg_emp_id = fields.Many2one('hr.employee','Employee')

    kg_dep_id = fields.Many2one('hr.department','Department')

    kg_nrm_hours = fields.Float('Normal Hours')

    kg_over_hours = fields.Float('Overtime Hours')

    kg_pending_nrm_hrs = fields.Float('Pending Normal Hours')

    kg_pending_over_hrs = fields.Float('Pending Overtime Hours')
    
    kg_install_req_id = fields.Many2one('kg.install.req','Install Request')
    
    kg_design_req_id = fields.Many2one('kg.design.request','Design Request')
    
    

    
    



