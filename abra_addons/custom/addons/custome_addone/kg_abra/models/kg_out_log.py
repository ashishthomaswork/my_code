# -*- coding: utf-8 -*-
from odoo import fields,models,api,_
from odoo import tools
from datetime import date
from dateutil.relativedelta import relativedelta

from odoo.exceptions import UserError, AccessError
from odoo import api, fields, models, SUPERUSER_ID, _

class kg_out_log(models.Model):
    _name="kg.out.log"

    name = fields.Char('Name')

    kg_partner= fields.Many2one('res.partner','Client')

    kg_trip_from = fields.Char('Trip From')

    kg_trip_to  = fields.Char('Trip To')

    kg_time = fields.Float('Time')

    kg_cost = fields.Float('Cost')

    kg_super = fields.Many2one('res.users','Supervisior')
    
    
    kg_att_url1 = fields.Char('')
    
    kg_att_url2 = fields.Char('')
    
    
    kg_att_url3 = fields.Char('')
    
    
    kg_att_url4 = fields.Char('')
    
    kg_att_url5 = fields.Char('')
    
    
    
    kg_req_date = fields.Date('Required Date')
    
    
    kg_raised_by = fields.Many2one('res.users','Raised By',default=lambda self: self.env.user and self.env.user.id or False)
    
    kg_assign_to = fields.Many2one('res.users','Assigned To')
    

    @api.model
    def create(self, vals):
        sequence_code = 'out.log'
        
        vals['name'] = self.env['ir.sequence'].next_by_code(sequence_code)
        print vals,'faffa'
        return super(kg_out_log, self).create(vals)
    
    
    
    @api.multi
    def create_inv(self):
        for record in self:
            vals={}
            vals['partner_id'] = record.kg_partner.id
            
            vals['type']='in_invoice'
            vals['date_invoice']= date.today()
            
            vals['journal_id']=self.env['account.journal'].search([('name','=','Vendor Bills'),('company_id','=',self.env.user.company_id.id)],limit=1).id
            
            inv_id = self.env['account.invoice'].create(vals)
            
            
            vals_line={}
            vals_line['invoice_id']=inv_id.id
            vals_line['name']=record.name
            vals_line['price_unit']=record.kg_cost
            
            vals_line['account_id']=self.env['account.account'].search([('name','=','Professional Services'),('company_id','=',self.env.user.company_id.id)],limit=1).id
            
            self.env['account.invoice.line'].create(vals_line)
            
            return True