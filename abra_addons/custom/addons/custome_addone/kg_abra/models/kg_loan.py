# -*- coding: utf-8 -*-
from odoo import fields,models,api,_
from odoo import tools
from datetime import date
from dateutil.relativedelta import relativedelta

from odoo.exceptions import UserError, AccessError
from odoo import api, fields, models, SUPERUSER_ID, _

class kg_loan(models.Model):
    _name = "kg.loan"

    name = fields.Char('Name')

    kg_emp_id = fields.Many2one('hr.employee','Employee')

    kg_job_id = fields.Many2one('hr.job','Job Title')

    kg_dep_id = fields.Many2one('hr.department','Department')

    kg_loan_amt = fields.Float('Loan Amount')

    kg_emi_based = fields.Char('EMI Based On')

    kg_ded_amt = fields.Float('Deduction Amount')

    kg_amt_paid = fields.Float('Amount Paid')

    kg_amt_pay = fields.Float('Amount to Pay')

    kg_loan_req_date = fields.Date('Loan Request Date')

    kg_loan_pay_start = fields.Date('Loan Payment Start Date')

    kg_pay_dur = fields.Integer('Duration(Months)')

    kg_loan_pay_end = fields.Date('Loan Payment End Date')

    kg_desc = fields.Text('Description')

    kg_state = fields.Selection([('request','Request'),('wait','Waiting Approval'),('appr','Approved'),('refused','Refused')],'State',default='request')

    kg_loan_installs = fields.One2many('kg.loan.install','kg_loan_id','Installaments')

class kg_loan_install(models.Model):

    _name = "kg.loan.install"

    name = fields.Char('Name')

    kg_date = fields.Date('Date')

    kg_narration = fields.Char('Narration')

    kg_amnt = fields.Float('Amount')

    kg_loan_id = fields.Many2one('kg.loan','Loan')
