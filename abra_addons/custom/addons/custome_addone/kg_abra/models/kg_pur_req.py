# -*- coding: utf-8 -*-
from odoo import fields,models,api,_
from odoo import tools
from datetime import date
from dateutil.relativedelta import relativedelta

from odoo.exceptions import UserError, AccessError,ValidationError
from odoo import api, fields, models, SUPERUSER_ID, _

class kg_pur_req(models.Model):
    
    _name='kg.pur.req'
    _inherit = ['mail.thread', 'ir.needaction_mixin']


    name = fields.Char('Name')
    
    kg_client = fields.Many2one('res.partner','Client')
    
    kg_project = fields.Many2one('project.project','Project')
    
    kg_mr_date = fields.Date('MR Date')
    
    
    kg_mr_type = fields.Char('MR Type')
    
    kg_req_date = fields.Date('Required Date', default=fields.Date.today())
    
    
    kg_raised_by = fields.Many2one('res.users','Raised By',default=lambda self: self.env.user and self.env.user.id or False)
    
    kg_assign_to = fields.Many2one('res.users','Assigned To')
    
    kg_job_no = fields.Many2one('mrp.production','Job No')
    
    kg_project_co = fields.Many2one('res.users','Project Co-ordinator')
    
    kg_item = fields.Many2one('product.product','Item Name')
    
    kg_mat_cat = fields.Many2one('product.category','Material Category')
    
    kg_qty = fields.Float('Qty')
    
    kg_uom = fields.Many2one('product.uom','UOM')
    
    kg_desc = fields.Text('Description')
    
    
    kg_state = fields.Selection([('draft','Draft'),('appr','Approved'),('done','Done')],'Status',default='draft')
    
    # kg_pur_req_lines = fields.One2many('kg.pur.req.lines','kg_pur_req_id','Lines')
    
    
    kg_vendor = fields.Many2one('res.partner','Vendor')

    kg_picking_id = fields.Many2one('stock.picking','Vendor')

    kg_dep_id = fields.Many2one('hr.department','Department')

    kg_po_id = fields.Many2one('purchase.order','Purchase Order', readonly=1)

    @api.multi
    def create_rfq(self):
        for record in self:
            if not record.kg_vendor:
                raise ValidationError(_('No Vendor Found.'))
            if record.kg_po_id:
                raise UserError(_('RFQ Already Exist!'))
            rfq = {
               'partner_id': record.kg_vendor.id,
               'date_order': date.today(),
               'company_id':self.env.user.company_id.id,
               'currency_id':self.env.user.company_id.currency_id.id
            }
            rfq_id = self.env['purchase.order'].create(rfq)
            lines={
                    'product_id':record.kg_item.id,
                    'name':record.kg_item.name,
                    'date_planned':record.kg_req_date,
                    'product_qty':record.kg_qty,
                    'product_uom':record.kg_uom.id,
                    'price_unit':record.kg_item.standard_price,
                    'order_id': rfq_id.id,
                    'account_analytic_id': record.kg_project and record.kg_project.analytic_account_id.id,
                    }
                
            self.env['purchase.order.line'].create(lines)
            record.kg_po_id = rfq_id.id
        return True
                
                
    
    
    
    
    # @api.onchange('kg_item')
    # def onchange_item(self):
    #
    #     for record in self:
    #         record.kg_mat_cat =record.kg_item.categ_id and record.kg_item.categ_id.id or False
    #         record.kg_uom =record.kg_item.uom_po_id and record.kg_item.uom_po_id.id or False
    #
            
            

#
#
#
#     @api.multi
#     def _track_subtype(self, init_values):
#         self.ensure_one()
#         if 'kg_state' in init_values and self.kg_state == 'In Progress':
#             return 'kg_abra.kg_purchase_inprogress'
#
#         if 'kg_state' in init_values and self.kg_state == 'new':
#             return 'kg_abra.kg_purchase_new'
#
#         if 'kg_state' in init_values and self.kg_state == 'assign':
#             return 'kg_abra.kg_purchase_assign'
#
#
#
#
#         if 'kg_state' in init_values and self.kg_state == 'Completed':
#             return 'kg_abra.kg_purchase_complete'
#         return super(kg_pur_req, self)._track_subtype(init_values)
#
#     kg_state = fields.Selection([('new','New'),('assign','Assigned'),('In Progress','In Progress'),('Completed','Completed')],'State',track_visibility='onchange',default='new')
#
#     @api.multi
#     def write(self, vals):
#         """Override default Odoo write function and extend."""
#         # Do your custom logic here
#
#         if vals.get('kg_state') and vals.get('kg_state') !='new' and vals.get('kg_state') !='assign' :
#
#                 vals_mail={}
#                 message="<b>Dear %(hr_man)s,</b><br/><br/>Your  Purchase Requisition: %(hd_name)s  has been changed to %(state)s State" % {
#                          'hr_man': self.kg_raised_by.partner_id.name,
#                            'hd_name': self.name,
#                            'state': vals.get('kg_state')
#
#
#                     }
#
#                 vals_mail['message']=message
#                 vals_mail['partner']=self.kg_raised_by.partner_id.email
#                 self.send_mail(vals_mail)
#         print vals,'hshddhhhd'
#         if vals.get('kg_assign_to'):
#             vals['kg_state']='assign'
#             user_obj=self.env['res.users'].browse([vals.get('kg_assign_to')])
#             fol={}
#             fol['res_model']='kg.pur.req'
#             fol['res_id']=self.id
#             fol['partner_id']=user_obj.partner_id.id
#             self.env['mail.followers'].create(fol)
#
#
#             vals_mail={}
#             message="<b>Dear %(hr_man)s,</b><br/><br/> Purchase Requisition : %(hd_name)s  has been assigned to You" % {
#                          'hr_man': user_obj.partner_id.name,
#                            'hd_name': self.name,
#
#                     }
#
#             vals_mail['message']=message
#             vals_mail['partner']=user_obj.partner_id.email
#             self.send_mail(vals_mail)
#
#
#         return super(kg_pur_req, self).write(vals)
#
#
#     @api.model
#     def create(self,vals):
#
#
#         dep_obj=self.env['hr.department'].browse([vals.get('kg_dep_id')])
#         partner_id=dep_obj.manager_id.user_id.partner_id
#
#         test =  [(0,0, {
#
#                                                 'res_model':'kg.pur.req',
#
#                                                  'partner_id':partner_id.id,
#
#                                                    })]
#
#
#         vals['message_follower_ids']=test
#
#
#
#
#         res= super(kg_pur_req, self).create(vals)
#
# #         dep_mgr_id =res.kg_dep_id.manager_id.user_id.partner_id.id
# #
# #         fol={}
# #         fol['res_model']='kg.boq.request'
# #         fol['res_id']=res.id
# #         fol['partner_id']=dep_mgr_id
# #         self.env['mail.followers'].create(fol)
#         vals_mail={}
#         message="<b>Dear %(hr_man)s,</b><br/><br/> New Purchase Requisition : %(hd_name)s from  %(emp_name)s has been created" % {
#                      'hr_man': partner_id.name,
#                        'hd_name': res.name,
#                          'emp_name': res.kg_raised_by.name,
#                 }
#
#         vals_mail['message']=message
#         vals_mail['partner']=partner_id.email
#         res.send_mail(vals_mail)
#         return res
#
#
#
#     def send_mail(self,vals):
#         main_content = {
#                 'subject': "Purchase Requisition!",
#
#                 'body_html': '<pre>%s</pre>' % vals['message'],
#                  'email_to': vals['partner'],
#             }
#         return self.env['mail.mail'].create(main_content).send()
#
#
#
#
#
            
# class kg_pur_req_lines(models.Model):
#     _name="kg.pur.req.lines"
#
#     name = fields.Char('Name')
#
#     kg_product = fields.Many2one('product.product','Item')
#
#     kg_req_date = fields.Date('Required Date')
#
#     kg_uom = fields.Many2one('product.uom','UOM')
#
#     kg_qty = fields.Float('Product Qty')
#
#     kg_unit = fields.Float('Unit Rate')
#     
#     
#     kg_total = fields.Float('Total')
    
    # kg_pur_req_id = fields.Many2one('kg.pur.req','Pur Req id')
    
#     @api.onchange('kg_qty','kg_unit')
#     def calc_total(self):
#         for record in self:
#             record.kg_total =record.kg_qty*record.kg_unit
#     
#     @api.onchange('kg_product')
#     def onchange_product(self):
#         for record in self:
#             record.kg_uom  = record.kg_product.uom_id.id or False

            