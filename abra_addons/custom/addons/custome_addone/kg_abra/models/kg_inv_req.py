# -*- coding: utf-8 -*-
from odoo import fields,models,api,_
from odoo import tools
from datetime import date
from dateutil.relativedelta import relativedelta

from odoo.exceptions import UserError, AccessError
from odoo import api, fields, models, SUPERUSER_ID, _

class kg_inv_req(models.Model):
    
    _name='kg.inv.req'
    
    name = fields.Char('Name')
    
    kg_client = fields.Many2one('res.partner','Client')
    
    kg_project = fields.Many2one('project.project','Project')
    
 #   kg_date = fields.Date('Date')
    
    kg_req_date = fields.Date('Required Date')
    
    
    kg_raised_by = fields.Many2one('res.users','Raised By',default=lambda self: self.env.user and self.env.user.id or False)
    
    kg_assign_to = fields.Many2one('res.users','Assigned To')
    
    kg_inv_amt = fields.Float('Invoice Amount')
    
    kg_desc = fields.Text('Description')
    
    kg_state = fields.Selection([('draft','Draft'),('appr','Approved'),('done','Done')],'Status',default='draft')