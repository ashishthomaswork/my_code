# -*- coding: utf-8 -*-
from odoo import fields,models,api,_
from odoo import tools
from datetime import date
from dateutil.relativedelta import relativedelta

from odoo.exceptions import UserError, AccessError
from odoo import api, fields, models, SUPERUSER_ID, _



class kg_fitout(models.Model):
    
    _name = 'kg.fitout'
    
    name = fields.Char('Name')
    
    kg_no = fields.Char('No')
    
    
    
    
    kg_ac_work_lines  = fields.One2many('kg.fitout.lines','kg_fitout_id' ,domain=[('kg_sub_type', '=', 'ac_work')])
    
    kg_ee_work_lines  = fields.One2many('kg.fitout.lines','kg_fitout_id' ,domain=[('kg_sub_type', '=', 'ee_work')])
    
    kg_fa_work_lines  = fields.One2many('kg.fitout.lines','kg_fitout_id',domain=[('kg_sub_type', '=', 'fa_work')])
    
    kg_em_work_lines  = fields.One2many('kg.fitout.lines','kg_fitout_id' ,domain=[('kg_sub_type', '=', 'em_work')])
    
    kg_ff_work_lines  = fields.One2many('kg.fitout.lines','kg_fitout_id' ,domain=[('kg_sub_type', '=', 'ff_work')])
    
    
    kg_misc_mep_lines  = fields.One2many('kg.fitout.lines','kg_fitout_id' ,domain=[('kg_sub_type', '=', 'misc_mep')])
    
    kg_part_civil_lines  = fields.One2many('kg.fitout.lines','kg_fitout_id' ,domain=[('kg_sub_type', '=', 'part_civil')])
    
    kg_floor_civil_lines  = fields.One2many('kg.fitout.lines','kg_fitout_id' ,domain=[('kg_sub_type', '=', 'floor_civil')])
    
    kg_ceil_civil_lines  = fields.One2many('kg.fitout.lines','kg_fitout_id' ,domain=[('kg_sub_type', '=', 'ceil_civil')])
    
    
    kg_sf_civil_lines  = fields.One2many('kg.fitout.lines','kg_fitout_id' ,domain=[('kg_sub_type', '=', 'sf_civil')])
    
    
    kg_shop_civil_lines  = fields.One2many('kg.fitout.lines','kg_fitout_id' ,domain=[('kg_sub_type', '=', 'shop_civil')])
    
    kg_pre_civil_lines  = fields.One2many('kg.fitout.lines','kg_fitout_id' ,domain=[('kg_sub_type', '=', 'pre_civil')])
    
    
    kg_misc_civil_lines  = fields.One2many('kg.fitout.lines','kg_fitout_id' ,domain=[('kg_sub_type', '=', 'misc_civil')])

    
    kg_boq_id = fields.Many2one('kg.boq.request','BOQ Request')
    
   
    
    
class kg_fitout_lines(models.Model):
    
    _name = 'kg.fitout.lines'
    
    name = fields.Char('Description')
    kg_type = fields.Selection([('mep','mep'),('civil','civil')])
    
    kg_sub_type = fields.Selection([('ac_work','ac_work'),('ee_work','ee_work'),('fa_work','fa_work'),('em_work','em_work'),('ff_work','ff_work'),('misc_mep','misc_mep'),('part_civil','part_civil'),
                                    ('floor_civil','floor_civil'), ('ceil_civil','ceil_civil'), ('sf_civil','sf_civil'), ('shop_civil','shop_civil'),('pre_civil','pre_civil'),('misc_civil','misc_civil')])
    
   # kg_product = fields.Many2one('product.product','Material')
    
    kg_uom = fields.Many2one('product.uom','Units')
    
    kg_qty  = fields.Float('Qty')
    
    kg_rate = fields.Float('Rate')
    
    kg_amt = fields.Float('Amount',compute='comp_amt')
    
    kg_remarks = fields.Char('Remarks')
    
    kg_fitout_id = fields.Many2one('kg.fitout','Fitout')
    
    
    @api.onchange('kg_product')
    def onchange_product(self):
        
        for record in self:
            record.name = record.kg_product.name
            record.kg_uom = record.kg_product.uom_id.id or False
            
    @api.depends('kg_qty','kg_rate')
    def comp_amt(self):
        for record in self:
            record.kg_amt = record.kg_qty*record.kg_rate
    
    