# -*- coding: utf-8 -*-
from odoo import fields,models,api,_
from odoo import tools
from datetime import date
from dateutil.relativedelta import relativedelta

from odoo.exceptions import UserError, AccessError
from odoo import api, fields, models, SUPERUSER_ID, _

class kg_overinstall_sch(models.Model):
    
    _name = 'kg.overinstall.sch'
    
    name  = fields.Char('Name')
    
    kg_job_id = fields.Many2one('mrp.production','Job No')
    
    kg_client = fields.Many2one('res.partner','Client')
    
    kg_prj_charge = fields.Many2one('res.users','Project In-Charge')
    
    kg_date = fields.Date('Date')
    
    kg_material_status = fields.Char('Material Status')
    
    kg_site_clear_status  = fields.Char('Site Clearance Status')
    
    kg_contact_no = fields.Char('Contact No')
    
    kg_country = fields.Many2one('res.country','Country')
    
    kg_city = fields.Char('City')
    
    kg_location = fields.Char('Location')
    
    kg_install_start_date = fields.Date('Installation Start Date')
    
    kg_install_time = fields.Selection([('day','Day'),('night','Night')],'Installation Time')
    
    kg_own_emp_no  = fields.Integer('Own Employees')
    
    kg_outsou_emp_no = fields.Integer('Outsourced Employees')
    
    kg_no_of_hrs = fields.Float('No of Hours / Day / Night')
    
    kg_install_end_date = fields.Date('Installation End Date')
    
    kg_advance = fields.Float('Advance if any')
    
    kg_travel_deposit = fields.Float('Travel Deposit if any')
    
    
    kg_visa_req = fields.Char('Visa Requirmen')
    
    kg_visa_arr = fields.Char('Visa Arranged By')
    
    kg_near_air = fields.Char('Nearest Airport')
    
    kg_onwards_date = fields.Datetime('Onwards date & time')
    
    kg_return_date = fields.Datetime('Return date & time')
    
    kg_no_of_nights = fields.Integer('No of Nights Stay')
    
    kg_expense_lines = fields.One2many('kg.overexpense.line','kg_overinstall_id')
    
    kg_install_req_id = fields.Many2one('kg.install.req','Install Request')
    
    kg_state = fields.Selection([('new','New'),('in_progress','In Progress'),('complete','Completed')],'State',default='new')
    
    
    company_id = fields.Many2one('res.company','Company',default=lambda self: self.env.user.company_id)
    
    
    
    


class kg_overexpense_line(models.Model):
    
    _name = 'kg.overexpense.line'
    
    name  = fields.Char('Expenses')
    
    kg_estimate = fields.Float('Estimate As Per Quote')
    
    kg_actual = fields.Float('Actual')
    
    kg_rem = fields.Char('Remarks')
    
    kg_overinstall_id = fields.Many2one('kg.overinstall.sch')





























