# -*- coding: utf-8 -*-
from odoo import fields,models,api,_
from odoo import tools
from datetime import date
from dateutil.relativedelta import relativedelta

from odoo.exceptions import UserError, AccessError
from odoo import api, fields, models, SUPERUSER_ID, _

class kg_mat_req(models.Model):
    
    _name='kg.mat.req'



    @api.multi
    def release(self):

        picking_id = self.picking_id and self.picking_id.id

        picking_obj = self.picking_id
        state = picking_obj.state

        if not picking_id:
        	raise UserError(_('create and attach transfer document'))

        if state != 'done':
        	raise UserError(_('transfer not completed'))



        kg_mrp_id = self.kg_mrp_id and self.kg_mrp_id.id
        if not kg_mrp_id:
        	raise UserError(_('Job not defined'))
        mrp_obj = self.kg_mrp_id
        consumed_lines = mrp_obj.move_raw_ids
        for line in consumed_lines:

        	line.action_assign()
        	if line.state != 'assigned':
				raise UserError(_('recheck the stock for product: %s') % line.product_id.name)

        self.kg_state = 'release'


    @api.multi
    def production_ack(self):

        state = self.kg_state

        if state != 'release':
        	raise UserError(_('Let store release first'))
  

        kg_mrp_id = self.kg_mrp_id and self.kg_mrp_id.id
        if not kg_mrp_id:
        	raise UserError(_('Job not defined'))
        mrp_obj = self.kg_mrp_id
        consumed_lines = mrp_obj.move_raw_ids
        for line in consumed_lines:

        	if line.state != 'assigned':

        		line.action_assign()

        		if line.state != 'assigned':
					raise UserError(_('recheck the stock for product: %s') % line.product_id.name)

        self.kg_state = 'ack'
        self.kg_production_ack_by = self.env.user and self.env.user.id


    @api.multi
    def done(self):

        self.kg_state = 'done'


    
    name = fields.Char('Name')
    
    kg_client = fields.Many2one('res.partner','Client')
    
    kg_project = fields.Many2one('project.project','Project')

    kg_mrp_id = fields.Many2one('mrp.production','Job')

    picking_id = fields.Many2one('stock.picking','Picking')
    
    #kg_date = fields.Date('Date')
    
    
    
    
    kg_raised_by = fields.Many2one('res.users','Raised By',default=lambda self: self.env.user and self.env.user.id or False)
    
    kg_assign_to = fields.Many2one('res.users','Assigned To')
    
    kg_production_ack_by = fields.Many2one('res.users','Production(Ack)')   
    
    kg_desc = fields.Text('Description')

    kg_availability_date = fields.Date('Availability Date')
    
    kg_mat_req_lines = fields.One2many('kg.mat.req.lines','kg_mat_req_id','Lines')
    
    kg_state = fields.Selection([('draft','Draft'),('release','Released'),('ack','Acknowledged By Production'),('done','Done')],'Status',default='draft')
    
    
class kg_mat_req_lines(models.Model):
    _name="kg.mat.req.lines"
    
    name = fields.Char('Name')
    
    kg_product = fields.Many2one('product.product','Item')
    
    kg_req_date = fields.Date('Required Date')
    
    kg_uom = fields.Many2one('product.uom','UOM')
    
    kg_qty = fields.Float('Product Qty')
    
#     kg_unit = fields.Float('Unit Rate')
#     
#     
#     kg_total = fields.Float('Total')
    
    kg_mat_req_id = fields.Many2one('kg.mat.req','Mat Req id')
    
#     @api.onchange('kg_qty','kg_unit')
#     def calc_total(self):
#         for record in self:
#             record.kg_total =record.kg_qty*record.kg_unit
#     
    @api.onchange('kg_product')
    def onchange_product(self):
        for record in self:
            record.kg_uom  = record.kg_product.uom_id.id or False
