# -*- coding: utf-8 -*-
from odoo import fields,models,api,_
from odoo import tools
from datetime import date
from dateutil.relativedelta import relativedelta

from odoo.exceptions import UserError, AccessError
from odoo import api, fields, models, SUPERUSER_ID, _

class kg_delivery_request(models.Model):
    
    _name = 'kg.delivery.request'
    
    
    
    
    
    name  = fields.Char('Name')
    
    kg_client = fields.Many2one('res.partner','Client')
    
    kg_project_id = fields.Many2one('project.project','Project')
    
    kg_raised_by = fields.Many2one('res.users','Raised By',default=lambda self: self.env.user and self.env.user.id or False)
    
    kg_raised_date = fields.Date('Raised Date')
    
    kg_assign_to = fields.Many2one('res.users','Assign To')
    
    kg_delivery_lines = fields.One2many('kg.delivery.request.line','kg_parent_id','Lines')
    
    _inherit = ['mail.thread', 'ir.needaction_mixin']
    
    
    
    
    @api.multi
    def _track_subtype(self, init_values):
        self.ensure_one()
        if 'kg_state' in init_values and self.kg_state == 'In Progress':
            return 'kg_abra.kg_delivery_inprogress'
        
        if 'kg_state' in init_values and self.kg_state == 'new':
            return 'kg_abra.kg_delivery_new'
        
        if 'kg_state' in init_values and self.kg_state == 'assign':
            return 'kg_abra.kg_delivery_assign'
        
        if 'kg_state' in init_values and self.kg_state == 'Quote Created':
            return 'kg_abra.kg_delivery_quote_created'
        
        
        if 'kg_state' in init_values and self.kg_state == 'Completed':
            return 'kg_abra.kg_delivery_complete'
        return super(kg_delivery_request, self)._track_subtype(init_values)
    
    kg_state = fields.Selection([('new','New'),('assign','Assigned'),('In Progress','In Progress'),('Completed','Completed')],'State',track_visibility='onchange',default='new')

    kg_dep_id = fields.Many2one('hr.department','Assigned Department')
    @api.multi
    def write(self, vals):
        """Override default Odoo write function and extend."""
        # Do your custom logic here
        
        if vals.get('kg_state') and vals.get('kg_state') !='new' and vals.get('kg_state') !='assign' :
            
                vals_mail={}
                message="<b>Dear %(hr_man)s,</b><br/><br/>Your  Delivery Request : %(hd_name)s  has been changed to %(state)s State" % {
                         'hr_man': self.kg_raised_by.partner_id.name,
                           'hd_name': self.name,
                           'state': vals.get('kg_state')
                           
                             
                    }
            
                vals_mail['message']=message
                vals_mail['partner']=self.kg_raised_by.partner_id.email
                self.send_mail(vals_mail)   
        print vals,'hshddhhhd'
        if vals.get('kg_assign_to'):
            vals['kg_state']='assign'
            user_obj=self.env['res.users'].browse([vals.get('kg_assign_to')])
            fol={}
            fol['res_model']='kg.delivery.request'
            fol['res_id']=self.id
            fol['partner_id']=user_obj.partner_id.id
            fol_id=self.env['mail.followers'].create(fol)
            subtypes= self.env['mail.message.subtype'].search([('res_model','=','kg.delivery.request')]).ids
            if subtypes:
                for i in subtypes:
                    self._cr.execute('INSERT INTO mail_followers_mail_message_subtype_rel (mail_followers_id, mail_message_subtype_id) values (%s, %s)', (fol_id.id, i))

        
        
            
            
            vals_mail={}
            message="<b>Dear %(hr_man)s,</b><br/><br/> Delivery Request : %(hd_name)s  has been assigned to You" % {
                         'hr_man': user_obj.partner_id.name,
                           'hd_name': self.name,
                             
                    }
            
            vals_mail['message']=message
            vals_mail['partner']=user_obj.partner_id.email
            self.send_mail(vals_mail)  
            
            
        return super(kg_delivery_request, self).write(vals)
    
    
    @api.model
    def create(self,vals):
     
        
        dep_obj=self.env['hr.department'].browse([vals.get('kg_dep_id')])    
        partner_id=dep_obj.manager_id.user_id.partner_id
        
        


            
        
        res= super(kg_delivery_request, self).create(vals)
        
        dep_mgr_id =res.kg_dep_id.manager_id.user_id.partner_id.id
         
        fol={}
        fol['res_model']='kg.delivery.request'
        fol['res_id']=res.id
        fol['partner_id']=dep_mgr_id
        fol_id=self.env['mail.followers'].create(fol)
        subtypes= self.env['mail.message.subtype'].search([('res_model','=','kg.delivery.request')]).ids
        if subtypes:
            for i in subtypes:
                self._cr.execute('INSERT INTO mail_followers_mail_message_subtype_rel (mail_followers_id, mail_message_subtype_id) values (%s, %s)', (fol_id.id, i))

        
        
        vals_mail={}
        message="<b>Dear %(hr_man)s,</b><br/><br/> New Delivery Request : %(hd_name)s from  %(emp_name)s has been created" % {
                     'hr_man': partner_id.name,
                       'hd_name': res.name,
                         'emp_name': res.kg_raised_by.name,
                }
        
        vals_mail['message']=message
        vals_mail['partner']=partner_id.email
        res.send_mail(vals_mail)
        res.write({'kg_state':''})
        res.write({'kg_state':'new'})
        return res
    
    
    
    def send_mail(self,vals):
        main_content = {
                'subject': "Delivery Request!",
              
                'body_html': '<pre>%s</pre>' % vals['message'],
                 'email_to': vals['partner'],
            }
        return self.env['mail.mail'].create(main_content).send()



class kg_delivery_request_line(models.Model):
    
    _name = 'kg.delivery.request.line'
    
    name = fields.Char('Name')
    
    kg_product = fields.Many2one('product.product','Product')
    
    kg_uom  = fields.Many2one('product.uom','UOM')
    
    kg_qty = fields.Float('Qty')
    
    kg_parent_id = fields.Many2one('kg.delivery.request','Delivery Request')