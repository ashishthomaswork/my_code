# -*- coding: utf-8 -*-
from odoo import fields,models,api,_
from odoo import tools
from datetime import date
from dateutil.relativedelta import relativedelta

from odoo.exceptions import UserError, AccessError
from odoo import api, fields, models, SUPERUSER_ID, _


#from odoo import one2many_sorted


class kg_manhour(models.TransientModel):
    _name='kg.manhour'
    
    name = fields.Char('Name')
    
    normal_hour = fields.Float('Normal Hour')
    
    ot_hour = fields.Float('Over Time Hour')
    
    total_hours = fields.Float('Total Hour')


class kg_estimation(models.Model):
    _name = 'kg.estimation'
    
    
    
    @api.multi
    def show_manhour(self):
        for record in self:
            
            
           
            materials= self.env['kg.unit.lines'].search([('kg_cat','=',15),('kg_unit_id.kg_est_id','=',record.id)]).mapped('kg_mat')
            print materials,'11111111111111111'
            lines=[]
            self.env['kg.manhour'].search([]).unlink()
            for mat in materials:
                srch = self.env['kg.unit.lines'].search([('kg_cat','=',15),('kg_mat','=',mat.id),('kg_unit_id.kg_est_id','=',record.id)],limit=1)
              #  lines.append(srch.id)
              
                vals={}
                vals['name']=srch.kg_mat.name
                vals['normal_hour']=srch.kg_normal_hours
                vals['ot_hour']=srch.kg_ot_hours
                vals['total_hours']=srch.kg_normal_hours+srch.kg_ot_hours
                self.env['kg.manhour'].create(vals)
            
            
            
            tot_inst_hrs=0
            doms=self.env['kg.domestic.install.lines'].search([('kg_est_id','=',record.id)])
            for dom in doms:
                tot_inst_hrs=tot_inst_hrs+dom.kg_install_lab_hrs
                
            doms=self.env['kg.inter.install.lines'].search([('kg_est_id','=',record.id)])
            for dom in doms:
                tot_inst_hrs=tot_inst_hrs+dom.kg_install_lab_hrs
            
            doms=self.env['kg.domestic.removal.lines'].search([('kg_est_id','=',record.id)])
            for dom in doms:
                tot_inst_hrs=tot_inst_hrs+dom.kg_removal_lab_hrs
                
            doms=self.env['kg.inter.removal.lines'].search([('kg_est_id','=',record.id)])
            for dom in doms:
                tot_inst_hrs=tot_inst_hrs+dom.kg_removal_lab_hrs
            
            vals={}
            vals['name']='Installation'
            vals['normal_hour']=tot_inst_hrs*0.6
            vals['ot_hour']=tot_inst_hrs*0.4
            vals['total_hours']=tot_inst_hrs
            self.env['kg.manhour'].create(vals)
            
            
            
            lines =self.env['kg.manhour'].search([]).ids
            
            
            return {
            'name': _('Manhour Distribution'),
            'view_type': 'form',
            'view_mode': 'tree',
            'res_model': 'kg.manhour',
            'view_id': self.env.ref('kg_abra.view_kg_manhour_tree').id,
            'type': 'ir.actions.act_window',
            'domain': [('id', 'in', lines)],
          
            'target': 'new',
        }
                
            
#             action = {
#             'name': 'Translate',
#             'res_model': 'ir.translation',
#             'type': 'ir.actions.act_window',
#             'view_mode': 'tree',
#             'view_id': self.env.ref('base.view_translation_dialog_tree').id,
#             'target': 'current',
#             'flags': {'search_view': True, 'action_buttons': True},
#             'domain': domain,
#         }
#     
    @api.one
    def copy_unit_lines(self):
        for record in self:
            for line in record.kg_unit_details:
                if line.kg_select:
                    unit_id = line.copy()
                    print unit_id,'>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>'
                    unit_lines=self.env['kg.unit.lines'].search([('kg_unit_id','=',line.id)])
                    for unit_line in unit_lines:
                        lines_copy = dict({})
                        lines_copy.update({
                        'kg_unit_id':unit_id.id,
                      })
                        unit_line.copy(lines_copy)
        return True

















    
    name = fields.Char('Name')
    
    
    kg_fin_lines = fields.One2many('kg.fin.lines','kg_est_id','Finance',default=lambda self: self._default_fin_lines(), store=True,copy=True)
    
    
  #  kg_fin_lines = fields.One2many('kg.fin.lines','kg_est_id','Finance', copy=True,compute="get_fin_lines")
    
    
    
    kg_quot_est = fields.Boolean('Quotation Estimation')
    
    kg_tech_est = fields.Boolean('Technical Estimation')
    
    
    @api.multi
    def write(self,vals):
        if vals.get('kg_quot_est') == True:
            
            if self.kg_org_est_id :
                if self.kg_org_est_id.kg_quot_est == True:
                    raise UserError(_('An Estimation is Already selected as Quotation Estimation'))
                est_count = self.search_count([('kg_org_est_id','=',self.kg_org_est_id.id),('kg_quot_est','=',True)])
                if est_count > 0 :
                
                    raise UserError(_('An Estimation is Already selected as Quotation Estimation'))
                
            est_count = self.search_count([('kg_org_est_id','=',self.id),('kg_quot_est','=',True)])
            if est_count > 0 :
                
                raise UserError(_('An Estimation is Already selected as Quotation Estimation'))
            
            
        if vals.get('kg_tech_est') == True:
            
            if self.kg_org_est_id :
                if self.kg_org_est_id.kg_tech_est == True:
                    raise UserError(_('An Estimation is Already selected as Technical Estimation'))
                est_count = self.search_count([('kg_org_est_id','=',self.kg_org_est_id.id),('kg_tech_est','=',True)])
                if est_count > 0 :
                
                    raise UserError(_('An Estimation is Already selected as Technical Estimation'))
                
            est_count = self.search_count([('kg_org_est_id','=',self.id),('kg_tech_est','=',True)])
            if est_count > 0 :
                
                raise UserError(_('An Estimation is Already selected as Technical Estimation'))
                
                
          
        
        return super(kg_estimation, self).write(vals)
        
        
    
    @api.model
    def _default_fin_lines(self):
        cat_ids =self.env['kg.category'].search([])
        b=[]
        for i in cat_ids:
            a={}
            a['kg_cat_id']=i
          
            c=(0,0,a)
            b.append(c)
        return b
    
    
    kg_job_no = fields.Many2one('mrp.production','Job No')
    
    kg_client = fields.Many2one('res.partner','Client')
    
    kg_quote_ref = fields.Many2one('sale.order','Quote Ref')
    
    kg_quote_date = fields.Date('Quote Date')
    
    kg_prj_ownr = fields.Many2one('res.users','Project Owner')
    
    kg_prj_mgr = fields.Many2one('res.users','Project Manager')
    
    kg_project_id = fields.Many2one('project.project','Project')
    
    kg_proj_desc  = fields.Char('Project Description')
    
    kg_opny =  fields.Many2one('crm.lead','Opportunity')
    
    kg_boq_id =  fields.Many2one('kg.boq.request','BOQ Request')
    
    @api.onchange('kg_boq_id')
    def onchane_boq_id(self):
        for record in self:
            record.kg_client = record.kg_boq_id and record.kg_boq_id.kg_client and record.kg_boq_id.kg_client.id or False
            record.kg_opny =record.kg_boq_id and record.kg_boq_id.kg_opp_id and record.kg_boq_id.kg_opp_id or False
            record.kg_prj_mgr = record.kg_boq_id and record.kg_boq_id.kg_assign_to and record.kg_boq_id.kg_assign_to.id or False
            
    kg_des_id =  fields.Many2one('kg.design.request','Design Request')
    
    
    kg_unit_details = fields.One2many('kg.unit','kg_est_id')
    
    
    kg_overall_unit_details = fields.One2many('kg.unit','kg_est_id')
    
    @api.model
    def _default_job_lines(self):
        cat_ids =self.env['hr.job'].search([('name','in',['Carpenter','Electrician','Supervisor','Helper'])])
        b=[]
        for i in cat_ids:
            a={}
            a['kg_emp_categ']=i
          
            c=(0,0,a)
            b.append(c)
        return b

<<<<<<< HEAD
    kg_domestic_install_lines = fields.One2many('kg.domestic.install.lines','kg_est_id','Domestic Install',default=lambda self: self._default_job_lines())
    
    kg_domestic_removal_lines = fields.One2many('kg.domestic.removal.lines','kg_est_id','Domestic Removal',default=lambda self: self._default_job_lines())

    kg_inter_install_lines = fields.One2many('kg.inter.install.lines','kg_est_id','International Install',default=lambda self: self._default_job_lines())

    kg_inter_removal_lines = fields.One2many('kg.inter.removal.lines','kg_est_id','International Removal',default=lambda self: self._default_job_lines())
    
    kg_inter_inst_flight_lines = fields.One2many('kg.inter.inst.flight.lines','kg_est_id','International Install Flight',default=lambda self: self._default_job_lines())
    
    kg_inter_inst_accomodation_lines = fields.One2many('kg.inter.inst.accomodation.lines','kg_est_id','International Install Flight',default=lambda self: self._default_job_lines())

    kg_inter_inst_visa_lines = fields.One2many('kg.inter.inst.visa.lines','kg_est_id','International Install Flight',default=lambda self: self._default_job_lines())

    kg_inter_inst_freight_lines = fields.One2many('kg.inter.inst.freight.lines','kg_est_id','International Install Flight')

    kg_inter_rem_flight_lines = fields.One2many('kg.inter.rem.flight.lines','kg_est_id','International Install Flight',default=lambda self: self._default_job_lines())

    kg_inter_rem_accomodation_lines = fields.One2many('kg.inter.rem.accomodation.lines','kg_est_id','International Install Flight',default=lambda self: self._default_job_lines())

    kg_inter_rem_visa_lines = fields.One2many('kg.inter.rem.visa.lines','kg_est_id','International Install Flight',default=lambda self: self._default_job_lines())

    kg_inter_rem_doc_lines = fields.One2many('kg.inter.rem.doc.lines','kg_est_id','International Install Flight')

    kg_travel_lines = fields.One2many('kg.travel.lines','kg_est_id','Flights &amp; Hotels &amp; Visa',default=lambda self: self._default_job_lines())
=======
    kg_domestic_install_lines = fields.One2many('kg.domestic.install.lines','kg_est_id','Domestic Install')
    
    kg_domestic_removal_lines = fields.One2many('kg.domestic.removal.lines','kg_est_id','Domestic Removal')

    kg_inter_install_lines = fields.One2many('kg.inter.install.lines','kg_est_id','International Install')

    kg_inter_removal_lines = fields.One2many('kg.inter.removal.lines','kg_est_id','International Removal')
    
    kg_inter_inst_flight_lines = fields.One2many('kg.inter.inst.flight.lines','kg_est_id','International Install Flight')
    
    kg_inter_inst_accomodation_lines = fields.One2many('kg.inter.inst.accomodation.lines','kg_est_id','International Install Flight')

    kg_inter_inst_visa_lines = fields.One2many('kg.inter.inst.visa.lines','kg_est_id','International Install Flight')

    kg_inter_inst_freight_lines = fields.One2many('kg.inter.inst.freight.lines','kg_est_id','International Install Flight')

    kg_inter_rem_flight_lines = fields.One2many('kg.inter.rem.flight.lines','kg_est_id','International Install Flight')

    kg_inter_rem_accomodation_lines = fields.One2many('kg.inter.rem.accomodation.lines','kg_est_id','International Install Flight')

    kg_inter_rem_visa_lines = fields.One2many('kg.inter.rem.visa.lines','kg_est_id','International Install Flight')

    kg_inter_rem_doc_lines = fields.One2many('kg.inter.rem.doc.lines','kg_est_id','International Install Flight')

    kg_travel_lines = fields.One2many('kg.travel.lines','kg_est_id','Flights &amp; Hotels &amp; Visa')
>>>>>>> 53dbf3ae642dcf11b75f3101395219e62b22b5a7
    
    kg_att_url1 = fields.Char('')
    
    kg_att_url2 = fields.Char('')
    
    
    kg_att_url3 = fields.Char('')
    
    
    kg_att_url4 = fields.Char('')
    
    kg_att_url5 = fields.Char('')
    
    kg_att_url6 = fields.Char('')
    
    kg_att_url7 = fields.Char('')
    
    kg_att_url8 = fields.Char('')
    
    kg_quot_count = fields.Integer('Quotation Count',compute='compute_quot_count')
    
    kg_rev =fields.Boolean('Revision')
    
    kg_org_est_id = fields.Many2one('kg.estimation','Original Estimation')
    
    
    kg_rev_count = fields.Integer('Revision Count',compute='compute_rev_count')
    
    
    kg_install_labour_cost = fields.Float('Installation Count',compute='comp_install_labour_cost')
    
    kg_install_labour_profit = fields.Float('Profit')
    
    kg_install_labour_sp = fields.Float('',compute='comp_install_labour_sp')
    
    
    kg_install_removal_cost = fields.Float('removal Count',compute='comp_install_removal_cost')
    
    kg_install_removal_profit = fields.Float('Profit')
    
    kg_install_removal_sp = fields.Float('',compute='comp_install_removal_sp')

    kg_delivery_location =fields.Many2one('kg.delivery.location','Delivery Location')
    
    
    
    @api.multi
    @api.depends('kg_domestic_install_lines')
    def comp_install_labour_cost(self):
        for record in self:
            lab_cost=0.0
            
            for dom in record.kg_domestic_install_lines:
                lab_cost=lab_cost+(dom.kg_install_lab_cost or 0.0)
                
            record.kg_install_labour_cost=lab_cost
            
            
    @api.multi
    @api.depends('kg_install_labour_cost','kg_install_labour_profit')
    def comp_install_labour_sp(self):
        for record in self:
            
                
            record.kg_install_labour_sp=(record.kg_install_labour_cost*(record.kg_install_labour_profit/100.0))+record.kg_install_labour_cost
                
    
    
    
    @api.multi
    @api.depends('kg_domestic_removal_lines')
    def comp_install_removal_cost(self):
        for record in self:
            lab_cost=0.0
            
            for dom in record.kg_domestic_removal_lines:
                lab_cost=lab_cost+(dom.kg_removal_lab_cost or 0.0)
                
            record.kg_install_removal_cost=lab_cost
            
            
    @api.multi
    @api.depends('kg_install_removal_cost','kg_install_removal_profit')
    def comp_install_removal_sp(self):
        for record in self:
            
                
            record.kg_install_removal_sp=(record.kg_install_removal_cost*(record.kg_install_removal_profit/100.0))+record.kg_install_removal_cost
                
    
    
    kg_install_labour_cost_inter = fields.Float('Installation Count',compute='comp_install_labour_cost_inter')
    
    kg_install_labour_profit_inter = fields.Float('Profit')
    
    kg_install_labour_sp_inter = fields.Float('',compute='comp_install_labour_sp_inter')
    
    
    kg_install_removal_cost_inter = fields.Float('removal Count',compute='comp_install_removal_cost_inter')
    
    kg_install_removal_profit_inter = fields.Float('Profit')
    
    kg_install_removal_sp_inter = fields.Float('',compute='comp_install_removal_sp_inter')
    
    
    
    @api.multi
    @api.depends('kg_inter_install_lines')
    def comp_install_labour_cost_inter(self):
        for record in self:
            lab_cost=0.0
            
            for dom in record.kg_inter_install_lines:
                lab_cost=lab_cost+(dom.kg_install_lab_cost or 0.0)
                
            record.kg_install_labour_cost_inter=lab_cost
            
            
    @api.multi
    @api.depends('kg_install_labour_cost_inter','kg_install_labour_profit_inter')
    def comp_install_labour_sp_inter(self):
        for record in self:
            
                
            record.kg_install_labour_sp_inter=(record.kg_install_labour_cost_inter*(record.kg_install_labour_profit_inter/100.0))+record.kg_install_labour_cost_inter
                
    
    
    
    @api.multi
    @api.depends('kg_inter_removal_lines')
    def comp_install_removal_cost_inter(self):
        for record in self:
            lab_cost=0.0
            
            for dom in record.kg_inter_removal_lines:
                lab_cost=lab_cost+(dom.kg_removal_lab_cost or 0.0)
                
            record.kg_install_removal_cost_inter=lab_cost
            
            
    @api.multi
    @api.depends('kg_install_removal_cost_inter','kg_install_removal_profit_inter')
    def comp_install_removal_sp_inter(self):
        for record in self:
            
                
            record.kg_install_removal_sp_inter=(record.kg_install_removal_cost_inter*(record.kg_install_removal_profit_inter/100.0))+record.kg_install_removal_cost_inter
                
      
    kg_subcon_install_cost_tab4 = fields.Float('Subcontractor Quote - Install')
    kg_subcon_install_cost =fields.Float('Subcontract Install Cost',compute='comp_kg_subcon_install_cost')
    
    @api.multi
    @api.depends( 'kg_subcon_install_cost_tab4' )
    def comp_kg_subcon_install_cost( self ):
        for record in self:
            if record.kg_subcon_install_cost_tab4:    
                record.kg_subcon_install_cost = record.kg_subcon_install_cost_tab4
    
    kg_subcon_install_profit =fields.Float('Subcontract Install Profit')
    
    kg_subcon_install_sp =fields.Float('Subcontract Install Sp',compute='subcon_install_sp')
    
    @api.multi
    @api.depends('kg_subcon_install_cost','kg_subcon_install_profit')
    def subcon_install_sp(self):
        for record in self:
            
                
            record.kg_subcon_install_sp=(record.kg_subcon_install_cost*(record.kg_subcon_install_profit/100.0))+record.kg_subcon_install_cost
                
    kg_subcon_removal_cost_tab4 = fields.Float('Subcontractor Quote - Removal')
    
    kg_subcon_removal_cost =fields.Float('Subcontract Removal Cost',compute='comp_kg_subcon_removal_cost')
    
    @api.multi
    @api.depends( 'kg_subcon_removal_cost_tab4' )
    def comp_kg_subcon_removal_cost( self ):
        for record in self:
            if record.kg_subcon_removal_cost_tab4:    
                record.kg_subcon_removal_cost = record.kg_subcon_removal_cost_tab4
    
    kg_subcon_removal_profit =fields.Float('Subcontract Removal Profit')
    
    kg_subcon_removal_sp =fields.Float('Subcontract Removal Sp',compute='subcon_removal_sp')
    
    @api.multi
    @api.depends('kg_subcon_removal_cost','kg_subcon_removal_profit')
    def subcon_removal_sp(self):
        for record in self:
            record.kg_subcon_removal_sp=(record.kg_subcon_removal_cost*(record.kg_subcon_removal_profit/100.0))+record.kg_subcon_removal_cost
                
    
    
    
    kg_other_charges = fields.One2many('kg.other.charges','kg_est_id','Other Charges')
    
    
    
    
    kg_transport_dom_own_cp = fields.Float('',compute='comp_transport_dom_own_cp')
    
    kg_transport_dom_own_profit = fields.Float('Profit')
    
    kg_transport_dom_own_sp = fields.Float('',compute='comp_transport_dom_own_sp')
    
    
    kg_transport_dom_hire_cp = fields.Float('',compute='comp_transport_dom_hire_cp')
    
    kg_transport_dom_hire_profit = fields.Float('Profit')
    
    kg_transport_dom_hire_sp = fields.Float('',compute='comp_transport_dom_hire_sp')
    
    
    
    @api.multi
    @api.depends('kg_inter_install_lines','kg_domestic_install_lines')
    def comp_transport_dom_own_cp(self):
        for record in self:
            lab_cost=0.0
                
                
            for dom in record.kg_domestic_install_lines:
                lab_cost=lab_cost+(dom.kg_own_veh_cost or 0.0)
                
            record.kg_transport_dom_own_cp=lab_cost
            
            
    @api.multi
    @api.depends('kg_transport_dom_own_cp','kg_transport_dom_own_profit')
    def comp_transport_dom_own_sp(self):
        for record in self:
            
                
            record.kg_transport_dom_own_sp=(record.kg_transport_dom_own_cp*(record.kg_transport_dom_own_profit/100.0))+record.kg_transport_dom_own_cp
                
    
    
    
    @api.multi
    @api.depends('kg_inter_install_lines','kg_domestic_removal_lines')
    def comp_transport_dom_hire_cp(self):
        for record in self:
            lab_cost=0.0
                
                
            for dom in record.kg_domestic_removal_lines:
                lab_cost=lab_cost+(dom.kg_hire_vehicle_cost or 0.0)
                
            record.kg_transport_dom_hire_cp=lab_cost
            
#     @api.multi
#     @api.depends('kg_inter_install_lines','kg_domestic_install_lines')
#     def comp_transport_dom_hire_cp(self):
#         for record in self:
#             lab_cost=0.0
#             
#             for dom in record.kg_inter_install_lines:
#                 lab_cost=lab_cost+(dom.kg_hire_veh_cost or 0.0)
#                 
#                 
#             for dom in record.kg_domestic_install_lines:
#                 lab_cost=lab_cost+(dom.kg_hire_veh_cost or 0.0)
#                 
#             record.kg_transport_dom_hire_cp=lab_cost
            
            
    @api.multi
    @api.depends('kg_transport_dom_hire_cp','kg_transport_dom_hire_profit')
    def comp_transport_dom_hire_sp(self):
        for record in self:
            record.kg_transport_dom_hire_sp=(record.kg_transport_dom_hire_cp*(record.kg_transport_dom_hire_profit/100.0))+record.kg_transport_dom_hire_cp
                
    
    kg_flight_cp = fields.Float('',compute='comp_flight_cp')
    
    kg_flight_profit = fields.Float('Profit')
    
    kg_flight_sp = fields.Float('',compute='comp_flight_sp')
    
    
    
    @api.multi
    @api.depends('kg_inter_inst_flight_lines','kg_inter_rem_flight_lines')
    def comp_flight_cp(self):
        for record in self:
            lab_cost=0.0
            
            for line in record.kg_inter_inst_flight_lines:
                lab_cost=lab_cost+(line.kg_flight_total or 0.0)
                
            for line in record.kg_inter_rem_flight_lines:
                lab_cost=lab_cost+(line.kg_flight_total or 0.0)
                
            
         
                
            record.kg_flight_cp=lab_cost
            
            
    @api.multi    
    @api.depends('kg_flight_cp','kg_flight_profit')
    def comp_flight_sp(self):
        for record in self:
            
                
            record.kg_flight_sp=(record.kg_flight_cp*(record.kg_flight_profit/100.0))+record.kg_flight_cp
            
            
    kg_hotel_cp = fields.Float('',compute='comp_hotel_cp')
    
    kg_hotel_profit = fields.Float('Profit')
    
    kg_hotel_sp = fields.Float('',compute='comp_hotel_sp')
    
    
    
    @api.multi
    @api.depends('kg_inter_inst_accomodation_lines','kg_inter_rem_accomodation_lines')
    def comp_hotel_cp(self):
        for record in self:
            lab_cost=0.0
            
            for dom in record.kg_inter_inst_accomodation_lines:
                lab_cost=lab_cost+(dom.kg_hotel_total or 0.0)
            
            for dom in record.kg_inter_rem_accomodation_lines:
                lab_cost=lab_cost+(dom.kg_hotel_total or 0.0)
            
                
         
                
            record.kg_hotel_cp=lab_cost
            
            
    @api.multi
    @api.depends('kg_hotel_cp','kg_hotel_profit')

    def comp_hotel_sp(self):
        for record in self:
            
                
            record.kg_hotel_sp=(record.kg_hotel_cp*(record.kg_hotel_profit/100.0))+record.kg_hotel_cp
            
            
    kg_visa_cp = fields.Float('',compute='comp_visa_cp')
    
    kg_visa_profit = fields.Float('Profit')
    
    kg_visa_sp = fields.Float('',compute='comp_visa_sp')
    
    
    
    @api.multi
    @api.depends('kg_inter_inst_visa_lines','kg_inter_rem_visa_lines')
    def comp_visa_cp(self):
        for record in self:
            lab_cost=0.0
            
            for dom in record.kg_inter_inst_visa_lines:
                lab_cost=lab_cost+(dom.kg_visa_tot or 0.0)
            
            for dom in record.kg_inter_rem_visa_lines:
                lab_cost=lab_cost+(dom.kg_visa_tot or 0.0)
            
                
         
                
            record.kg_visa_cp=lab_cost
            
            
    @api.multi
    @api.depends('kg_visa_cp','kg_visa_profit')
    def comp_visa_sp(self):
        for record in self:
            
                
            record.kg_visa_sp=(record.kg_visa_cp*(record.kg_visa_profit/100.0))+record.kg_visa_cp
                
                
    
    
    
    @api.multi
    def compute_rev_count(self):
        for record in self:
            
            
            record.kg_rev_count = self.search_count([('kg_org_est_id', '=',record.id)]) 
    @api.one
    def create_tech_est(self,default=None):
           
        print self,'aaaaaaaaaaa'
        
    
        
        default = dict(default or {})
        
        rev_count=self.search_count([('kg_org_est_id', '=',self.id),('kg_rev','=',True)]) 
        
        print default,'agagag'
        
        default.update({
            'kg_tech_est':True,
            'kg_quot_est':False,
            'kg_org_est_id':self.id,
            'name':self.name+"_"+str(rev_count+1),
            'kg_rev':True})
        
        rev_id= super(kg_estimation, self).copy(default)
        
        domestic_lines = self.env['kg.domestic.install.lines'].search([('kg_est_id','=',self.id)])
        for dom in domestic_lines:
            dom_rev=dict({})
            dom_rev.update({
            'kg_est_id':rev_id.id,
          })
            dom.copy(dom_rev)

        domestic_lines_rem = self.env['kg.domestic.removal.lines'].search([('kg_est_id','=',self.id)])
        for dom in domestic_lines_rem:
            dom_rev=dict({})
            dom_rev.update({
            'kg_est_id':rev_id.id,
          })
            dom.copy(dom_rev)
            
            
            
        inter_lines = self.env['kg.inter.install.lines'].search([('kg_est_id','=',self.id)])
        for inter in inter_lines:
            inter_rev=dict({})
            inter_rev.update({
            'kg_est_id':rev_id.id,
          })
            inter.copy(inter_rev)

        inter_inst_flight_lines_ins = self.env['kg.inter.inst.flight.lines'].search([('kg_est_id','=',self.id)])
        for inter in inter_inst_flight_lines_ins:
            inter_rev=dict({})
            inter_rev.update({
            'kg_est_id':rev_id.id,
          })
            inter.copy(inter_rev)

        inter_inst_accomodation_lines_ins = self.env['kg.inter.inst.accomodation.lines'].search([('kg_est_id','=',self.id)])
        for inter in inter_inst_accomodation_lines_ins:
            inter_rev=dict({})
            inter_rev.update({
<<<<<<< HEAD
            'kg_est_id':rev_id.id,
          })
            inter.copy(inter_rev)

        inter_inst_visa_lines_ins = self.env['kg.inter.inst.visa.lines'].search([('kg_est_id','=',self.id)])
        for inter in inter_inst_visa_lines_ins:
            inter_rev=dict({})
            inter_rev.update({
            'kg_est_id':rev_id.id,
          })
            inter.copy(inter_rev)

        inter_inst_freight_lines_ins = self.env['kg.inter.inst.freight.lines'].search([('kg_est_id','=',self.id)])
        for inter in inter_inst_freight_lines_ins:
            inter_rev=dict({})
            inter_rev.update({
            'kg_est_id':rev_id.id,
          })
            inter.copy(inter_rev)


        inter_lines_rem = self.env['kg.inter.removal.lines'].search([('kg_est_id','=',self.id)])
        for inter in inter_lines_rem:
            inter_rev=dict({})
            inter_rev.update({
            'kg_est_id':rev_id.id,
          })
            inter.copy(inter_rev)


        inter_rem_flight_lines_ins = self.env['kg.inter.rem.flight.lines'].search([('kg_est_id','=',self.id)])
        for inter in inter_rem_flight_lines_ins:
            inter_rev=dict({})
            inter_rev.update({
            'kg_est_id':rev_id.id,
          })
            inter.copy(inter_rev)

        inter_rem_accomodation_lines_ins = self.env['kg.inter.rem.accomodation.lines'].search([('kg_est_id','=',self.id)])
        for inter in inter_rem_accomodation_lines_ins:
            inter_rev=dict({})
            inter_rev.update({
            'kg_est_id':rev_id.id,
          })
            inter.copy(inter_rev)

        inter_rem_visa_lines_ins = self.env['kg.inter.rem.visa.lines'].search([('kg_est_id','=',self.id)])
        for inter in inter_rem_visa_lines_ins:
            inter_rev=dict({})
            inter_rev.update({
            'kg_est_id':rev_id.id,
          })
            inter.copy(inter_rev)

        inter_rem_doc_lines_ins = self.env['kg.inter.rem.doc.lines'].search([('kg_est_id','=',self.id)])
        for inter in inter_rem_doc_lines_ins:
            inter_rev=dict({})
            inter_rev.update({
=======
>>>>>>> 53dbf3ae642dcf11b75f3101395219e62b22b5a7
            'kg_est_id':rev_id.id,
          })
            inter.copy(inter_rev)

<<<<<<< HEAD
=======
        inter_inst_visa_lines_ins = self.env['kg.inter.inst.visa.lines'].search([('kg_est_id','=',self.id)])
        for inter in inter_inst_visa_lines_ins:
            inter_rev=dict({})
            inter_rev.update({
            'kg_est_id':rev_id.id,
          })
            inter.copy(inter_rev)

        inter_inst_freight_lines_ins = self.env['kg.inter.inst.freight.lines'].search([('kg_est_id','=',self.id)])
        for inter in inter_inst_freight_lines_ins:
            inter_rev=dict({})
            inter_rev.update({
            'kg_est_id':rev_id.id,
          })
            inter.copy(inter_rev)


        inter_lines_rem = self.env['kg.inter.removal.lines'].search([('kg_est_id','=',self.id)])
        for inter in inter_lines_rem:
            inter_rev=dict({})
            inter_rev.update({
            'kg_est_id':rev_id.id,
          })
            inter.copy(inter_rev)


        inter_rem_flight_lines_ins = self.env['kg.inter.rem.flight.lines'].search([('kg_est_id','=',self.id)])
        for inter in inter_rem_flight_lines_ins:
            inter_rev=dict({})
            inter_rev.update({
            'kg_est_id':rev_id.id,
          })
            inter.copy(inter_rev)

        inter_rem_accomodation_lines_ins = self.env['kg.inter.rem.accomodation.lines'].search([('kg_est_id','=',self.id)])
        for inter in inter_rem_accomodation_lines_ins:
            inter_rev=dict({})
            inter_rev.update({
            'kg_est_id':rev_id.id,
          })
            inter.copy(inter_rev)

        inter_rem_visa_lines_ins = self.env['kg.inter.rem.visa.lines'].search([('kg_est_id','=',self.id)])
        for inter in inter_rem_visa_lines_ins:
            inter_rev=dict({})
            inter_rev.update({
            'kg_est_id':rev_id.id,
          })
            inter.copy(inter_rev)

        inter_rem_doc_lines_ins = self.env['kg.inter.rem.doc.lines'].search([('kg_est_id','=',self.id)])
        for inter in inter_rem_doc_lines_ins:
            inter_rev=dict({})
            inter_rev.update({
            'kg_est_id':rev_id.id,
          })
            inter.copy(inter_rev)

>>>>>>> 53dbf3ae642dcf11b75f3101395219e62b22b5a7

            
        # travel_lines = self.env['kg.travel.lines'].search([('kg_est_id','=',self.id)])
        # for travel in travel_lines:
        #     travel_rev=dict({})
        #     travel_rev.update({
        #     'kg_est_id':rev_id.id,
        #   })
        #     travel.copy(travel_rev)
            
         
        
        units=self.env['kg.unit'].search([('kg_est_id','=',self.id)])
        for unit in units:
            default = dict(default or {})
            default.update({
            'kg_est_id':rev_id.id,
          })
            unit_rev_id=unit.copy(default)
            unit_lines=self.env['kg.unit.lines'].search([('kg_unit_id','=',unit.id)])
            for unit_line in unit_lines:
                lines_copy = dict({})
                lines_copy.update({
                'kg_unit_id':unit_rev_id.id,
              })
                unit_line.copy(lines_copy)
        return True
    
    @api.one
    def create_revision(self,default=None):
           
        print self,'aaaaaaaaaaa'
        
    
        
        default = dict(default or {})
        
        rev_count=self.search_count([('kg_org_est_id', '=',self.id),('kg_rev','=',True)]) 
        
        print default,'agagag'
        
        default.update({
            'kg_tech_est':False,
            'kg_quot_est':False,
            'kg_org_est_id':self.id,
            'name':self.name+"_"+str(rev_count+1),
            'kg_rev':True})
        
        rev_id= super(kg_estimation, self).copy(default)
        
        domestic_lines_ins = self.env['kg.domestic.install.lines'].search([('kg_est_id','=',self.id)])
        for dom in domestic_lines_ins:
            dom_rev=dict({})
            dom_rev.update({
            'kg_est_id':rev_id.id,
          })
            dom.copy(dom_rev)

        domestic_lines_rem = self.env['kg.domestic.removal.lines'].search([('kg_est_id','=',self.id)])
        for dom in domestic_lines_rem:
            dom_rev=dict({})
            dom_rev.update({
            'kg_est_id':rev_id.id,
          })
            dom.copy(dom_rev)
            
            
        inter_lines_ins = self.env['kg.inter.install.lines'].search([('kg_est_id','=',self.id)])
        for inter in inter_lines_ins:
            inter_rev=dict({})
            inter_rev.update({
            'kg_est_id':rev_id.id,
          })
            inter.copy(inter_rev)

        inter_inst_flight_lines_ins = self.env['kg.inter.inst.flight.lines'].search([('kg_est_id','=',self.id)])
        for inter in inter_inst_flight_lines_ins:
            inter_rev=dict({})
            inter_rev.update({
            'kg_est_id':rev_id.id,
          })
            inter.copy(inter_rev)

        inter_inst_accomodation_lines_ins = self.env['kg.inter.inst.accomodation.lines'].search([('kg_est_id','=',self.id)])
        for inter in inter_inst_accomodation_lines_ins:
            inter_rev=dict({})
            inter_rev.update({
            'kg_est_id':rev_id.id,
          })
            inter.copy(inter_rev)

        inter_inst_visa_lines_ins = self.env['kg.inter.inst.visa.lines'].search([('kg_est_id','=',self.id)])
        for inter in inter_inst_visa_lines_ins:
            inter_rev=dict({})
            inter_rev.update({
            'kg_est_id':rev_id.id,
          })
            inter.copy(inter_rev)

        inter_inst_freight_lines_ins = self.env['kg.inter.inst.freight.lines'].search([('kg_est_id','=',self.id)])
        for inter in inter_inst_freight_lines_ins:
            inter_rev=dict({})
            inter_rev.update({
            'kg_est_id':rev_id.id,
          })
            inter.copy(inter_rev)


        inter_lines_rem = self.env['kg.inter.removal.lines'].search([('kg_est_id','=',self.id)])
        for inter in inter_lines_rem:
            inter_rev=dict({})
            inter_rev.update({
            'kg_est_id':rev_id.id,
          })
            inter.copy(inter_rev)


        inter_rem_flight_lines_ins = self.env['kg.inter.rem.flight.lines'].search([('kg_est_id','=',self.id)])
        for inter in inter_rem_flight_lines_ins:
            inter_rev=dict({})
            inter_rev.update({
            'kg_est_id':rev_id.id,
          })
            inter.copy(inter_rev)

        inter_rem_accomodation_lines_ins = self.env['kg.inter.rem.accomodation.lines'].search([('kg_est_id','=',self.id)])
        for inter in inter_rem_accomodation_lines_ins:
            inter_rev=dict({})
            inter_rev.update({
            'kg_est_id':rev_id.id,
          })
            inter.copy(inter_rev)

        inter_rem_visa_lines_ins = self.env['kg.inter.rem.visa.lines'].search([('kg_est_id','=',self.id)])
        for inter in inter_rem_visa_lines_ins:
            inter_rev=dict({})
            inter_rev.update({
            'kg_est_id':rev_id.id,
          })
            inter.copy(inter_rev)

        inter_rem_doc_lines_ins = self.env['kg.inter.rem.doc.lines'].search([('kg_est_id','=',self.id)])
        for inter in inter_rem_doc_lines_ins:
            inter_rev=dict({})
            inter_rev.update({
            'kg_est_id':rev_id.id,
          })
            inter.copy(inter_rev)

            
        # travel_lines = self.env['kg.travel.lines'].search([('kg_est_id','=',self.id)])
        # for travel in travel_lines:
        #     travel_rev=dict({})
        #     travel_rev.update({
        #     'kg_est_id':rev_id.id,
        #   })
        #     travel.copy(travel_rev)
            
         
        
        units=self.env['kg.unit'].search([('kg_est_id','=',self.id)])
        for unit in units:
            default = dict(default or {})
            default.update({
            'kg_est_id':rev_id.id,
          })
            unit_rev_id=unit.copy(default)
            unit_lines=self.env['kg.unit.lines'].search([('kg_unit_id','=',unit.id)])
            for unit_line in unit_lines:
                lines_copy = dict({})
                lines_copy.update({
                'kg_unit_id':unit_rev_id.id,
              })
                unit_line.copy(lines_copy)
        return True
    
    
    @api.multi
    def compute_quot_count(self):
        for record in self:
            sale_obj = self.env['sale.order']
            
            record.kg_quot_count = sale_obj.search_count([('kg_est_id', '=',record.id)]) 
    
    
    
    
    @api.multi
    def create_quot(self):
        for record in self:
            vals={}
            vals['partner_id']=record.kg_client and record.kg_client.id or False
            vals['kg_est_id']=record.id
            vals['kg_opp_id']=record.kg_opny.id
            
            
            if record.kg_tech_est == True:
                vals['kg_tech_est']=record.id
                
            tech_est_id=self.search([('kg_org_est_id','=',record.id),('kg_tech_est','=',True)])
            if len(tech_est_id) >1:
                raise UserError(_('More than one Technical Estimation found!'))
            if tech_est_id:
                vals['kg_tech_est']=tech_est_id.id
                
            
            
            order_id=self.env['sale.order'].create(vals)
            record.write({'kg_quote_ref':order_id.id})
            for line in record.kg_overall_unit_details:
                vals_line={}
                vals_line['product_id']=line.kg_unit_id.id
                vals_line['name']=line.kg_unit_id.name
                vals_line['product_uom_qty']=line.kg_qty
                vals_line['product_uom']=line.kg_unit_id.uom_id.id
                vals_line['price_unit']=line.kg_per_sp
                vals_line['order_id']=order_id.id
                self.env['sale.order.line'].create(vals_line)
    
    
    
    kg_total_overall_cost = fields.Float('Total Overall Cost',compute='comp_total_overall_cost')
    
    @api.multi
    @api.depends('kg_install_labour_cost','kg_install_removal_cost','kg_install_labour_cost_inter','kg_install_removal_cost_inter','kg_subcon_install_cost','kg_subcon_removal_cost','kg_transport_dom_own_cp','kg_transport_dom_hire_cp','kg_flight_cp','kg_hotel_cp','kg_visa_cp')
    def comp_total_overall_cost(self):
        for record in self:
            record.kg_total_overall_cost=record.kg_install_labour_cost+record.kg_install_removal_cost+record.kg_install_labour_cost_inter+record.kg_install_removal_cost_inter+record.kg_subcon_install_cost+record.kg_subcon_removal_cost+record.kg_transport_dom_own_cp+record.kg_transport_dom_hire_cp+record.kg_flight_cp+record.kg_hotel_cp+record.kg_visa_cp

    @api.model
    def create(self,vals):
        if vals.get('kg_rev') == True:
            return super(kg_estimation, self).create(vals)
        else:
            sequence_code='kg.estimation'
            vals['name']= self.env['ir.sequence'].next_by_code(sequence_code)
            return super(kg_estimation, self).create(vals)
        
        
    kg_subtotal_install_cost = fields.Float('Total Installation Cost',compute='comp_kg_subtotal_install_cost')
    
    kg_dummy_subtotal_install_cost = fields.Float('Total Installation Cost',compute='comp_kg_subtotal_install_cost')
    
    kg_subtotal_install_sp = fields.Float('Total Installation Selling Price',compute='comp_kg_subtotal_install_sp')
    
    kg_dummy_subtotal_install_sp = fields.Float('Total Installation Selling Price',compute='comp_kg_subtotal_install_sp')
    
    
    @api.multi
    @api.depends('kg_install_labour_cost','kg_install_removal_cost','kg_install_labour_cost_inter','kg_install_removal_cost_inter','kg_subcon_install_cost','kg_subcon_removal_cost','kg_flight_cp','kg_hotel_cp','kg_visa_cp','kg_other_charges')
    def comp_kg_subtotal_install_cost(self):
        for record in self:
            line_sum=0
            for line in record.kg_other_charges:
                if line.kg_categ=='install':
                    line_sum=line_sum+line.kg_cp
                    
            record.kg_subtotal_install_cost=line_sum+record.kg_install_labour_cost+record.kg_install_removal_cost+record.kg_install_labour_cost_inter+record.kg_install_removal_cost_inter+record.kg_subcon_install_cost+record.kg_subcon_removal_cost+record.kg_flight_cp+record.kg_hotel_cp+record.kg_visa_cp
            record.kg_dummy_subtotal_install_cost=line_sum+record.kg_install_labour_cost+record.kg_install_removal_cost+record.kg_install_labour_cost_inter+record.kg_install_removal_cost_inter+record.kg_subcon_install_cost+record.kg_subcon_removal_cost+record.kg_flight_cp+record.kg_hotel_cp+record.kg_visa_cp

    
    @api.multi
    @api.depends('kg_install_labour_sp','kg_install_removal_sp','kg_install_labour_sp_inter','kg_install_removal_sp_inter','kg_subcon_install_sp','kg_subcon_removal_sp','kg_flight_cp','kg_hotel_cp','kg_visa_cp','kg_other_charges')
    def comp_kg_subtotal_install_sp(self):
        for record in self:
            line_sum=0
            for line in record.kg_other_charges:
                if line.kg_categ=='install':
                    line_sum=line_sum+line.kg_sp
                    
            record.kg_subtotal_install_sp=line_sum+record.kg_install_labour_sp+record.kg_install_removal_sp+record.kg_install_labour_sp_inter+record.kg_install_removal_sp_inter+record.kg_subcon_install_sp+record.kg_subcon_removal_sp+record.kg_flight_sp+record.kg_hotel_sp+record.kg_visa_sp
            record.kg_dummy_subtotal_install_sp=line_sum+record.kg_install_labour_sp+record.kg_install_removal_sp+record.kg_install_labour_sp_inter+record.kg_install_removal_sp_inter+record.kg_subcon_install_sp+record.kg_subcon_removal_sp+record.kg_flight_sp+record.kg_hotel_sp+record.kg_visa_sp
     
    
    
    kg_subtotal_trans_cost = fields.Float('Total Transportation Cost',compute='comp_kg_subtotal_trans_cost')
    
    kg_subtotal_trans_sp = fields.Float('Total Transportatiion Selling Price',compute='comp_kg_subtotal_trans_sp')
    
    @api.multi
    @api.depends('kg_transport_dom_own_cp','kg_transport_dom_hire_cp','kg_other_charges')
    def comp_kg_subtotal_trans_cost(self):
        for record in self:
            line_sum=0
            for line in record.kg_other_charges:
                if line.kg_categ=='trans':
                    line_sum=line_sum+line.kg_cp
                    
            record.kg_subtotal_trans_cost=record.kg_transport_dom_own_cp+record.kg_transport_dom_hire_cp+line_sum
            
    @api.multi
    @api.depends('kg_transport_dom_own_sp','kg_transport_dom_hire_sp','kg_other_charges')
    def comp_kg_subtotal_trans_sp(self):
        for record in self:
            line_sum=0
            for line in record.kg_other_charges:
                if line.kg_categ=='trans':
                    line_sum=line_sum+line.kg_sp
                    
            record.kg_subtotal_trans_sp=record.kg_transport_dom_own_sp+record.kg_transport_dom_hire_sp+line_sum
            
    kg_subtotal_other_cost = fields.Float('Total Other Cost',compute='comp_other')
    
    kg_subtotal_other_sp = fields.Float('Total Other Selling Price',compute='comp_other')
    
    @api.multi
    @api.depends('kg_other_charges')
    def comp_other(self):
        for record in self:
            line_cp=0
            line_sp =0
            for line in record.kg_other_charges:
                if line.kg_categ=='others':
                    line_cp=line_cp+line.kg_cp
                    line_sp = line_sp+line.kg_sp
                    
            record.kg_subtotal_other_cost=line_cp
            record.kg_subtotal_other_sp = line_sp
            
    kg_subtotal_lab_cp = fields.Float('Subtotal Labour Cost',compute='comp_kg_agg')
    
    kg_subtotal_lab_sp = fields.Float('Subtotal Labour Sp',compute='comp_kg_agg')
    
    
    kg_subtotal_sub_cp = fields.Float('Subtotal Contractor Cost',compute='comp_kg_agg')
     
    kg_subtotal_sub_sp = fields.Float('Subtotal Contractor Sp',compute='comp_kg_agg')
#     
#     
    kg_subtotal_spe_cp = fields.Float('Subtotal Special Material Cost',compute='comp_kg_agg')
     
    kg_subtotal_spe_sp = fields.Float('Subtotal Special Material Sp',compute='comp_kg_agg')
     
#      
    kg_subtotal_mat_cp = fields.Float('Subtotal Material Cost',compute='comp_kg_agg')
      
    kg_subtotal_mat_sp = fields.Float('Subtotal Material Sp',compute='comp_kg_agg')
    
    @api.multi
    @api.depends('kg_fin_lines')
    def comp_kg_agg(self):
        for record in self:
            spe_sum=0
            spe_sp=0
            sub_sum=0
            sub_sp=0
            lab_sum=0
            lab_sp=0
            mat_sum=0
            mat_sp=0
            for line in record.kg_fin_lines:
                if line.kg_cat_id.name=='Special Materials':
                    spe_sum=spe_sum+line.kg_cp
                    spe_sp=spe_sp+line.kg_sp
                elif line.kg_cat_id.name=='Subcontractor Materials':
                    sub_sum=sub_sum+line.kg_cp
                    sub_sp=sub_sp+line.kg_sp
                elif line.kg_cat_id.name=='Manpower':
                    lab_sum=lab_sum+line.kg_cp
                    lab_sp=lab_sp+line.kg_sp
                else:
                    mat_sum=mat_sum+line.kg_cp
                    mat_sp=mat_sp+line.kg_sp
            record.kg_subtotal_lab_cp=lab_sum
            record.kg_subtotal_lab_sp=lab_sp
             
            record.kg_subtotal_sub_cp=sub_sum
            record.kg_subtotal_sub_sp=sub_sp
             
             
            record.kg_subtotal_spe_cp=spe_sum
            record.kg_subtotal_spe_sp=spe_sp
             
             
            record.kg_subtotal_mat_cp=mat_sum
            record.kg_subtotal_mat_sp=mat_sp
            
            
    kg_subtotal_cp = fields.Float('Overall Total',compute='comp_overall_cp_total')
    
    @api.multi
    @api.depends('kg_subtotal_lab_cp','kg_subtotal_sub_cp','kg_subtotal_spe_cp','kg_subtotal_mat_cp','kg_subtotal_other_cost','kg_subtotal_trans_cost','kg_subtotal_install_cost')
    def comp_overall_cp_total(self):
        for record in self:
            record.kg_subtotal_cp=record.kg_subtotal_lab_cp+record.kg_subtotal_sub_cp+record.kg_subtotal_spe_cp+record.kg_subtotal_mat_cp+record.kg_subtotal_other_cost+record.kg_subtotal_trans_cost+record.kg_subtotal_install_cost
            if record.kg_subtotal_cp > 0.0:
                record.kg_mat_sharing=round((record.kg_subtotal_mat_cp/record.kg_subtotal_cp)*100,0)
                
                record.kg_spe_sharing=round((record.kg_subtotal_spe_cp/record.kg_subtotal_cp)*100,0)
                
                record.kg_sub_sharing=round((record.kg_subtotal_sub_cp/record.kg_subtotal_cp)*100,0)
                
                record.kg_lab_sharing=round((record.kg_subtotal_lab_cp/record.kg_subtotal_cp)*100,0)
                
                record.kg_other_sharing=round((record.kg_subtotal_other_cost/record.kg_subtotal_cp)*100,0)
                
                record.kg_trans_sharing=round((record.kg_subtotal_trans_cost/record.kg_subtotal_cp)*100,0)
                
                record.kg_install_sharing=round((record.kg_subtotal_install_cost/record.kg_subtotal_cp)*100,0)
            
            
    kg_subtotal_sp = fields.Float('Overall Total',compute='comp_overall_sp_total')
    
    @api.multi
    @api.depends('kg_subtotal_lab_sp','kg_subtotal_sub_sp','kg_subtotal_spe_sp','kg_subtotal_mat_sp','kg_subtotal_other_sp','kg_subtotal_trans_sp','kg_subtotal_install_sp')
    def comp_overall_sp_total(self):
        for record in self:
            record.kg_subtotal_sp=record.kg_subtotal_lab_sp+record.kg_subtotal_sub_sp+record.kg_subtotal_spe_sp+record.kg_subtotal_mat_sp+record.kg_subtotal_other_sp+record.kg_subtotal_trans_sp+record.kg_subtotal_install_sp
#                                          
            
                     
                
                
    kg_mat_sharing = fields.Integer('Material Sharing',compute='comp_overall_cp_total')
    
    kg_spe_sharing = fields.Integer('special Sharing',compute='comp_overall_cp_total')
    
    kg_sub_sharing = fields.Integer('Sub Sharing',compute='comp_overall_cp_total')
    
    kg_lab_sharing = fields.Integer('Lab Sharing',compute='comp_overall_cp_total')
    
    kg_install_sharing = fields.Integer('Material Sharing',compute='comp_overall_cp_total')
    
    kg_trans_sharing = fields.Integer('Material Sharing',compute='comp_overall_cp_total')
    
    kg_other_sharing = fields.Integer('Material Sharing',compute='comp_overall_cp_total')
    
    
    
    kg_mat_profit = fields.Float('Mat Profit',compute='comp_profit_mat')
    kg_mat_profit_per = fields.Float('Mat Profit per',compute='comp_profit_mat')
    
    @api.multi
    @api.depends('kg_subtotal_mat_cp','kg_subtotal_mat_sp')
    def comp_profit_mat(self):
        for record in self:
            record.kg_mat_profit=record.kg_subtotal_mat_sp-record.kg_subtotal_mat_cp
            if record.kg_subtotal_mat_sp > 0:
                record.kg_mat_profit_per =round(( record.kg_mat_profit/record.kg_subtotal_mat_sp)*100,0)
                
                
    
    kg_spe_profit = fields.Float('Spe Profit',compute='comp_profit_spe')
    kg_spe_profit_per = fields.Float('Spe Profit per',compute='comp_profit_spe')
    
    @api.multi
    @api.depends('kg_subtotal_spe_cp','kg_subtotal_spe_sp')
    def comp_profit_spe(self):
        for record in self:
            record.kg_spe_profit=record.kg_subtotal_spe_sp-record.kg_subtotal_spe_cp
            if record.kg_subtotal_spe_sp > 0:
                record.kg_spe_profit_per =round(( record.kg_spe_profit/record.kg_subtotal_spe_sp)*100,0)
                
                
    kg_sub_profit = fields.Float('sub Profit',compute='comp_profit_sub')
    kg_sub_profit_per = fields.Float('sub Profit per',compute='comp_profit_sub')
    
    @api.multi
    @api.depends('kg_subtotal_sub_cp','kg_subtotal_sub_sp')
    def comp_profit_sub(self):
        for record in self:
            record.kg_sub_profit=record.kg_subtotal_sub_sp-record.kg_subtotal_sub_cp
            if record.kg_subtotal_sub_sp > 0:
                record.kg_sub_profit_per =round(( record.kg_sub_profit/record.kg_subtotal_sub_sp)*100,0)
                
    
    
    kg_lab_profit = fields.Float('lab Profit',compute='comp_profit_lab')
    kg_lab_profit_per = fields.Float('lab Profit per',compute='comp_profit_lab')
    
    @api.multi
    @api.depends('kg_subtotal_lab_cp','kg_subtotal_lab_sp')
    def comp_profit_lab(self):
        for record in self:
            record.kg_lab_profit=record.kg_subtotal_lab_sp-record.kg_subtotal_lab_cp
            if record.kg_subtotal_lab_sp > 0:
                record.kg_lab_profit_per =round(( record.kg_lab_profit/record.kg_subtotal_lab_sp)*100,0)
                
    
    kg_other_profit = fields.Float('lab Profit',compute='comp_profit_other')
    kg_other_profit_per = fields.Float('lab Profit per',compute='comp_profit_other')
    
    @api.multi
    @api.depends('kg_subtotal_other_cost','kg_subtotal_other_sp')
    def comp_profit_other(self):
        for record in self:
            record.kg_other_profit=record.kg_subtotal_other_sp-record.kg_subtotal_other_cost
            if record.kg_subtotal_other_sp > 0:
                record.kg_other_profit_per =round(( record.kg_other_profit/record.kg_subtotal_other_sp)*100,0)
                
                
                
    kg_trans_profit = fields.Float('lab Profit',compute='comp_profit_trans')
    kg_trans_profit_per = fields.Float('lab Profit per',compute='comp_profit_trans')
    
    @api.multi
    @api.depends('kg_subtotal_trans_cost','kg_subtotal_trans_sp')
    def comp_profit_trans(self):
        for record in self:
            record.kg_trans_profit=record.kg_subtotal_trans_sp-record.kg_subtotal_trans_cost
            if record.kg_subtotal_trans_sp > 0:
                record.kg_trans_profit_per =round(( record.kg_trans_profit/record.kg_subtotal_trans_sp)*100,0)
                
    kg_install_profit = fields.Float('lab Profit',compute='comp_profit_install')
    kg_install_profit_per = fields.Float('lab Profit per',compute='comp_profit_install')
    
    @api.multi
    @api.depends('kg_subtotal_install_cost','kg_subtotal_install_sp')
    def comp_profit_install(self):
        for record in self:
            record.kg_install_profit=record.kg_subtotal_install_sp-record.kg_subtotal_install_cost
            if record.kg_subtotal_install_sp > 0:
                record.kg_install_profit_per =round(( record.kg_install_profit/record.kg_subtotal_install_sp)*100,0)
                
                
    kg_subtotal_profit = fields.Float('Subotl Profit',compute='comp_profit_subtotal')
    kg_subtotal_profit_per = fields.Float('Subotl Profit per',compute='comp_profit_subtotal')
    
    @api.multi
    @api.depends('kg_subtotal_cp','kg_subtotal_sp')
    def comp_profit_subtotal(self):
        for record in self:
            record.kg_subtotal_profit=record.kg_subtotal_sp-record.kg_subtotal_cp
            if record.kg_subtotal_sp > 0:
                record.kg_subtotal_profit_per =round(( record.kg_subtotal_profit/record.kg_subtotal_sp)*100,0)
    
                      
                

    # Markup
    mat_mark = fields.Float('Materials Markup')
    
    spe_mark  = fields.Float('Special Materials Markup')
    
    sub_mark = fields.Float('Subcontract Materials Markup')
    
    man_mark = fields.Float('Manpower Markup')

    install_mark = fields.Float('Install Markup')

    transport_mark = fields.Float('Transport Markup')

    design_mark = fields.Float('Design Markup')

    other_mark = fields.Float('Other Markup')
    

    @api.multi
    def save_markup(self):
        for record in self:
            #Install
            record.kg_install_labour_profit = record.install_mark
            record.kg_install_removal_profit = record.install_mark
            record.kg_install_labour_profit_inter = record.install_mark
            record.kg_install_removal_profit_inter = record.install_mark
            record.kg_subcon_install_profit = record.install_mark
            record.kg_subcon_removal_profit = record.install_mark
            record.kg_flight_profit = record.install_mark
            record.kg_visa_profit = record.install_mark
            record.kg_hotel_profit = record.install_mark
            #Transportation
            record.kg_transport_dom_own_profit = record.transport_mark
            record.kg_transport_dom_hire_profit = record.transport_mark

            for line in record.kg_other_charges:
                if (line.name).lower() in ['drafting works fee']: 
                    line.write({'kg_markup':record.design_mark})
                else:
                    line.write({'kg_markup':record.other_mark})

            for line in record.kg_fin_lines:
                if line.kg_cat_id.name == 'Manpower':
                    line.write({'kg_mp':record.man_mark})
                elif line.kg_cat_id.name == 'Subcontractor Materials':
                    line.write({'kg_mp':record.sub_mark})
                elif line.kg_cat_id.name == 'Special Materials':
                    line.write({'kg_mp':record.spe_mark})
                else:
                    line.write({'kg_mp':record.mat_mark})

            
            


class kg_other_charges(models.Model):
    _name="kg.other.charges" 
    
    name = fields.Char('Name')
    
    kg_categ = fields.Selection([('install','Installation'),('trans','Transportation'),('others','Others')],'Type')
    
    kg_cp = fields.Float('Cost Price') 
    
    kg_markup = fields.Float('Profit(%)')
    
    kg_sp = fields.Float('Selling Price',compute='comp_sp')
    
    kg_est_id = fields.Many2one('kg.estimation','Estimation')
    
    @api.multi
    @api.depends('kg_cp','kg_markup')
    def comp_sp(self):
        for record in self:
            
                
            record.kg_sp=(record.kg_cp*(record.kg_markup/100.0))+record.kg_cp
            
    
    
    
                
    
    
    
class kg_unit(models.Model):
    
    _name= 'kg.unit'
    
    
    
    
    
            
    
    name = fields.Char('Name')
    
    kg_unit_id  =fields.Many2one('product.product','Unit')
    
    kg_uom = fields.Many2one('product.uom','Unit')
    
    kg_qty  = fields.Float('Quantity')
    
    kg_width = fields.Float('Width')
    
    kg_height = fields.Float('Height')
    
    kg_depth = fields.Float('Depth')
    
    
    kg_total_area = fields.Float('Total Area',compute='_compute_total_area')
    
    kg_volume = fields.Float('Volume',compute='_compute_volume')
    
    
    
    kg_est_id = fields.Many2one('kg.estimation','Estimation')
    
    kg_material_tot_val =  fields.Float('Materials Cost',compute="calc_element_cost")
    
    
    kg_mat_markup = fields.Float('Markup',compute="mat_mp", copy=True)
    
    kg_change_mat = fields.Float('Change Material Markup')
    
    
    @api.depends('kg_est_id','kg_change_mat')
    def mat_mp(self):
        for record in self:
            print 'haiiiiiiiiiiii'
            if record.kg_change_mat > 0:
                record.kg_mat_markup = record.kg_change_mat
            else:
                for line in record.kg_est_id.kg_fin_lines:
                    print 'hkloooooo'
                    if line.kg_cat_id.name not in ['Manpower','Subcontractor Materials','Special Materials']:
                        print 'nowqqqqq'
                        record.kg_mat_markup = line.kg_mp
             
    
    
    
    
    kg_mat_sp = fields.Float('Selling Price',compute="mat_sp")
    
    kg_manpower_tot_val = fields.Float('ManPower Cost',compute="calc_element_cost")
    
    
    kg_man_markup = fields.Float('Markup',compute="man_mp",copy=True)
    
    kg_change_man = fields.Float('Change Material Markup')
    kg_change_outso = fields.Float('Change Outso Markup')
    kg_change_spec = fields.Float('Change Special Markup')
    
    @api.depends('kg_est_id','kg_change_man')
    def man_mp(self):
        for record in self:
            print 'haiiiiiiiiiiii'
            if record.kg_change_man > 0:
                record.kg_man_markup = record.kg_change_man
         
            else:
                for line in record.kg_est_id.kg_fin_lines:
                    print 'hkloooooo'
                    if line.kg_cat_id.name in ['Manpower']:
                        print 'nowqqqqq'
                        record.kg_man_markup = line.kg_mp
              
                        
    @api.depends('kg_est_id','kg_change_outso')
    def outso_mp(self):
        for record in self:
            print 'haiiiiiiiiiiii'
          
            if record.kg_change_outso > 0:
                record.kg_outso_markup = record.kg_change_outso
          
            else:
                for line in record.kg_est_id.kg_fin_lines:
                    print 'hkloooooo'
                  
                    if line.kg_cat_id.name in ['Subcontractor Materials']:
                        print 'nowqqqqq'
                        record.kg_outso_markup = line.kg_mp
                
                        
    @api.depends('kg_est_id','kg_change_spec')
    def spec_mp(self):
        for record in self:
            print 'haiiiiiiiiiiii'
         
            if record.kg_change_spec > 0:
                record.kg_spec_markup = record.kg_change_spec
            else:
                for line in record.kg_est_id.kg_fin_lines:
                    print 'hkloooooo'
                 
                    if line.kg_cat_id.name in ['Special Materials']:
                        print 'nowqqqqq'
                        record.kg_spec_markup = line.kg_mp
    
    kg_man_sp = fields.Float('Selling Price',compute="man_sp")
    
    kg_outso_tot_val = fields.Float('Outsourced Cost',compute="calc_element_cost")
    
    
    kg_outso_markup = fields.Float('Markup',compute="outso_mp",copy=True)
    
    kg_outso_sp = fields.Float('Selling Price',compute="outso_sp")
    
    kg_spec_tot_val = fields.Float('Special Material Cost',compute="calc_element_cost")
    
    kg_spec_markup = fields.Float('Markup',compute="spec_mp",copy=True)
    
    kg_spec_sp = fields.Float('Selling Price',compute="spec_sp")
    
    kg_tot_val = fields.Float('Total Quantity Value',compute="calc_element_cost")
    
    kg_tot_sp = fields.Float('Selling Price',compute="calc_tot_sp")
    
    
    kg_per_tot_val = fields.Float('Per Quantity Value',compute="calc_element_cost")
    
    kg_per_sp = fields.Float('Selling Price',compute="calc_tot_sp")
    
    
    kg_tot_markup = fields.Float('Total Markup (%)',compute="calc_tot_markup")

   






    
    
    
    kg_1_lines  = fields.One2many('kg.unit.lines','kg_unit_id'  ,'Acrylic List',domain=[('kg_cat.id', '=', 1)])
    
    
    kg_2_lines  = fields.One2many('kg.unit.lines','kg_unit_id' ,domain=[('kg_cat.id', '=', 2)])
    
    kg_3_lines  = fields.One2many('kg.unit.lines','kg_unit_id' ,domain=[('kg_cat.id', '=', 3)])
    
    kg_4_lines  = fields.One2many('kg.unit.lines','kg_unit_id' ,domain=[('kg_cat.id', '=', 4)])
    
    kg_5_lines  = fields.One2many('kg.unit.lines','kg_unit_id' ,domain=[('kg_cat.id', '=', 5)])
    
    kg_6_lines  = fields.One2many('kg.unit.lines','kg_unit_id' ,domain=[('kg_cat.id', '=', 6)])
    
    kg_7_lines  = fields.One2many('kg.unit.lines','kg_unit_id' ,domain=[('kg_cat.id', '=', 7)])
    
    
    kg_8_lines  = fields.One2many('kg.unit.lines','kg_unit_id' ,domain=[('kg_cat.id', '=', 8)])
    
    
    kg_9_lines  = fields.One2many('kg.unit.lines','kg_unit_id' ,domain=[('kg_cat.id', '=', 9)])
    
    
    kg_10_lines  = fields.One2many('kg.unit.lines','kg_unit_id' ,domain=[('kg_cat.id', '=', 10)])
    
    
    kg_11_lines  = fields.One2many('kg.unit.lines','kg_unit_id' ,domain=[('kg_cat.id', '=', 11)],default=lambda self: self._default_local_lines())
    
    kg_12_lines  = fields.One2many('kg.unit.lines','kg_unit_id' ,domain=[('kg_cat.id', '=', 12)],default=lambda self: self._default_export_lines())
    
    kg_13_lines  = fields.One2many('kg.unit.lines','kg_unit_id' ,domain=[('kg_cat.id', '=', 13)])
    
    kg_14_lines  = fields.One2many('kg.unit.lines','kg_unit_id' ,domain=[('kg_cat.id', '=', 14)])
    
<<<<<<< HEAD
    kg_15_lines  = fields.One2many('kg.unit.lines','kg_unit_id' ,domain=[('kg_cat.id', '=', 15)],default=lambda self: self._default_manpower_lines())
    
    @api.model
    def _default_manpower_lines(self):
        cat_ids =self.env['product.product'].search([('name','in',['CNC Works (Acrylic / MDF)','Acrylic Assembly','MDF preparation (Cutting)','MDF Assembly (Joinery)','Painting/ Finishing','MDF Final Assembly','Glass','Electrical Works','Metal Works','Screen Printing','Sticker Pasting','Packaging','Crating','Additional Labor hours'])])
        b=[]
        for i in cat_ids:
            a={}
            a['kg_mat']=i
          
            c=(0,0,a)
            b.append(c)
        return b
    
    @api.model
    def _default_export_lines(self):
        cat_ids =self.env['product.product'].search([('name','in',['Thermocol Sheet','12Mm Packing Ply 8X4'])])
        b=[]
        for i in cat_ids:
            a={}
            a['kg_mat']=i
          
            c=(0,0,a)
            b.append(c)
        return b
=======
    kg_15_lines  = fields.One2many('kg.unit.lines','kg_unit_id' ,domain=[('kg_cat.id', '=', 15)])

    kg_select= fields.Boolean('Select', copy=False)


    @api.multi
    def select(self):
        for obj in self:
            obj.kg_select = True


    @api.multi
    def unselect(self):
        for obj in self:
            obj.kg_select = False

>>>>>>> 53dbf3ae642dcf11b75f3101395219e62b22b5a7
    
    @api.model
    def _default_local_lines(self):
        cat_ids =self.env['product.product'].search([('name','in',['Packing Roll','Clear Tape','Corner Pad L Type 2Mtr','Corrugated Roll 2Ply','Brown Packing Paper','Carton Box 800x600x600','Air Bubble Roll _x000D_','Clips'])])
        b=[]
        for i in cat_ids:
            a={}
            a['kg_mat']=i
          
            c=(0,0,a)
            b.append(c)
        return b


    

    kg_select= fields.Boolean('Select', copy=False)


    @api.multi
    def select(self):
        for obj in self:
            obj.kg_select = True


    @api.multi
    def unselect(self):
        for obj in self:
            obj.kg_select = False

    
    @api.depends('kg_qty')
    def calc_element_cost(self):
        for record in self:
            if isinstance(record.id,int) is True:
            
                self.env.cr.execute("""
                        SELECT SUM(kg_qty*kg_rate)
                        FROM kg_unit_lines where  kg_unit_id =%s and kg_cat=%s""",
                    (record.id, 13,))
                spe_sum = self.env.cr.fetchone()[0] or 0.0
                
                self.env.cr.execute("""
                        SELECT SUM(kg_qty*kg_rate)
                        FROM kg_unit_lines where  kg_unit_id =%s and kg_cat=%s""",
                    (record.id, 14,))
                out_sum = self.env.cr.fetchone()[0] or 0.0
                
                
                self.env.cr.execute("""
                        SELECT SUM(kg_qty*kg_rate)
                        FROM kg_unit_lines where  kg_unit_id =%s and kg_cat=%s""",
                    (record.id, 15,))
                man_sum = self.env.cr.fetchone()[0] or 0.0
                
                
                self.env.cr.execute("""
                        SELECT SUM(kg_qty*kg_rate)
                        FROM kg_unit_lines where  kg_unit_id =%s """,
                    (record.id,))
                tot_sum = self.env.cr.fetchone()[0] or 0.0
                
                record.kg_manpower_tot_val=man_sum
              #  print record.kg_mat_markup,'ashish'
         #       record.kg_1=man_sum*(record.kg_mat_markup/100.0)
                record.kg_outso_tot_val=out_sum
                record.kg_spec_tot_val=spe_sum
                record.kg_material_tot_val=tot_sum-man_sum-out_sum-spe_sum
                
                record.kg_tot_val=tot_sum
                
                
                if record.kg_qty==0.0:
                    record.kg_per_tot_val=0.0
                else:
                    record.kg_per_tot_val=tot_sum/record.kg_qty
                
    
    @api.depends('kg_tot_sp','kg_tot_val')
    def calc_tot_markup(self):
        for record in self:
            if record.kg_tot_val>0:
                record.kg_tot_markup=((record.kg_tot_sp-record.kg_tot_val)/record.kg_tot_val)*100         
                
    @api.depends('kg_material_tot_val','kg_mat_markup')
    def mat_sp(self):
        for record in self:
             
             record.kg_mat_sp=(record.kg_material_tot_val*(record.kg_mat_markup/100.0))+record.kg_material_tot_val
             
    @api.depends('kg_manpower_tot_val','kg_man_markup')
    def man_sp(self):
        for record in self:
             record.kg_man_sp=(record.kg_manpower_tot_val*(record.kg_man_markup/100.0))+record.kg_manpower_tot_val
             
    @api.depends('kg_outso_tot_val','kg_outso_markup')
    def outso_sp(self):
        for record in self:
             record.kg_outso_sp=(record.kg_outso_tot_val*(record.kg_outso_markup/100.0))+record.kg_outso_tot_val
             
    @api.depends('kg_spec_tot_val','kg_spec_markup')
    def spec_sp(self):
        for record in self:
             record.kg_spec_sp=(record.kg_spec_tot_val*(record.kg_spec_markup/100.0))+record.kg_spec_tot_val
             
    @api.depends('kg_mat_sp','kg_man_sp','kg_outso_sp','kg_spec_sp','kg_qty')
    def calc_tot_sp(self):
        for record in self:
             record.kg_tot_sp=record.kg_mat_sp+record.kg_man_sp+record.kg_outso_sp+record.kg_spec_sp
             
             if record.kg_qty==0.0:
                 record.kg_per_sp=0.0
             else:
                 record.kg_per_sp=(record.kg_mat_sp+record.kg_man_sp+record.kg_outso_sp+record.kg_spec_sp)/record.kg_qty
                 
     
     
     
    
    @api.depends('kg_width','kg_qty','kg_height')
    def _compute_total_area(self):
        for record in self:
            if record.kg_width and record.kg_qty and record.kg_height:
                record.kg_total_area=(record.kg_width/1000)*(record.kg_height/1000)*record.kg_qty
            
    @api.depends('kg_width','kg_depth','kg_height')
    def _compute_volume(self):
        for record in self:
            if record.kg_width and record.kg_depth and record.kg_height:
                record.kg_volume=(record.kg_width/1000)*(record.kg_height/1000)*(record.kg_depth/1000)
            
            
        
    
class kg_unit_lines(models.Model):
    
    _name = "kg.unit.lines"
    
    name = fields.Char('Material Code')
    
    kg_mat = fields.Many2one('product.product','Material', required="1")
    
    kg_cat = fields.Many2one('kg.category','Category')
    
    kg_qty = fields.Float('Qty')
    
    kg_unit = fields.Many2one('product.uom','Unit')
    
    kg_rate = fields.Float('Rate')
    
    kg_amt = fields.Float('Amt',compute='compute_amt')
    
    kg_normal_hours = fields.Float('Normal Hours',compute='compute_hours')
    
    kg_ot_hours = fields.Float('Over Time Hours',compute='compute_hours')
    
  #  kg_tot_hours = fields.Float('Total Hours',compute='compute_hours')
    
    
   
    
    
    kg_total_qty_in_all_units  = fields.Float('Material wise Total  Quantity in All Units',compute="calc_total_mat_wise")
    
    
    @api.multi
    @api.depends('kg_total_qty_in_all_units')
    def compute_hours(self):
        for record in self:
            record.kg_normal_hours=record.kg_total_qty_in_all_units*0.6
            record.kg_ot_hours=record.kg_total_qty_in_all_units*0.4
           # record.kg_tot_hours=record.kg_normal_hours+record.kg_ot_hours
    
    kg_total_in_all_units  = fields.Float('Material wise total  value in All Units',compute="calc_total_mat_wise")
    
    kg_unit_id = fields.Many2one('kg.unit','Unit')
    
    @api.depends('kg_mat')
    def calc_total_mat_wise(self):
        for record in self:
            if record.kg_mat :
                if isinstance(record.kg_unit_id.id,int) is True and isinstance(record.kg_unit_id.kg_est_id.id,int) is True:
                    
                    print record.kg_unit_id.id,'kg_unit_idkg_unit_idkg_unit_idkg_unit_id'
                    
                    print record.kg_unit_id.kg_est_id,'kg_est_idkg_est_idkg_est_idkg_est_idkg_est_id'
                
                    self.env.cr.execute("""
                            SELECT SUM(kg_unit_lines.kg_qty*kg_unit_lines.kg_rate)
                            FROM kg_unit_lines INNER JOIN kg_unit ON  kg_unit_lines.kg_unit_id = kg_unit.id 
                            WHERE kg_unit_lines.kg_mat=%s 
                               AND kg_unit.kg_est_id = %s""",
                        (record.kg_mat.id, record.kg_unit_id.kg_est_id.id,))
                    result = self.env.cr.fetchone()[0] or 0.0
                    
                    
                    self.env.cr.execute("""
                            SELECT SUM(kg_unit_lines.kg_qty)
                            FROM kg_unit_lines INNER JOIN kg_unit ON  kg_unit_lines.kg_unit_id = kg_unit.id 
                            WHERE kg_unit_lines.kg_mat=%s 
                               AND kg_unit.kg_est_id =%s""",
                        (record.kg_mat.id, record.kg_unit_id.kg_est_id.id,))
                    result1 = self.env.cr.fetchone()[0] or 0.0
                    
                    record.kg_total_qty_in_all_units=result1
                    
                    record.kg_total_in_all_units=result
                
            
            
            
    
    @api.multi
    def create(self,vals):
        res=super(kg_unit_lines, self).create(vals)   
        
        
            
            
        for fin in res.kg_unit_id.kg_est_id.kg_fin_lines:
            
            self.env.cr.execute("""
                            SELECT SUM(kg_qty*kg_rate)
                            FROM kg_unit_lines where  kg_unit_id in (select id from kg_unit where kg_est_id= %s ) and kg_cat=%s""",
                        (res.kg_unit_id.kg_est_id.id, fin.kg_cat_id.id,))
            cp= self.env.cr.fetchone()[0] or 0.0
            
            fin.write({'kg_cp':cp})
        return res
        
        
    
    
    @api.multi
    def write(self,vals):
        for record in self:
            for fin in record.kg_unit_id.kg_est_id.kg_fin_lines:
                
                self.env.cr.execute("""
                                SELECT SUM(kg_qty*kg_rate)
                                FROM kg_unit_lines where  kg_unit_id in (select id from kg_unit where kg_est_id= %s ) and kg_cat=%s""",
                            (record.kg_unit_id.kg_est_id.id, fin.kg_cat_id.id,))
                cp= self.env.cr.fetchone()[0] or 0.0
                
                fin.write({'kg_cp':cp})
                
                
        return super(kg_unit_lines, self).write(vals)            
    
    
    @api.depends('kg_rate','kg_qty')
    def compute_amt(self):
        for record in self:
            if record.kg_qty and record.kg_rate:
                record.kg_amt=record.kg_qty*record.kg_rate
    
    
    
class kg_category(models.Model):
    
    _name= "kg.category"
    
    name  = fields.Char('Name')


class kg_domestic_install_lines(models.Model):

    _name="kg.domestic.install.lines"

    name = fields.Char('Name')

    kg_emp_categ = fields.Many2one('hr.job','Category')

    kg_install_nos = fields.Integer('Install Nos')

    kg_install_rate = fields.Float('Install Rate')

    kg_wkg_hrs_day = fields.Float('Wkg Hours/Day')

    kg_no_days = fields.Integer('No of Days')

    kg_install_lab_cost = fields.Float('Install Labour Cost',compute="_compute_kg_install_lab")

    kg_install_lab_hrs = fields.Float('Install Labour Hours',compute="_compute_kg_install_lab")

    kg_own_veh_rate  = fields.Float('Own Vehicle Rate')

    kg_own_veh_km = fields.Float('Own Vehicle KM')
    
    kg_own_veh_days = fields.Float('Own Vehicle Days')

    kg_own_veh_cost =fields.Float('Own Vehicle Cost',compute="_compute_own_veh")

    kg_tot_dom_tpt_cost  = fields.Float('Total Domestic TPT Cost',compute="_compute_tot_cost")

    kg_est_id = fields.Many2one('kg.estimation','Estimation')

    @api.depends('kg_install_nos','kg_install_rate','kg_wkg_hrs_day','kg_no_days')
    def _compute_kg_install_lab(self):
        for record in self:
            record.kg_install_lab_cost = record.kg_install_nos *  record.kg_install_rate * record.kg_wkg_hrs_day * record.kg_no_days
            record.kg_install_lab_hrs = record.kg_install_nos * record.kg_wkg_hrs_day * record.kg_no_days


    @api.depends('kg_own_veh_rate', 'kg_own_veh_km', 'kg_own_veh_days')
    def _compute_own_veh(self):
        for record in self:
            record.kg_own_veh_cost = record.kg_own_veh_rate * record.kg_own_veh_km * record.kg_own_veh_days

    @api.depends('kg_own_veh_cost', 'kg_install_lab_cost')
    def _compute_tot_cost(self):
        for record in self:
            record.kg_tot_dom_tpt_cost = record.kg_own_veh_cost + record.kg_install_lab_cost


class kg_domestic_removal_lines(models.Model):

    _name="kg.domestic.removal.lines"

    name = fields.Char('Name')

    kg_emp_categ = fields.Many2one('hr.job','Category')

    kg_rem_nos = fields.Integer('Removal Nos')

    kg_rem_rate = fields.Float('Removal Rate')

    kg_wkg_hrs_day = fields.Float('Wkg Hours/Day')

    kg_no_days = fields.Integer('No of Days')

    kg_removal_lab_cost = fields.Float('Removal Labour Cost',compute="_compute_kg_removal_lab")

    kg_removal_lab_hrs = fields.Float('Remove Labour Hours',compute="_compute_kg_removal_lab")

    kg_hire_veh_rate = fields.Float('Hire Vehicle Rate')

    kg_hire_veh_km = fields.Float('Hire Vehicle KM')

    kg_hire_veh_days = fields.Float('Hire Vehicle Days')


    kg_hire_vehicle_cost = fields.Float('Hire Vehicle Cost',compute="_compute_hire_veh")

    kg_tot_dom_tpt_cost  = fields.Float('Total Domestic TPT Cost',compute="_compute_tot_cost")

    kg_est_id = fields.Many2one('kg.estimation','Estimation')


    @api.depends('kg_rem_nos', 'kg_rem_rate', 'kg_wkg_hrs_day', 'kg_no_days')
    def _compute_kg_removal_lab(self):
        for record in self:
            record.kg_removal_lab_cost = record.kg_rem_nos * record.kg_rem_rate * record.kg_wkg_hrs_day * record.kg_no_days
            record.kg_removal_lab_hrs = record.kg_rem_nos * record.kg_wkg_hrs_day * record.kg_no_days


    @api.depends('kg_hire_veh_rate', 'kg_hire_veh_km', 'kg_hire_veh_days')
    def _compute_hire_veh(self):
        for record in self:
            record.kg_hire_vehicle_cost = record.kg_hire_veh_rate * record.kg_hire_veh_km * record.kg_hire_veh_days

    @api.depends('kg_hire_vehicle_cost','kg_removal_lab_cost')
    def _compute_tot_cost(self):
        for record in self:
            record.kg_tot_dom_tpt_cost = record.kg_hire_vehicle_cost + record.kg_removal_lab_cost

    


class kg_inter_install_lines(models.Model):

    _name="kg.inter.install.lines"

    name = fields.Char('Name')

    kg_emp_categ = fields.Many2one('hr.job','Category')

    kg_install_nos = fields.Integer('Install Nos')

    kg_install_rate = fields.Float('Install Rate')

    kg_wkg_hrs_day = fields.Float('Wkg Hours/Day')

    kg_no_days = fields.Integer('No of Days')

    kg_install_lab_cost = fields.Float('Install Labour Cost',compute="_compute_kg_install_lab")

    kg_install_lab_hrs = fields.Float('Install Labour Hours',compute="_compute_kg_install_lab")

    kg_est_id = fields.Many2one('kg.estimation','Estimation')

    @api.depends('kg_install_nos','kg_install_rate','kg_wkg_hrs_day','kg_no_days')
    def _compute_kg_install_lab(self):
        for record in self:
            record.kg_install_lab_cost = record.kg_install_nos *  record.kg_install_rate * record.kg_wkg_hrs_day * record.kg_no_days
            record.kg_install_lab_hrs = record.kg_install_nos * record.kg_wkg_hrs_day * record.kg_no_days



    @api.depends('kg_rem_nos', 'kg_rem_rate', 'kg_wkg_hrs_day', 'kg_no_days')
    def _compute_kg_removal_lab(self):
        for record in self:
            record.kg_removal_lab_cost = record.kg_rem_nos * record.kg_rem_rate * record.kg_wkg_hrs_day * record.kg_no_days
            record.kg_removal_lab_hrs = record.kg_rem_nos * record.kg_wkg_hrs_day * record.kg_no_days

class kg_inter_inst_flight_lines(models.Model):

    _name="kg.inter.inst.flight.lines"



    name = fields.Char('Name')

    kg_emp_categ = fields.Many2one('hr.job', 'Category')

    kg_nos = fields.Integer("No's")

    kg_flight_rate = fields.Float('Flight Rate')

    kg_flight_total = fields.Float('Flight Total',compute="comp_flight_total")
    
    kg_est_id = fields.Many2one('kg.estimation','Estimation')

    @api.depends('kg_nos','kg_flight_rate')
    def comp_flight_total(self):
        for record in self:
            record.kg_flight_total=record.kg_nos*record.kg_flight_rate




class kg_inter_inst_accomodation_lines(models.Model):

    _name="kg.inter.inst.accomodation.lines"


    name = fields.Char('Name')

    kg_emp_categ = fields.Many2one('hr.job', 'Category')

    kg_hotel_nights = fields.Integer('Hotel Nights')

    kg_hotel_rate = fields.Float('Hotel Rate')

    kg_hotel_total = fields.Float('Hotel Total',compute="comp_hot_tot")

    kg_est_id = fields.Many2one('kg.estimation','Estimation')

    @api.depends('kg_hotel_nights','kg_hotel_rate')
    def comp_hot_tot(self):
        for record in self:
            record.kg_hotel_total=record.kg_hotel_nights*record.kg_hotel_rate



class kg_inter_inst_visa_lines(models.Model):

    _name="kg.inter.inst.visa.lines"


    kg_emp_categ = fields.Many2one('hr.job', 'Category')

    kg_visa_nos = fields.Integer("Visa No's.")

    kg_visa_rate = fields.Float('Visa+Misc')

    kg_visa_tot = fields.Float('Total',compute="comp_visa_tot")
    
    kg_est_id = fields.Many2one('kg.estimation','Estimation')


    @api.depends('kg_visa_nos', 'kg_visa_rate')
    def comp_visa_tot(self):
        for record in self:
            record.kg_visa_tot = record.kg_visa_nos * record.kg_visa_rate
            
            




class kg_inter_inst_freight_lines(models.Model):

    _name="kg.inter.inst.freight.lines"

    kg_emp_categ = fields.Many2one('kg.freight.wight', 'Freight Weight')

    kg_nos = fields.Integer('Nos')

    kg_freight_rate = fields.Float('Freight Rate')

    kg_freight_tot = fields.Float('Freight Rate',compute="comp_freight_tot")
    
    kg_est_id = fields.Many2one('kg.estimation','Estimation')

    @api.depends('kg_nos','kg_freight_rate')
    def comp_freight_tot(self):
        for record in self:
            record.kg_freight_tot = record.kg_nos * record.kg_freight_rate



class kg_inter_rem_flight_lines(models.Model):

    _name="kg.inter.rem.flight.lines"


    name = fields.Char('Name')

    kg_emp_categ = fields.Many2one('hr.job', 'Category')

    kg_nos = fields.Integer("No's")

    kg_flight_rate = fields.Float('Flight Rate')

    kg_flight_total = fields.Float('Flight Total',compute="comp_flight_total")
    
    kg_est_id = fields.Many2one('kg.estimation','Estimation')

    @api.depends('kg_nos','kg_flight_rate')
    def comp_flight_total(self):
        for record in self:
            record.kg_flight_total=record.kg_nos*record.kg_flight_rate




class kg_inter_rem_accomodation_lines(models.Model):

    _name="kg.inter.rem.accomodation.lines"



    name = fields.Char('Name')

    kg_emp_categ = fields.Many2one('hr.job', 'Category')

    kg_hotel_nights = fields.Integer('Hotel Nights')

    kg_hotel_rate = fields.Float('Hotel Rate')

    kg_hotel_total = fields.Float('Hotel Total',compute="comp_hot_tot")

    kg_est_id = fields.Many2one('kg.estimation','Estimation')

    @api.depends('kg_hotel_nights','kg_hotel_rate')
    def comp_hot_tot(self):
        for record in self:
            record.kg_hotel_total=record.kg_hotel_nights*record.kg_hotel_rate


class kg_inter_rem_visa_lines(models.Model):

    _name="kg.inter.rem.visa.lines"


    kg_emp_categ = fields.Many2one('hr.job', 'Category')

    kg_visa_nos = fields.Integer("Visa No's.")

    kg_visa_rate = fields.Float('Visa+Misc')

    kg_visa_tot = fields.Float('Total',compute="comp_visa_tot")
    
    kg_est_id = fields.Many2one('kg.estimation','Estimation')


    @api.depends('kg_visa_nos', 'kg_visa_rate')
    def comp_visa_tot(self):
        for record in self:
            record.kg_visa_tot = record.kg_visa_nos * record.kg_visa_rate
            
            

class kg_inter_rem_doc_lines(models.Model):

    _name="kg.inter.rem.doc.lines"


    name = fields.Char('Name')

    kg_nos = fields.Integer("No's.")

    kg_rate = fields.Float('Rate')

    kg_visa_tot = fields.Float('Total',compute="comp_doc_tot")
    
    kg_est_id = fields.Many2one('kg.estimation','Estimation')

    @api.depends('kg_nos','kg_rate')
    def comp_doc_tot(self):
        for record in self:
            record.kg_visa_tot = record.kg_nos * record.kg_rate






class kg_inter_removal_lines(models.Model):

    _name="kg.inter.removal.lines"

    name = fields.Char('Name')

    kg_emp_categ = fields.Many2one('hr.job','Category')

    kg_rem_nos = fields.Integer('Removal Nos')

    kg_rem_rate = fields.Float('Removal Rate')

    kg_wkg_hrs_day = fields.Float('Wkg Hours/Day')

    kg_no_days = fields.Integer('No of Days')

    kg_removal_lab_cost = fields.Float('Removal Labour Cost',compute="_compute_kg_removal_lab")

    kg_removal_lab_hrs = fields.Float('Remove Labour Hours',compute="_compute_kg_removal_lab")

    kg_est_id = fields.Many2one('kg.estimation','Estimation')

    @api.depends('kg_install_nos','kg_install_rate','kg_wkg_hrs_day','kg_no_days')
    def _compute_kg_install_lab(self):
        for record in self:
            record.kg_install_lab_cost = record.kg_install_nos *  record.kg_install_rate * record.kg_wkg_hrs_day * record.kg_no_days
            record.kg_install_lab_hrs = record.kg_install_nos * record.kg_wkg_hrs_day * record.kg_no_days



    @api.depends('kg_rem_nos', 'kg_rem_rate', 'kg_wkg_hrs_day', 'kg_no_days')
    def _compute_kg_removal_lab(self):
        for record in self:
            record.kg_removal_lab_cost = record.kg_rem_nos * record.kg_rem_rate * record.kg_wkg_hrs_day * record.kg_no_days
            record.kg_removal_lab_hrs = record.kg_rem_nos * record.kg_wkg_hrs_day * record.kg_no_days

    
    
class kg_travel_lines(models.Model):

    _name="kg.travel.lines"


    name = fields.Char('Name')

    kg_emp_categ = fields.Many2one('hr.job', 'Category')

    kg_nos = fields.Integer("No's")

    kg_no_flight = fields.Integer('Flights')

    kg_flight_rate = fields.Float('Flight Rate')

    kg_flight_total = fields.Float('Flight Total',compute="comp_flight_total")

    kg_hotel_nights = fields.Integer('Hotel Nights')

    kg_sub_tot_nights = fields.Integer('Sub-Total Nights',compute="comp_sub_nights")

    kg_hotel_rate = fields.Float('Hotel Rate')

    kg_hotel_total = fields.Float('Hotel Total',compute="comp_hot_tot")

    kg_visa_nos = fields.Integer('Visa Nos')

    kg_visa_rate = fields.Float('Visa Rate')

    kg_visa_tot = fields.Float('Visa Rate',compute="comp_visa_tot")
    
    kg_est_id = fields.Many2one('kg.estimation','Estimation')

    @api.depends('kg_nos','kg_no_flight','kg_flight_rate')
    def comp_flight_total(self):
        for record in self:
            record.kg_flight_total=record.kg_nos*record.kg_no_flight*record.kg_flight_rate

    @api.depends('kg_hotel_nights','kg_nos')
    def comp_sub_nights(self):
        for record in self:
            record.kg_sub_tot_nights=record.kg_hotel_nights*record.kg_nos

    @api.depends('kg_hotel_rate','kg_sub_tot_nights')
    def comp_hot_tot(self):
        for record in self:
            record.kg_hotel_total=record.kg_hotel_rate*record.kg_sub_tot_nights

    @api.depends('kg_visa_nos', 'kg_visa_rate')
    def comp_visa_tot(self):
        for record in self:
            record.kg_visa_tot = record.kg_visa_nos * record.kg_visa_rate
            
            
            

    
    


    


class kg_estimation_pricelist(models.Model):
    
    _name = 'kg.estimation.pricelist'
    
    name = fields.Char('Name')
    
    kg_product = fields.Many2one('product.product','Item')
    
    kg_product_code = fields.Char('Item Code')
    
    kg_uom = fields.Many2one('product.uom','UOM')
    
    kg_partner = fields.Many2one('res.partner','Customer')
    
    
    
    kg_unit_price = fields.Float('Unit Price')
    
    
    
    kg_att_url1 = fields.Char('')
    
    kg_att_url2 = fields.Char('')
    
    
    kg_att_url3 = fields.Char('')
    
    
    kg_att_url4 = fields.Char('')
    
    kg_att_url5 = fields.Char('')
    
    
    @api.onchange('kg_product')
    def onchange_product(self):
        self.kg_unit_price=self.kg_product and self.kg_product.list_price or 0.0
        self.kg_product_code = self.kg_product and self.kg_product.default_code or ''
        self.kg_uom = self.kg_product.uom_id and self.kg_product.uom_id.id or False
    
    @api.model
    def create(self,vals):
        sequence_code='kg.estimation.pricelist'
        vals['name']= self.env['ir.sequence'].next_by_code(sequence_code)
        return super(kg_estimation_pricelist, self).create(vals)
    
    
class kg_fin_lines(models.Model):
    
    _name ='kg.fin.lines'
    
    name = fields.Char('Name')
    
    kg_cat_id = fields.Many2one('kg.category','Category')
    
    
    
    kg_est_id = fields.Many2one('kg.estimation','Estimation')
    
    kg_cp =fields.Float('Cost Price')
    
    kg_mp = fields.Float('Markup(%)')
    
    kg_sp = fields.Float('Selling Price',compute='comp_sp')
    
    
    kg_sharing  = fields.Integer('Sharing',compute='comp_sharing')
  
    
    
    @api.depends('kg_cp')
    def comp_sharing(self):
        for record in self:
            if record.kg_cat_id.name != 'Manpower':
                den=(record.kg_est_id.kg_subtotal_mat_cp+record.kg_est_id.kg_subtotal_sub_cp+record.kg_est_id.kg_subtotal_spe_cp) 
                if den > 0:
                    record.kg_sharing=round((record.kg_cp/den)*100,0)
            else:
                print record.kg_est_id.kg_subtotal_cp,'5555555'
                if record.kg_est_id.kg_subtotal_cp ==0.0:
                    den=1
                else:
                    den=record.kg_est_id.kg_subtotal_cp
                print den    
                record.kg_sharing= round((record.kg_cp/den)*100,0)
            
    
    
    @api.depends('kg_mp','kg_cp')
    def comp_sp(self):
        
        for record in self:
            
            record.kg_sp=((record.kg_mp/100.0)*record.kg_cp)+record.kg_cp
    
    
    
    
#     
#     @api.onchange('kg_cat_id','kg_est_id')
#     def comp_cp(self):
#         for record in self:
#             if isinstance(record.kg_est_id,int) is True  and isinstance(record.kg_cat_id.id,int) is True:
#           
#                 print record.kg_est_id.id,'11111111111'
#                 print record.kg_cat_id.id,'11111111111'
#                 print self.env.context,'afafafa'
#                  
#                  
#                  
#                 self.env.cr.execute("""
#                         SELECT SUM(kg_qty*kg_rate)
#                         FROM kg_unit_lines where  kg_unit_id in (select id from kg_unit where kg_est_id= %s ) and kg_cat=%s""",
#                     (record.kg_est_id.id, record.kg_cat_id.id,))
#                 record.kg_cp = self.env.cr.fetchone()[0] or 0.0



    
class kg_freight_wight(models.Model):
    _name = 'kg.freight.wight'

    name = fields.Char('Name')