# -*- coding: utf-8 -*-
from odoo import fields,models,api,_
from odoo import tools
from datetime import date
from dateutil.relativedelta import relativedelta

from odoo.exceptions import UserError, AccessError
from odoo import api, fields, models, SUPERUSER_ID, _

class kg_cust_targ(models.Model):
    
    _name ='kg.cust.targ'
    
    name = fields.Char('Name',required=True)
    
   
    kg_date = fields.Date('Date')
    kg_acc_mgr_id = fields.Many2one('res.users','Account Manager')
    
    kg_amt = fields.Float('Amount')
    
    kg_cus_id = fields.Many2one('res.partner','Customer')
    
    
    @api.onchange('kg_cus_id')
    def onchange_cus(self):
        for record in self:
            record.kg_acc_mgr_id = record.kg_cus_id.kg_acc_mgr_id.id