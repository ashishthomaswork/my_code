import kg_estimation
import kg_boq_request
import kg_log_req
import kg_sample_material
import kg_request_passport
import kg_pack_list
import kg_out_log
import kg_vendor
import kg_design_request
#import kg_loan   using new module for loan

import kg_sale_revision
import kg_inv_req
import kg_pur_req
import kg_mat_req
import kg_qc
import kg_rfi
import kg_helpdesk
import kg_addmat_req
import kg_install_req
import kg_fitout
import kg_tripsumm
import kg_overinstall_sch
import kg_timesheet

import kg_overinstall_expense
import kg_delivery_request
import kg_invoice_request
import kg_packing_requisition
import kg_export
import kg_cust_targ
