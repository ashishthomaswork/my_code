# -*- coding: utf-8 -*-
from odoo import fields,models,api,_
from odoo import tools
from datetime import date
from dateutil.relativedelta import relativedelta

from odoo.exceptions import UserError, AccessError
from odoo import api, fields, models, SUPERUSER_ID, _

class kg_log_req(models.Model):
    
    _name="kg.log.req"
   
    name =  fields.Char('Name')
    
    _inherit = ['mail.thread', 'ir.needaction_mixin']
    
    
   # kg_date = fields.Date('Date')
    
    kg_partner  = fields.Many2one('res.partner','Contact Details of the Client/Supplier')
    
    
    kg_req_by_dep  = fields.Many2one('hr.department','Requested by/Department')
    
    
    kg_job_no  = fields.Many2one('mrp.production','Job No')
    
    kg_type=  fields.Selection([('pickup','Pick Up'),('delivery','Delivery'),('site','Site'),('import','Import'),('export','Export')],'Type')
    
    kg_log_mode  = fields.Selection([('local','Local'),('air','Air'),('sea','Sea'),('road','Road')],'Logistics Mode')
    
    kg_desc = fields.Char('Goods description/Details')
    
    kg_ship_weight = fields.Float('Shipment weight')
    
    kg_ship_dim  = fields.Char('Shipment dimension')
    
    kg_req_veh = fields.Many2one('fleet.vehicle','Required Vehicle')
    
    kg_app_log_cost = fields.Float('Approved logistics cost')
    
    kg_act_log_cost  =  fields.Float('Actual Logistics cost')
    
    kg_no_peo = fields.Float('No.of people traveling')
    
    kg_dest = fields.Char('Destination')
    
    kg_reporting_date = fields.Datetime('Reporting Time/Date')
    
    kg_starting_point = fields.Char('Starting Point')
    
    kg_starting_date = fields.Datetime('Starting Time/Date')
    
    kg_vehicle_staytime = fields.Float('Vehicle stay back/Time')
    
    kg_req_by  = fields.Many2one('res.users','Requested By',default=lambda self: self.env.user)
    
    kg_ver_by  = fields.Many2one('res.users','Verified By')
    
    kg_app_by =  fields.Many2one('res.users','Approved By')
    
    kg_spec_ins = fields.Text('Specific instructions if any')
    
  #  kg_state = fields.Selection([('request','Request'),('verify','Verified'),('approve','Approved')],'State',default='request')
    
    
    kg_req_date = fields.Date('Required Date')
    
    
    kg_raised_by = fields.Many2one('res.users','Raised By',default=lambda self: self.env.user and self.env.user.id or False)
    
    kg_assign_to = fields.Many2one('res.users','Assigned To')
    
    kg_att_url1 = fields.Char('')
    
    kg_att_url2 = fields.Char('')
    
    
    kg_att_url3 = fields.Char('')
    
    
    kg_att_url4 = fields.Char('')
    
    kg_att_url5 = fields.Char('')
    
    kg_del_note = fields.Many2one('stock.picking','Delivery Note')
    
    
    kg_project_id = fields.Many2one('project.project','Project')
    
    kg_dep = fields.Many2one('hr.department', 'Department')
    
    
    @api.multi
    def _track_subtype(self, init_values):
        self.ensure_one()
        if 'kg_state' in init_values and self.kg_state == 'In Progress':
            return 'kg_abra.kg_log_inprogress'
        
        if 'kg_state' in init_values and self.kg_state == 'new':
            return 'kg_abra.kg_log_new'
        
        if 'kg_state' in init_values and self.kg_state == 'assign':
            return 'kg_abra.kg_log_assign'
      
        
        
        if 'kg_state' in init_values and self.kg_state == 'Completed':
            return 'kg_abra.kg_log_complete'
        return super(kg_log_req, self)._track_subtype(init_values)
    
    kg_state = fields.Selection([('new','New'),('assign','Assigned'),('In Progress','In Progress'),('Completed','Completed')],'State',track_visibility='onchange',default='new')
    
    
    @api.multi
    def write(self, vals):
        """Override default Odoo write function and extend."""
        # Do your custom logic here
        
        if vals.get('kg_state') and vals.get('kg_state') !='new' and vals.get('kg_state') !='assign' :
            
                vals_mail={}
                message="<b>Dear %(hr_man)s,</b><br/><br/>Your  Logistics Requisition   : %(hd_name)s  has been changed to %(state)s State" % {
                         'hr_man': self.kg_raised_by.partner_id.name,
                           'hd_name': self.name,
                           'state': vals.get('kg_state')
                           
                             
                    }
            
                vals_mail['message']=message
                vals_mail['partner']=self.kg_raised_by.partner_id.email
                self.send_mail(vals_mail)   
        print vals,'hshddhhhd'
        if vals.get('kg_assign_to'):
            vals['kg_state']='assign'
            user_obj=self.env['res.users'].browse([vals.get('kg_assign_to')])
            fol={}
            fol['res_model']='kg.log.req'
            fol['res_id']=self.id
            fol['partner_id']=user_obj.partner_id.id
            fol_id=self.env['mail.followers'].create(fol)
            subtypes= self.env['mail.message.subtype'].search([('res_model','=','kg.log.req')]).ids
            if subtypes:
                for i in subtypes:
                    self._cr.execute('INSERT INTO mail_followers_mail_message_subtype_rel (mail_followers_id, mail_message_subtype_id) values (%s, %s)', (fol_id.id, i))

        
        
            
            
            vals_mail={}
            message="<b>Dear %(hr_man)s,</b><br/><br/> Logistics Requisition  : %(hd_name)s  has been assigned to You" % {
                         'hr_man': user_obj.partner_id.name,
                           'hd_name': self.name,
                             
                    }
            
            vals_mail['message']=message
            vals_mail['partner']=user_obj.partner_id.email
            self.send_mail(vals_mail)  
            
            
        return super(kg_log_req, self).write(vals)
    
    
    @api.model
    def create(self,vals):
     
        
        dep_obj=self.env['hr.department'].browse([vals.get('kg_dep')])    
        partner_id=dep_obj.manager_id.user_id.partner_id
        
        


            
        
        sequence_code='log.req'
        vals['name']= self.env['ir.sequence'].next_by_code(sequence_code)
        res= super(kg_log_req, self).create(vals)       
        
        dep_mgr_id =res.kg_dep.manager_id.user_id.partner_id.id
         
        fol={}
        fol['res_model']='kg.log.req'
        fol['res_id']=res.id
        fol['partner_id']=dep_mgr_id
        fol_id=self.env['mail.followers'].create(fol)
        subtypes= self.env['mail.message.subtype'].search([('res_model','=','kg.log.req')]).ids
        if subtypes:
            for i in subtypes:
                self._cr.execute('INSERT INTO mail_followers_mail_message_subtype_rel (mail_followers_id, mail_message_subtype_id) values (%s, %s)', (fol_id.id, i))

        
        
        vals_mail={}
        message="<b>Dear %(hr_man)s,</b><br/><br/> New Logistics Requisition : %(hd_name)s from  %(emp_name)s has been created" % {
                     'hr_man': partner_id.name,
                       'hd_name': res.name,
                         'emp_name': res.kg_raised_by.name,
                }
        
        vals_mail['message']=message
        vals_mail['partner']=partner_id.email
        res.send_mail(vals_mail)
        res.write({'kg_state':''})
        res.write({'kg_state':'new'})
        return res
    
    
    
    def send_mail(self,vals):
        main_content = {
                'subject': "Logistics Requisition !",
              
                'body_html': '<pre>%s</pre>' % vals['message'],
                 'email_to': vals['partner'],
            }
        return self.env['mail.mail'].create(main_content).send()
    
    
    
   
    
    
    
    
    
#     @api.multi
#     def kg_verify(self):
#         for record in self:
#             record.write({'kg_state':'verify','kg_ver_by':self.env.user.id})
#             
#     @api.multi
#     def kg_approve(self):
#         for record in self:
#             record.write({'kg_state':'approve','kg_app_by':self.env.user.id})
#             
    
    
    
    
    
        
    
    