# -*- coding: utf-8 -*-
from odoo import fields,models,api,_
from odoo import tools
from datetime import date
from dateutil.relativedelta import relativedelta

from odoo.exceptions import UserError, AccessError
from odoo import api, fields, models, SUPERUSER_ID, _


class kg_install_req(models.Model):
    
    _name='kg.install.req'
    
    
    name  =fields.Char('Document No')
    
    kg_client = fields.Many2one('res.partner','Client')
    
    kg_project_id = fields.Many2one('project.project','Project')
    
    kg_doc_date = fields.Date('Document Date')
    
    kg_job_no = fields.Many2one('mrp.production','Job Order No')
    
    kg_install_date = fields.Date('Installation Date')
    
    kg_install_from = fields.Float('From')
    
    kg_install_to = fields.Float('To')
    
    kg_install_req_lines = fields.One2many('kg.install.req.lines','kg_install_req_id','Lines')
    
    kg_site_con_name = fields.Char('Name')
    
    
    kg_site_con_tel = fields.Char('Tel')
    
    kg_site_con_loc = fields.Char('Location')
    
    
    kg_prod_in_charge = fields.Many2one('res.users','Production In-Charge')
    
    kg_log_dept = fields.Many2one('res.users','Logistics Dept')
    
    
    
    kg_team_leader =fields.Many2one('res.users','Team Leader')
    
    kg_no_per = fields.Integer('No of Persons')
    
    
    kg_req_by  = fields.Many2one('res.users','Requested By',default=lambda self: self.env.user)
    
    kg_assign_to = fields.Many2one('res.users','Assign To')

    
    kg_site_permit = fields.Boolean('Site Permits Attached')
    
    kg_loc_map = fields.Boolean('Location Map attached')
    
    
    company_id = fields.Many2one('res.company','Company',default=lambda self: self.env.user.company_id)

    kg_overinstall_sch_count = fields.Integer('Overseas Installion Schedue Count',compute='compute_overinstall_count')
    
    kg_overinstall_exp_count = fields.Integer('Overseas Installion Expense Count',compute='compute_overinstall_exp_count')
    
    
    kg_timesheet_lines = fields.One2many('kg.timesheet','kg_install_req_id','Timesheet')
    
    _inherit = ['mail.thread', 'ir.needaction_mixin']
    
    
    
    
    @api.multi
    def _track_subtype(self, init_values):
        self.ensure_one()
        if 'kg_state' in init_values and self.kg_state == 'In Progress':
            return 'kg_abra.kg_install_inprogress'
        
        if 'kg_state' in init_values and self.kg_state == 'new':
            return 'kg_abra.kg_install_new'
        
        if 'kg_state' in init_values and self.kg_state == 'assign':
            return 'kg_abra.kg_install_assign'
        
        if 'kg_state' in init_values and self.kg_state == 'Quote Created':
            return 'kg_abra.kg_install_quote_created'
        
        
        if 'kg_state' in init_values and self.kg_state == 'Completed':
            return 'kg_abra.kg_install_complete'
        return super(kg_install_req, self)._track_subtype(init_values)
    
    kg_state = fields.Selection([('new','New'),('assign','Assigned'),('In Progress','In Progress'),('Completed','Completed')],'State',track_visibility='onchange',default='new')

    kg_dep_id = fields.Many2one('hr.department','Department')
    @api.multi
    def write(self, vals):
        """Override default Odoo write function and extend."""
        # Do your custom logic here
        
        if vals.get('kg_state') and vals.get('kg_state') !='new' and vals.get('kg_state') !='assign' :
            
                vals_mail={}
                message="<b>Dear %(hr_man)s,</b><br/><br/>Your  Install Request : %(hd_name)s  has been changed to %(state)s State" % {
                         'hr_man': self.kg_req_by.partner_id.name,
                           'hd_name': self.name,
                           'state': vals.get('kg_state')
                           
                             
                    }
            
                vals_mail['message']=message
                vals_mail['partner']=self.kg_req_by.partner_id.email
                self.send_mail(vals_mail)   
        print vals,'hshddhhhd'
        if vals.get('kg_assign_to'):
            vals['kg_state']='assign'
            user_obj=self.env['res.users'].browse([vals.get('kg_assign_to')])
            fol={}
            fol['res_model']='kg.install.req'
            fol['res_id']=self.id
            fol['partner_id']=user_obj.partner_id.id
            fol_id=self.env['mail.followers'].create(fol)
            subtypes= self.env['mail.message.subtype'].search([('res_model','=','kg.install.req')]).ids
            if subtypes:
                for i in subtypes:
                    self._cr.execute('INSERT INTO mail_followers_mail_message_subtype_rel (mail_followers_id, mail_message_subtype_id) values (%s, %s)', (fol_id.id, i))

            
            
            vals_mail={}
            message="<b>Dear %(hr_man)s,</b><br/><br/> Install Request : %(hd_name)s  has been assigned to You" % {
                         'hr_man': user_obj.partner_id.name,
                           'hd_name': self.name,
                             
                    }
            
            vals_mail['message']=message
            vals_mail['partner']=user_obj.partner_id.email
            self.send_mail(vals_mail)  
            
            
        return super(kg_install_req, self).write(vals)
    
    
    @api.model
    def create(self,vals):
     
        
        dep_obj=self.env['hr.department'].browse([vals.get('kg_dep_id')])    
        partner_id=dep_obj.manager_id.user_id.partner_id
        
        

            
        
        res= super(kg_install_req, self).create(vals)
        
        dep_mgr_id =res.kg_dep_id.manager_id.user_id.partner_id.id
         
        fol={}
        fol['res_model']='kg.install.req'
        fol['res_id']=res.id
        fol['partner_id']=dep_mgr_id
        fol_id=self.env['mail.followers'].create(fol)
        subtypes= self.env['mail.message.subtype'].search([('res_model','=','kg.install.req')]).ids
        if subtypes:
            for i in subtypes:
                self._cr.execute('INSERT INTO mail_followers_mail_message_subtype_rel (mail_followers_id, mail_message_subtype_id) values (%s, %s)', (fol_id.id, i))

        vals_mail={}
        message="<b>Dear %(hr_man)s,</b><br/><br/> New Install Request : %(hd_name)s from  %(emp_name)s has been created" % {
                     'hr_man': partner_id.name,
                       'hd_name': res.name,
                         'emp_name': res.kg_req_by.name,
                }
        
        vals_mail['message']=message
        vals_mail['partner']=partner_id.email
        res.send_mail(vals_mail)
        res.write({'kg_state':''})
        res.write({'kg_state':'new'})
        return res
    
    
    
    def send_mail(self,vals):
        main_content = {
                'subject': "Install Request!",
              
                'body_html': '<pre>%s</pre>' % vals['message'],
                 'email_to': vals['partner'],
            }
        return self.env['mail.mail'].create(main_content).send()


    
    
#     @api.model
#     def create(self,vals):
#         sequence_code='kg.design.request'
#         vals['name']= self.env['ir.sequence'].next_by_code(sequence_code) 
#     
#         return super(kg_design_request, self).create(vals)
    
    @api.multi
    def compute_overinstall_count(self):
        for record in self:
            est_obj = self.env['kg.overinstall.sch']
            
            record.kg_overinstall_sch_count = est_obj.search_count([('kg_install_req_id', '=',record.id)]) 
            
    @api.multi
    def compute_overinstall_exp_count(self):
        for record in self:
            est_obj = self.env['kg.overinstall.expense']
            
            record.kg_overinstall_exp_count = est_obj.search_count([('kg_install_req_id', '=',record.id)]) 

class kg_install_req_lines(models.Model):
    
    _name = 'kg.install.req.lines'
    
    name = fields.Char('Material/Description')
    
    kg_qty = fields.Float('Qty')
    
    
    kg_install_req_id = fields.Many2one('kg.install.req','Install Req')
    