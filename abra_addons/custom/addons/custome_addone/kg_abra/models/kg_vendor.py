# -*- coding: utf-8 -*-
from odoo import fields,models,api,_
from odoo import tools
from datetime import date
from dateutil.relativedelta import relativedelta

from odoo.exceptions import UserError, AccessError
from odoo import api, fields, models, SUPERUSER_ID, _

class ResPartner(models.Model):

    _inherit="res.partner"

    kg_spon = fields.Char('Sponsors')

    kg_owners = fields.Char('Owners')

  #  kg_post = fields.Char('Position')

    kg_payee = fields.Char('Payee Name')

    kg_reason = fields.Text('If Company name is different from Payee Name, please state the reason :')

    kg_bank = fields.Char('Bank ')

    kg_branch_no = fields.Char('Branch Number')

    kg_bnk_sreet= fields.Char('Address')

    kg_bnk_street2 = fields.Char('')

    kg_bnk_city = fields.Char('City')

    kg_bnk_state_id = fields.Many2one('res.country.state')

    kg_bnk_zip  =fields.Char('Zip')

    kg_bnk_country = fields.Many2one('res.country')

    kg_acc_no = fields.Char('Account Number')

    kg_iban = fields.Char('IBAN')


    kg_swift_code = fields.Char('Swift Code')

    kg_years =  fields.Integer('Years of Establishing the Company')

    kg_trade_lic_no = fields.Char('Trade Licensing Number')

    kg_trade_expiry_date = fields.Date('Trade License Expiry Date')

    kg_desc_serv = fields.Char('Description of Product/Service Order')

    kg_repeat = fields.Boolean('Has your company previously supplied goods or services to ABRA VM & SD INTL')

    kg_last_order_date = fields.Date('If yes, when was last time')
    
    
    kg_acc_mgr_id = fields.Many2one('res.users','Account Manager')
    
    
    kg_target_count = fields.Integer('Target Count',compute='compute_target_count')
    
    
    @api.multi
    def compute_target_count(self):
        for record in self:
            tar_obj = self.env['kg.cust.targ']
            
            record.kg_target_count = tar_obj.search_count([('kg_cus_id', '=',record.id)]) 







