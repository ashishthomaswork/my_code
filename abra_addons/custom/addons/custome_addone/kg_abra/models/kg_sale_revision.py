# -*- coding: utf-8 -*-
from odoo import fields,models,api,_
from odoo import tools
from datetime import date
from dateutil.relativedelta import relativedelta

from odoo.exceptions import UserError, AccessError
from odoo import api, fields, models, SUPERUSER_ID, _

class SaleOrder(models.Model):
    
    _inherit="sale.order"
    
    org_so_id = fields.Many2one('sale.order','Orginal Sale Order')
    
    
    kg_rev_count = fields.Integer('Revision Count',compute='compute_rev_count')
    
    @api.multi
    def compute_rev_count(self):
        for record in self:
            
            
            record.kg_rev_count = self.search_count([('org_so_id', '=',record.id)]) 
    
    
    @api.one
    def create_revision(self,default=None):
           
        print self,'aaaaaaaaaaa'
        
    
        
        default = dict(default or {})
        
        rev_count=self.search_count([('org_so_id', '=',self.id),('state','=','revision')]) 
        
        print default,'agagag'
        
        default.update({
            'org_so_id':self.id,
            'name':self.name+"_"+str(rev_count+1),
            'state':'revision'})
        
        return super(SaleOrder, self).copy(default)

