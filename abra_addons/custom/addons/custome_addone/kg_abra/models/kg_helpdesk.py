# -*- coding: utf-8 -*-
from odoo import fields,models,api,_
from odoo import tools
from datetime import date
from dateutil.relativedelta import relativedelta

from odoo.exceptions import UserError, AccessError
from odoo import api, fields, models, SUPERUSER_ID, _
from email import _name

class kg_helpdesk(models.Model):
    
    _name = 'kg.helpdesk'
    
    name =fields.Char('Request No')
    
    kg_emp_id = fields.Many2one('hr.employee','Employee')
    
    kg_req_type = fields.Selection([('new_hw','New Hardware'),('complaint','Complaint Registration')],'Type')
    
    kg_fin_app = fields.Boolean('Need Finance Approval')
    
    kg_remarks = fields.Text('Remarks')
    
    kg_sub_date = fields.Date('Submiited On')
    
    kg_subject = fields.Char('Subject')
    
    kg_it_admin = fields.Many2one('hr.employee','IT Admin')
    
    kg_manager  =fields.Many2one('hr.employee','Manager')
    
    
    kg_vendor = fields.Many2one('res.partner','Vendor')
    
    kg_product = fields.Many2one('product.product','Product')
    
    
    kg_cost_inv = fields.Boolean('Cost Involved')
    
    kg_cost = fields.Float('Cost')
    
    kg_debit_acc = fields.Many2one('account.account','Debit Account')
    
    kg_credit_acc = fields.Many2one('account.account','Credit Account')
    
    kg_hd_category = fields.Many2one('kg.hd.category','Category')
    
    kg_supp_war = fields.Boolean('Supplier Warranty')
    
    kg_release = fields.Boolean('Released')
    
    
    kg_view_release_btn = fields.Boolean('View Release Button')
    
    
    kg_device_desc = fields.Char('Device Description')
    
    kg_fin_mgr = fields.Many2one('hr.employee','Finace Manager')
    
    
    kg_po_count = fields.Integer('PO Count',compute='compute_po_count')
    
    
    @api.multi
    def compute_po_count(self):
        for record in self:
            po_ob = self.env['purchase.order']
            
            record.kg_po_count = po_ob.search_count([('kg_hd_id', '=',record.id),]) 
    

    
    kg_state = fields.Selection([('new','New'),('pending','Pending'),('sent_to_supp','Sent To Supplier'),('rec_at_store','Received at Store'),('need_fin_app','Need Finance Approval'),('fin_app','Finance Approved'),('completed','Completed')],'State',default='new')
    
    
    @api.multi
    def btn_compl(self):
        for record in self:
            
            if record.kg_req_type == 'new_hw':
                 req_type='New Hardware'
            else:
                req_type='Complaint Registration'
            
            subject = "<b> Request - %(req_type)s - %(subj)s  Completed</b>"% {
                     'req_type': req_type,
                       'subj': record.kg_subject,
                }
            
            
            
            body_html="<b>Dear %(emp_name)s,</b><br/><br/>  Your request for device with Reference %(hd_name)s has been completed and will be delivered Shortly" % {
                     'emp_name': record.kg_emp_id.name,
                       'hd_name': record.name,
                }
            
        
         
            
            
           
        
    
            main_content = {
                'subject': subject,
          #      'author_id': record.env.user.partner_id.id,
                'body_html': body_html,
                'email_to': record.kg_emp_id.user_id.login,
            }
            print main_content,'Employeeeeeeeeeeeeeee'
            self.env['mail.mail'].create(main_content).send()
            
            body_html="<b>Dear %(hr_man)s,</b><br/><br/>   Request : %(hd_name)s from Employee %(emp_name)s has been completed and delivered shortly" % {
                     'hr_man': record.kg_manager.name,
                       'hd_name': record.name,
                         'emp_name': record.kg_emp_id.name,
                }
          
           
            
          
        
    
            main_content = {
                'subject': subject,
             #   'author_id': record.env.user.partner_id.id,
                'body_html': body_html,
                'email_to': record.kg_manager.user_id.login,
            }
            print main_content,'Managerrrrrrrrrrrrrrrrrrr'
            self.env['mail.mail'].create(main_content).send()
            
            
          
           
            
            
            
            body_html="<b>Dear %(it_admin)s,</b><br/><br/> Request : %(hd_name)s from Employee %(emp_name)s has been completed and will be delivered quickly" % {
                     'it_admin': record.kg_it_admin.name,
                       'hd_name': record.name,
                         'emp_name': record.kg_emp_id.name,
                }
          
           
            
          
        
    
            main_content = {
                'subject': subject,
             #   'author_id': record.env.user.partner_id.id,
                'body_html': body_html,
                'email_to': record.kg_it_admin.user_id.login,
            }
            print main_content,'It Adminnnnnnnnnnnnnnnnnnnnnnn'
            self.env['mail.mail'].create(main_content).send()
            
            return record.write({'kg_state':'completed'})
    
    @api.multi
    def fin_appr(self):
        for record in self:
            
            if record.kg_req_type == 'new_hw':
                 req_type='New Hardware'
            else:
                req_type='Complaint Registration'
            
            subject = "<b> Request - %(req_type)s - %(subj)s  Request for New Device</b>"% {
                     'req_type': req_type,
                       'subj': record.kg_subject,
                }
            
            
            
            body_html="<b>Dear %(emp_name)s,</b><br/><br/>  Your request for device with Reference %(hd_name)s has been issued" % {
                     'emp_name': record.kg_emp_id.name,
                       'hd_name': record.name,
                }
            
        
         
            
            
           
        
    
            main_content = {
                'subject': subject,
          #      'author_id': record.env.user.partner_id.id,
                'body_html': body_html,
                'email_to': record.kg_emp_id.user_id.login,
            }
            print main_content,'Employeeeeeeeeeeeeeee'
            self.env['mail.mail'].create(main_content).send()
            
            body_html="<b>Dear %(hr_man)s,</b><br/><br/> Request for new device in  Request : %(hd_name)s from Employee %(emp_name)s has been issued" % {
                     'hr_man': record.kg_manager.name,
                       'hd_name': record.name,
                         'emp_name': record.kg_emp_id.name,
                }
          
           
            
          
        
    
            main_content = {
                'subject': subject,
             #   'author_id': record.env.user.partner_id.id,
                'body_html': body_html,
                'email_to': record.kg_manager.user_id.login,
            }
            print main_content,'Managerrrrrrrrrrrrrrrrrrr'
            self.env['mail.mail'].create(main_content).send()
            
            
            
            body_html="<b>Dear %(fin_mgr)s,</b><br/><br/> Request for new device in  Request : %(hd_name)s from Employee %(emp_name)s has been issued" % {
                     'fin_mgr': record.kg_fin_mgr.name,
                       'hd_name': record.name,
                         'emp_name': record.kg_emp_id.name,
                }
          
           
            
          
        
    
            main_content = {
                'subject': subject,
             #   'author_id': record.env.user.partner_id.id,
                'body_html': body_html,
                'email_to': record.kg_fin_mgr.user_id.login,
            }
            print main_content,'Managerrrrrrrrrrrrrrrrrrr'
            self.env['mail.mail'].create(main_content).send()
           
           
            
            
            
            body_html="<b>Dear %(it_admin)s,</b><br/><br/>Request for new device in  Request : %(hd_name)s from Employee %(emp_name)s has been issued" % {
                     'it_admin': record.kg_it_admin.name,
                       'hd_name': record.name,
                         'emp_name': record.kg_emp_id.name,
                }
          
           
            
          
        
    
            main_content = {
                'subject': subject,
             #   'author_id': record.env.user.partner_id.id,
                'body_html': body_html,
                'email_to': record.kg_it_admin.user_id.login,
            }
            print main_content,'It Adminnnnnnnnnnnnnnnnnnnnnnn'
            self.env['mail.mail'].create(main_content).send()
            
            return record.write({'kg_state':'fin_app'})
            
    @api.multi
    def kg_need_fin_app(self):
        for record in self:
            return record.write({'kg_state':'need_fin_app'})
    
    @api.multi
    def kg_return(self):
        for record in self:
            
            if record.kg_req_type == 'new_hw':
                 req_type='New Hardware'
            else:
                req_type='Complaint Registration'
                
            subject = "<b>Product in Request - %(req_type)s - %(subj)s  Reached Store</b>"% {
                     'req_type': req_type,
                       'subj': record.kg_subject,
                }
            
            body_html="<b>Dear %(hr_man)s,</b><br/><br/> Product in  Request : %(hd_name)s from Employee %(emp_name)s has reached Store" % {
                     'hr_man': record.kg_manager.name,
                       'hd_name': record.name,
                         'emp_name': record.kg_emp_id.name,
                }
          
           
            
          
        
    
            main_content = {
                'subject': subject,
             #   'author_id': record.env.user.partner_id.id,
                'body_html': body_html,
                'email_to': record.kg_manager.user_id.login,
            }
            print main_content,'Managerrrrrrrrrrrrrrrrrrr'
            self.env['mail.mail'].create(main_content).send()
           
            
            
            
            body_html="<b>Dear %(it_admin)s,</b><br/><br/>Product in  Request : %(hd_name)s from Employee %(emp_name)s has been reached store" % {
                     'it_admin': record.kg_it_admin.name,
                       'hd_name': record.name,
                         'emp_name': record.kg_emp_id.name,
                }
          
           
            
          
        
    
            main_content = {
                'subject': subject,
             #   'author_id': record.env.user.partner_id.id,
                'body_html': body_html,
                'email_to': record.kg_it_admin.user_id.login,
            }
            print main_content,'It Adminnnnnnnnnnnnnnnnnnnnnnn'
            self.env['mail.mail'].create(main_content).send()
            return record.write({'kg_state':'rec_at_store','kg_view_release_btn':False})
    
    @api.multi
    def release(self):
        for record in self:
            return record.write({'kg_release':True,'kg_view_release_btn':True})
    
    @api.multi
    def sent_to_supp(self):
        for record in self:
            return record.write({'kg_state':'sent_to_supp'})
    
    @api.onchange('kg_emp_id')
    def onchange_emp(self):
        for record in self:
            record.kg_it_admin =record.kg_emp_id.kg_it_admin.id
            record.kg_manager= record.kg_emp_id.parent_id.id
            
            
    
    @api.multi
    def problem_resolved(self):
        for record in self:
            
            if record.kg_req_type == 'new_hw':
                 req_type='New Hardware'
           
                
            subject = "<b>Request - %(req_type)s - %(subj)s  Completed</b>"% {
                     'req_type': req_type,
                       'subj': record.kg_subject,
                }
           
           
           
           
            body_html="<b>Dear %(emp_name)s,</b><br/><br/>  Your request with Reference %(hd_name)s successfully completed" % {
                     'emp_name': record.kg_emp_id.name,
                       'hd_name': record.name,
                }
            
        
         
            
            
           
        
    
            main_content = {
                'subject': subject,
          #      'author_id': record.env.user.partner_id.id,
                'body_html': body_html,
                'email_to': record.kg_emp_id.user_id.login,
            }
            print main_content,'Employeeeeeeeeeeeeeee'
            self.env['mail.mail'].create(main_content).send()
            
            body_html="<b>Dear %(hr_man)s,</b><br/><br/>  Request : %(hd_name)s from Employee %(emp_name)s has been successfuly completed" % {
                     'hr_man': record.kg_manager.name,
                       'hd_name': record.name,
                         'emp_name': record.kg_emp_id.name,
                }
          
           
            
          
        
    
            main_content = {
                'subject': subject,
             #   'author_id': record.env.user.partner_id.id,
                'body_html': body_html,
                'email_to': record.kg_manager.user_id.login,
            }
            print main_content,'Managerrrrrrrrrrrrrrrrrrr'
            self.env['mail.mail'].create(main_content).send()
           
            
            
            
            body_html="<b>Dear %(it_admin)s,</b><br/><br/>  Request : %(hd_name)s from Employee %(emp_name)s has been successfuly completed" % {
                     'it_admin': record.kg_it_admin.name,
                       'hd_name': record.name,
                         'emp_name': record.kg_emp_id.name,
                }
          
           
            
          
        
    
            main_content = {
                'subject': subject,
             #   'author_id': record.env.user.partner_id.id,
                'body_html': body_html,
                'email_to': record.kg_it_admin.user_id.login,
            }
            print main_content,'It Adminnnnnnnnnnnnnnnnnnnnnnn'
            self.env['mail.mail'].create(main_content).send()
            
            
            if record.kg_cost_inv == True and record.kg_cost > 0:
                journal=self.env['account.journal'].search([('name','=','Miscellaneous Operations'),('company_id','=',record.kg_emp_id.company_id.id)],limit=1)
                
                
                
                move = {
             'name': '/',
             'journal_id': journal.id,
             'date': date.today(),
               'company_id':journal.company_id.id,
 
             'line_ids': [(0, 0, {
                     'name': record.name or '/',
                     'credit': record.kg_cost,
                     'account_id': record.kg_credit_acc.id,
                     'company_id':journal.company_id.id
                    
                 }), (0, 0, {
                     'name': record.name or '/',
                     'debit': record.kg_cost,
                     'account_id': record.kg_debit_acc.id,
                      'company_id':journal.company_id.id
                     
                 })]
         }
            
                move_id = self.env['account.move'].create(move)
                move_id.post()   
             
            
            return record.write({'kg_state':'completed'})
    
    @api.multi
    def submit(self):
        for record in self:
            
            if record.kg_req_type == 'new_hw':
                 req_type='New Hardware'
            else:
                req_type='Complaint Registration'
                
                
            subject = "<b>Request - %(req_type)s - %(subj)s </b>"% {
                     'req_type': req_type,
                       'subj': record.kg_subject,
                }
           
           
           
           
            body_html="<b>Dear %(emp_name)s,</b><br/><br/>  Your request with Reference %(hd_name)s successfully submitted" % {
                     'emp_name': record.kg_emp_id.name,
                       'hd_name': record.name,
                }
            
        
         
            
            
           
        
    
            main_content = {
                'subject': subject,
          #      'author_id': record.env.user.partner_id.id,
                'body_html': body_html,
                'email_to': record.kg_emp_id.user_id.login,
            }
            print main_content,'Employeeeeeeeeeeeeeee'
            self.env['mail.mail'].create(main_content).send()
            
            body_html="<b>Dear %(hr_man)s,</b><br/><br/>  Request : %(hd_name)s from Employee %(emp_name)s" % {
                     'hr_man': record.kg_manager.name,
                       'hd_name': record.name,
                         'emp_name': record.kg_emp_id.name,
                }
          
           
            
          
        
    
            main_content = {
                'subject': subject,
             #   'author_id': record.env.user.partner_id.id,
                'body_html': body_html,
                'email_to': record.kg_manager.user_id.login,
            }
            print main_content,'Managerrrrrrrrrrrrrrrrrrr'
            self.env['mail.mail'].create(main_content).send()
            record.write({'state':'pending','kg_sub_date':date.today()})
            
            
            
            
            body_html="<b>Dear %(it_admin)s,</b><br/><br/>  Request : %(hd_name)s from Employee %(emp_name)s" % {
                     'it_admin': record.kg_it_admin.name,
                       'hd_name': record.name,
                         'emp_name': record.kg_emp_id.name,
                }
          
           
            
          
        
    
            main_content = {
                'subject': subject,
             #   'author_id': record.env.user.partner_id.id,
                'body_html': body_html,
                'email_to': record.kg_it_admin.user_id.login,
            }
            print main_content,'It Adminnnnnnnnnnnnnnnnnnnnnnn'
            self.env['mail.mail'].create(main_content).send()
            record.write({'kg_state':'pending','kg_sub_date':date.today()})
            return True
    
    @api.model
    def create(self,vals):
        sequence_code='seq.helpdesk'
        vals['name']= self.env['ir.sequence'].next_by_code(sequence_code) 
      
        return super(kg_helpdesk, self).create(vals)
    
    
class kg_hd_category(models.Model):
    
    _name = 'kg.hd.category'
    
    name = fields.Char('Name',required=True)


class PurchaseOrder(models.Model):
    
    _inherit = 'purchase.order'
    
    kg_hd_id = fields.Many2one('kg.helpdesk','HelpDesk')
    
    