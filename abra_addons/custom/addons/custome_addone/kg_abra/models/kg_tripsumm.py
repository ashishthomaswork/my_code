from odoo import fields,models,api,_
from odoo import tools
from datetime import date
from dateutil.relativedelta import relativedelta

from odoo.exceptions import UserError, AccessError
from odoo import api, fields, models, SUPERUSER_ID, _




class kg_tripsumm(models.Model):
    
    _name = 'kg.tripsumm'
    
    name = fields.Char('Name')
    
    kg_emp_id  = fields.Many2one('hr.employee','Employee',default=lambda self: self._default_emp())
    
    kg_loc_from = fields.Char('Location From')
    
    kg_loc_to = fields.Char('Location To')
    
    kg_start_odo = fields.Float('Start Odometer')
    
    kg_end_odo = fields.Float('End Odometer')
    
    kg_start_time =  fields.Datetime('Start Time')
    
    kg_end_time = fields.Datetime('End Time')
    
    
    @api.model
    def _default_emp(self):
        print self.env.context,'11111111111'
        
        user_obj = self.env.user
        
        
        emp_id =self.env['hr.employee'].search([('user_id','=',user_obj.id)],limit=1).id or False
        
        return emp_id
    
    
class kg_log_req(models.Model):
    _inherit='kg.log.req'
    
    kg_trip_id = fields.Many2one('kg.tripsumm','Trip Summary')
    
    kg_veh_type_id = fields.Many2one('kg.veh.type','Vehicle Type')
    
    kg_outsour_req = fields.Boolean('Need Outsourcing')
    
    
    kg_driver = fields.Many2one('res.partner','Driver')
    
    
    kg_out_count = fields.Integer('Estimation Count',compute='compute_out_count')
    
    
   
    
    
    @api.multi
    def compute_out_count(self):
        for record in self:
            out_obj = self.env['kg.out.log']
            
            record.kg_out_count = out_obj.search_count([('kg_log_req_id', '=',record.id)]) 
    
class kg_out_log(models.Model):
    
    _inherit = 'kg.out.log'
    
    kg_log_req_id = fields.Many2one('kg.log.req','Logistics Requistion')
    
    
    