# -*- coding: utf-8 -*-
from odoo import fields,models,api,_
from odoo import tools
from datetime import date
from dateutil.relativedelta import relativedelta

from odoo.exceptions import UserError, AccessError
from odoo import api, fields, models, SUPERUSER_ID, _

class kg_sample_material(models.Model):
    
    _name="kg.sample.material"
    
    name =  fields.Char('Name')
    
    kg_date_rst = fields.Date('Date Requested')
    
    kg_date_req = fields.Date('Date Required')
    
   
    
    kg_raised_by = fields.Many2one('res.users','Raised By',default=lambda self: self.env.user and self.env.user.id or False)
    
    kg_assign_to = fields.Many2one('res.users','Assigned To')
    
    
    kg_req_by  = fields.Many2one('res.users','Requested By')
    
    kg_client = fields.Many2one('res.partner','Client')
    
    kg_project = fields.Many2one('project.project','Project')
    
    kg_proj_status = fields.Selection([('Enquiry','Enquiry'),('Tender','Tender'),('Awarded','Awarded')],'Project Status')
    
    
    kg_so_id = fields.Many2one('sale.order','Quotation')
    kg_material_lines = fields.One2many('kg.material.line','kg_mat_id')
    
    kg_matpur_lines = fields.One2many('kg.matpur.line','kg_mat_id')
    
    kg_comment = fields.Text('Comments')
    
    
    kg_att_url1 = fields.Char('')
    
    kg_att_url2 = fields.Char('')
    
    
    kg_att_url3 = fields.Char('')
    
    
    kg_att_url4 = fields.Char('')
    
    kg_att_url5 = fields.Char('')
    
    
    
    
    
    
    @api.multi
    def done(self):
        for record in self:
            supplist = self.env['kg.matpur.line'].search([('kg_mat_id','=',record.id)]).mapped('kg_supplier')
        
            print supplist
            
            if supplist:
                for i in supplist:
                    vals={}
                    vals['partner_id']= i.id
                    
                    vals['currency_id'] = record.env.user.company_id.currency_id.id
                    
                    vals['state']='draft'
                    
                    rfq_id=self.env['purchase.order'].create(vals)
                    
                    
                    
                    lines = self.env['kg.matpur.line'].search([('kg_supplier','=',i.id),('kg_mat_id','=',record.id)])
                    for line in lines:
                      
                        vals_line={
                        'product_id':line.kg_product.id,
                        'name':line.kg_desc,
                        'date_planned':record.kg_date_req,
                        'product_qty':line.kg_qty,
                        'product_uom':line.kg_product.uom_id.id,
                        'price_unit':line.kg_price_ind,
                        'order_id': rfq_id.id
                        
                        }
                        
                        self.env['purchase.order.line'].create(vals_line)
                        
                        
                        
    _inherit = ['mail.thread', 'ir.needaction_mixin']
    
    
    
    
    @api.multi
    def _track_subtype(self, init_values):
        self.ensure_one()
        if 'kg_state' in init_values and self.kg_state == 'In Progress':
            return 'kg_abra.kg_sample_inprogress'
        
        if 'kg_state' in init_values and self.kg_state == 'new':
            return 'kg_abra.kg_sample_new'
        
        if 'kg_state' in init_values and self.kg_state == 'assign':
            return 'kg_abra.kg_sample_assign'
        
        if 'kg_state' in init_values and self.kg_state == 'Quote Created':
            return 'kg_abra.kg_sample_quote_created'
        
        
        if 'kg_state' in init_values and self.kg_state == 'Completed':
            return 'kg_abra.kg_sample_complete'
        return super(kg_sample_material, self)._track_subtype(init_values)
    
    kg_state = fields.Selection([('new','New'),('assign','Assigned'),('In Progress','In Progress'),('Completed','Completed')],'State',track_visibility='onchange',default='new')

    kg_dep_id = fields.Many2one('hr.department','Assigned Department')
    @api.multi
    def write(self, vals):
        """Override default Odoo write function and extend."""
        # Do your custom logic here
        
        if vals.get('kg_state') and vals.get('kg_state') !='new' and vals.get('kg_state') !='assign' :
            
                vals_mail={}
                message="<b>Dear %(hr_man)s,</b><br/><br/>Your  Sample Request : %(hd_name)s  has been changed to %(state)s State" % {
                         'hr_man': self.kg_raised_by.partner_id.name,
                           'hd_name': self.name,
                           'state': vals.get('kg_state')
                           
                             
                    }
            
                vals_mail['message']=message
                vals_mail['partner']=self.kg_raised_by.partner_id.email
                self.send_mail(vals_mail)   
        print vals,'hshddhhhd'
        if vals.get('kg_assign_to'):
            vals['kg_state']='assign'
            user_obj=self.env['res.users'].browse([vals.get('kg_assign_to')])
            check=self.env['mail.followers'].search([('partner_id','=',user_obj.partner_id.id),('res_model','=','kg.sample.material'),('res_id','=',self.id)])
            if not check:
                fol={}
                fol['res_model']='kg.sample.material'
                fol['res_id']=self.id
                fol['partner_id']=user_obj.partner_id.id
                fol_id=self.env['mail.followers'].create(fol)
                subtypes= self.env['mail.message.subtype'].search([('res_model','=','kg.sample.material')]).ids
                if subtypes:
                    for i in subtypes:
                        self._cr.execute('INSERT INTO mail_followers_mail_message_subtype_rel (mail_followers_id, mail_message_subtype_id) values (%s, %s)', (fol_id.id, i))
    
                
            
            vals_mail={}
            message="<b>Dear %(hr_man)s,</b><br/><br/> Sample Request : %(hd_name)s  has been assigned to You" % {
                         'hr_man': user_obj.partner_id.name,
                           'hd_name': self.name,
                             
                    }
            
            vals_mail['message']=message
            vals_mail['partner']=user_obj.partner_id.email
            self.send_mail(vals_mail)  
            
            
        return super(kg_sample_material, self).write(vals)
    
    
    @api.model
    def create(self,vals):
     
        sequence_code='kg.sample.material'
        vals['name']= self.env['ir.sequence'].next_by_code(sequence_code)
        dep_obj=self.env['hr.department'].browse([vals.get('kg_dep_id')])    
        partner_id=dep_obj.manager_id.user_id.partner_id
        
        


            
        
        res= super(kg_sample_material, self).create(vals)
        
        dep_mgr_id =res.kg_dep_id.manager_id.user_id.partner_id.id
        check=self.env['mail.followers'].search([('partner_id','=',dep_mgr_id),('res_model','=','kg.sample.material'),('res_id','=',res.id)])
        if not check:
            fol={}
            fol['res_model']='kg.sample.material'
            fol['res_id']=res.id
            fol['partner_id']=dep_mgr_id
            fol_id=self.env['mail.followers'].create(fol)
            subtypes= self.env['mail.message.subtype'].search([('res_model','=','kg.sample.material')]).ids
            if subtypes:
                for i in subtypes:
                    self._cr.execute('INSERT INTO mail_followers_mail_message_subtype_rel (mail_followers_id, mail_message_subtype_id) values (%s, %s)', (fol_id.id, i))

        
        vals_mail={}
        message="<b>Dear %(hr_man)s,</b><br/><br/> New Sample Request : %(hd_name)s from  %(emp_name)s has been created" % {
                     'hr_man': partner_id.name,
                       'hd_name': res.name,
                         'emp_name': res.kg_raised_by.name,
                }
        
        vals_mail['message']=message
        vals_mail['partner']=partner_id.email
        res.send_mail(vals_mail)
        res.write({'kg_state':''})
        res.write({'kg_state':'new'})
        return res
    
    
    
    def send_mail(self,vals):
        main_content = {
                'subject': "Sample Request!",
              
                'body_html': '<pre>%s</pre>' % vals['message'],
                 'email_to': vals['partner'],
            }
        return self.env['mail.mail'].create(main_content).send()


                
                        
            
    


class kg_material_line(models.Model):
    
    _name="kg.material.line"
    
    name = fields.Char('Name')
    
    kg_product_id = fields.Many2one('product.product','Product')
    
    kg_desc = fields.Char('Description')
    
    kg_ref_code  = fields.Char('Ref Code')
    
    kg_manf = fields.Char('Manufacturer')
    
    kg_reqd_size = fields.Float('Required Size')
    
    kg_qty_reqd = fields.Float('Quantity Required')
    
    kg_lead_time = fields.Float('Lead Time')
    
    kg_match =  fields.Selection([('original','Original'),('1','90-99% Match'),('2','75-80% Match')],'Match')
    
    
    kg_mat_id = fields.Many2one('kg.sample.material','Material Id')
    
    
class kg_matpur_line(models.Model):
    
    _name ="kg.matpur.line"
    
    name = fields.Char('Name')
    
    kg_supplier = fields.Many2one('res.partner','Supplier')
    
    kg_product = fields.Many2one('product.product','Product')
    
    
    kg_desc = fields.Char('Description')
    
    kg_prod_code = fields.Char('Product Code')
    
    kg_qty = fields.Float('Qty')
    
    
    kg_price_ind =fields.Float('Price Included')
    
    kg_product_from  = fields.Selection([('import','Import'),('local','Local')],'Product From')
    
    
    kg_mat_id = fields.Many2one('kg.sample.material','Material Id')
    
    
    @api.onchange('kg_product')
    def onchange_product(self):
        for record in self:
            record.kg_desc = record.kg_product.name
    
