# -*- coding: utf-8 -*-
from odoo import fields,models,api,_
from odoo import tools
from datetime import date
from dateutil.relativedelta import relativedelta

from odoo.exceptions import UserError, AccessError
from odoo import api, fields, models, SUPERUSER_ID, _


class kg_qc(models.Model):
    
    _name="kg.qc"
    
    name =fields.Char('Name')
    
    kg_project =fields.Many2one('project.project','Project')
    
    kg_date =fields.Date('Date')
    
    kg_desc = fields.Text('Comments')
    
    kg_ins_by  = fields.Many2one('res.users','Inspected BY')
    
    kg_partner = fields.Many2one('res.partner','Supplied By')
    
    kg_delivery_note = fields.Many2one('stock.picking','Delivery Note')
    
    kg_sub =fields.Char('SUBJECT')
    
    kg_qc_lines = fields.One2many('kg.qc.lines','kg_qc_id','QC Lines',default=lambda self: self._default_qc_lines())
    
  #  kg_del_note = fields.Many2one('stock.picking','Delivery Note')
    
    kg_images = fields.Many2many(comodel_name="ir.attachment", relation="m2m_ir_attachment_relation", column1="m2m_id", column2="attachment_id", string="Attachments") 
    #kg_images = fields.Many2many('kg.images')
    
    
    kg_category = fields.Many2one('kg.qc.category','Category')
    
    @api.model
    def _default_qc_lines(self):
        print self.env.context,'11111111111'
        
        del_id =self.env.context.get('default_kg_delivery_note') or False
        
        if del_id is not False:
            del_obj =self.env['stock.picking'].browse([del_id])
            b=[]
            for line in del_obj.pack_operation_product_ids:
                a={}
                a['product_id']=line.product_id.id
                a['name']=line.product_id.name
                a['product_uom_id']=line.product_uom_id.id
                a['product_qty']=line.product_qty
                c=(0,0,a)
                b.append(c)
            return b
#         cat_ids =self.env['kg.category'].search([])
#         b=[]
#         for i in cat_ids:
#             a={}
#             a['kg_cat_id']=i
#           
#             c=(0,0,a)
#             b.append(c)
#         return b
class kg_qc_category(models.Model):
    _name='kg.qc.category'
    
    name = fields.Char('Name',required=True)
     
     
class kg_images(models.Model):
    
    _name = 'kg.images'
    
    name = fields.Binary('')
class kg_qc_lines(models.Model):
    
    _name ="kg.qc.lines"
    
    name = fields.Char('Item Description')
    
    product_id = fields.Many2one('product.product','Product')
    
    product_uom_id = fields.Many2one('product.uom','Uom')
    
    kg_sizes =fields.Char('Sizes')
    
    product_qty =fields.Float('Total Quantity')
    
    kg_qty_pass = fields.Float('Quantity Passed')
    
    kg_qty_rej = fields.Float('Quantity Rejected')
    
    kg_remarks = fields.Char('Remarks')
    
    kg_qc_id = fields.Many2one('kg.qc','QC')
    