# -*- coding: utf-8 -*-
from odoo import fields,models,api,_
from odoo import tools
from datetime import date
from dateutil.relativedelta import relativedelta

from odoo.exceptions import UserError, AccessError
from odoo import api, fields, models, SUPERUSER_ID, _


class kg_request_passport(models.Model):
    
    _name = "kg.request.passport"
    
    _inherit = ['mail.thread', 'ir.needaction_mixin']

    name = fields.Char('Code')

    kg_emp_id = fields.Many2one('hr.employee', 'Name')

    kg_emp_code = fields.Char('Emp. Code')

    kg_des = fields.Many2one('hr.job', 'Designation')

    kg_dep = fields.Many2one('hr.department', 'Department')

    kg_req = fields.Date('Required On')

    kg_prop_ret = fields.Date('Will Be Returned On')

    kg_purp_req = fields.Text('Purpose Of Request')

    kg_issued_on = fields.Date('Issued On')

    kg_return_on = fields.Date('Returned On')
    
    kg_raised_by = fields.Many2one('res.users','Raised By',default=lambda self: self.env.user and self.env.user.id or False)
    
    kg_assign_to = fields.Many2one('res.users','Assign To')
    
    @api.multi
    def _track_subtype(self, init_values):
        self.ensure_one()
        if 'kg_state' in init_values and self.kg_state == 'In Progress':
            return 'kg_abra.kg_passport_inprogress'
        
        if 'kg_state' in init_values and self.kg_state == 'new':
            return 'kg_abra.kg_passport_new'
        
        if 'kg_state' in init_values and self.kg_state == 'assign':
            return 'kg_abra.kg_passport_assign'
      
        
        
        if 'kg_state' in init_values and self.kg_state == 'Completed':
            return 'kg_abra.kg_passport_complete'
        return super(kg_request_passport, self)._track_subtype(init_values)
    
    kg_state = fields.Selection([('new','New'),('assign','Assigned'),('In Progress','In Progress'),('Completed','Completed')],'State',track_visibility='onchange',default='new')
    
    
    @api.multi
    def write(self, vals):
        """Override default Odoo write function and extend."""
        # Do your custom logic here
        
        if vals.get('kg_state') and vals.get('kg_state') !='new' and vals.get('kg_state') !='assign' :
            
                vals_mail={}
                message="<b>Dear %(hr_man)s,</b><br/><br/>Your  Request for Passport  : %(hd_name)s  has been changed to %(state)s State" % {
                         'hr_man': self.kg_raised_by.partner_id.name,
                           'hd_name': self.name,
                           'state': vals.get('kg_state')
                           
                             
                    }
            
                vals_mail['message']=message
                vals_mail['partner']=self.kg_raised_by.partner_id.email
                self.send_mail(vals_mail)   
        print vals,'hshddhhhd'
        if vals.get('kg_assign_to'):
            vals['kg_state']='assign'
            user_obj=self.env['res.users'].browse([vals.get('kg_assign_to')])
            fol={}
            fol['res_model']='kg.request.passport'
            fol['res_id']=self.id
            fol['partner_id']=user_obj.partner_id.id
            fol_id=self.env['mail.followers'].create(fol)
            subtypes= self.env['mail.message.subtype'].search([('res_model','=','kg.request.passport')]).ids
            if subtypes:
                for i in subtypes:
                    self._cr.execute('INSERT INTO mail_followers_mail_message_subtype_rel (mail_followers_id, mail_message_subtype_id) values (%s, %s)', (fol_id.id, i))

        
        
            
            
            vals_mail={}
            message="<b>Dear %(hr_man)s,</b><br/><br/> Delivery Request : %(hd_name)s  has been assigned to You" % {
                         'hr_man': user_obj.partner_id.name,
                           'hd_name': self.name,
                             
                    }
            
            vals_mail['message']=message
            vals_mail['partner']=user_obj.partner_id.email
            self.send_mail(vals_mail)  
            
            
        return super(kg_request_passport, self).write(vals)
    
    
    @api.model
    def create(self,vals):
     
        
        dep_obj=self.env['hr.department'].browse([vals.get('kg_dep')])    
        partner_id=dep_obj.manager_id.user_id.partner_id
        
        


            
        
        sequence_code = "req.pass"
        vals['name'] = self.env['ir.sequence'].next_by_code(sequence_code)
        res= super(kg_request_passport, self).create(vals)
        
        dep_mgr_id =res.kg_dep.manager_id.user_id.partner_id.id
         
        fol={}
        fol['res_model']='kg.request.passport'
        fol['res_id']=res.id
        fol['partner_id']=dep_mgr_id
        fol_id=self.env['mail.followers'].create(fol)
        subtypes= self.env['mail.message.subtype'].search([('res_model','=','kg.request.passport')]).ids
        if subtypes:
            for i in subtypes:
                self._cr.execute('INSERT INTO mail_followers_mail_message_subtype_rel (mail_followers_id, mail_message_subtype_id) values (%s, %s)', (fol_id.id, i))

        
        
        vals_mail={}
        message="<b>Dear %(hr_man)s,</b><br/><br/> New Delivery Request : %(hd_name)s from  %(emp_name)s has been created" % {
                     'hr_man': partner_id.name,
                       'hd_name': res.name,
                         'emp_name': res.kg_raised_by.name,
                }
        
        vals_mail['message']=message
        vals_mail['partner']=partner_id.email
        res.send_mail(vals_mail)
        res.write({'kg_state':''})
        res.write({'kg_state':'new'})
        return res
    
    
    
    def send_mail(self,vals):
        main_content = {
                'subject': "Request For Passport!",
              
                'body_html': '<pre>%s</pre>' % vals['message'],
                 'email_to': vals['partner'],
            }
        return self.env['mail.mail'].create(main_content).send()
    
    
    
   
        

