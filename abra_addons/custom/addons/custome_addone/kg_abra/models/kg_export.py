# -*- coding: utf-8 -*-
from odoo import fields,models,api,_
from odoo import tools
from datetime import date
from dateutil.relativedelta import relativedelta

from odoo.exceptions import UserError, AccessError
from odoo import api, fields, models, SUPERUSER_ID, _

class kg_export_state(models.Model):
    
    _name='kg.export.state'
    
    name = fields.Char('Name',required=True)
    
class kg_ship_term(models.Model):
    
    _name='kg.ship.term'
    
    name = fields.Char('Name',required=True)

class kg_export(models.Model):
    
    _name = 'kg.export'
    
    
    name = fields.Char('Export No')
    
    kg_con_address = fields.Many2one('res.partner','Consignee Address')
    
    kg_inv_id = fields.Many2one('account.invoice','Invoice')
    
    kg_ship_term_id = fields.Many2one('kg.ship.term','Shipping Terms')
    
    
    
    kg_images = fields.Many2many(comodel_name="ir.attachment", relation="m2m_ir_attachment_relation", column1="m2m_id", column2="attachment_id", string="Attachments") 

    
    
    
    kg_export_lines = fields.One2many('kg.export.lines','kg_export_id','Lines')
    
    
        
    @api.model
    def create(self,vals):
        sequence_code='seq.export'
        vals['name']= self.env['ir.sequence'].next_by_code(sequence_code) 
      
        return super(kg_export, self).create(vals)
    
    
    
class kg_export_lines(models.Model):
    
    _name = 'kg.export.lines'
    
    name = fields.Char('Description')
    
    kg_date = fields.Date('Date')
    
    kg_notes = fields.Text('Note')
    
    kg_state_id = fields.Many2one('kg.export.state','State')  
    
    
    kg_export_id = fields.Many2one('kg.export','export')
    